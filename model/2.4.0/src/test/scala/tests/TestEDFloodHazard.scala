package diva.tests

import org.scalatest._
import org.scalactic.TolerantNumerics

import diva.module.flooding.EDFloodHazard
import diva.types.TypeComputations._

/*  def createRandomEDFloodHazard(): EDFloodHazard {
        val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val shift: Float = r.nextFloat()
    val edfh: EDFloodHazard = new EDFloodHazard(mue, sigma)
    edfh.shift = shift
  }
*/

class TestEDFloodHazard extends FlatSpec with Matchers {
  val epsilon10 = 1e-10d
  val epsilon06 = 1e-6d
  val epsilon05 = 1e-5f
  val epsilon03 = 1e-3f
  val epsilon02 = 1e-2f
  val epsilon01 = 1e-1f
  val epsilon0p5 = 0.5
  val r = scala.util.Random

  //  implicit val doubleEq = TolerantNumerics.tolerantDoubleEquality(epsilon)
  def createRandomEDFloodHazard(): EDFloodHazard = {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val shift: Float = r.nextFloat()
    val edfh: EDFloodHazard = new EDFloodHazard(mue, sigma)
    edfh.shift = shift
    edfh
  }

  "EDFloodHazard.cdf" should "generate 0 for h1; 0.1 for h10 and 0.999 for h1000" in {
    val edfh: EDFloodHazard = createRandomEDFloodHazard()

    val h1: Double = edfh.returnPeriodHeight(1)
    val h10: Double = edfh.returnPeriodHeight(10)
    val h1000: Double = edfh.returnPeriodHeight(1000)

    edfh.cdf(h1) should be(0)
    edfh.cdf(h10) should be(0.9 +- epsilon05)
    edfh.cdf(h1000) should be(0.999 +- epsilon05)
  }

  it should "imply exceedance probability 1 for hx when hx is < h1" in {
    val edfh: EDFloodHazard = createRandomEDFloodHazard()

    val h0_9: Double = edfh.returnPeriodHeight(0.9)
    val h0_5: Double = edfh.returnPeriodHeight(0.5)
    val h0_1: Double = edfh.returnPeriodHeight(0.1)

    edfh.exceedanceProb(h0_9) should be(1)
    edfh.exceedanceProb(h0_5) should be(1)
    edfh.exceedanceProb(h0_1) should be(1)
  }

  it should "be monotonous" in {
    val edfh: EDFloodHazard = createRandomEDFloodHazard()

    var wl_old: Double = 0
    var wl_new: Double = 0

    for (i <- 1 to 100) {
      val x: Double = r.nextDouble() * 0.1
      wl_new = wl_old + x
      if (!(edfh.cdf(wl_new).isNaN)) {
        edfh.cdf(wl_old) should be <= edfh.cdf(wl_new)
      }
      wl_old = wl_new
    }
  }

  it should "shifted by the shift" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val shift: Float = r.nextFloat()
    val edfh_unshifted: EDFloodHazard = new EDFloodHazard(mue, sigma)
    val edfh_shifted: EDFloodHazard = new EDFloodHazard(mue, sigma)
    edfh_shifted.shift = shift

    val h1_unshifted: Double = edfh_unshifted.returnPeriodHeight(1)
    val h10_unshifted: Double = edfh_unshifted.returnPeriodHeight(10)
    val h1000_unshifted: Double = edfh_unshifted.returnPeriodHeight(1000)

    val h1_shifted: Double = edfh_shifted.returnPeriodHeight(1)
    val h10_shifted: Double = edfh_shifted.returnPeriodHeight(10)
    val h1000_shifted: Double = edfh_shifted.returnPeriodHeight(1000)

    h1_shifted should be(h1_unshifted + shift +- epsilon01)
    h10_shifted should be(h10_unshifted + shift +- epsilon01)
    h1000_shifted should be(h1000_unshifted + shift +- epsilon01)

    for (i <- 1 to 100) {
      val x_unshifted: Double = r.nextFloat() * (h1000_unshifted - h1_unshifted) + h1_unshifted
      val rp_unshifted: Double = edfh_unshifted.returnPeriod(x_unshifted)
      val rp_shifted: Double = edfh_shifted.returnPeriod(x_unshifted + shift)
      rp_shifted should be(rp_unshifted +- epsilon01)
    }

    for (i <- 1 to 100) {
      val rp: Double = r.nextFloat() * 1000
      val h_unshifted: Double = edfh_unshifted.returnPeriodHeight(rp)
      val h_shifted: Double = edfh_shifted.returnPeriodHeight(rp)
      h_shifted should be(h_unshifted + shift +- epsilon01)
    }
  }

  "EDFloodHazard.returnPeriodHeight" should "be the inverse of EDFloodHazard.returnPeriod" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val shift: Float = r.nextFloat()
    val edfh: EDFloodHazard = new EDFloodHazard(mue, sigma)
    edfh.shift = shift

    val h1: Double = edfh.returnPeriodHeight(1)
    val h10000: Double = edfh.returnPeriodHeight(10000)

    for (i <- h1 to h10000 by 0.01) {
      edfh.returnPeriodHeight(edfh.returnPeriod(i)).toDouble should be(i +- epsilon05)
    }
  }

  "EDFloodHazard.cdf_inverse" should "be the inverse of EDFloodHazard.cdf" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val shift: Float = r.nextFloat()
    val edfh: EDFloodHazard = new EDFloodHazard(mue, sigma)
    edfh.shift = shift

    val h1: Double = edfh.returnPeriodHeight(1)
    val h1000: Double = edfh.returnPeriodHeight(1000)

    for (i <- 1 to 100) {
      val x: Double = (r.nextFloat() * (h1000 - h1)) + h1
      if (!edfh.cdf_inverse(edfh.cdf(x)).isInfinite()) {
        edfh.cdf_inverse(edfh.cdf(x)).toDouble should be(x +- epsilon02)
      }
    }
  }

  "EDFloodHazard.cdf_prime" should "be the derivative of EDFloodHazard.cdf" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val shift: Float = r.nextFloat()
    val edfh: EDFloodHazard = new EDFloodHazard(mue, sigma)
    edfh.shift = shift

    val h1: Double = edfh.returnPeriodHeight(1)
    val h1000: Double = edfh.returnPeriodHeight(1000)

    for (i <- 1 to 100) {
      val x: Double = (r.nextFloat() * (h1000 - h1)) + h1
      val f_prime_x: Double = edfh.cdf_prime(x)
      val f_x_plus_eps: Double = edfh.cdf(x + epsilon05)
      val f_x_minus_eps: Double = edfh.cdf(x - epsilon05)
      if (scala.math.abs(f_prime_x) > 10) {
        f_prime_x should be(((f_x_plus_eps - f_x_minus_eps) / (2 * epsilon05)) +- ((f_x_plus_eps - f_x_minus_eps) / (2 * epsilon05)) * (1 + epsilon0p5))
      } else {
        f_prime_x should be(((f_x_plus_eps - f_x_minus_eps) / (2 * epsilon05)) +- epsilon0p5)
      }
    }
  }

}
