package diva.tests

import org.scalatest._
import org.scalactic.TolerantNumerics

import diva.module.flooding.GEVFloodHazard
import diva.types.TypeComputations._

class TestGEVFloodHazard extends FlatSpec with Matchers {
  val epsilon10 = 1e-10f
  val epsilon05 = 1e-5f
  val epsilon02 = 1e-2f
  val epsilon01 = 1e-1f
  val epsilon0p5 = 0.5
  val r = scala.util.Random
  r.setSeed(java.lang.System.currentTimeMillis())

  //implicit val doubleEq = TolerantNumerics.tolerantDoubleEquality(epsilon)

  "GEVFloodHazard.cdf" should "generate 0 for h1; 0.1 for h10 and 0.999 for h1000" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val xi: Float = r.nextFloat() - 0.5f
    val n: Float = r.nextInt(5) + 1

    val gevfh: GEVFloodHazard = new GEVFloodHazard(mue, sigma, xi, n)

    val h1: Double = gevfh.returnPeriodHeight(1)
    val h10: Double = gevfh.returnPeriodHeight(10)
    val h1000: Double = gevfh.returnPeriodHeight(1000)

    //println (mue + ", "+ sigma + ", " + xi)
    //println ("h1 =" + h1 + "  gevfh.cdf(h1) =" + gevfh.cdf(h1))
    //println ("h100 =" + h10 + "  gevfh.cdf(h100) =" + gevfh.cdf(h10))

    gevfh.cdf(h1) should be(0.0 +- epsilon05)
    gevfh.cdf(h10) should be(0.9 +- epsilon02)
    gevfh.cdf(h1000) should be(0.999 +- epsilon02)
  }

  it should "imply exceedance probability 1 for hx when hx is < h1" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val xi: Float = r.nextFloat() - 0.5f
    val n: Float = r.nextInt(5) + 1

    val gevfh: GEVFloodHazard = new GEVFloodHazard(mue, sigma, xi, n)
    val h1: Double = gevfh.returnPeriodHeight(1)

    //println(mue + ", " + sigma + ", " + xi + ", " + n + ", " + h1)

    for (i <- 1 to 10) {
      val x: Double = r.nextDouble() * h1
      //println("x=" + x + " ,  p(x)=" + gevfh.cdf(x))
      if (!(gevfh.cdf(x).isNaN)) {
        gevfh.cdf(x) should be(0d +- epsilon10)
      }
    }
  }

  it should "be monotonous" in {
    var mue: Float = r.nextFloat() + 0.25f
    var sigma: Float = (r.nextFloat() + 0.05f) / 5f
    var xi: Float = r.nextFloat() - 0.5f
    var n: Float = r.nextInt(5) + 1
    var gevfh: GEVFloodHazard = new GEVFloodHazard(mue, sigma, xi, n)

    var wl_old: Double = r.nextFloat() * 2.0f - 4.0f
    var wl_new: Double = 0

    while (gevfh.cdf(wl_old).isNaN) {
      wl_old = r.nextFloat() * 2.0f - 4.0f
      r.setSeed(java.lang.System.currentTimeMillis())
      mue = r.nextFloat() + 0.25f
      sigma = (r.nextFloat() + 0.05f) / 5f
      xi = r.nextFloat() - 0.5f
      n = r.nextInt(5) + 1
      gevfh = new GEVFloodHazard(mue, sigma, xi, n)
    }

    for (i <- 1 to 100) {
      val x: Double = r.nextDouble() * 0.01
      wl_new = wl_old + x
      if (!(gevfh.cdf(wl_new).isNaN)) {
        gevfh.cdf(wl_old) should be <= gevfh.cdf(wl_new)
      }
      wl_old = wl_new
    }
  }

  it should "shifted by the shift" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val xi: Float = r.nextFloat() - 0.5f
    val n: Float = r.nextInt(5) + 1

    val shift: Float = r.nextFloat()
    val gevfh_unshifted: GEVFloodHazard = new GEVFloodHazard(mue, sigma, xi, n)
    val gevfh_shifted: GEVFloodHazard = new GEVFloodHazard(mue, sigma, xi, n)
    gevfh_shifted.shift = shift

    val h1_unshifted: Double = gevfh_unshifted.returnPeriodHeight(1)
    val h10_unshifted: Double = gevfh_unshifted.returnPeriodHeight(10)
    val h1000_unshifted: Double = gevfh_unshifted.returnPeriodHeight(1000)

    val h1_shifted: Double = gevfh_shifted.returnPeriodHeight(1)
    val h10_shifted: Double = gevfh_shifted.returnPeriodHeight(10)
    val h1000_shifted: Double = gevfh_shifted.returnPeriodHeight(1000)

    h1_shifted should be(h1_unshifted + shift +- epsilon01)
    h10_shifted should be(h10_unshifted + shift +- epsilon01)
    h1000_shifted should be(h1000_unshifted + shift +- epsilon01)

    //println(mue + ", " + sigma + ", " + xi + ", " + shift)

    for (i <- 1 to 10) {
      val x_unshifted: Double = r.nextFloat() * (h1000_unshifted - h1_unshifted) + h1_unshifted
      //println("x_unshifted = " + x_unshifted)
      val rp_unshifted: Double = gevfh_unshifted.returnPeriod(x_unshifted)
      val rp_shifted: Double = gevfh_shifted.returnPeriod(x_unshifted + shift)
      rp_shifted should be(rp_unshifted +- epsilon01)
    }

    for (i <- 1 to 10) {
      val rp: Double = r.nextFloat() * 1000
      val h_unshifted: Double = gevfh_unshifted.returnPeriodHeight(rp)
      val h_shifted: Double = gevfh_shifted.returnPeriodHeight(rp)
      h_shifted should be(h_unshifted + shift +- epsilon01)
    }
  }

  "GEVFloodHazard.returnPeriodHeight" should "be the inverse of GEVFloodHazard.returnPeriod" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val xi: Float = r.nextFloat() - 0.5f
    val n: Float = r.nextInt(5) + 1

    val gevfh: GEVFloodHazard = new GEVFloodHazard(mue, sigma, xi, n)

    val h1: Double = gevfh.returnPeriodHeight(1)
    val h10000: Double = gevfh.returnPeriodHeight(10000)

    for (i <- h1 to h10000 by 0.01) {
      gevfh.returnPeriodHeight(gevfh.returnPeriod(i)).toDouble should be(i +- epsilon02)
    }
  }

  "GEVFloodHazard.cdf_inverse" should "be the inverse of GEVFloodHazard.cdf" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val xi: Float = r.nextFloat() - 0.5f
    val n: Float = r.nextInt(5) + 1

    val gevfh: GEVFloodHazard = new GEVFloodHazard(mue, sigma, xi, n)

    val h1: Double = gevfh.returnPeriodHeight(1)
    val h1000: Double = gevfh.returnPeriodHeight(1000)

    for (i <- 1 to 100) {
      val x: Double = (r.nextDouble() * (h1000 - h1)) + h1
      if (gevfh.cdf(x) > 0) {
        gevfh.cdf_inverse(gevfh.cdf(x)).toDouble should be(x +- epsilon02)
      }
    }
  }

  "GEVFloodHazard.cdf_prime" should "be the derivative of GEVFloodHazard.cdf" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val xi: Float = r.nextFloat() - 0.5f
    val n: Float = r.nextInt(5) + 1
    val shift: Float = r.nextFloat()

    val gevfh: GEVFloodHazard = new GEVFloodHazard(mue, sigma, xi, n)
    gevfh.shift = shift

    val h1: Double = gevfh.returnPeriodHeight(1)
    val h1000: Double = gevfh.returnPeriodHeight(1000)

    for (i <- 1 to 100) {
      val x: Double = (r.nextFloat() * (h1000 - h1)) + h1
      val f_prime_x: Double = gevfh.cdf_prime(x)
      val f_x_plus_eps: Double = gevfh.cdf(x + epsilon05)
      val f_x_minus_eps: Double = gevfh.cdf(x - epsilon05)
      f_prime_x should be(((f_x_plus_eps - f_x_minus_eps) / (2 * epsilon05)) +- epsilon0p5)
    }
  }
}