package diva.tests

import org.scalatest._
import org.scalactic.TolerantNumerics

import diva.coastalplanemodel.CoastalPlaneModel

class SocioEconomicDevelopmentTest extends FlatSpec with Matchers {

  val epsilon10 = 1e-10f
  val epsilon05 = 1e-5f
  val epsilon03 = 1e-3f
  val epsilon02 = 1e-2f
  val epsilon01 = 1e-1f
  val r = scala.util.Random

  //  implicit val doubleEq = TolerantNumerics.tolerantDoubleEquality(epsilon)

  def createRandomCoastalPlane(): CoastalPlaneModel = {
    // gdpc: random value between 500 and 50000 -- helper for computing asstes
    val gdpc: Float = (r.nextFloat() * 49500) + 500
    // length: random value between 0 and 1000km
    val length: Float = r.nextFloat() * 1000
    // usual DIVA elevation levels
    var levels: Array[Float] = Array(0f, 1.5f, 2.5f, 3.5f, 4.5f, 5.5f, 6.5f, 7.5f, 8.5f, 9.5f, 10.5f, 11.5f, 12.5f, 13.5f, 14.5f, 15.5f, 16.5f, 17.5f, 18.5f, 19.5f, 20.5f)
    // areas: random values between 0 and 1000
    var areas: Array[Float] = levels.map(_ => r.nextFloat() * 1000)
    // areas: random values between 0 and 10000
    var populations: Array[Float] = levels.map(_ => (r.nextFloat() * 10000))
    // assets: population * 2.8 * gdpc
    var assets: Array[Float] = populations.map(_ * 2.8f * gdpc)
    var landuse: Array[Int] = levels.map(_ => -999)

    areas(0) = 0.0f
    populations(0) = 0.0f
    assets(0) = 0.0f

    val cpm: CoastalPlaneModel = new CoastalPlaneModel(length, levels, areas, populations, assets, landuse)
    cpm
  }

  /*
  "socio economic development: population growth" should "growth population" in {
    val cpm: CoastalPlaneModel = createRandomCoastalPlane
    val pop_before: Float = cpm.populationExposure.lExposure.foldLeft(0f)((sum, x) => sum + x.asInstanceOf[Float])
    cpm.socioEconomicDevelopment(1.035264924f, 0, 20)
    val pop_after: Float = cpm.populationExposure.lExposure.foldLeft(0f)((sum, x) => sum + x.asInstanceOf[Float])

    //(pop_before * scala.math.pow(1.035264924f, 20).toFloat) should be(pop_after +- epsilon02)
    (pop_before * 2.0f) should be(pop_after +- (pop_after * epsilon05))
  }
  */

}