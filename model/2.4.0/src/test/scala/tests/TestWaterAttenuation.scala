package diva.tests

import org.scalatest._
import org.scalactic.TolerantNumerics

import diva.coastalplanemodel.CoastalPlaneModel

class TestWaterAttenuation extends FlatSpec with Matchers {
  val epsilon10 = 1e-10f
  val epsilon05 = 1e-5f
  val epsilon03 = 1e-3f
  val epsilon02 = 1e-2f
  val epsilon01 = 1e-1f
  val r = scala.util.Random

  def createRandomCPwithLanduse(lu: Int): CoastalPlaneModel = {
    val length: Float = (r.nextFloat() * 200) + 0.1f
    val heights: Array[Float] = Array(0.0f, 0.5f, 1.0f, 1.5f, 2.0f, 2.5f, 3.0f, 3.5f, 4.0f, 4.5f, 5.0f, 5.5f, 6.0f, 6.5f, 7.0f, 7.5f, 8.0f, 8.5f, 9.0f, 9.5f, 10.0f)
    val areas: Array[Float] = heights.map(x => if (x > 0) { r.nextFloat() * 10 } else x)
    val people: Array[Float] = areas.map(x => x * (r.nextFloat() * 1000))
    val assets: Array[Float] = people.map(x => x * 2.8f)
    val landuse: Array[Int] = areas.map(x => lu)

    new CoastalPlaneModel(length, heights, areas, people, assets, landuse)
  }

  def createHomogenousCPwithLanduse(lu: Int): CoastalPlaneModel = {
    val length: Float = (r.nextFloat() * 200) + 0.1f
    val heights: Array[Float] = Array(0.0f, 0.5f, 1.0f, 1.5f, 2.0f, 2.5f, 3.0f, 3.5f, 4.0f, 4.5f, 5.0f, 5.5f, 6.0f, 6.5f, 7.0f, 7.5f, 8.0f, 8.5f, 9.0f, 9.5f, 10.0f)
    val a: Float = r.nextFloat() * 10
    val areas: Array[Float] = heights.map(x => if (x > 0) { a } else { 0 })
    val p: Float = r.nextFloat() * 1000
    val people: Array[Float] = areas.map(x => x * p)
    val assets: Array[Float] = people.map(x => x * 2.8f)
    val landuse: Array[Int] = areas.map(x => lu)

    new CoastalPlaneModel(length, heights, areas, people, assets, landuse)
  }

  "CoastalPlaneModel" should " be constructed correctly" in {

    for (i <- 0 until 100) {
      val landuse = r.nextInt(7)
      val cpm: CoastalPlaneModel = createHomogenousCPwithLanduse(landuse)

      //println(cpm.elevationExposure.levelsInverse.toList)
      //println(cpm.elevationExposure.lCumulativeExposureInverse.toList)

      for (j <- 0 until cpm.elevationExposure.lCumulativeWiths.length) {
        cpm.elevationExposure.lCumulativeWiths(j) should be((cpm.areaExposure.lCumulativeExposure(j) / cpm.pLength) +- epsilon03)
      }
      for (j <- 0 until 100) {
        val elev: Float = (r.nextFloat() * 20)
        cpm.elevationExposure.cumulativeExposureInverse(elev).toFloat should be((cpm.areaExposure.cumulativeExposure(elev) / cpm.pLength) +- epsilon02)
      }
    }
  }

  "CoastalPlaneModel" should " not attenuate with unknown landuse" in {
    val cpm: CoastalPlaneModel = createRandomCPwithLanduse(-999)
    val cpmAttenuation: CoastalPlaneModel = cpm.clone
    var landuseClasses: List[Int] = List(0, 1, 2, 3, 4, 5, 6)
    var waterlevelAttenuationRates: List[Float] = List(25, 50, 100, 40, 25, 50, 25)

    val waterlevelAttenuationMap: Map[Int, Float] = (landuseClasses zip waterlevelAttenuationRates.map((r: Float) => r / 100f)).toMap

    cpmAttenuation.attenuate(waterlevelAttenuationMap)

    for (i <- 1 to 100) {
      val elev: Float = r.nextFloat() * 10
      //println(cpm.areaExposure.cumulativeExposureAttenuation(elev) + " - " + cpm.areaExposure.cumulativeExposure(elev))
      cpmAttenuation.areaExposure.cumulativeExposureAttenuation(elev) should be(cpm.areaExposure.cumulativeExposure(elev) +- epsilon05)
    }
  }

  "CoastalPlaneModel" should " attenuate with known landuse" in {
    var landuseClasses: List[Int] = List(0, 1, 2, 3, 4, 5, 6)
    var waterlevelAttenuationRates: List[Float] = List(25, 50, 100, 40, 25, 50, 25)

    val waterlevelAttenuationMap: Map[Int, Float] = (landuseClasses zip waterlevelAttenuationRates.map((r: Float) => r / 100f)).toMap

    for (i <- 1 to 100) {
      val landuse = r.nextInt(7)
      val cpm: CoastalPlaneModel = createRandomCPwithLanduse(landuse)
      val cpmAttenuation: CoastalPlaneModel = cpm.clone
      val elev: Float = r.nextFloat() * 10 + 0.1f
      cpmAttenuation.attenuate(waterlevelAttenuationMap)
      cpmAttenuation.areaExposure.cumulativeExposureAttenuation(elev) should be < (cpm.areaExposure.cumulativeExposure(elev))
    }

  }

  "CoastalPlaneModel" should " attenuate correctly" in {
    var landuseClasses: List[Int] = List(0, 1, 2, 3, 4, 5, 6)
    var waterlevelAttenuationRates: List[Float] = List(25, 50, 100, 40, 25, 50, 25)

    val waterlevelAttenuationMap: Map[Int, Float] = (landuseClasses zip waterlevelAttenuationRates.map((r: Float) => r / 100f)).toMap

    for (i <- 0 until 100) {
      val landuse = r.nextInt(7)
      val cpm: CoastalPlaneModel = createHomogenousCPwithLanduse(landuse)

      val cpmAttenuation: CoastalPlaneModel = cpm.clone
      cpmAttenuation.attenuate(waterlevelAttenuationMap)

      //for (j <- 0 until cpm.elevationExposure.lCumulativeWiths.length) {
      //  cpmAttenuation.elevationExposure.lCumulativeWiths(j) should be((cpmAttenuation.areaExposure.lCumulativeExposure(j) / cpmAttenuation.pLength) +- epsilon03)
      //}
      for (j <- 0 until 100) {
        val elev: Float = (r.nextFloat() * 20)
        cpmAttenuation.elevationExposureAttenuation.cumulativeExposureInverse(elev).toFloat should be((cpmAttenuation.areaExposure.cumulativeExposureAttenuation(elev) / cpmAttenuation.pLength) +- epsilon02)
      }

      for (j <- 0 until 100) {
        // This test simply tests the geometry
        // the coastal profile can be written as f1(x) = slope * x
        // the attenuated extend can be written as f2(x) = -ar_in_m * x + surge_height
        // thus we can calculate the attenuated extent as the intersection of these two lines
        // we get: extent_ATT = 1/(slope+ar_in_m) * (extent_orig * slope)

        val elev: Float = (r.nextFloat() * 10)
        val ar: Float = waterlevelAttenuationMap(landuse)
        val ar_in_m: Float = (ar / 1000)
        var slope: Float = elev / (cpm.elevationExposure.cumulativeExposureInverse(elev).toFloat * 1000)

        // test the extent
        (cpmAttenuation.elevationExposureAttenuation.cumulativeExposureInverse(elev).toFloat / cpm.elevationExposure.cumulativeExposureInverse(elev).toFloat should be((slope / (slope + ar_in_m)) +- epsilon02))
        // test the area
        (cpmAttenuation.areaExposure.cumulativeExposureAttenuation(elev).toFloat / cpm.areaExposure.cumulativeExposure(elev).toFloat should be((slope / (slope + ar_in_m)) +- epsilon02))
        // test population
        (cpmAttenuation.populationExposure.cumulativeExposureAttenuation(elev).toFloat / cpm.populationExposure.cumulativeExposure(elev).toFloat should be((slope / (slope + ar_in_m)) +- epsilon02))
        // test the area
        (cpmAttenuation.assetsExposure.cumulativeExposureAttenuation(elev).toFloat / cpm.assetsExposure.cumulativeExposure(elev).toFloat should be((slope / (slope + ar_in_m)) +- epsilon02))
      }
      

      /*
      println("slope: " + slope)
      println("landuse: " + landuse + " --- ar: " + ar_in_m)
      println("elev*1: " + elev + ";  " + cpm.elevationExposure.cumulativeExposureInverse(elev).toFloat)
      println("elev*2: " + (elev * 2) + ";  " + cpm.elevationExposure.cumulativeExposureInverse(elev * 2).toFloat)

      println("elev*1: " + elev + ";  " + cpmAttenuation.elevationExposureAttenuation.cumulativeExposureInverse(elev))
      println("elev*2: " + (elev * 2) + ";  " + cpmAttenuation.elevationExposureAttenuation.cumulativeExposureInverse(elev * 2))

      //println("elev*1 attenuated: " + (elev - (cpm.elevationExposure.cumulativeExposureInverse(elev)*ar).toFloat))
      //println("elev*2 attenuated: " + ((elev*2) - (cpm.elevationExposure.cumulativeExposureInverse(elev*2)*ar).toFloat))

      println("elev*1_extend_attenuated/elev*1_extend: " + cpmAttenuation.elevationExposureAttenuation.cumulativeExposureInverse(elev).toFloat / cpm.elevationExposure.cumulativeExposureInverse(elev))
      println("should be: " + (slope / (slope + ar_in_m)))

      //  + elev + " - flood extent: " + cpm.elevationExposure.cumulativeExposureInverse(elev).toFloat + "    " + cpmAttenuation.elevationExposureAttenuation.cumulativeExposureInverse(elev).toFloat)
      //println("Factor " + cpm.elevationExposure.cumulativeExposureInverse(elev).toFloat / cpmAttenuation.elevationExposureAttenuation.cumulativeExposureInverse(elev).toFloat + " should be " + (1.0f / (waterlevelAttenuationMap(landuse))))
      //println(elev + ": " + cpm.areaExposure.cumulativeExposure(elev).toFloat + "    " + cpmAttenuation.areaExposure.cumulativeExposureAttenuation(elev).toFloat)
      //println("length = " + cpm.pLength)
      println(cpm.elevationExposure.lLevels.toList)
      println(cpm.elevationExposure.lCumulativeExposure.toList)
      println(cpmAttenuation.elevationExposureAttenuation.lLevels.toList)
      println(cpmAttenuation.elevationExposureAttenuation.lCumulativeExposure.toList)
      println("Factor " + cpm.elevationExposure.cumulativeExposureInverse(elev).toFloat / cpmAttenuation.elevationExposureAttenuation.cumulativeExposureInverse(elev).toFloat + " should be " + (1.0f / (waterlevelAttenuationMap(landuse))))
		  */
    }

  }

  /*
  it should "imply exceedance probability 1 for hx when hx is < h1" in {
    val edfh: EDFloodHazard = createRandomEDFloodHazard()

    val h0_9: Double = edfh.returnPeriodHeight(0.9)
    val h0_5: Double = edfh.returnPeriodHeight(0.5)
    val h0_1: Double = edfh.returnPeriodHeight(0.1)

    edfh.exceedanceProb(h0_9) should be(1)
    edfh.exceedanceProb(h0_5) should be(1)
    edfh.exceedanceProb(h0_1) should be(1)
  }*/

}