package diva.tests

import org.scalatest._
import org.scalactic.TolerantNumerics

import diva.module.flooding.GPDFloodHazard
import diva.types.TypeComputations._

class TestGPDFloodHazard extends FlatSpec with Matchers {
  val epsilon10 = 1e-10f
  val epsilon05 = 1e-5f
  val epsilon02 = 1e-2f
  val epsilon01 = 1e-1f
  val r = scala.util.Random

  //implicit val doubleEq = TolerantNumerics.tolerantDoubleEquality(epsilon)

  "GPDFloodHazard.cdf" should "generate 0 for h1; 0.1 for h10 and 0.999 for h1000" in {
    var mue: Float = r.nextFloat() + 0.25f
    var sigma: Float = (r.nextFloat() + 0.05f) / 5f
    var zeta: Float = r.nextFloat() - 0.5f
    var n: Float = r.nextFloat() * 10f + 0.5f
    var shift: Float = r.nextFloat()

    var gpdfh: GPDFloodHazard = new GPDFloodHazard(mue, sigma, zeta, n)

    var h1: Double = gpdfh.returnPeriodHeight(1)
    var h10: Double = gpdfh.returnPeriodHeight(10)
    var h1000: Double = gpdfh.returnPeriodHeight(1000)

    gpdfh.cdf(h1) should be(0.0 +- epsilon05)
    gpdfh.cdf(h10) should be(0.9 +- epsilon02)
    gpdfh.cdf(h1000) should be(0.999 +- epsilon02)

    zeta = 0
    gpdfh = new GPDFloodHazard(mue, sigma, zeta, n)

    h1 = gpdfh.returnPeriodHeight(1)
    h10 = gpdfh.returnPeriodHeight(10)
    h1000 = gpdfh.returnPeriodHeight(1000)

    gpdfh.cdf(h1) should be(0.0 +- epsilon05)
    gpdfh.cdf(h10) should be(0.9 +- epsilon02)
    gpdfh.cdf(h1000) should be(0.999 +- epsilon02)
  }

  it should "imply exceedance probability 1 for hx when hx is < h1" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val zeta: Float = r.nextFloat() - 0.5f
    val n: Float = r.nextFloat() * 10f + 0.5f
    val shift: Float = r.nextFloat()

    val gpdfh: GPDFloodHazard = new GPDFloodHazard(mue, sigma, zeta, n)
    val h1: Double = gpdfh.returnPeriodHeight(1)

    for (i <- 1 to 100) {
      val x: Double = r.nextDouble() * h1
      gpdfh.cdf(x) should be(0)
    }
  }

  it should "be monotonous" in {
    var mue: Float = r.nextFloat() + 0.25f
    var sigma: Float = (r.nextFloat() + 0.05f) / 5f
    var zeta: Float = r.nextFloat() - 0.5f
    var n: Float = r.nextFloat() * 10f + 0.5f
    var shift: Float = r.nextFloat()
    var gpdfh: GPDFloodHazard = new GPDFloodHazard(mue, sigma, zeta, n)

    var wl_old: Double = 0
    var wl_new: Double = 0

    for (i <- 1 to 10) {
      val x: Double = r.nextDouble() * 0.15
      wl_new = wl_old + x
      if (!gpdfh.cdf(wl_new).isNaN()) {
        gpdfh.cdf(wl_old) should be <= gpdfh.cdf(wl_new)
      } else {
        println("Warning: !gpdfh.cdf(" + wl_new + ") in NaN. Parameters (mue,sigme,zeta,n): " + mue + ", " + sigma + ", " + zeta + ", " + n + ". shift = " + shift)
      }
      wl_old = wl_new
    }

    zeta = 0
    gpdfh = new GPDFloodHazard(mue, sigma, zeta, n)

    wl_old = 0
    wl_new = 0

    for (i <- 1 to 10) {
      val x: Double = r.nextDouble() * 0.15
      wl_new = wl_old + x
      if (!gpdfh.cdf(wl_new).isNaN()) {
        gpdfh.cdf(wl_old) should be <= gpdfh.cdf(wl_new)
      } else {
        println("Warning: !gpdfh.cdf(" + wl_new + ") in NaN. Parameters (mue,sigme,zeta,n): " + mue + ", " + sigma + ", " + zeta + ", " + n + ". shift = " + shift)
      }
      wl_old = wl_new
    }
  }

  it should "shifted by the shift" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    var zeta: Float = r.nextFloat() - 0.5f
    val n: Float = r.nextFloat() * 10f + 0.5f
    val shift: Float = r.nextFloat()
    var gpdfh_unshifted: GPDFloodHazard = new GPDFloodHazard(mue, sigma, zeta, n)
    var gpdfh_shifted: GPDFloodHazard = new GPDFloodHazard(mue, sigma, zeta, n)
    gpdfh_shifted.shift = shift

    var h1_unshifted: Double = gpdfh_unshifted.returnPeriodHeight(1)
    var h10_unshifted: Double = gpdfh_unshifted.returnPeriodHeight(10)
    var h1000_unshifted: Double = gpdfh_unshifted.returnPeriodHeight(1000)

    var h1_shifted: Double = gpdfh_shifted.returnPeriodHeight(1)
    var h10_shifted: Double = gpdfh_shifted.returnPeriodHeight(10)
    var h1000_shifted: Double = gpdfh_shifted.returnPeriodHeight(1000)

    h1_shifted should be(h1_unshifted + shift +- epsilon01)
    h10_shifted should be(h10_unshifted + shift +- epsilon01)
    h1000_shifted should be(h1000_unshifted + shift +- epsilon01)

    for (i <- 1 to 100) {
      val x_unshifted: Double = r.nextFloat() * (h1000_unshifted - h1_unshifted) + h1_unshifted
      val rp_unshifted: Double = gpdfh_unshifted.returnPeriod(x_unshifted)
      val rp_shifted: Double = gpdfh_shifted.returnPeriod(x_unshifted + shift)
      rp_shifted should be(rp_unshifted +- epsilon01)
    }

    for (i <- 1 to 100) {
      val rp: Double = r.nextFloat() * 1000
      val h_unshifted: Double = gpdfh_unshifted.returnPeriodHeight(rp)
      val h_shifted: Double = gpdfh_shifted.returnPeriodHeight(rp)
      h_shifted should be(h_unshifted + shift +- epsilon01)
    }

    zeta = 0f
    gpdfh_unshifted = new GPDFloodHazard(mue, sigma, zeta, n)
    gpdfh_shifted = new GPDFloodHazard(mue, sigma, zeta, n)
    gpdfh_shifted.shift = shift

    h1_unshifted = gpdfh_unshifted.returnPeriodHeight(1)
    h10_unshifted = gpdfh_unshifted.returnPeriodHeight(10)
    h1000_unshifted = gpdfh_unshifted.returnPeriodHeight(1000)

    h1_shifted = gpdfh_shifted.returnPeriodHeight(1)
    h10_shifted = gpdfh_shifted.returnPeriodHeight(10)
    h1000_shifted = gpdfh_shifted.returnPeriodHeight(1000)

    h1_shifted should be(h1_unshifted + shift +- epsilon01)
    h10_shifted should be(h10_unshifted + shift +- epsilon01)
    h1000_shifted should be(h1000_unshifted + shift +- epsilon01)

    for (i <- 1 to 100) {
      val x_unshifted: Double = r.nextFloat() * (h1000_unshifted - h1_unshifted) + h1_unshifted
      val rp_unshifted: Double = gpdfh_unshifted.returnPeriod(x_unshifted)
      val rp_shifted: Double = gpdfh_shifted.returnPeriod(x_unshifted + shift)
      rp_shifted should be(rp_unshifted +- epsilon01)
    }

    for (i <- 1 to 100) {
      val rp: Double = r.nextFloat() * 1000
      val h_unshifted: Double = gpdfh_unshifted.returnPeriodHeight(rp)
      val h_shifted: Double = gpdfh_shifted.returnPeriodHeight(rp)
      h_shifted should be(h_unshifted + shift +- epsilon01)
    }

  }

  "GPDFloodHazard.returnPeriodHeight" should "be the inverse of GPDFloodHazard.returnPeriod" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    var zeta: Float = r.nextFloat() - 0.5f
    val n: Float = r.nextFloat() * 10f + 0.5f
    val shift: Float = r.nextFloat()

    var gpdfh: GPDFloodHazard = new GPDFloodHazard(mue, sigma, zeta, n)

    var h1: Double = gpdfh.returnPeriodHeight(1)
    var h10000: Double = gpdfh.returnPeriodHeight(10000)

    for (i <- h1 to h10000 by 0.01) {
      gpdfh.returnPeriodHeight(gpdfh.returnPeriod(i)).toDouble should be(i +- epsilon02)
    }
    
    zeta = 0f
    gpdfh = new GPDFloodHazard(mue, sigma, zeta, n)
    h1 = gpdfh.returnPeriodHeight(1)
    h10000 = gpdfh.returnPeriodHeight(10000)

    for (i <- h1 to h10000 by 0.01) {
      gpdfh.returnPeriodHeight(gpdfh.returnPeriod(i)).toDouble should be(i +- epsilon02)
    }
  }

  "GPDFloodHazard.cdf_inverse" should "be the inverse of GPDFloodHazard.cdf" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    var zeta: Float = r.nextFloat() - 0.5f
    val n: Float = r.nextFloat() * 10f + 0.5f

    var gpdfh: GPDFloodHazard = new GPDFloodHazard(mue, sigma, zeta, n)

    var h1: Double = gpdfh.returnPeriodHeight(1)
    var h1000: Double = gpdfh.returnPeriodHeight(1000)

    for (i <- 1 to 1000) {
      val x: Double = (r.nextDouble() * (h1000 - h1)) + h1
      if (gpdfh.cdf(x) > 0) {
        gpdfh.cdf_inverse(gpdfh.cdf(x)).toDouble should be(x +- epsilon02)
      }
    }

    zeta = 0f
    gpdfh = new GPDFloodHazard(mue, sigma, zeta, n)

    h1 = gpdfh.returnPeriodHeight(1)
    h1000 = gpdfh.returnPeriodHeight(1000)

    for (i <- 1 to 1000) {
      val x: Double = (r.nextDouble() * (h1000 - h1)) + h1
      if (gpdfh.cdf(x) > 0) {
        gpdfh.cdf_inverse(gpdfh.cdf(x)).toDouble should be(x +- epsilon02)
      }
    }
  }

  /*
  "GPDFloodHazard.cdf_prime" should "be the derivative of GPDFloodHazard.cdf" in {
    val mue: Float = r.nextFloat() + 0.25f
    val sigma: Float = (r.nextFloat() + 0.05f) / 5f
    val zeta: Float = r.nextFloat() - 0.5f
    val n: Float = r.nextFloat() * 10f + 0.5f
    val shift: Float = r.nextFloat()

    val gpdfh: GPDFloodHazard = new GPDFloodHazard(mue, sigma, zeta, n)
    gpdfh.shift = shift

    val h1: Double = gpdfh.returnPeriodHeight(1)
    val h1000: Double = gpdfh.returnPeriodHeight(1000)

    for (i <- 1 to 100) {
      val x: Double = (r.nextFloat() * (h1000 - h1)) + h1
      val f_prime_x: Double = gpdfh.cdf_prime(x)
      val f_x_plus_eps: Double = gpdfh.cdf(x + epsilon10)
      val f_x_minus_eps: Double = gpdfh.cdf(x - epsilon10)
      f_prime_x should be(((f_x_plus_eps - f_x_minus_eps) / (2 * epsilon05)) +- epsilon01)
    }
  }
  *
  */
}