/* Automatically generated data IO for Cz
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data.io

import diva.io._
import diva.types.InterpolationMode.InterpolationMode
import diva.feature.data.CzData

class CzDataIO extends FeatureDataIO[CzData] {

  def writeData(out: divaOutput, d: CzData) {
    out.write(d.adminid)
    d.area_below_.keys.toList.sorted.foreach { x => out.write(d.area_below_(x)) }
    d.area_below_h.keys.toList.sorted.foreach { x => out.write(d.area_below_h(x)) }
    d.assets_below_.keys.toList.sorted.foreach { x => out.write(d.assets_below_(x)) }
    d.assets_below_h.keys.toList.sorted.foreach { x => out.write(d.assets_below_h(x)) }
    d.assets_developable_below_.keys.toList.sorted.foreach { x => out.write(d.assets_developable_below_(x)) }
    d.assets_developed_below_.keys.toList.sorted.foreach { x => out.write(d.assets_developed_below_(x)) }
    out.write(d.gdpc)
    out.write(d.landloss_initial_land_below_S1)
    out.write(d.landloss_submergence)
    out.write(d.landlosscost_submergence)
    out.write(d.landuse)
    out.write(d.localgdp)
    out.write(d.migration_erosion)
    out.write(d.migration_retreat)
    out.write(d.migration_submergence)
    out.write(d.migrationcost_retreat)
    out.write(d.migrationcost_submergence)
    out.write(d.par)
    d.pop_below_.keys.toList.sorted.foreach { x => out.write(d.pop_below_(x)) }
    d.pop_below_h.keys.toList.sorted.foreach { x => out.write(d.pop_below_h(x)) }
    out.write(d.population_correction)
    out.write(d.seafloodcost)
    d.seafloodcost_h.keys.toList.sorted.foreach { x => out.write(d.seafloodcost_h(x)) }
    out.write(d.seafloodcost_min)
    out.write(d.seafloodcost_relative)
    out.write(d.setbackzone_area)
    out.write(d.setbackzone_assets)
    out.write(d.setbackzone_deprecation)
    out.write(d.setbackzone_population)
  }

  def writeSelectedData(out: divaOutput, d: CzData, ignoreThisOutput: Array[Boolean]) {
    var c: Int = 0
    if (!ignoreThisOutput(c)) {out.write(d.adminid)}; c+=1
    d.area_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.area_below_(x))}; c+=1 } } 
    d.area_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.area_below_h(x))}; c+=1 } } 
    d.assets_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_below_(x))}; c+=1 } } 
    d.assets_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_below_h(x))}; c+=1 } } 
    d.assets_developable_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_developable_below_(x))}; c+=1 } } 
    d.assets_developed_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_developed_below_(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.gdpc)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landloss_initial_land_below_S1)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landloss_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landlosscost_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landuse)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.localgdp)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_erosion)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_retreat)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migrationcost_retreat)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migrationcost_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.par)}; c+=1
    d.pop_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.pop_below_(x))}; c+=1 } } 
    d.pop_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.pop_below_h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.population_correction)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost)}; c+=1
    d.seafloodcost_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_min)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_relative)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_area)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_assets)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_deprecation)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_population)}; c+=1
  }

  def readInputValue(in: divaInput, d:CzData, parName: String, locid: String, time: Int) {
    if (parName=="adminid") {d.adminid = in.getInputValue[String]("adminid", locid, time)}
    else if (parName=="clsid") {d.clsid = in.getInputValue[String]("clsid", locid, time)}
    else if (parName=="coastalzone") {d.coastalzone = in.getInputValue[Boolean]("coastalzone", locid, time)}
    // else println("error:" + parName + " not found in Cz")
  }

  def readOptionInputValue(in: divaInput, d:CzData, parName: String, locid: String, time: Int) {
    if (parName=="area00_5") {d.area00_5 = in.getOptionInputValue[Float]("area00_5", locid, time)}
    else if (parName=="area01_0") {d.area01_0 = in.getOptionInputValue[Float]("area01_0", locid, time)}
    else if (parName=="area01_5") {d.area01_5 = in.getOptionInputValue[Float]("area01_5", locid, time)}
    else if (parName=="area02_0") {d.area02_0 = in.getOptionInputValue[Float]("area02_0", locid, time)}
    else if (parName=="area02_5") {d.area02_5 = in.getOptionInputValue[Float]("area02_5", locid, time)}
    else if (parName=="area03_0") {d.area03_0 = in.getOptionInputValue[Float]("area03_0", locid, time)}
    else if (parName=="area03_5") {d.area03_5 = in.getOptionInputValue[Float]("area03_5", locid, time)}
    else if (parName=="area04_0") {d.area04_0 = in.getOptionInputValue[Float]("area04_0", locid, time)}
    else if (parName=="area04_5") {d.area04_5 = in.getOptionInputValue[Float]("area04_5", locid, time)}
    else if (parName=="area05_0") {d.area05_0 = in.getOptionInputValue[Float]("area05_0", locid, time)}
    else if (parName=="area05_5") {d.area05_5 = in.getOptionInputValue[Float]("area05_5", locid, time)}
    else if (parName=="area06_0") {d.area06_0 = in.getOptionInputValue[Float]("area06_0", locid, time)}
    else if (parName=="area06_5") {d.area06_5 = in.getOptionInputValue[Float]("area06_5", locid, time)}
    else if (parName=="area07_0") {d.area07_0 = in.getOptionInputValue[Float]("area07_0", locid, time)}
    else if (parName=="area07_5") {d.area07_5 = in.getOptionInputValue[Float]("area07_5", locid, time)}
    else if (parName=="area08_0") {d.area08_0 = in.getOptionInputValue[Float]("area08_0", locid, time)}
    else if (parName=="area08_5") {d.area08_5 = in.getOptionInputValue[Float]("area08_5", locid, time)}
    else if (parName=="area09_0") {d.area09_0 = in.getOptionInputValue[Float]("area09_0", locid, time)}
    else if (parName=="area09_5") {d.area09_5 = in.getOptionInputValue[Float]("area09_5", locid, time)}
    else if (parName=="area10_0") {d.area10_0 = in.getOptionInputValue[Float]("area10_0", locid, time)}
    else if (parName=="area10_5") {d.area10_5 = in.getOptionInputValue[Float]("area10_5", locid, time)}
    else if (parName=="area11_0") {d.area11_0 = in.getOptionInputValue[Float]("area11_0", locid, time)}
    else if (parName=="area11_5") {d.area11_5 = in.getOptionInputValue[Float]("area11_5", locid, time)}
    else if (parName=="area12_0") {d.area12_0 = in.getOptionInputValue[Float]("area12_0", locid, time)}
    else if (parName=="area12_5") {d.area12_5 = in.getOptionInputValue[Float]("area12_5", locid, time)}
    else if (parName=="area13_0") {d.area13_0 = in.getOptionInputValue[Float]("area13_0", locid, time)}
    else if (parName=="area13_5") {d.area13_5 = in.getOptionInputValue[Float]("area13_5", locid, time)}
    else if (parName=="area14_0") {d.area14_0 = in.getOptionInputValue[Float]("area14_0", locid, time)}
    else if (parName=="area14_5") {d.area14_5 = in.getOptionInputValue[Float]("area14_5", locid, time)}
    else if (parName=="area15_0") {d.area15_0 = in.getOptionInputValue[Float]("area15_0", locid, time)}
    else if (parName=="area15_5") {d.area15_5 = in.getOptionInputValue[Float]("area15_5", locid, time)}
    else if (parName=="area16_0") {d.area16_0 = in.getOptionInputValue[Float]("area16_0", locid, time)}
    else if (parName=="area16_5") {d.area16_5 = in.getOptionInputValue[Float]("area16_5", locid, time)}
    else if (parName=="area17_0") {d.area17_0 = in.getOptionInputValue[Float]("area17_0", locid, time)}
    else if (parName=="area17_5") {d.area17_5 = in.getOptionInputValue[Float]("area17_5", locid, time)}
    else if (parName=="area18_0") {d.area18_0 = in.getOptionInputValue[Float]("area18_0", locid, time)}
    else if (parName=="area18_5") {d.area18_5 = in.getOptionInputValue[Float]("area18_5", locid, time)}
    else if (parName=="area19_0") {d.area19_0 = in.getOptionInputValue[Float]("area19_0", locid, time)}
    else if (parName=="area19_5") {d.area19_5 = in.getOptionInputValue[Float]("area19_5", locid, time)}
    else if (parName=="area20_0") {d.area20_0 = in.getOptionInputValue[Float]("area20_0", locid, time)}
    else if (parName=="area20_5") {d.area20_5 = in.getOptionInputValue[Float]("area20_5", locid, time)}
    else if (parName=="assets00_5") {d.assets00_5 = in.getOptionInputValue[Float]("assets00_5", locid, time)}
    else if (parName=="assets01_0") {d.assets01_0 = in.getOptionInputValue[Float]("assets01_0", locid, time)}
    else if (parName=="assets01_5") {d.assets01_5 = in.getOptionInputValue[Float]("assets01_5", locid, time)}
    else if (parName=="assets02_0") {d.assets02_0 = in.getOptionInputValue[Float]("assets02_0", locid, time)}
    else if (parName=="assets02_5") {d.assets02_5 = in.getOptionInputValue[Float]("assets02_5", locid, time)}
    else if (parName=="assets03_0") {d.assets03_0 = in.getOptionInputValue[Float]("assets03_0", locid, time)}
    else if (parName=="assets03_5") {d.assets03_5 = in.getOptionInputValue[Float]("assets03_5", locid, time)}
    else if (parName=="assets04_0") {d.assets04_0 = in.getOptionInputValue[Float]("assets04_0", locid, time)}
    else if (parName=="assets04_5") {d.assets04_5 = in.getOptionInputValue[Float]("assets04_5", locid, time)}
    else if (parName=="assets05_0") {d.assets05_0 = in.getOptionInputValue[Float]("assets05_0", locid, time)}
    else if (parName=="assets05_5") {d.assets05_5 = in.getOptionInputValue[Float]("assets05_5", locid, time)}
    else if (parName=="assets06_0") {d.assets06_0 = in.getOptionInputValue[Float]("assets06_0", locid, time)}
    else if (parName=="assets06_5") {d.assets06_5 = in.getOptionInputValue[Float]("assets06_5", locid, time)}
    else if (parName=="assets07_0") {d.assets07_0 = in.getOptionInputValue[Float]("assets07_0", locid, time)}
    else if (parName=="assets07_5") {d.assets07_5 = in.getOptionInputValue[Float]("assets07_5", locid, time)}
    else if (parName=="assets08_0") {d.assets08_0 = in.getOptionInputValue[Float]("assets08_0", locid, time)}
    else if (parName=="assets08_5") {d.assets08_5 = in.getOptionInputValue[Float]("assets08_5", locid, time)}
    else if (parName=="assets09_0") {d.assets09_0 = in.getOptionInputValue[Float]("assets09_0", locid, time)}
    else if (parName=="assets09_5") {d.assets09_5 = in.getOptionInputValue[Float]("assets09_5", locid, time)}
    else if (parName=="assets10_0") {d.assets10_0 = in.getOptionInputValue[Float]("assets10_0", locid, time)}
    else if (parName=="assets10_5") {d.assets10_5 = in.getOptionInputValue[Float]("assets10_5", locid, time)}
    else if (parName=="assets11_0") {d.assets11_0 = in.getOptionInputValue[Float]("assets11_0", locid, time)}
    else if (parName=="assets11_5") {d.assets11_5 = in.getOptionInputValue[Float]("assets11_5", locid, time)}
    else if (parName=="assets12_0") {d.assets12_0 = in.getOptionInputValue[Float]("assets12_0", locid, time)}
    else if (parName=="assets12_5") {d.assets12_5 = in.getOptionInputValue[Float]("assets12_5", locid, time)}
    else if (parName=="assets13_0") {d.assets13_0 = in.getOptionInputValue[Float]("assets13_0", locid, time)}
    else if (parName=="assets13_5") {d.assets13_5 = in.getOptionInputValue[Float]("assets13_5", locid, time)}
    else if (parName=="assets14_0") {d.assets14_0 = in.getOptionInputValue[Float]("assets14_0", locid, time)}
    else if (parName=="assets14_5") {d.assets14_5 = in.getOptionInputValue[Float]("assets14_5", locid, time)}
    else if (parName=="assets15_0") {d.assets15_0 = in.getOptionInputValue[Float]("assets15_0", locid, time)}
    else if (parName=="assets15_5") {d.assets15_5 = in.getOptionInputValue[Float]("assets15_5", locid, time)}
    else if (parName=="assets16_0") {d.assets16_0 = in.getOptionInputValue[Float]("assets16_0", locid, time)}
    else if (parName=="assets16_5") {d.assets16_5 = in.getOptionInputValue[Float]("assets16_5", locid, time)}
    else if (parName=="assets17_0") {d.assets17_0 = in.getOptionInputValue[Float]("assets17_0", locid, time)}
    else if (parName=="assets17_5") {d.assets17_5 = in.getOptionInputValue[Float]("assets17_5", locid, time)}
    else if (parName=="assets18_0") {d.assets18_0 = in.getOptionInputValue[Float]("assets18_0", locid, time)}
    else if (parName=="assets18_5") {d.assets18_5 = in.getOptionInputValue[Float]("assets18_5", locid, time)}
    else if (parName=="assets19_0") {d.assets19_0 = in.getOptionInputValue[Float]("assets19_0", locid, time)}
    else if (parName=="assets19_5") {d.assets19_5 = in.getOptionInputValue[Float]("assets19_5", locid, time)}
    else if (parName=="assets20_0") {d.assets20_0 = in.getOptionInputValue[Float]("assets20_0", locid, time)}
    else if (parName=="assets20_5") {d.assets20_5 = in.getOptionInputValue[Float]("assets20_5", locid, time)}
    else if (parName=="dluc00_5") {d.dluc00_5 = in.getOptionInputValue[Int]("dluc00_5", locid, time)}
    else if (parName=="dluc01_0") {d.dluc01_0 = in.getOptionInputValue[Int]("dluc01_0", locid, time)}
    else if (parName=="dluc01_5") {d.dluc01_5 = in.getOptionInputValue[Int]("dluc01_5", locid, time)}
    else if (parName=="dluc02_0") {d.dluc02_0 = in.getOptionInputValue[Int]("dluc02_0", locid, time)}
    else if (parName=="dluc02_5") {d.dluc02_5 = in.getOptionInputValue[Int]("dluc02_5", locid, time)}
    else if (parName=="dluc03_0") {d.dluc03_0 = in.getOptionInputValue[Int]("dluc03_0", locid, time)}
    else if (parName=="dluc03_5") {d.dluc03_5 = in.getOptionInputValue[Int]("dluc03_5", locid, time)}
    else if (parName=="dluc04_0") {d.dluc04_0 = in.getOptionInputValue[Int]("dluc04_0", locid, time)}
    else if (parName=="dluc04_5") {d.dluc04_5 = in.getOptionInputValue[Int]("dluc04_5", locid, time)}
    else if (parName=="dluc05_0") {d.dluc05_0 = in.getOptionInputValue[Int]("dluc05_0", locid, time)}
    else if (parName=="dluc05_5") {d.dluc05_5 = in.getOptionInputValue[Int]("dluc05_5", locid, time)}
    else if (parName=="dluc06_0") {d.dluc06_0 = in.getOptionInputValue[Int]("dluc06_0", locid, time)}
    else if (parName=="dluc06_5") {d.dluc06_5 = in.getOptionInputValue[Int]("dluc06_5", locid, time)}
    else if (parName=="dluc07_0") {d.dluc07_0 = in.getOptionInputValue[Int]("dluc07_0", locid, time)}
    else if (parName=="dluc07_5") {d.dluc07_5 = in.getOptionInputValue[Int]("dluc07_5", locid, time)}
    else if (parName=="dluc08_0 ") {d.dluc08_0  = in.getOptionInputValue[Int]("dluc08_0 ", locid, time)}
    else if (parName=="dluc08_5") {d.dluc08_5 = in.getOptionInputValue[Int]("dluc08_5", locid, time)}
    else if (parName=="dluc09_0") {d.dluc09_0 = in.getOptionInputValue[Int]("dluc09_0", locid, time)}
    else if (parName=="dluc09_5") {d.dluc09_5 = in.getOptionInputValue[Int]("dluc09_5", locid, time)}
    else if (parName=="dluc10_0") {d.dluc10_0 = in.getOptionInputValue[Int]("dluc10_0", locid, time)}
    else if (parName=="dluc10_5") {d.dluc10_5 = in.getOptionInputValue[Int]("dluc10_5", locid, time)}
    else if (parName=="dluc11_0") {d.dluc11_0 = in.getOptionInputValue[Int]("dluc11_0", locid, time)}
    else if (parName=="dluc11_5") {d.dluc11_5 = in.getOptionInputValue[Int]("dluc11_5", locid, time)}
    else if (parName=="dluc12_0") {d.dluc12_0 = in.getOptionInputValue[Int]("dluc12_0", locid, time)}
    else if (parName=="dluc12_5") {d.dluc12_5 = in.getOptionInputValue[Int]("dluc12_5", locid, time)}
    else if (parName=="dluc13_0") {d.dluc13_0 = in.getOptionInputValue[Int]("dluc13_0", locid, time)}
    else if (parName=="dluc13_5") {d.dluc13_5 = in.getOptionInputValue[Int]("dluc13_5", locid, time)}
    else if (parName=="dluc14_0") {d.dluc14_0 = in.getOptionInputValue[Int]("dluc14_0", locid, time)}
    else if (parName=="dluc14_5") {d.dluc14_5 = in.getOptionInputValue[Int]("dluc14_5", locid, time)}
    else if (parName=="dluc15_0") {d.dluc15_0 = in.getOptionInputValue[Int]("dluc15_0", locid, time)}
    else if (parName=="dluc15_5") {d.dluc15_5 = in.getOptionInputValue[Int]("dluc15_5", locid, time)}
    else if (parName=="dluc16_0") {d.dluc16_0 = in.getOptionInputValue[Int]("dluc16_0", locid, time)}
    else if (parName=="dluc16_5") {d.dluc16_5 = in.getOptionInputValue[Int]("dluc16_5", locid, time)}
    else if (parName=="dluc17_0") {d.dluc17_0 = in.getOptionInputValue[Int]("dluc17_0", locid, time)}
    else if (parName=="dluc17_5") {d.dluc17_5 = in.getOptionInputValue[Int]("dluc17_5", locid, time)}
    else if (parName=="dluc18_0") {d.dluc18_0 = in.getOptionInputValue[Int]("dluc18_0", locid, time)}
    else if (parName=="dluc18_5") {d.dluc18_5 = in.getOptionInputValue[Int]("dluc18_5", locid, time)}
    else if (parName=="dluc19_0") {d.dluc19_0 = in.getOptionInputValue[Int]("dluc19_0", locid, time)}
    else if (parName=="dluc19_5") {d.dluc19_5 = in.getOptionInputValue[Int]("dluc19_5", locid, time)}
    else if (parName=="dluc20_0") {d.dluc20_0 = in.getOptionInputValue[Int]("dluc20_0", locid, time)}
    else if (parName=="dluc20_5") {d.dluc20_5 = in.getOptionInputValue[Int]("dluc20_5", locid, time)}
    else if (parName=="fraction_developable") {d.fraction_developable = in.getOptionInputValue[Float]("fraction_developable", locid, time)}
    else if (parName=="fraction_developed") {d.fraction_developed = in.getOptionInputValue[Float]("fraction_developed", locid, time)}
    else if (parName=="landuse") {d.landuse = in.getOptionInputValue[Int]("landuse", locid, time)}
    else if (parName=="pop00_5") {d.pop00_5 = in.getOptionInputValue[Float]("pop00_5", locid, time)}
    else if (parName=="pop01_0") {d.pop01_0 = in.getOptionInputValue[Float]("pop01_0", locid, time)}
    else if (parName=="pop01_5") {d.pop01_5 = in.getOptionInputValue[Float]("pop01_5", locid, time)}
    else if (parName=="pop02_0") {d.pop02_0 = in.getOptionInputValue[Float]("pop02_0", locid, time)}
    else if (parName=="pop02_5") {d.pop02_5 = in.getOptionInputValue[Float]("pop02_5", locid, time)}
    else if (parName=="pop03_0") {d.pop03_0 = in.getOptionInputValue[Float]("pop03_0", locid, time)}
    else if (parName=="pop03_5") {d.pop03_5 = in.getOptionInputValue[Float]("pop03_5", locid, time)}
    else if (parName=="pop04_0") {d.pop04_0 = in.getOptionInputValue[Float]("pop04_0", locid, time)}
    else if (parName=="pop04_5") {d.pop04_5 = in.getOptionInputValue[Float]("pop04_5", locid, time)}
    else if (parName=="pop05_0") {d.pop05_0 = in.getOptionInputValue[Float]("pop05_0", locid, time)}
    else if (parName=="pop05_5") {d.pop05_5 = in.getOptionInputValue[Float]("pop05_5", locid, time)}
    else if (parName=="pop06_0") {d.pop06_0 = in.getOptionInputValue[Float]("pop06_0", locid, time)}
    else if (parName=="pop06_5") {d.pop06_5 = in.getOptionInputValue[Float]("pop06_5", locid, time)}
    else if (parName=="pop07_0") {d.pop07_0 = in.getOptionInputValue[Float]("pop07_0", locid, time)}
    else if (parName=="pop07_5") {d.pop07_5 = in.getOptionInputValue[Float]("pop07_5", locid, time)}
    else if (parName=="pop08_0") {d.pop08_0 = in.getOptionInputValue[Float]("pop08_0", locid, time)}
    else if (parName=="pop08_5") {d.pop08_5 = in.getOptionInputValue[Float]("pop08_5", locid, time)}
    else if (parName=="pop09_0") {d.pop09_0 = in.getOptionInputValue[Float]("pop09_0", locid, time)}
    else if (parName=="pop09_5") {d.pop09_5 = in.getOptionInputValue[Float]("pop09_5", locid, time)}
    else if (parName=="pop10_0") {d.pop10_0 = in.getOptionInputValue[Float]("pop10_0", locid, time)}
    else if (parName=="pop10_5") {d.pop10_5 = in.getOptionInputValue[Float]("pop10_5", locid, time)}
    else if (parName=="pop11_0") {d.pop11_0 = in.getOptionInputValue[Float]("pop11_0", locid, time)}
    else if (parName=="pop11_5") {d.pop11_5 = in.getOptionInputValue[Float]("pop11_5", locid, time)}
    else if (parName=="pop12_0") {d.pop12_0 = in.getOptionInputValue[Float]("pop12_0", locid, time)}
    else if (parName=="pop12_5") {d.pop12_5 = in.getOptionInputValue[Float]("pop12_5", locid, time)}
    else if (parName=="pop13_0") {d.pop13_0 = in.getOptionInputValue[Float]("pop13_0", locid, time)}
    else if (parName=="pop13_5") {d.pop13_5 = in.getOptionInputValue[Float]("pop13_5", locid, time)}
    else if (parName=="pop14_0") {d.pop14_0 = in.getOptionInputValue[Float]("pop14_0", locid, time)}
    else if (parName=="pop14_5") {d.pop14_5 = in.getOptionInputValue[Float]("pop14_5", locid, time)}
    else if (parName=="pop15_0") {d.pop15_0 = in.getOptionInputValue[Float]("pop15_0", locid, time)}
    else if (parName=="pop15_5") {d.pop15_5 = in.getOptionInputValue[Float]("pop15_5", locid, time)}
    else if (parName=="pop16_0") {d.pop16_0 = in.getOptionInputValue[Float]("pop16_0", locid, time)}
    else if (parName=="pop16_5") {d.pop16_5 = in.getOptionInputValue[Float]("pop16_5", locid, time)}
    else if (parName=="pop17_0") {d.pop17_0 = in.getOptionInputValue[Float]("pop17_0", locid, time)}
    else if (parName=="pop17_5") {d.pop17_5 = in.getOptionInputValue[Float]("pop17_5", locid, time)}
    else if (parName=="pop18_0") {d.pop18_0 = in.getOptionInputValue[Float]("pop18_0", locid, time)}
    else if (parName=="pop18_5") {d.pop18_5 = in.getOptionInputValue[Float]("pop18_5", locid, time)}
    else if (parName=="pop19_0") {d.pop19_0 = in.getOptionInputValue[Float]("pop19_0", locid, time)}
    else if (parName=="pop19_5") {d.pop19_5 = in.getOptionInputValue[Float]("pop19_5", locid, time)}
    else if (parName=="pop20_0") {d.pop20_0 = in.getOptionInputValue[Float]("pop20_0", locid, time)}
    else if (parName=="pop20_5") {d.pop20_5 = in.getOptionInputValue[Float]("pop20_5", locid, time)}
    // else println("error:" + parName + " not found in Cz")
  }

  def readScenarioValue(in: divaInput, d:CzData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

  def readOptionScenarioValue(in: divaInput, d:CzData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
    if (parName=="popgrowth") {d.popgrowth = in.getOptionScenarioValue[Float]("popgrowth", locid, time, pInterpolationMode)}
    // else println("error:" + parName + " not found in Cz")
  }

}
