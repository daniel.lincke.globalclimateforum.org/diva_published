package diva.io.sql

/**
 * @author lincke
 */

import diva.io.divaOutput
import diva.io.logging.LoggerI
import diva.types.TypeComputations._
import scala.collection.mutable.ArrayBuffer
import scala.reflect.runtime.{ universe => ru }
import java.io.FileOutputStream
import java.io.FileWriter
import java.io.BufferedWriter
//import java.sql._
import java.sql.Connection
import java.sql.DriverManager
import java.sql.Statement

class divaSQLOutput(connection: Connection, val pLogger: LoggerI, val tableName: String) extends divaOutput {

  logger = pLogger
  var columns: Array[String] = _
  private var initLine: Boolean = true
  private var initData: Boolean = true
  var currentSQLCodeLine: String = _

  def writeInit = {
    currentSQLCodeLine = "INSERT INTO " + tableName + " ("
    if (columns.length > 0) {
      currentSQLCodeLine = currentSQLCodeLine + columns.head
      columns.tail.foreach { x => { currentSQLCodeLine = currentSQLCodeLine + ", " + x } }
    }
    currentSQLCodeLine = currentSQLCodeLine + ") VALUES ";
    initLine = true
    initData = true
  }

  def write[T: toPSQLString](t: T) = {
    if ((!initLine) && (initData)) { currentSQLCodeLine = currentSQLCodeLine + ", (" }
    if ((initLine) && (initData)) { currentSQLCodeLine = currentSQLCodeLine + "  (" }
    if (!initData) { currentSQLCodeLine = currentSQLCodeLine + ", " }
    currentSQLCodeLine = currentSQLCodeLine + convertToPSQLString[T](t)
    initData = false
    initLine = false
  }

  def writeNewLine() {
    currentSQLCodeLine = currentSQLCodeLine + ")"
    initData = true
  }

  def writeFinish = {
    currentSQLCodeLine = currentSQLCodeLine + ";"
    var stmt: Statement = connection.createStatement()
    try {
      stmt.executeUpdate(currentSQLCodeLine)
      stmt.close()
    } catch {
      case e: Throwable =>
        logger.error("Postgresql could not create table " + tableName + " in database: " + e.toString() + ". Code to execute: " + currentSQLCodeLine)
    }
    currentSQLCodeLine = ""
  }

  def writeStringLine(v: Array[String], types: scala.collection.mutable.ArraySeq[scala.reflect.runtime.universe.Type]) = {}

  def writeHeaderLine(c: Array[String], types: scala.collection.mutable.ArraySeq[scala.reflect.runtime.universe.Type]) {
    columns = c
    var stmt: Statement = connection.createStatement()
    var sqlcode: String = "CREATE TABLE IF NOT EXISTS " + tableName + " ("
    val valuesAndType = (c zip types)
    if (valuesAndType.length > 0) {
      sqlcode = sqlcode + valuesAndType.head._1 + " " + getPSQLType(valuesAndType.head._2)
      valuesAndType.tail.foreach { x => sqlcode = sqlcode + ", " + x._1 + " " + getPSQLType(x._2) }
    }
    sqlcode = sqlcode + ");"
    //println("trying to execude: " + sqlcode)
    try {
      stmt.executeUpdate(sqlcode)
      stmt.close()
    } catch {
      case e: Throwable =>
        logger.error("Postgresql could not create table " + tableName + " in database: " + e.toString() + ". Code to execute: " + sqlcode)
    }
  }

  private def getPSQLType(pType: scala.reflect.runtime.universe.Type): String = {
    if (pType =:= ru.typeOf[String]) "text"
    else if (pType =:= ru.typeOf[Byte]) "integer"
    else if (pType =:= ru.typeOf[Short]) "integer"
    else if (pType =:= ru.typeOf[Int]) "integer"
    else if (pType =:= ru.typeOf[Long]) "integer"
    else if (pType =:= ru.typeOf[Float]) "real"
    else if (pType =:= ru.typeOf[Double]) "real"
    else if (pType =:= ru.typeOf[Boolean]) "boolean"
    else if (pType.typeConstructor =:= ru.typeOf[Option[_]].typeConstructor) {
      val lInnerType = pType.typeArgs.head
      getPSQLType(lInnerType)
    } else {
      "text"
    }
  }
}