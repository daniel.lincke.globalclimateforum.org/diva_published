/* Automatically generated data IO for Basin
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data.io

import diva.io._
import diva.types.InterpolationMode.InterpolationMode
import diva.feature.data.BasinData

class BasinDataIO extends FeatureDataIO[BasinData] {

  def writeData(out: divaOutput, d: BasinData) {
    out.write(d.flatloss_erosion_indirect)
    out.write(d.nbbenefit)
    out.write(d.sandloss_erosion_direct)
    out.write(d.sandloss_erosion_including_nourishment)
    out.write(d.sandloss_erosion_indirect)
    out.write(d.sandloss_erosion_total)
  }

  def writeSelectedData(out: divaOutput, d: BasinData, ignoreThisOutput: Array[Boolean]) {
    var c: Int = 0
    if (!ignoreThisOutput(c)) {out.write(d.flatloss_erosion_indirect)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.nbbenefit)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_direct)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_including_nourishment)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_indirect)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_total)}; c+=1
  }

  def readInputValue(in: divaInput, d:BasinData, parName: String, locid: String, time: Int) {
    if (parName=="ce") {d.ce = in.getInputValue[Float]("ce", locid, time)}
    else if (parName=="clsid") {d.clsid = in.getInputValue[String]("clsid", locid, time)}
    else if (parName=="lagoona") {d.lagoona = in.getInputValue[Float]("lagoona", locid, time)}
    else if (parName=="nc") {d.nc = in.getInputValue[Float]("nc", locid, time)}
    else if (parName=="nd") {d.nd = in.getInputValue[Float]("nd", locid, time)}
    else if (parName=="nf") {d.nf = in.getInputValue[Float]("nf", locid, time)}
    else if (parName=="numinlet") {d.numinlet = in.getInputValue[Int]("numinlet", locid, time)}
    else if (parName=="wsc") {d.wsc = in.getInputValue[Float]("wsc", locid, time)}
    else if (parName=="wsd") {d.wsd = in.getInputValue[Float]("wsd", locid, time)}
    else if (parName=="wsf") {d.wsf = in.getInputValue[Float]("wsf", locid, time)}
    // else println("error:" + parName + " not found in Basin")
  }

  def readOptionInputValue(in: divaInput, d:BasinData, parName: String, locid: String, time: Int) {
  }

  def readScenarioValue(in: divaInput, d:BasinData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

  def readOptionScenarioValue(in: divaInput, d:BasinData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

}
