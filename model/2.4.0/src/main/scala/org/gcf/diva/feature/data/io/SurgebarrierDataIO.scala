/* Automatically generated data IO for Surgebarrier
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data.io

import diva.io._
import diva.types.InterpolationMode.InterpolationMode
import diva.feature.data.SurgebarrierData

class SurgebarrierDataIO extends FeatureDataIO[SurgebarrierData] {

  def writeData(out: divaOutput, d: SurgebarrierData) {
    out.write(d.surge_barrier_cost)
    out.write(d.surge_barrier_height)
    out.write(d.surge_barrier_maintenance_cost)
    out.write(d.surge_barrier_protection_level)
    out.write(d.surge_barrier_width)
  }

  def writeSelectedData(out: divaOutput, d: SurgebarrierData, ignoreThisOutput: Array[Boolean]) {
    var c: Int = 0
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_height)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_protection_level)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_width)}; c+=1
  }

  def readInputValue(in: divaInput, d:SurgebarrierData, parName: String, locid: String, time: Int) {
    if (parName=="surge_barrier_width") {d.surge_barrier_width = in.getInputValue[Float]("surge_barrier_width", locid, time)}
    // else println("error:" + parName + " not found in Surgebarrier")
  }

  def readOptionInputValue(in: divaInput, d:SurgebarrierData, parName: String, locid: String, time: Int) {
  }

  def readScenarioValue(in: divaInput, d:SurgebarrierData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

  def readOptionScenarioValue(in: divaInput, d:SurgebarrierData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

}
