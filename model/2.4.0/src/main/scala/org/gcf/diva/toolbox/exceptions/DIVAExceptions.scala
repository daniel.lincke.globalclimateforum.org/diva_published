package diva.toolbox.exceptions

case class arrayLengthsNotEqualException(message:String) extends Exception(message,null)
case class xArrayNotSortedException(message:String) extends Exception(message,null)
case class GPDWithShapeZeroException(message:String = "") extends Exception(message,null)
case class GEVWithNonPositiveScaleException(message:String = "") extends Exception(message,null)
case class GPDWithNonPositiveScaleException(message:String = "") extends Exception(message,null)