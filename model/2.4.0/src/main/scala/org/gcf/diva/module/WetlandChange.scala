package diva.module

import scala.collection.parallel.immutable.ParSeq
import scala.collection.mutable.ListBuffer

import diva.model.divaState
import diva.model.ModuleI

import diva.model.divaFeatures._
import diva.feature._


class WetlandChange extends ModuleI {

  var slopeMode: Boolean = false
  var includeCriticalSlope: Boolean = false
  var wetlandModuleStartYear: Int = 2015

  var criticalSlope: Float = 0.0f
  var criticalSlopePercentile: Float = 0.95f
  var slr_reference_year: Int = 2010

  val regression_intersect_marsh: Float = -1.5f
  val regression_slope_marsh: Float = 3.42f
  val regression_exponenent_marsh: Float = 0.915f

  val regression_intersect_mangrove: Float = -1.5f
  val regression_slope_mangrove: Float = 3.57f
  val regression_exponenent_mangrove: Float = 0.915f

  var wlCorrectedSLR: scala.collection.mutable.Map[Int, scala.collection.mutable.AnyRefMap[String, Float]] = scala.collection.mutable.Map[Int, scala.collection.mutable.AnyRefMap[String, Float]]()
  var wlCorrectedSLRrates: scala.collection.mutable.Map[Int, scala.collection.mutable.AnyRefMap[String, Float]] = scala.collection.mutable.Map[Int, scala.collection.mutable.AnyRefMap[String, Float]]()
  var wlSLRCorrectetion: scala.collection.mutable.AnyRefMap[String, Float] = scala.collection.mutable.AnyRefMap[String, Float]()

  override val scenarioModule: Boolean = true

  def moduleName: String = "Wetland Change"
  def moduleVersion: String = "3.0 (new wetland module after Mark Schuerchs new algorithm)"
  def moduleDescription: String = ""
  def moduleAuthor: String = "Mark Schuerch, Daniel Lincke"

  def initModule(initialState: divaState) = doNothing

  def computeScenario(currentState: diva.model.divaState) = {

    var correctedSlr: scala.collection.mutable.AnyRefMap[String, Float] = scala.collection.mutable.AnyRefMap[String, Float]()
    var correctedSlrRates: scala.collection.mutable.AnyRefMap[String, Float] = scala.collection.mutable.AnyRefMap[String, Float]()
    if (currentState.time == slr_reference_year) {
      initialSLRCorrection(currentState)
    }
    if (currentState.time > slr_reference_year) {
      SLRCorrection(currentState, slr_reference_year)
    }
    if (currentState.time >= wetlandModuleStartYear) {
      SLRRateCorrection(currentState)
    }

  }

  def init(state: divaState) = {

    var clss: ParSeq[Cls] = state.getFeatures[Cls].par
    clss.foreach {
      cls =>
        {
          computeTotalWetlandArea(cls)
          computeWetlandWidth(cls)
          computeWetlandFraction(cls)
        }
    }

    if (includeCriticalSlope) {
      criticalSlope = computeSlopePercentile(state, criticalSlopePercentile)
    }
    accumulate(state)
  }

  def invoke(state: divaState) = {

    if (state.time >= wetlandModuleStartYear) {
      var clss: ParSeq[Cls] = state.getFeatures[Cls].par
      clss.foreach {
        cls =>
          {
            computeAccomodationSpace(cls)
            computeWetlandChange(cls, state.time, state.timeStep)
          }
      }
    }
    accumulate(state)
  }

  private def computeSlopePercentile(state: divaState, pCriticalSlopePercentile: Float): Float = {
    var slopes: ListBuffer[Float] = new ListBuffer()

    var clss: List[Cls] = state.getFeatures[Cls]
    clss.foreach {
      cls =>
        if (cls.data.wetland_area_total > 0f) {

        }
    }

    slopes.sortWith(_ < _)
    slopes(scala.math.floor(slopes.length * pCriticalSlopePercentile).asInstanceOf[Int])
  }

  private def computeTotalWetlandArea(cls: Cls) {
    cls.data.wetland_area_total = cls.data.wetfresh + cls.data.wetsalt + cls.data.wetmang
  }

  private def computeWetlandFraction(cls: Cls) {
    cls.data.wetland_fraction =
      if (cls.coastalPlane.areaExposure.cumulativeExposure(cls.data.mhws) > 0f) {
        (cls.data.wetland_area_total / cls.coastalPlane.areaExposure.cumulativeExposure(cls.data.mhws)).asInstanceOf[Float]
      } else {
        0
      }

    // ATTENTION: cls.data.wetland_fraction can be BIGGER than 1. This is because the Wetland can be 
    // by now (04/2017) a wetland can be much broader than a segment. The wetland is then attached to
    // one segment by the means of the centroid.
    //cls.data.wetland_fraction = diva.toolbox.Toolbox.limited(cls.data.wetland_fraction,0,1)
  }

  private def computeWetlandWidth(cls: Cls) {
    if (cls.data.wetland_area_total > 0f) {
      // in Kilometers
      cls.data.wetland_width = cls.coastalPlane.elevationExposure.cumulativeExposureInverse(cls.data.mhws).asInstanceOf[Float]

      if (cls.data.wetland_width > 0f) {
        cls.data.wetland_length = cls.data.wetland_area_total / cls.data.wetland_width
      }
    }
  }

  private def computeAccomodationSpace(cls: Cls) {
    // compute distance end of wetland to barrier (critical slope or dike)
    // up to now: if dike exist: aspace = 0
    if (includeCriticalSlope) {
      cls.data.aspace =
        if (cls.coastalPlane.dikeHeigth > 0f) 0
        else cls.coastalPlane.elevationExposure.cumulativeExposureInverse(20.5f).asInstanceOf[Float]
      // else cls.data.aspace=firstCriticalSlopePoint
    } else {
      cls.data.aspace =
        if (cls.coastalPlane.dikeHeigth > 0f) 0
        else cls.coastalPlane.elevationExposure.cumulativeExposureInverse(20.5f).asInstanceOf[Float]
    }
  }

  private def computeWetlandChange(cls: Cls, time: Int, timestep: Int) {

    if (cls.data.wetland_area_total > 0f) {
      val unconstrainedWetlandGain: Float = computeUnconstrainedWetlandGain(cls, time, timestep)
      val constrainedWetlandGain: Float = constrainUnconstrainedWetlandGain(unconstrainedWetlandGain, cls)

      val unconstrainedWetlandLoss: Float = computeUnconstrainedWetlandLoss(cls, time, timestep)
      val constrainedWetlandLoss: Float = constrainUnconstrainedWetlandLoss(unconstrainedWetlandLoss, cls, time, timestep)

      val wetlandChange: Float = constrainedWetlandGain - constrainedWetlandLoss
      val wetlandChangeRelative: Float = (wetlandChange + cls.data.wetland_area_total) / cls.data.wetland_area_total

      /*if (cls.locationid == "CLS_AUS_00086") {
        println("unconstrainedWetlandGain = " + unconstrainedWetlandGain)
        println("constrainedWetlandGain = " + constrainedWetlandGain)
        println("unconstrainedWetlandLoss = " + unconstrainedWetlandLoss)
        println("constrainedWetlandLoss = " + constrainedWetlandLoss)
        println("wetlandChange = " + wetlandChange)
        println("wetlandChangeRelative = " + wetlandChangeRelative)
        println("cls.data.wetland_fraction = " + cls.data.wetland_fraction)
      }*/

      if (wetlandChangeRelative > 0) {
        cls.data.wetland_area_total = cls.data.wetland_area_total * wetlandChangeRelative
        cls.data.wetfresh = cls.data.wetfresh * wetlandChangeRelative
        cls.data.wetsalt = cls.data.wetsalt * wetlandChangeRelative
        cls.data.wetmang = cls.data.wetmang * wetlandChangeRelative
      } else {
        cls.data.wetland_area_total = 0f
        cls.data.wetfresh = 0f
        cls.data.wetsalt = 0f
        cls.data.wetmang = 0f
      }
    }

  }

  private def computeUnconstrainedWetlandGain(cls: Cls, time: Int, timestep: Int): Float = {
    //val rslr_new: Float = cls.data.rslr_memory(time)
    //val rslr_old: Float = cls.data.rslr_memory(time - timestep)
    val rslr_new: Float = wlCorrectedSLR(time)(cls.locationid)
    val rslr_old: Float = wlCorrectedSLR(time - timestep)(cls.locationid)
    cls.coastalPlane.areaExposure.cumulativeExposure(cls.data.mhws + rslr_new).asInstanceOf[Float] -
      cls.coastalPlane.areaExposure.cumulativeExposure(cls.data.mhws + rslr_old).asInstanceOf[Float] * cls.data.wetland_fraction.asInstanceOf[Float]
  }

  private def constrainUnconstrainedWetlandGain(pUnconstrainedWetlandGain: Float, cls: Cls): Float = {
    if (cls.data.aspace == 0)
      0f
    else
      pUnconstrainedWetlandGain
  }

  private def computeUnconstrainedWetlandLoss(cls: Cls, time: Int, timestep: Int): Float = {
    //val rslr_new: Float = cls.data.rslr_memory(time)
    //val rslr_old: Float = cls.data.rslr_memory(time - timestep)
    val rslr_new: Float = wlCorrectedSLR(time)(cls.locationid)
    val rslr_old: Float = wlCorrectedSLR(time - timestep)(cls.locationid)
    cls.coastalPlane.areaExposure.cumulativeExposure(rslr_new).asInstanceOf[Float] -
      cls.coastalPlane.areaExposure.cumulativeExposure(rslr_old).asInstanceOf[Float] * cls.data.wetland_fraction
  }

  private def constrainUnconstrainedWetlandLoss(pUnconstrainedWetlandLoss: Float, cls: Cls, time: Int, timestep: Int): Float = {

    //val slr_rate: Float = cls.data.slr_current_annual_rate
    val slr_rate: Float = wlCorrectedSLRrates(time)(cls.locationid)

    var rslr_tidal_forcing: Float =
      if ((cls.data.wetfresh + cls.data.wetsalt) > 0f) {
        regression_slope_marsh * (slr_rate * 1000 + regression_intersect_marsh) / scala.math.pow(cls.data.mtidalrng, regression_exponenent_marsh).asInstanceOf[Float]
      } else {
        regression_slope_mangrove * (slr_rate * 1000 + regression_intersect_mangrove) / scala.math.pow(cls.data.mtidalrng, regression_exponenent_mangrove).asInstanceOf[Float]
      }

    if ((rslr_tidal_forcing < 0) || (slr_rate < 0)) {
      rslr_tidal_forcing = 0
    }

    val sediment_surplus: Float = cls.data.tsm - rslr_tidal_forcing
    var sediment_surplus_normalised: Float = sediment_surplus * 5 / 40

    if (sediment_surplus_normalised > 5) sediment_surplus_normalised = 5.0f
    if (sediment_surplus_normalised < -5) sediment_surplus_normalised = -5.0f

    var vertical_acreation_score: Float = scala.math.round(sediment_surplus_normalised)

    var constrainedWetlandLoss: Float =
      if (vertical_acreation_score < 0) {
        //-0.2f * vertical_acreation_score * pUnconstrainedWetlandLoss
        pUnconstrainedWetlandLoss * (-0.2f * vertical_acreation_score)
      } else {
        0f
      }
    constrainedWetlandLoss
  }

  private def initialSLRCorrection(state: divaState) {
    var correctedSlr: scala.collection.mutable.AnyRefMap[String, Float] = scala.collection.mutable.AnyRefMap[String, Float]()
    var clss: List[Cls] = state.getFeatures[Cls]
    clss.foreach {
      cls =>
        {
          if (cls.data.rslr < 0f) {
            wlSLRCorrectetion += (cls.locationid, 0)
          } else {
            wlSLRCorrectetion += (cls.locationid, cls.data.rslr)
          }
          correctedSlr += (cls.locationid, 0)
        }
    }
    wlCorrectedSLR += ((state.time, correctedSlr))
  }

  private def SLRCorrection(state: divaState, slrReferenceYear: Int) {
    var correctedSlr: scala.collection.mutable.AnyRefMap[String, Float] = scala.collection.mutable.AnyRefMap[String, Float]()
    var clss: List[Cls] = state.getFeatures[Cls]
    clss.foreach {
      cls => correctedSlr += (cls.locationid, cls.data.rslr - wlSLRCorrectetion(cls.locationid))
    }
    wlCorrectedSLR += ((state.time, correctedSlr))
    clss.foreach {
      cls =>
        {
          var slrUntilNow: scala.collection.mutable.ArrayBuffer[Float] = scala.collection.mutable.ArrayBuffer()
          for (i <- slrReferenceYear to state.time by state.timeStep) {
            slrUntilNow += wlCorrectedSLR(i)(cls.locationid)
          }
          wlCorrectedSLR(state.time)(cls.locationid) = slrUntilNow.max
        }
    }
  }

  private def SLRRateCorrection(state: divaState) {
    var correctedSlrRate: scala.collection.mutable.AnyRefMap[String, Float] = scala.collection.mutable.AnyRefMap[String, Float]()
    var correctedSlrT: scala.collection.mutable.AnyRefMap[String, Float] = wlCorrectedSLR(state.time)
    var correctedSlrTMinus1: scala.collection.mutable.AnyRefMap[String, Float] = wlCorrectedSLR(state.time - state.timeStep)
    var clss: List[Cls] = state.getFeatures[Cls]
    clss.foreach {
      cls =>
        {
          correctedSlrRate += (cls.locationid, (correctedSlrT(cls.locationid) - correctedSlrTMinus1(cls.locationid)) / state.timeStep)
        }
    }
    wlCorrectedSLRrates += ((state.time, correctedSlrRate))
  }

  private def accumulate(state: divaState) {
    var admins: List[Admin] = state.getFeatures[Admin]
    admins.par.foreach {
      (admin: Admin) =>
        {
          admin.data.wetland_area_freshmarshes = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetfresh))
          admin.data.wetland_area_saltmarshes = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetsalt))
          admin.data.wetland_area_mangroves = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetmang))
          admin.data.wetland_area_total = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetland_area_total))
        }
    }

    var deltas: List[Delta] = state.getFeatures[Delta]
    deltas.par.foreach {
      (delta: Delta) =>
        {
          delta.data.wetland_area_freshmarshes = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetfresh))
          delta.data.wetland_area_saltmarshes = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetsalt))
          delta.data.wetland_area_mangroves = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetmang))
          delta.data.wetland_area_total = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetland_area_total))
        }
    }

    var countries: List[Country] = state.getFeatures[Country]
    countries.par.foreach {
      (country: Country) =>
        {
          country.data.wetland_area_freshmarshes = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.wetland_area_freshmarshes))
          country.data.wetland_area_saltmarshes = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.wetland_area_saltmarshes))
          country.data.wetland_area_mangroves = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.wetland_area_mangroves))
          country.data.wetland_area_total = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.wetland_area_total))
        }
    }

    var regions: List[Region] = state.getFeatures[Region]
    regions.par.foreach {
      (region: Region) =>
        {
          region.data.wetland_area_freshmarshes = region.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetfresh))
          region.data.wetland_area_saltmarshes = region.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetsalt))
          region.data.wetland_area_mangroves = region.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetmang))
          region.data.wetland_area_total = region.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.wetland_area_total))
        }
    }
    var globals: List[Global] = state.getFeatures[Global]
    globals.foreach {
      (global: Global) =>
        {
          global.data.wetland_area_freshmarshes = global.countries.foldLeft(0f)((sum, admin) => sum + (admin.data.wetland_area_freshmarshes))
          global.data.wetland_area_saltmarshes = global.countries.foldLeft(0f)((sum, admin) => sum + (admin.data.wetland_area_saltmarshes))
          global.data.wetland_area_mangroves = global.countries.foldLeft(0f)((sum, admin) => sum + (admin.data.wetland_area_mangroves))
          global.data.wetland_area_total = global.countries.foldLeft(0f)((sum, admin) => sum + (admin.data.wetland_area_total))
        }
    }
  }

}