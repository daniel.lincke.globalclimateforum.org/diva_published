package diva.feature.data.io

import diva.io._
import diva.types.InterpolationMode.InterpolationMode

trait FeatureDataIO[T] {
  
  def writeData(out: divaOutput, d:T)
  def writeSelectedData(out: divaOutput, d: T, ignoreThisOutput: Array[Boolean])
  def readInputValue(in: divaInput, d:T, parName: String, locid: String, time: Int)
  def readOptionInputValue(in: divaInput, d:T, parName: String, locid: String, time: Int)
  def readScenarioValue(in: divaInput, d:T, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode)
  def readOptionScenarioValue(in: divaInput, d:T, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode)

}