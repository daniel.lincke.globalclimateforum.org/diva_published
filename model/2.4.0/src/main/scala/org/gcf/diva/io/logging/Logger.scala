package diva.io.logging

import diva.io.logging.LogLevel._
import java.io.PrintStream

class Logger(private val pMyLevel: LogLevel = INFO, private var out: PrintStream = System.out, val startTime: Long = System.currentTimeMillis)
  extends LoggerI {

  def diffTime(): Long = System.currentTimeMillis - startTime

  def log(l: LogLevel, message: String) {
    if (l >= pMyLevel) {
      l match {
        case FATAL => fatalLog(FATAL, message)
        case ERROR => errorLog(ERROR, message)
        case WARNING => warnLog(WARNING, message)
        case d => out.println(diffTime + "ms\t" + d + " (" + id + "):\t" + message)
      }
    }
  }

  def fatalLog(l: LogLevel, message: String) = {
    // a FATAL error beeps.
    print("\u0007")
    errorLog(l, message)
  }

  def errorLog(l: LogLevel, message: String) = {
    // if a ERROR occurs we log it, we print it also on the screen, and we quit
    // but we avoid double screen-output
    if (out != System.out) {
      out.println(diffTime + "ms\t" + l + " (" + id + "):\t" + message)
    }
    println(l.toString() + ":\t" + message)
    System.exit(0)
  }

  def warnLog(l: LogLevel, message: String) = {
    if (out != System.out) {
      out.println(diffTime + "ms\t" + l + " (" + id + "):\t" + message)
    }
    //leads to mistake:
    //println(l + ":\t" + message)
    println(l.toString() + ":\t" + message)
  }

  def copy(): Logger = {
    val l = new Logger(pMyLevel, out, startTime)
    l.id = id
    l
  }

}