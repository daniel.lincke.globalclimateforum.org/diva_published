/* Automatically generated data features for Cz
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data

import diva.annotations._
import diva.types._
import diva.types.TypeComputations._
import scala.collection.mutable._


class CzData {

  @Output
  @Input
  @Parameter
  @Description(value = "Administrative unit identifier code, associating each coastline segment with the corresponding administrative unit information.")
  var adminid :  String = default[String]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 0.5m ")
  var area00_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 1.0m (but above 0.5m if area00_5 is given) ")
  var area01_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 1.5m (but above 1.0m) ")
  var area01_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 2.0m (but above 1.5m resp. 1.0m) ")
  var area02_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 2.5m (but above 2.0m) ")
  var area02_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 3.0m (but above 2.5m resp. 2.0m) ")
  var area03_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 3.5m (but above 3.0m) ")
  var area03_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 4.0m (but above 3.5m resp. 3.0m) ")
  var area04_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 4.5m (but above 4.0m) ")
  var area04_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 5.0m (but above 4.5m resp. 4.0m) ")
  var area05_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 5.5m (but above 5.0m) ")
  var area05_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 6.0m (but above 5.5m resp. 5.0m) ")
  var area06_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 6.5m (but above 6.0m) ")
  var area06_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 7.0m (but above 6.5m resp. 6.0m) ")
  var area07_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 7.5m (but above 7.0m) ")
  var area07_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 8.0m (but above 8.5m resp. 8.0m) ")
  var area08_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 8.5m (but above 8.0m) ")
  var area08_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 9.0m (but above 8.5m resp. 8.0m) ")
  var area09_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 9.5m (but above 9.0m) ")
  var area09_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 10.0m (but above 9.5m resp. 9.0m) ")
  var area10_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 10.5m (but above 10.0m) ")
  var area10_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 11.0m (but above 10.5m resp. 10.0m) ")
  var area11_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 11.5m (but above 11.0m) ")
  var area11_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 12.0m (but above 11.5m resp. 11.0m) ")
  var area12_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 12.5m (but above 12.0m) ")
  var area12_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 13.0m (but above 12.5m resp. 12.0m) ")
  var area13_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 13.5m (but above 13.0m) ")
  var area13_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 14.0m (but above 13.5m resp. 13.0m) ")
  var area14_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 14.5m (but above 14.0m) ")
  var area14_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 15.0m (but above 14.5m resp. 14.0m) ")
  var area15_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 15.5m (but above 15.0m) ")
  var area15_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 16.0m (but above 15.5m resp. 15.0m) ")
  var area16_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 16.5m (but above 16.0m) ")
  var area16_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 17.0m (but above 16.5m resp. 16.0m) ")
  var area17_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 17.5m (but above 17.0m) ")
  var area17_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 18.0m (but above 17.5m resp. 17.0m) ")
  var area18_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 18.5m (but above 18.0m) ")
  var area18_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 19.0m (but above 18.5m resp. 18.0m) ")
  var area19_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 19.5m (but above 19.0m) ")
  var area19_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 20.0m (but above 19.5m resp. 19.0m) ")
  var area20_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total area (coming from digital elevation model) below 20.5m (but above 20.0m resp. 19.5m) ")
  var area20_5 :  Option[Float] = default[Option[Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Land area below ? m (ignoring see dikes)")
  var area_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Land area below the one in ? flood level (ignoring see dikes)")
  var area_below_h :  Map[Int ,Float] = default[Map[Int ,Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 0.5m elevation from the elevation dataset")
  var assets00_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 1.0m elevation from the elevation dataset")
  var assets01_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 1.5m elevation from the elevation dataset")
  var assets01_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 2.0m elevation from the elevation dataset")
  var assets02_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 2.5m elevation from the elevation dataset")
  var assets02_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 3.0m elevation from the elevation dataset")
  var assets03_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 3.5m elevation from the elevation dataset")
  var assets03_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 4.0m elevation from the elevation dataset")
  var assets04_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 4.5m elevation from the elevation dataset")
  var assets04_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 5.0m elevation from the elevation dataset")
  var assets05_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 5.5m elevation from the elevation dataset")
  var assets05_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 6.0m elevation from the elevation dataset")
  var assets06_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 6.5m elevation from the elevation dataset")
  var assets06_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 7.0m elevation from the elevation dataset")
  var assets07_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 7.5m elevation from the elevation dataset")
  var assets07_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 8.0m elevation from the elevation dataset")
  var assets08_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 8.5m elevation from the elevation dataset")
  var assets08_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 9.0m  elevation from the elevation dataset")
  var assets09_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 9.5m elevation from the elevation dataset")
  var assets09_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 10.0m elevation from the elevation dataset")
  var assets10_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 10.5m elevation from the elevation dataset")
  var assets10_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 11.0m elevation from the elevation dataset")
  var assets11_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 11.5m elevation from the elevation dataset")
  var assets11_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 12.0m elevation from the elevation dataset")
  var assets12_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 12.5m elevation from the elevation dataset")
  var assets12_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 13.0m elevation from the elevation dataset")
  var assets13_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 12.5m elevation from the elevation dataset")
  var assets13_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 13.0m elevation from the elevation dataset")
  var assets14_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 13.5m elevation from the elevation dataset")
  var assets14_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 14.0m elevation from the elevation dataset")
  var assets15_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 15.5m elevation from the elevation dataset")
  var assets15_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 16.0m elevation from the elevation dataset")
  var assets16_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 16.5m elevation from the elevation dataset")
  var assets16_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 17.0m elevation from the elevation dataset")
  var assets17_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 17.5m elevation from the elevation dataset")
  var assets17_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 18.0m elevation from the elevation dataset")
  var assets18_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 18.5m elevation from the elevation dataset")
  var assets18_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 19.0m elevation from the elevation dataset")
  var assets19_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 19.5m elevation from the elevation dataset")
  var assets19_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 20.0m elevation from the elevation dataset")
  var assets20_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total assets situated below or at 20.5m elevation from the elevation dataset")
  var assets20_5 :  Option[Float] = default[Option[Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets below ? m (ignoring see dikes)")
  var assets_below_ :  Map[Float,Float] = default[Map[Float,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Assets below the one in ? flood level (ignoring see dikes)")
  var assets_below_h :  Map[Int ,Float] = default[Map[Int ,Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets in developable areas below ? m (ignoring see dikes)")
  var assets_developable_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets in developed areas below ? m (ignoring see dikes)")
  var assets_developed_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Input
  @Parameter
  @Description(value = "Coast line segment to which the costal zone belongs")
  var clsid :  String = default[String]

  @Input
  @Parameter
  @Description(value = "Flag that indicates if the zone is a coastal zone or an inland zone")
  var coastalzone :  Boolean = true

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 0.5m ")
  var dluc00_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 1.0m (but above 0.5m if dluc00_5 is given) ")
  var dluc01_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 1.5m (but above 1.0m) ")
  var dluc01_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 2.0m (but above 1.5m resp. 1.0m) ")
  var dluc02_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 2.5m (but above 2.0m) ")
  var dluc02_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 3.0m (but above 2.5m resp. 2.0m) ")
  var dluc03_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 3.5m (but above 3.0m) ")
  var dluc03_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 4.0m (but above 3.5m resp. 3.0m) ")
  var dluc04_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 4.5m (but above 4.0m) ")
  var dluc04_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 5.0m (but above 4.5m resp. 4.0m) ")
  var dluc05_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 5.5m (but above 5.0m) ")
  var dluc05_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 6.0m (but above 5.5m resp. 5.0m) ")
  var dluc06_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 6.5m (but above 6.0m) ")
  var dluc06_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 7.0m (but above 6.5m resp. 6.0m) ")
  var dluc07_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 7.5m (but above 7.0m) ")
  var dluc07_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 8.0m (but above 8.5m resp. 8.0m) ")
  var dluc08_0  :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 8.5m (but above 8.0m) ")
  var dluc08_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 9.0m (but above 8.5m resp. 8.0m) ")
  var dluc09_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 9.5m (but above 9.0m) ")
  var dluc09_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 10.0m (but above 9.5m resp. 9.0m) ")
  var dluc10_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 10.5m (but above 10.0m) ")
  var dluc10_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 11.0m (but above 10.5m resp. 10.0m) ")
  var dluc11_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 11.5m (but above 11.0m) ")
  var dluc11_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 12.0m (but above 11.5m resp. 11.0m) ")
  var dluc12_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 12.5m (but above 12.0m) ")
  var dluc12_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 13.0m (but above 12.5m resp. 12.0m) ")
  var dluc13_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 13.5m (but above 13.0m) ")
  var dluc13_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 14.0m (but above 13.5m resp. 13.0m) ")
  var dluc14_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 14.5m (but above 14.0m) ")
  var dluc14_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 15.0m (but above 14.5m resp. 14.0m) ")
  var dluc15_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 15.5m (but above 15.0m) ")
  var dluc15_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 16.0m (but above 15.5m resp. 15.0m) ")
  var dluc16_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 16.5m (but above 16.0m) ")
  var dluc16_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 17.0m (but above 16.5m resp. 16.0m) ")
  var dluc17_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 17.5m (but above 17.0m) ")
  var dluc17_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 18.0m (but above 17.5m resp. 17.0m) ")
  var dluc18_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 18.5m (but above 18.0m) ")
  var dluc18_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 19.0m (but above 18.5m resp. 18.0m) ")
  var dluc19_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 19.5m (but above 19.0m) ")
  var dluc19_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 20.0m (but above 19.5m resp. 19.0m) ")
  var dluc20_0 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Dominant Landuse class (from landuse model) below 20.5m (but above 20.0m resp. 19.5m) ")
  var dluc20_5 :  Option[Int] = default[Option[Int]]

  @Input
  @OptionalParameter
  @Description(value = "Fraction of undeveloped but buildable area in the coastal zone")
  var fraction_developable :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Fraction of developed area in the coastal zone")
  var fraction_developed :  Option[Float] = default[Option[Float]]

  @Output
  @Variable
  @Description(value = "Average annual income")
  var gdpc :  Float = default[Float]

  @Variable
  @Description(value = "Annual growth rate of GDP per capita, memory for scenario")
  var gdpcgrowth :  Float = 0f
  var gdpcgrowth_memory: Map[Int ,Float] = Map()

  @Variable
  @Description(value = "Total growth rate of GDP per capita since model start, memory for scenario and optimization")
  var gdpcgrowth_total :  Float = 0f
  var gdpcgrowth_total_memory: Map[Int ,Float] = Map()

  @Output
  @Variable
  @Description(value = "Land area that is not protected but below the one in one flood level at initialisation")
  var landloss_initial_land_below_S1 :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Change in land area below the one in one flood level, taking Into account see dikes")
  var landloss_submergence :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Change in land area below the one in one flood level, taking Into account see dikes")
  var landlosscost_submergence :  Float = default[Float]

  @Output
  @Input
  @OptionalParameter
  @Description(value = "Land use from the Image data - 19 classes.")
  var landuse :  Option[Int] = default[Option[Int]]

  @Variable
  @Description(value = "")
  var landvalue_unit :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Local GDP below 20.5m")
  var localgdp :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to land loss (erosion) (per year)")
  var migration_erosion :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to retreat (per year)")
  var migration_retreat :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to land loss (flooding/submergence) (per year)")
  var migration_submergence :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration costs")
  var migrationcost_retreat :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration costs")
  var migrationcost_submergence :  Float = default[Float]

  @Variable
  @Description(value = "Elevation below which nobody lives")
  var nopopbelow :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "People at risk of flooding: average number of people flooded per year by storm surge allowing for the effect of flood defences (segment level).")
  var par :  Float = default[Float]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 0.5m elevation from the overlay of population and elevation dataset")
  var pop00_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 1m elevation from the overlay of population and elevation dataset")
  var pop01_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 1.5m elevation from the  overlay of population and elevation dataset")
  var pop01_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 2m elevation from the overlay of population and elevation dataset")
  var pop02_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 2.5m elevation from the  overlay of population and elevation dataset")
  var pop02_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 3m elevation from the overlay of population and elevation dataset")
  var pop03_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 3.5m elevation from the  overlay of population and elevation dataset")
  var pop03_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 4m elevation from the overlay of population and elevation dataset")
  var pop04_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 4.5m elevation from the  overlay of population and elevation dataset")
  var pop04_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 5m elevation from the overlay of population and elevation dataset")
  var pop05_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 5.5m elevation from the  overlay of population and elevation dataset")
  var pop05_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 6m elevation from the overlay of population and elevation dataset")
  var pop06_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 6.5m elevation from the  overlay of population and elevation dataset")
  var pop06_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 7m elevation from the overlay of population and elevation dataset")
  var pop07_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 7.5m elevation from the  overlay of population and elevation dataset")
  var pop07_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 6-8m elevation from the overlay of population and elevation dataset")
  var pop08_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 8.5m elevation from the  overlay of population and elevation dataset")
  var pop08_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 9m elevation from the overlay of population and elevation dataset")
  var pop09_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 9.5m elevation from the  overlay of population and elevation dataset")
  var pop09_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 10m elevation from the overlay of population and elevation dataset")
  var pop10_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 10.5m elevation from the  overlay of population and elevation dataset")
  var pop10_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 11m elevation from the overlay of population and elevation dataset")
  var pop11_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 11.5m elevation from the  overlay of population and elevation dataset")
  var pop11_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 12m elevation from the overlay of population and elevation dataset")
  var pop12_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 12.5m elevation from the  overlay of population and elevation dataset")
  var pop12_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 13m elevation from the overlay of population and elevation dataset")
  var pop13_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 13.5m elevation from the  overlay of population and elevation dataset")
  var pop13_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 14m elevation from the overlay of population and elevation dataset")
  var pop14_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 14.5m elevation from the  overlay of population and elevation dataset")
  var pop14_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 15m elevation from the overlay of population and elevation dataset")
  var pop15_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 15.5m elevation from the  overlay of population and elevation dataset")
  var pop15_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 16m elevation from the overlay of population and elevation dataset")
  var pop16_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 16.5m elevation from the  overlay of population and elevation dataset")
  var pop16_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 17m elevation from the overlay of population and elevation dataset")
  var pop17_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 17.5m elevation from the  overlay of population and elevation dataset")
  var pop17_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 18m elevation from the overlay of population and elevation dataset")
  var pop18_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 18.5m elevation from the overlay of population and elevation dataset")
  var pop18_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 19m elevation from the overlay of population and elevation dataset")
  var pop19_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 19.5m elevation from the overlay of population and elevation dataset")
  var pop19_5 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 20m elevation from the overlay of population and elevation dataset")
  var pop20_0 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Total population situated below or at 20.5m elevation from the overlay of population and elevation dataset")
  var pop20_5 :  Option[Float] = default[Option[Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Exposure (people) that are located below ? m")
  var pop_below_ :  Map[Float,Float] = default[Map[Float,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Exposure (people) that are located below the h? flood")
  var pop_below_h :  Map[Int,Float] = default[Map[Int,Float]]

  @OptionalDriver
  @Interpolate(mode = "RIGHT")
  @Description(value = "")
  var popgrowth :  Option[Float] = default[Option[Float]]
  var popgrowth_memory: Map[Int ,Option[Float]] = Map()

  @Variable
  @Description(value = "")
  var popgrowth_average :  Float = default[Float]
  var popgrowth_average_memory: Map[Int ,Float] = Map()

  @Variable
  @Description(value = "Total growth rate of population since model start, memory for scenario and optimization")
  var popgrowth_total :  Float = 0f
  var popgrowth_total_memory: Map[Int ,Float] = Map()

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Forced migration due to the initial population correction")
  var population_correction :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Expected costs of sea floods")
  var seafloodcost :  Float = 0f

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Damage (assets) caused by the h? flood")
  var seafloodcost_h :  Map[Int,Float] = default[Map[Int,Float]]

  @Output
  @Variable
  @Description(value = "Sea flood cost by the minimal flood (the first one wich overtops the dike)")
  var seafloodcost_min :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "total expected costs of sea floods as fractionof local gdp")
  var seafloodcost_relative :  Float = 0f

  @Variable
  @Description(value = "Target sea flood costs (memory to keep seafloodcost constant over time)")
  var seafloodcost_target :  Float = 0f

  @Output
  @Variable
  @Description(value = "Current setback zone area.")
  var setbackzone_area :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Capital deprecation in the setback zone.")
  var setbackzone_assets :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Capital deprecation in the setback zone.")
  var setbackzone_deprecation :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Population living currently in the setback zone.")
  var setbackzone_population :  Float = default[Float]

  override def clone : CzData = {
    var ret: CzData  = new CzData
    ret.adminid = adminid
    ret.area00_5 = area00_5
    ret.area01_0 = area01_0
    ret.area01_5 = area01_5
    ret.area02_0 = area02_0
    ret.area02_5 = area02_5
    ret.area03_0 = area03_0
    ret.area03_5 = area03_5
    ret.area04_0 = area04_0
    ret.area04_5 = area04_5
    ret.area05_0 = area05_0
    ret.area05_5 = area05_5
    ret.area06_0 = area06_0
    ret.area06_5 = area06_5
    ret.area07_0 = area07_0
    ret.area07_5 = area07_5
    ret.area08_0 = area08_0
    ret.area08_5 = area08_5
    ret.area09_0 = area09_0
    ret.area09_5 = area09_5
    ret.area10_0 = area10_0
    ret.area10_5 = area10_5
    ret.area11_0 = area11_0
    ret.area11_5 = area11_5
    ret.area12_0 = area12_0
    ret.area12_5 = area12_5
    ret.area13_0 = area13_0
    ret.area13_5 = area13_5
    ret.area14_0 = area14_0
    ret.area14_5 = area14_5
    ret.area15_0 = area15_0
    ret.area15_5 = area15_5
    ret.area16_0 = area16_0
    ret.area16_5 = area16_5
    ret.area17_0 = area17_0
    ret.area17_5 = area17_5
    ret.area18_0 = area18_0
    ret.area18_5 = area18_5
    ret.area19_0 = area19_0
    ret.area19_5 = area19_5
    ret.area20_0 = area20_0
    ret.area20_5 = area20_5
    ret.area_below_ = area_below_
    ret.area_below_h = area_below_h
    ret.assets00_5 = assets00_5
    ret.assets01_0 = assets01_0
    ret.assets01_5 = assets01_5
    ret.assets02_0 = assets02_0
    ret.assets02_5 = assets02_5
    ret.assets03_0 = assets03_0
    ret.assets03_5 = assets03_5
    ret.assets04_0 = assets04_0
    ret.assets04_5 = assets04_5
    ret.assets05_0 = assets05_0
    ret.assets05_5 = assets05_5
    ret.assets06_0 = assets06_0
    ret.assets06_5 = assets06_5
    ret.assets07_0 = assets07_0
    ret.assets07_5 = assets07_5
    ret.assets08_0 = assets08_0
    ret.assets08_5 = assets08_5
    ret.assets09_0 = assets09_0
    ret.assets09_5 = assets09_5
    ret.assets10_0 = assets10_0
    ret.assets10_5 = assets10_5
    ret.assets11_0 = assets11_0
    ret.assets11_5 = assets11_5
    ret.assets12_0 = assets12_0
    ret.assets12_5 = assets12_5
    ret.assets13_0 = assets13_0
    ret.assets13_5 = assets13_5
    ret.assets14_0 = assets14_0
    ret.assets14_5 = assets14_5
    ret.assets15_0 = assets15_0
    ret.assets15_5 = assets15_5
    ret.assets16_0 = assets16_0
    ret.assets16_5 = assets16_5
    ret.assets17_0 = assets17_0
    ret.assets17_5 = assets17_5
    ret.assets18_0 = assets18_0
    ret.assets18_5 = assets18_5
    ret.assets19_0 = assets19_0
    ret.assets19_5 = assets19_5
    ret.assets20_0 = assets20_0
    ret.assets20_5 = assets20_5
    ret.assets_below_ = assets_below_
    ret.assets_below_h = assets_below_h
    ret.assets_developable_below_ = assets_developable_below_
    ret.assets_developed_below_ = assets_developed_below_
    ret.clsid = clsid
    ret.coastalzone = coastalzone
    ret.dluc00_5 = dluc00_5
    ret.dluc01_0 = dluc01_0
    ret.dluc01_5 = dluc01_5
    ret.dluc02_0 = dluc02_0
    ret.dluc02_5 = dluc02_5
    ret.dluc03_0 = dluc03_0
    ret.dluc03_5 = dluc03_5
    ret.dluc04_0 = dluc04_0
    ret.dluc04_5 = dluc04_5
    ret.dluc05_0 = dluc05_0
    ret.dluc05_5 = dluc05_5
    ret.dluc06_0 = dluc06_0
    ret.dluc06_5 = dluc06_5
    ret.dluc07_0 = dluc07_0
    ret.dluc07_5 = dluc07_5
    ret.dluc08_0  = dluc08_0 
    ret.dluc08_5 = dluc08_5
    ret.dluc09_0 = dluc09_0
    ret.dluc09_5 = dluc09_5
    ret.dluc10_0 = dluc10_0
    ret.dluc10_5 = dluc10_5
    ret.dluc11_0 = dluc11_0
    ret.dluc11_5 = dluc11_5
    ret.dluc12_0 = dluc12_0
    ret.dluc12_5 = dluc12_5
    ret.dluc13_0 = dluc13_0
    ret.dluc13_5 = dluc13_5
    ret.dluc14_0 = dluc14_0
    ret.dluc14_5 = dluc14_5
    ret.dluc15_0 = dluc15_0
    ret.dluc15_5 = dluc15_5
    ret.dluc16_0 = dluc16_0
    ret.dluc16_5 = dluc16_5
    ret.dluc17_0 = dluc17_0
    ret.dluc17_5 = dluc17_5
    ret.dluc18_0 = dluc18_0
    ret.dluc18_5 = dluc18_5
    ret.dluc19_0 = dluc19_0
    ret.dluc19_5 = dluc19_5
    ret.dluc20_0 = dluc20_0
    ret.dluc20_5 = dluc20_5
    ret.fraction_developable = fraction_developable
    ret.fraction_developed = fraction_developed
    ret.gdpc = gdpc
    ret.gdpcgrowth = gdpcgrowth
    ret.gdpcgrowth_memory = gdpcgrowth_memory
    ret.gdpcgrowth_total = gdpcgrowth_total
    ret.gdpcgrowth_total_memory = gdpcgrowth_total_memory
    ret.landloss_initial_land_below_S1 = landloss_initial_land_below_S1
    ret.landloss_submergence = landloss_submergence
    ret.landlosscost_submergence = landlosscost_submergence
    ret.landuse = landuse
    ret.landvalue_unit = landvalue_unit
    ret.localgdp = localgdp
    ret.migration_erosion = migration_erosion
    ret.migration_retreat = migration_retreat
    ret.migration_submergence = migration_submergence
    ret.migrationcost_retreat = migrationcost_retreat
    ret.migrationcost_submergence = migrationcost_submergence
    ret.nopopbelow = nopopbelow
    ret.par = par
    ret.pop00_5 = pop00_5
    ret.pop01_0 = pop01_0
    ret.pop01_5 = pop01_5
    ret.pop02_0 = pop02_0
    ret.pop02_5 = pop02_5
    ret.pop03_0 = pop03_0
    ret.pop03_5 = pop03_5
    ret.pop04_0 = pop04_0
    ret.pop04_5 = pop04_5
    ret.pop05_0 = pop05_0
    ret.pop05_5 = pop05_5
    ret.pop06_0 = pop06_0
    ret.pop06_5 = pop06_5
    ret.pop07_0 = pop07_0
    ret.pop07_5 = pop07_5
    ret.pop08_0 = pop08_0
    ret.pop08_5 = pop08_5
    ret.pop09_0 = pop09_0
    ret.pop09_5 = pop09_5
    ret.pop10_0 = pop10_0
    ret.pop10_5 = pop10_5
    ret.pop11_0 = pop11_0
    ret.pop11_5 = pop11_5
    ret.pop12_0 = pop12_0
    ret.pop12_5 = pop12_5
    ret.pop13_0 = pop13_0
    ret.pop13_5 = pop13_5
    ret.pop14_0 = pop14_0
    ret.pop14_5 = pop14_5
    ret.pop15_0 = pop15_0
    ret.pop15_5 = pop15_5
    ret.pop16_0 = pop16_0
    ret.pop16_5 = pop16_5
    ret.pop17_0 = pop17_0
    ret.pop17_5 = pop17_5
    ret.pop18_0 = pop18_0
    ret.pop18_5 = pop18_5
    ret.pop19_0 = pop19_0
    ret.pop19_5 = pop19_5
    ret.pop20_0 = pop20_0
    ret.pop20_5 = pop20_5
    ret.pop_below_ = pop_below_
    ret.pop_below_h = pop_below_h
    ret.popgrowth = popgrowth
    ret.popgrowth_memory = popgrowth_memory
    ret.popgrowth_average = popgrowth_average
    ret.popgrowth_average_memory = popgrowth_average_memory
    ret.popgrowth_total = popgrowth_total
    ret.popgrowth_total_memory = popgrowth_total_memory
    ret.population_correction = population_correction
    ret.seafloodcost = seafloodcost
    ret.seafloodcost_h = seafloodcost_h
    ret.seafloodcost_min = seafloodcost_min
    ret.seafloodcost_relative = seafloodcost_relative
    ret.seafloodcost_target = seafloodcost_target
    ret.setbackzone_area = setbackzone_area
    ret.setbackzone_assets = setbackzone_assets
    ret.setbackzone_deprecation = setbackzone_deprecation
    ret.setbackzone_population = setbackzone_population
    ret
  }

}
