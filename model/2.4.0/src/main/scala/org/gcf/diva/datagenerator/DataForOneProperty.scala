package diva.datagenerator

class DataForOneProperty {
  var propName: String = ""
  var propType: String = ""
  var divaType: String = ""
  var rawType: String = ""
  var dataRawType: String = ""
  var Unit: String = ""
  var Max: String = ""
  var Min: String = ""
  var Default: String = ""
  var Description: String = "" 
  var Interpolation: String = "" 
  var Configuration: String = "" 
  var Output: Boolean = false
  var Input: Boolean = false
  var OptionalInput: Boolean = false
  var OptionType: Boolean = false
  var resetEveryPeriod: Boolean = false
  var Accumulate: Boolean = false
  var Memory: Boolean = false
}