package diva.main

object divaMain {

  def main(args: Array[String]) {

    val usage = """
    Usage: divaMain command options 

      command should be in
	- diva-single-run:	do a diva single run
	- diva-multi-run:	do multiple diva runs
	- generate: generate diva features
	- csvappend: use the diva csvappend tool
	- Grid2Shape:		use the Grid2Shape tool
  """

    if (args.length < 1) {
      println("not enough arguments");
      println(usage)
      System.exit(0)
    }

    val command: String = args(0)
    val remainingArgs: Array[String] = args.drop(1)
    command match {
      case "diva-single-run" => diva.model.divaModel.run(remainingArgs)
      case "diva-multi-run"  => println("diva-multi-run")
      case "generate"        => diva.datagenerator.DataGenerator.run(remainingArgs)
      case "Grid2Shape"      => tools.Grid2Shape.Grid2Shape.run(remainingArgs)
      case "csvappend"       => tools.CSVappend.CSVappend.run(remainingArgs)
      case _                 => println("unknown command: " + command); println(usage); System.exit(0)
    }
  }

}