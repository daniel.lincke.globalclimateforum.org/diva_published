package diva.io.logging

import diva.io.logging.LogLevel._

trait LoggerI {
	
	var id: String = ""
  
	def log(l: LogLevel, message: String)
  
	def debug(message: String) = log(DEBUG, message)
	def info(message: String)  = log(INFO,  message)	
	def warn(message: String)  = log(WARNING,  message)	
	def error(message: String) = log(ERROR, message)	
	def fatal(message: String) = log(FATAL, message)	
	
  def copy: LoggerI 
}