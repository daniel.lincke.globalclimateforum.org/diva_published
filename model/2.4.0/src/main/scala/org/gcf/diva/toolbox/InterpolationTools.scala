package diva.toolbox

object InterpolationTools {

  trait Interpolatable[T] {
    def interpolateLinear(x: T, y: T, t1: Int, t2: Int, t: Int): T
    //def interpolateGrowthRate(x: T, y: T, t1: Int, t2: Int, t: Int): T
    def interpolateLeft(x: T, y: T, t1: Int, t2: Int, t: Int): T = x
    def interpolateRight(x: T, y: T, t1: Int, t2: Int, t: Int): T = y
  }

  implicit object IntIsInterpolatable extends Interpolatable[Int] {
    def interpolateLinear(x: Int, y: Int, t1: Int, t2: Int, t: Int): Int =
      x + scala.math.floor((y - x).asInstanceOf[Float] * ((t.asInstanceOf[Float] - t1.asInstanceOf[Float]) / (t2.asInstanceOf[Float] - t1.asInstanceOf[Float]))).asInstanceOf[Int]
    //def interpolateGrowthRate(x: Int, y: Int, t1: Int, t2: Int, t: Int): Int = x
  }

  implicit object FloatIsInterpolatable extends Interpolatable[Float] {
    def interpolateLinear(x: Float, y: Float, t1: Int, t2: Int, t: Int): Float =
      x + (y - x) * ((t.asInstanceOf[Float] - t1.asInstanceOf[Float]) / (t2.asInstanceOf[Float] - t1.asInstanceOf[Float]))
    //def interpolateGrowthRate(x: Int, y: Int, t1: Int, t2: Int, t: Int): Int = x
  }

  implicit object DoubleIsInterpolatable extends Interpolatable[Double] {
    def interpolateLinear(x: Double, y: Double, t1: Int, t2: Int, t: Int): Double =
      x + (y - x) * ((t.asInstanceOf[Float] - t1.asInstanceOf[Float]) / (t2.asInstanceOf[Float] - t1.asInstanceOf[Float]))
    //def interpolateGrowthRate(x: Int, y: Int, t1: Int, t2: Int, t: Int): Int = x
  }

  def interpolatLinear[T: Interpolatable](x: T, y: T, t1: Int, t2: Int, t: Int): T =
    implicitly[Interpolatable[T]].interpolateLinear(x, y, t1, t2, t)

  def interpolatLeft[T: Interpolatable](x: T, y: T, t1: Int, t2: Int, t: Int): T =
    implicitly[Interpolatable[T]].interpolateLeft(x, y, t1, t2, t)
    
  def interpolatRight[T: Interpolatable](x: T, y: T, t1: Int, t2: Int, t: Int): T =
    implicitly[Interpolatable[T]].interpolateRight(x, y, t1, t2, t)
}