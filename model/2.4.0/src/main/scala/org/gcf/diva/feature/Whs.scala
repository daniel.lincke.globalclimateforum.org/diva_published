package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class Whs extends FeatureI {
  type Data = WhsData
  type DataIO = WhsDataIO

  val dataTypeTag: ru.TypeTag[WhsData] = ru.typeTag[WhsData]
  val dataClassTag: ClassTag[WhsData] = classTag[WhsData]
  val dataTypeSave: ru.Type = ru.typeOf[WhsData]

  var mustHaveInputFile = false

  var data: WhsData = null
  var dataIO: WhsDataIO = null

  def init() {
    data = new WhsData
    dataIO = new WhsDataIO
  }

  def newInstanceCopy: Whs = {
    val ret = new Whs
    ret.data = new WhsData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }
}