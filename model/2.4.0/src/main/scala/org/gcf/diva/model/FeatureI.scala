package diva.model

import diva.feature.data.io._
import diva.annotations._
import diva.io._
import diva.types.TypeComputations._
import diva.types.InterpolationMode.InterpolationMode
import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.api.JavaUniverse
import scala.collection.mutable.AnyRefMap

trait FeatureI extends hasLogger {

  // the abstract type Data has to be defined by every class that implements FeatureI.
  // for instance: Cls: type Data = ClsData; Country: type Data = CountryData ...
  // data is the actual data object
  type Data
  var data: Data

  type DataIO <: FeatureDataIO[Data]
  var dataIO: DataIO

  // These tags are necessary to overcome type erasure problems
  val dataTypeTag: ru.TypeTag[Data]
  val dataClassTag: scala.reflect.ClassTag[Data]
  val dataTypeSave: ru.Type

  def init
  def newInstanceCopy: FeatureI

  def deepCopy(f: FeatureI) {
    this.mustHaveInputFile = f.mustHaveInputFile
    this.inputFileNames = f.inputFileNames
    this.scenarioFileName = f.scenarioFileName
    this.outputFileName = f.outputFileName
    this.metainformation = f.metainformation
    this.inputs = f.inputs
    this.drivers = f.drivers
    this.outputs = f.outputs
    this.typeMap = f.typeMap
    this.interpolationModeMap = f.interpolationModeMap
    this.fieldSymbols = f.fieldSymbols
    this.allFields = f.allFields
    this.outputStream = f.outputStream
    this.scenario = f.scenario
    this.logger = f.logger
    this.actualDrivers = f.actualDrivers
    this.actualInputs = f.actualInputs
    this.variablesToReset = f.variablesToReset
    this.im = f.im
    this.elevationValuesForExposureOutput = f.elevationValuesForExposureOutput
    this.rpValuesForExposureOutput = f.rpValuesForExposureOutput
    this.ignoreOutputs = f.ignoreOutputs
    this.ignoreAnyOutputs = f.ignoreAnyOutputs
    this.ignoreThisOutput = f.ignoreThisOutput
  }

  // a field mirror for every field of the data object
  var fieldMap: collection.mutable.AnyRefMap[String, ru.FieldMirror] = new collection.mutable.AnyRefMap[String, ru.FieldMirror]
  var typeMap: collection.mutable.AnyRefMap[String, ru.Type] = new collection.mutable.AnyRefMap[String, ru.Type]
  var interpolationModeMap: collection.mutable.AnyRefMap[String, InterpolationMode] = new collection.mutable.AnyRefMap[String, InterpolationMode]
  var variablesToResetReflectionMap: collection.mutable.AnyRefMap[String, ru.FieldMirror] = new collection.mutable.AnyRefMap[String, ru.FieldMirror]

  var inputFileNames: Option[List[String]] = None
  var scenarioFileName: Option[List[String]] = None
  var outputFileName: Option[String] = None

  var mustHaveInputFile: Boolean

  var outputStream: Option[divaOutput] = None
  var scenario: Option[divaInput] = None

  var metainformation: Array[String] = Array("locationid", "locationname", "time")
  var inputs: Array[String] = null
  var drivers: Array[String] = null
  var actualDrivers: Array[String] = null
  var optionalDrivers: Array[String] = null

  var parameters: Array[String] = null
  var actualInputs: Array[String] = null
  var optionalParameters: Array[String] = null
  var variables: Array[String] = null

  var outputs: Array[String] = null
  var configurableOutputs: Array[String] = null

  //var ignoreOutputs: collection.mutable.AnyRefMap[String, Boolean] = new collection.mutable.AnyRefMap[String, Boolean]
  var ignoreOutputs: Array[String] = null
  var ignoreThisOutput: Array[Boolean] = null
  var ignoreAnyOutputs: Boolean = false

  var variablesToReset: Array[String] = null
  var allFields: Array[String] = _
  var fieldSymbols: List[ru.Symbol] = _

  def fileName: String = this.getClass().getCanonicalName()
  def name: String = this.getClass().getSimpleName.toLowerCase()

  def hasInputs: Boolean = inputs.length > 0
  def hasDrivers: Boolean = drivers.length > 0

  var elevationValuesForExposureOutput: List[Float] = List()
  var rpValuesForExposureOutput: List[Int] = List()

  // id and name for the features that are not initialised by an input file
  var locationid: String = name.toUpperCase + "_000"
  var locationname: String = name.toUpperCase + " feature generated automatically"

  val runtimeMirror = ru.runtimeMirror(getClass.getClassLoader)
  var im: ru.InstanceMirror = null

  def reflectClass(implicit ev: scala.reflect.ClassTag[Data] = dataClassTag) {
    parameters = getAllAnnotatedFields[Parameter]
    optionalParameters = getAllAnnotatedFields[OptionalParameter]
    variables = getAllAnnotatedFields[Variable]
    variablesToReset = getAllAnnotatedFields[ResetEveryPeriod]
    inputs = getAllAnnotatedFields[Input]
    outputs = getAllAnnotatedFields[Output]
    drivers = getAllAnnotatedFields[Driver]
    optionalDrivers = getAllAnnotatedFields[OptionalDriver]
    allFields = getAllFields

    val im = runtimeMirror.reflect(this.data)
    fieldSymbols = im.symbol.typeSignature.members.filter(m => !m.isMethod).toList
    fieldSymbols.foreach((sym: reflect.runtime.universe.Symbol) => { typeMap += (sym.name.toString.trim, sym.typeSignature) })

    drivers.foreach((drv: String) => { interpolationModeMap += (drv, getInterpolationMode(drv)) })
    optionalDrivers.foreach((drv: String) => { interpolationModeMap += (drv, getInterpolationMode(drv)) })

    configurableOutputs = outputs.filter { o: String => (typeMap(o).typeConstructor =:= ru.typeOf[scala.collection.mutable.Map[_, _]].typeConstructor) }
  }

  def reflectInstance(implicit ev: scala.reflect.ClassTag[Data] = dataClassTag) {
    im = runtimeMirror.reflect(this.data)
    //allFields.foreach((s: String) => addOneFieldToFieldTable(im, s))
    variablesToReset.foreach { variableName: String =>
      variablesToResetReflectionMap += (variableName -> im.reflectField(dataTypeSave.decl(ru.TermName(variableName)).asTerm.accessed.asTerm))
    }
  }

  private def addOneFieldToFieldTable(im: ru.InstanceMirror, fn: String) {
    val field: ru.TermSymbol = dataTypeSave.decl(ru.TermName(fn)).asTerm.accessed.asTerm
    val fm: ru.FieldMirror = im.reflectField(field)
    //fieldMap(fn) = fm
  }

  // TODO: check this procedure!
  def checkParameters(parvar: Array[String]) {
    // parvar = parameters found in the input file!

    // check: which parameters are in the input file but not in the actual feature code
    val notInSet = parvar.filterNot((inputs ++ metainformation).contains(_))
    if (notInSet.size > 0) {
      // To do: warn or is this an error?
      logger.warn("found inputs in " + inputFileNames.get + " which are not requested by " + name + ": " + notInSet.toList.toString())
    }

    actualInputs = parvar.filter((inputs /*++ metainformation*/ ).contains(_))

    val inputFieldsNoParVars = inputs.filterNot((parameters ++ optionalParameters ++ variables).contains(_))
    if (inputFieldsNoParVars.size > 0) {
      logger.error("found fields declared as input, which are not declared as variable/parameter in " + name + ": " + inputFieldsNoParVars.toList.toString)
    }

    val requiredInputs = inputs.filterNot((optionalParameters).contains(_))
    val requiredInputsNotFoundInFile = requiredInputs.filterNot((parvar).contains(_))
    val optionalInputsNotFoundInFile = optionalParameters.filterNot((parvar).contains(_))

    if (requiredInputsNotFoundInFile.size > 0) {
      logger.error("the following required input fields where not found in " + inputFileNames.get + ": " + requiredInputsNotFoundInFile.toList)
    }

    if (optionalInputsNotFoundInFile.size > 0) {
      logger.warn("the following optional fields declared as input where not found in " + inputFileNames.get + " and will thus not be set: " + optionalInputsNotFoundInFile.toList)
    }
  }

  def checkDrivers(driv: Array[String]) {
    // driv = drivers found in the scenario file!

    // check: which parameters are in the input file but not in the actual feature code
    val notRequested = driv.filterNot((drivers ++ optionalDrivers ++ metainformation).contains(_))
    if (notRequested.size > 0) {
      // To do: warn or is this an error?
      logger.warn("found drivers in " + scenarioFileName.get + " which are not requested by " + name + ": " + notRequested.toList)
    }

    actualDrivers = driv.filter((drivers ++ optionalDrivers /*++ metainformation*/ ).contains(_))

    // check: which feature drivers are not in the input file 
    val requiredDriversNotInFile = (drivers ++ metainformation).filterNot(driv.contains(_))
    if (requiredDriversNotInFile.size > 0) {
      // To do: warn or is this an error?
      logger.error("found not all drivers in " + scenarioFileName.get + " which are requested by " + name + ", the following are missing: " + requiredDriversNotInFile.toList.toString)
    }

    // check: which optional feature drivers are not in the input file 
    val optionalDriversNotInFile = (optionalDrivers ++ metainformation).filterNot(driv.contains(_))
    if (optionalDriversNotInFile.size > 0) {
      // To do: warn or is this an error?
      logger.warn("found not all optional drivers in " + scenarioFileName.get + ", the following are missing and thus ignored: " + optionalDriversNotInFile.toList.toString)
    }

  }

  def initialise(fis: divaInput, locid: String, pTime: Int) {
    initialiseMetaInformation(fis, locid, pTime)
    initialiseInitialInputs(fis, locid, pTime)
  }

  def initialiseMetaInformation(fis: divaInput, locid: String, pTime: Int) {
    try {
      locationid = fis.getInputValue[String]("locationid", locid, pTime)
      locationname = fis.getInputValue[String]("locationname", locid, pTime)
      val timeInFIS = fis.getInputValue[Int]("time", locid, pTime)
    } catch {
      case e: Throwable => logger.error("error in reading the metainformation of location " + locid + " at time " + pTime + ". Maybe the wrong time is given in the input file?")
    }
  }

  private def setOneVariableEfficient(fis: divaInput, pName: String, locid: String, pTime: Int) {
    val pType: ru.Type = typeMap(pName)
    try {
      if (pType =:= ru.typeOf[Float]) dataIO.readInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Int]) dataIO.readInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Double]) dataIO.readInputValue(fis, data, pName, locid, pTime)
      else if (pType.typeConstructor =:= ru.typeOf[Option[_]].typeConstructor) {
        val pInnerType = pType.typeArgs.head
        trySetOptionalOneWithExplicitTypeEfficient(fis, pName, locid, pTime, pType, pInnerType)
      } else if (pType =:= ru.typeOf[String]) dataIO.readInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Long]) dataIO.readInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Short]) dataIO.readInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Byte]) dataIO.readInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Boolean]) dataIO.readInputValue(fis, data, pName, locid, pTime)
      else {
        logger.error("Could not set field " + pName + " in " + name + ": unknown type (" + pType.toString() + ")")
      }
    } catch {
      case e: Throwable =>
        logger.error("Could not set field " + pName + " in " + name + " (locationid = " + locid + ", time= " + pTime + ") of type " + pType.toString + " : unknown error(" + e.toString + "). ")
    }
  }

  private def setOneScenarioValueEfficient(fis: divaInput, pName: String, locid: String, pTime: Int) {
    val lType: ru.Type = typeMap(pName)
    val lInterpolationMode: InterpolationMode = interpolationModeMap(pName)
    try {
      if (lType =:= ru.typeOf[Float]) dataIO.readScenarioValue(fis, data, pName, locid, pTime, lInterpolationMode)
      else if (lType =:= ru.typeOf[Int]) dataIO.readScenarioValue(fis, data, pName, locid, pTime, lInterpolationMode)
      else if (lType =:= ru.typeOf[Double]) dataIO.readScenarioValue(fis, data, pName, locid, pTime, lInterpolationMode)
      else if (lType =:= ru.typeOf[String]) dataIO.readScenarioValue(fis, data, pName, locid, pTime, lInterpolationMode)
      else if (lType =:= ru.typeOf[Boolean]) dataIO.readScenarioValue(fis, data, pName, locid, pTime, lInterpolationMode)
      else if (lType.typeConstructor =:= ru.typeOf[Option[_]].typeConstructor) {
        val lInnerType = lType.typeArgs.head
        //println("reading " + pName)
        trySetOptionalScenarioOneWithExplicitTypeEfficient(fis, pName, locid, pTime, lType, lInnerType, lInterpolationMode)
      } else if (lType =:= ru.typeOf[Long]) dataIO.readScenarioValue(fis, data, pName, locid, pTime, lInterpolationMode)
      else if (lType =:= ru.typeOf[Short]) dataIO.readScenarioValue(fis, data, pName, locid, pTime, lInterpolationMode)
      else if (lType =:= ru.typeOf[Byte]) dataIO.readScenarioValue(fis, data, pName, locid, pTime, lInterpolationMode)
      else {
        logger.error("Could not set field " + pName + " in " + name + ": unknown type (" + lType.toString() + ")")
      }
    } catch {
      case e: Throwable => logger.error("Could not set field " + pName + " in " + name + " (locationid = " + locid + ", time= " + pTime + ") of type " + lType.toString + ": unknown error(" + e.toString + ")")
    }
  }

  // this one sets an optional  parameter, if it is not found in the input, it is set to None
  private def trySetOptionalOneWithExplicitTypeEfficient(fis: divaInput, pName: String, locid: String, pTime: Int, pOriginalType: ru.Type, pType: ru.Type) {
    try {
      if (pType =:= ru.typeOf[Float]) dataIO.readOptionInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Int]) dataIO.readOptionInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[String]) dataIO.readOptionInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Double]) dataIO.readOptionInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Boolean]) dataIO.readOptionInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Byte]) dataIO.readOptionInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Short]) dataIO.readOptionInputValue(fis, data, pName, locid, pTime)
      else if (pType =:= ru.typeOf[Long]) dataIO.readOptionInputValue(fis, data, pName, locid, pTime)
      else {
        logger.error("Could not set field " + pName + " in " + name + ": unknown type (" + pOriginalType.toString + ")")
      }
    } catch {
      case e: Throwable =>
      // TODO:
      //case e: Throwable => logger.error("Could not set field " + pName + " in " + name + ": unknown error(" + e.toString + ")")
    }
  }

  // this one sets an optional  parameter, if it is not found in the input, it is set to None
  private def trySetOptionalScenarioOneWithExplicitTypeEfficient(fis: divaInput, pName: String, locid: String, pTime: Int, pOriginalType: ru.Type, pType: ru.Type, pInterpolationMode: InterpolationMode) {
    /*if (locid == "CLS_DEU_00001") {
      println("Reading: " + pName)
      println(pOriginalType.toString)
      println(pType.toString)
    }*/
    try {
      if (pType =:= ru.typeOf[String]) dataIO.readOptionScenarioValue(fis, data, pName, locid, pTime, pInterpolationMode)
      else if (pType =:= ru.typeOf[Float]) dataIO.readOptionScenarioValue(fis, data, pName, locid, pTime, pInterpolationMode)
      else if (pType =:= ru.typeOf[Int]) dataIO.readOptionScenarioValue(fis, data, pName, locid, pTime, pInterpolationMode)
      else if (pType =:= ru.typeOf[Double]) dataIO.readOptionScenarioValue(fis, data, pName, locid, pTime, pInterpolationMode)
      else if (pType =:= ru.typeOf[Boolean]) dataIO.readOptionScenarioValue(fis, data, pName, locid, pTime, pInterpolationMode)
      else if (pType =:= ru.typeOf[Byte]) dataIO.readOptionScenarioValue(fis, data, pName, locid, pTime, pInterpolationMode)
      else if (pType =:= ru.typeOf[Short]) dataIO.readOptionScenarioValue(fis, data, pName, locid, pTime, pInterpolationMode)
      else if (pType =:= ru.typeOf[Long]) dataIO.readOptionScenarioValue(fis, data, pName, locid, pTime, pInterpolationMode)
      else {
        logger.error("Could not set field " + pName + " in " + name + ": unknown type (" + pOriginalType.toString + ")")
      }
    } catch {
      case e: Throwable =>
      // TODO:
      //case e: Throwable => logger.error("Could not set field " + pName + " in " + name + ": unknown error(" + e.toString + ")")
    }
  }

  def saveHeader() {
    outputStream match {
      case Some(stream) => {
        val metaInformationTypes = scala.collection.mutable.ArraySeq(ru.typeOf[String]) ++ scala.collection.mutable.ArraySeq(ru.typeOf[String]) ++ scala.collection.mutable.ArraySeq(ru.typeOf[String]) ++ scala.collection.mutable.ArraySeq(ru.typeOf[Int])

        val outputsNamesForHeader = outputs.map {
          o: String =>
            if (configurableOutputs.contains(o)) constructConfigurableOutputNames(o) else List(o)
        }.flatten.filterNot(ignoreOutputs.contains(_))

        val outputTypes /*: scala.collection.mutable.ArraySeq[scala.reflect.runtime.universe.Type]*/ =
          outputs.map { o: String =>
            if (configurableOutputs.contains(o)) constructConfigurableOutputNames(o).filterNot(ignoreOutputs.contains(_)).map { x => typeMap(o).typeArgs(1) }
            else {
              if (ignoreOutputs.contains(o)) List() else List(typeMap(o))
            }
          }.toList.flatten

        val headerLine: Array[String] = Array("DIVA_version", "caseid") ++ metainformation ++ outputsNamesForHeader
        val headerTypes /*: Array[scala.reflect.runtime.universe.Type]*/ = metaInformationTypes.toList ++ outputTypes
        stream.writeHeaderLine(headerLine, headerTypes)
      }
      case None => // do nothing
    }
  }

  def saveData(t: Int) {
    outputStream match {
      case Some(stream) => {
        stream.write(locationid)
        stream.write(locationname)
        stream.write(t)
        val outs = outputs
        outs.foreach { (s: String) =>
          {
            val g: Option[ru.FieldMirror] = fieldMap.get(s)
            g match {
              case Some(v) => stream.write(v.get.toString())
              case None    => logger.warn("Did not find value for output field " + s + " in " + name)
            }
          }
        }
        stream.writeNewLine
      }
      case None => {}
    }
  }

  def saveInit() {
    outputStream match {
      case Some(stream) => {
        stream.writeInit
      }
      case None => {}
    }
  }

  def saveDataEfficient(t: Int, cid: String, vers: String) {
    outputStream match {
      case Some(stream) => {
        stream.write(vers)
        stream.write(cid)
        stream.write(locationid)
        stream.write(locationname)
        stream.write(t)
        if (ignoreAnyOutputs) {
          dataIO.writeSelectedData(stream, data, ignoreThisOutput)
        } else {
          dataIO.writeData(stream, data)
        }
        stream.writeNewLine
      }
      case None => {}
    }
  }

  def saveFinish() {
    outputStream match {
      case Some(stream) => {
        stream.writeFinish
      }
      case None => {}
    }
  }

  def readScenarioValues(locid: String, pTime: Int) {
    scenario match {
      case Some(inp) => actualDrivers.foreach { (s: String) => setOneScenarioValueEfficient(inp, s, locid, pTime) }
      case None      =>
    }
  }

  def resetVariables()(implicit ev: scala.reflect.ClassTag[Data] = dataClassTag) {
    variablesToReset.foreach { variableName: String => variablesToResetReflectionMap(variableName).set(0) }
  }

  def constructIgnoreOutputArray() = {
    val outputsNames = outputs.map {
      o: String =>
        if (configurableOutputs.contains(o)) constructConfigurableOutputNames(o) else List(o)
    }.flatten

    ignoreThisOutput = new Array[Boolean](outputsNames.size)
    for (i <- 0 until ignoreThisOutput.size) {
      ignoreThisOutput(i) = ignoreOutputs.contains(outputsNames(i))
    }
    //println("outputsNamesForHeader: " + outputsNamesForHeader.toList)
    ignoreAnyOutputs = ignoreThisOutput.contains(true)
  }

  private def initialiseInitialInputs(inp: divaInput, locid: String, pTime: Int) = {
    actualInputs.foreach { (inputName: String) => setOneVariableEfficient(inp, inputName, locid, pTime) }
  }

  private def getAllFields: Array[String] = {
    // first, build an Iteratable of all vars/vals
    val cl = data.getClass
    val allMembers: ru.MemberScope = ru.runtimeMirror(cl.getClassLoader).classSymbol(cl).toType.members
    val fields: Iterable[ru.Symbol] = allMembers.filter { s: ru.Symbol => (s.asTerm.isVal || s.asTerm.isVar) }

    // return their names
    fields.map { x => x.name.toString.trim }.toArray.sorted
  }

  private def getAllAnnotatedFields[A: ru.TypeTag](): Array[String] = {
    // first, build an Iteratable of all vars/vals
    val cl = data.getClass
    val allMembers: ru.MemberScope = ru.runtimeMirror(cl.getClassLoader).classSymbol(cl).toType.members
    val fields: Iterable[ru.Symbol] = allMembers.filter { s: ru.Symbol => (s.asTerm.isVal || s.asTerm.isVar) }

    // then only keep the ones with a A annotation
    val fieldsWithAnnotation: Iterable[ru.Symbol] = fields.filter { f => (f.annotations.find { an => an.tree.tpe =:= ru.typeOf[A] } != None) }

    // return their names
    fieldsWithAnnotation.map { x => x.name.toString.trim }.toArray.sorted
  }

  private def getAnnotationValue[A: ru.TypeTag](fieldName: String, position: Int): String = {
    val cl = data.getClass
    val allMembers: ru.MemberScope = ru.runtimeMirror(cl.getClassLoader).classSymbol(cl).toType.members
    val fields: Iterable[ru.Symbol] = allMembers.filter { s: ru.Symbol => (s.name.toString.trim == fieldName) }

    if (fields.size == 0) {
      logger.error("Did not find field " + fieldName + " in " + name)
    }

    val field = fields.head
    val annotation = field.annotations.find { an => an.tree.tpe =:= ru.typeOf[A] }

    annotation match {
      case Some(a) => {
        val args: List[ru.Tree] = a.tree.children.tail
        if (args.size < (position + 1)) {
          logger.error("Did not find annotation argument " + position + " in annotation " + a.toString + " for " + fieldName + " in " + name + ": only " + args.size + " arguments available.")
        }
        // remove double quotes at the beginning and the end
        args(position).toString.replaceAll("^\"|\"$", "");
      }
      case None => ""
    }
  }

  private def constructConfigurableOutputNames(configurableOutputs: Array[String]): List[String] = {
    configurableOutputs.map { o: String => constructConfigurableOutputNames(o) }.toList.flatten
  }

  private def constructConfigurableOutputNames(configurableOutput: String): List[String] = {
    var annVal: String = getAnnotationValue[Configure](configurableOutput, 0)
    if (annVal == "ELEVATION") {
      elevationValuesForExposureOutput.sorted.map { x => (configurableOutput + x).replaceAll("\\.", "p") }
    } else if (annVal == "RETURNPERIOD") {
      rpValuesForExposureOutput.sorted.map { x => (configurableOutput + x).replaceAll("\\.", "p") }
    } else {
      logger.warn("Unknown configuration mode " + annVal + " for field " + configurableOutput + ". Not used for output.")
      List()
    }
  }

  private def getInterpolationMode(fieldName: String): InterpolationMode = {
    // Unfortunately, by now it is not possible to access annotation values by name
    // so we use the 'position' as Int. As 'Interpolate' should have only one value, we have to use this one (with number 0)
    var annVal: String = getAnnotationValue[Interpolate](fieldName, 0)
    if (annVal == "") {
      logger.warn("No interpolation Annotation found for driver " + fieldName + ". Using left side interpolation instead.")
      annVal = "LEFT"
    }

    val ret: InterpolationMode =
      try { annVal }
      catch {
        case nf: java.util.NoSuchElementException =>
          logger.warn("Interpolation mode " + annVal + " for driver " + fieldName + " is unknown. Using left side interpolation instead.")
          "LEFT"
        case e: Throwable => {
          logger.warn("something wrong (" + e.toString() + ") with Interpolation mode " + annVal + " for driver " + fieldName + ". Using left side interpolation.")
          "LEFT"
        }
      }
    ret
  }
}