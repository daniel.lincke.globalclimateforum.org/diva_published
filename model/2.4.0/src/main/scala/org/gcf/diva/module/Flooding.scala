package diva.module

import scala.collection.parallel.immutable.ParSeq

import diva.model.ModuleI
import diva.model.divaState
import diva.module.flooding._
import diva.model.divaFeatures._
import diva.model.Config
import diva.toolbox.NumericalIntegration._
import diva.toolbox.Toolbox._
import diva.feature._
import diva.types.DikePositionMode._
import diva.types.DikeMode._
import diva.types.RetreatMode._
import diva.types.TypeComputations._

class Flooding extends ModuleI {

  def moduleName: String = "Flooding Algorithm, including Adaptation"
  def moduleVersion: String = "1.0"
  def moduleDescription: String = "Calculates coastal flooding due to sea-level rise and storm surges, taking into account the effect of flood defences."
  def moduleAuthor: String = "Daniel Lincke, Jochen Hinkel, Cezar Ionescu"

  def getOutput: Array[java.lang.String] = Array("cls.h1", "cls.h10", "cls.h100", "cls.h1000", "cls.a1000_1000", "cls.a100_100", "cls.a100_1000", "cls.a10_10",
    "cls.a10_100", "cls.a10_1000", "cls.a1_1", "cls.a1_10", "cls.a1_100", "cls.a1_1000", "cls.d1000_1000", "cls.d100_100",
    "cls.d100_1000", "cls.d10_10", "cls.d10_100", "cls.d10_1000", "cls.d1_1", "cls.d1_10", "cls.d1_100", "cls.d1_1000", "cls.par",
    "cls.phz", "admin.phz", "admin.par", "admin.seafloodcost", "country.seafloodcost", "cls.seafloodcost",
    "gva.seafloodcost", "global.seafloodcost", "country.par", "country.phz", "cls.landlsub_new", "country.landlsub_new",
    "admin.landlsub_new", "gva.landlsub_new", "sres.landlsub_new", "gva.phz", "sres.phz", "global.phz", "gvas.par", "sres.par",
    "global.par", "sres.pfloodarea_new", "admin.pfloodarea_new", "gva.pfloodarea_new", "country.pfloodarea_new", "global.pfloodarea_new",
    "cls.exposure_s1", "admin.exposure_s1", "gva.exposure_s1", "sres.exposure_s1", "gva.exposure_s1", "sres.exposure_s1", "global.exposure_s1",
    "cls.exposure_s10", "admin.exposure_s10", "gva.exposure_s10", "sres.exposure_s10", "gva.exposure_s10", "sres.exposure_s10", "global.exposure_s10",
    "cls.damage_s1", "admin.damage_s1", "gva.damage_s1", "sres.damage_s1", "gva.damage_s1", "sres.damage_s1", "global.damage_s1",
    "cls.damage_s10", "admin.damage_s10", "gva.damage_s10", "sres.damage_s10", "gva.damage_s10", "sres.damage_s10", "global.damage_s10")

  def getInput: Array[java.lang.String] = Array("cls.area1", "cls.area2", "cls.area3", "cls.area4", "cls.area5", "cls.area6_8", "cls.area9_12", "cls.area13_16", "cls.pop", "cls.rslr",
    "cls.s1", "cls.s10", "cls.s100", "cls.s1000", "cls.smax", "cls.seadikehght", "country.gdpc", "cls.adminid", "cls.countryid", "cls.wethigh",
    "cls.wetsalt", "cls.wetmang", "cls.wetlow", "cls.wetfresh", "cls.wetforst")

  // maximal number of steps for numerical integration
  var integrationMaxSteps: Int = 50
  var upperLimitReturnPeriod: Int = 10000

  // Damage function configuration
  var halfDamageDepth: Float = 1.0f
  var nondamageableFration: Float = 0.0f

  // dike configuration parameter
  var dikePositionMode: DikePositionMode = ONLY_COASTAL
  var dikeInitilisationMode: DikeMode = DEMAND_FOR_SAFETY_ORIGINAL
  var dikeRaisingMode: DikeMode = DEMAND_FOR_SAFETY_ORIGINAL
  var dikeRaisingStartYear: Int = 1995
  var dikeConstructionHorizon: Int = 0
  var minimalDikeRaise: Float = 0
  var seadikeConstructionCostFactor: Float = 1.0f
  var bcrToAllowNewDike: Float = 1.0f

  // parameters for calculating protlevel
  var popdensHalf: Float = 80f
  var popdensUninhabitedThreshold: Float = 30f
  var popdensRuralThreshold: Float = 300f
  var popdensUrbanThreshold: Float = 1000f
  var protectionLevelFactor: Float = 1.0f
  var protlevelInhabitedThreshold: Float = 30.0f
  var targetRelativeFloodRisk: Float = 0.0001f

  // prarameters for dike optimization
  var dikeOptimizationRPStart: Int = 10
  var dikeOptimizationRPEnd: Int = 10000
  var dikeOptimizationRPStep: Int = 10

  // population correction
  var populationCorrection: Boolean = true
  var populationCorrectionReturnPeriod: Float = 1.0f
  var populationCorrectionDistributionReturnPeriod: Float = 100.0f
  //var populationCorrectionYear = 1995

  // submergence migration
  var submergenceMigration: Boolean = false
  // Welfare cost of migrating one person  to GDP per capita
  var migrcostToGdpcRatio: Float = 3f
  // rate of people migrating out of the coastal zone
  var migrationAwayRate: Float = 1.0f
  // rate of people migrating out of the coastal zone
  var submergenceMigrationLevel: Float = 1.0f

  // Retreat configuration
  var retreatMode: RetreatMode = NONE
  var retreatYear: Int = 2015
  // in meter!
  var setbackWidth: Float = 100.0f
  // This comes from Shows1978:
  // initial: US$5,600 per km
  // mandatory five-year reviews at expected cost of $11,200 per km.
  // Annual administrative costs: $2,500
  var setbackZoneAdministrativeCostInitial = 3620 // 5,600/5 + 2,500
  var setbackZoneAdministrativeCost = 4720 // 11,200/5 + 2,500

  // retreat below this return period
  var retreatLevel: Int = 100
  var cityRetreat: Boolean = false
  var retreatProtectionLevel: Int = 1

  // deprecation of abandoned areas
  var deprecationRate: Float = 3.0f

  var adaptationManager: AdaptationManagement = _
  var impactComputation: ImpactComputation = _

  var adaptationPerCountry: Boolean = false
  var adaptationGlobal: Boolean = false

  var landuseClasses: List[Int] = List(0, 1, 2, 3, 4, 5, 6)
  var waterlevelAttenuationRates: List[Float] = List(25, 50, 100, 40, 25, 50, 25)
  var waterlevelAttenuationFactor: Float = 1.0f
  var waterlevelAttenuation: Boolean = false

  val dikeModesNotSuitableForInit: List[DikeMode] = List(CONSTANT_FLOOD_RISK, CONSTANT_RELATIVE_FLOOD_RISK, CONSTANT_PROTECTION_LEVELS, COUNTRY_CONSTANT_RELATIVE_FLOOD_RISK, GLOBAL_CONSTANT_RELATIVE_FLOOD_RISK, NO_DIKE_RAISE)

  def initModule(initialState: divaState) = {
    setConfigurableVariables(initialState)
    checkParamterConfigurations(initialState)

    if (dikeModesNotSuitableForInit.contains(dikeInitilisationMode)) {
      logger.error("dikeInitilisationMode = " + dikeInitilisationMode + " is not suitable for dike initialization (only for dike raising).")
    }

    adaptationPerCountry = (dikeRaisingMode == COUNTRY_TARGET_RELATIVE_FLOOD_RISK) || (dikeRaisingMode == COUNTRY_CONSTANT_RELATIVE_FLOOD_RISK)
    adaptationGlobal = (dikeRaisingMode == GLOBAL_TARGET_RELATIVE_FLOOD_RISK) || (dikeRaisingMode == GLOBAL_CONSTANT_RELATIVE_FLOOD_RISK)
  }

  def computeScenario(currentState: divaState) = doNothing

  def init(state: divaState) {

    if (dikeOptimizationRPStart > dikeOptimizationRPEnd) {
      logger.error("Return period values for dike optimisaton is given as an empty range.")
    }

    if (dikeOptimizationRPStart <= 0) {
      logger.error("Minimum return period for dike optimisaton should be larger than zero. (Zero ist testet seperately anyway).")
    }

    impactComputation = new ImpactComputation(upperLimitReturnPeriod, integrationMaxSteps)
    adaptationManager = new AdaptationManagement(impactComputation, dikePositionMode, dikeInitilisationMode, dikeRaisingMode, dikeOptimizationRPStart, dikeOptimizationRPEnd, dikeOptimizationRPStep)
    adaptationManager.popdensHalf = popdensHalf
    adaptationManager.logger = logger
    adaptationManager.popdensUninhabitedThreshold = popdensUninhabitedThreshold
    adaptationManager.popdensRuralThreshold = popdensRuralThreshold
    adaptationManager.popdensUrbanThreshold = popdensUrbanThreshold
    adaptationManager.dikeRaisingStartYear = dikeRaisingStartYear
    logger.info("setting discount rate to " + state.discountRate + " percent.")
    adaptationManager.discountRate = state.discountRate / 100
    adaptationManager.submergenceMigration = submergenceMigration
    adaptationManager.protectionLevelFactor = protectionLevelFactor
    adaptationManager.migrcostToGdpcRatio = migrcostToGdpcRatio
    adaptationManager.seadikeConstructionCostFactor = seadikeConstructionCostFactor
    adaptationManager.protlevelInhabitedThreshold = protlevelInhabitedThreshold
    adaptationManager.targetRelativeFloodRisk = targetRelativeFloodRisk
    adaptationManager.submergenceMigrationLevel = submergenceMigrationLevel
    adaptationManager.bcrToAllowNewDike = bcrToAllowNewDike

    val waterlevelAttenuationMap: Map[Int, Float] = waterlevelAttenuation match {
      case true => (landuseClasses zip waterlevelAttenuationRates.map((r: Float) => r * waterlevelAttenuationFactor / 100f)).toMap
      case false => (landuseClasses zip landuseClasses.map(_ => 0.0f)).toMap
    }

    var clssp: ParSeq[Cls] = state.getFeatures[Cls].par
    clssp.foreach { cls =>
      dikeInitialisation(cls, state)
      computePopulationCorrection(cls)
    }

    var czs: ParSeq[Cz] = state.getFeatures[Cz].par
    czs.foreach { cz =>
      {
        cz.coastalPlane.assetsExposure.halfDamageDepth = halfDamageDepth
        cz.coastalPlane.deprecationRate = deprecationRate
        cz.coastalPlane.setbackWidth = setbackWidth
        cz.coastalPlane.attenuate(waterlevelAttenuationMap)
      }
    }

    var clss: ParSeq[Cls] = state.getFeatures[Cls].par
    clss.foreach { cls =>
      {
        if (state.time >= retreatYear) {
          retreatManagement(cls, state.timeStep)
        }
        // area correction?
        getCurrentExposure(cls)
        getCurrentFloodimpacts(cls)
        landLoss(cls, true, state.time, state.timeStep)
      }
    }

    accumulate(state)
    accumulateInits(state)
  }

  def invoke(state: divaState) {
    if (adaptationPerCountry) {
      invokeCountry(state)
    }
    if (adaptationGlobal) {
      invokeGlobal(state)
    }
    if (!adaptationPerCountry && !adaptationGlobal) {
      invokeCLS(state)
    }
  }

  def invokeCLS(state: divaState) {
    var clss: ParSeq[Cls] = state.getFeatures[Cls].par
    clss.foreach { cls =>
      {

        if (state.time >= dikeRaisingStartYear)
          dikeRaising(cls, state)
        else
          keepDikeInitialisation(cls, state)
        if (state.time >= retreatYear)
          retreatManagement(cls, state.timeStep)
        landLoss(cls, false, state.time, state.timeStep)
        getCurrentExposure(cls)
        getCurrentFloodimpacts(cls)
      }
    }

    accumulate(state)
  }

  def invokeCountry(state: divaState) {
    println("per Country!")
    var countries: List[Country] = state.getFeatures[Country]

    countries.foreach { country =>
      //      if (country.clss.filter(cls => cls.coastalPlane.populationDensityBelow(cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float]) > popdensUninhabitedThreshold).length > 0) {
      if (country.clss.length > 0) {
        if (state.time >= dikeRaisingStartYear) {
          dikeRaisingPerCountry(country.clss, country, state)
        } else {
          country.clss.par.foreach(cls => keepDikeInitialisation(cls, state))
        }
        if (state.time >= retreatYear) {
          // Change later?
          country.clss.par.foreach(cls => retreatManagement(cls, state.timeStep))
        }
        country.clss.par.foreach { cls =>
          getCurrentExposure(cls)
          getCurrentFloodimpacts(cls)
        }
      }
    }

    accumulate(state)
  }

  def invokeGlobal(state: divaState) {
    var clss: ParSeq[Cls] = state.getFeatures[Cls].par

    if (state.time >= dikeRaisingStartYear) {
      // dikeRaising(clss.seq, state)
    } else {
      clss.foreach(cls => keepDikeInitialisation(cls, state))
    }
    if (state.time >= retreatYear) {
      // Change later?
      clss.foreach(cls => retreatManagement(cls, state.timeStep))
    }
    clss.foreach { cls =>
      getCurrentExposure(cls)
      getCurrentFloodimpacts(cls)
    }

    accumulate(state)
  }

  def setConfigurableVariables(initialState: divaState) = {
    var elvationBasedMap: scala.collection.mutable.Map[Float, Float] = scala.collection.mutable.Map()
    var rpBasedMap: scala.collection.mutable.Map[Int, Float] = scala.collection.mutable.Map();

    initialState.elevationValuesForExposureOutput.foreach { el => elvationBasedMap += (el -> 0f) }
    initialState.rpValuesForExposureOutput.foreach { rp => rpBasedMap += (rp -> 0f) }

    var czs = initialState.getFeatures[Cz]
    czs.foreach { cz =>
      cz.data.area_below_ = elvationBasedMap.clone
      cz.data.assets_below_ = elvationBasedMap.clone
      cz.data.assets_developable_below_ = elvationBasedMap.clone
      cz.data.assets_developed_below_ = elvationBasedMap.clone
      cz.data.pop_below_ = elvationBasedMap.clone
      cz.data.area_below_h = rpBasedMap.clone
      cz.data.assets_below_h = rpBasedMap.clone
      cz.data.pop_below_h = rpBasedMap.clone
      cz.data.seafloodcost_h = rpBasedMap.clone
    }

    var clss = initialState.getFeatures[Cls]
    clss.foreach { cls =>
      cls.data.area_below_ = elvationBasedMap.clone
      cls.data.assets_below_ = elvationBasedMap.clone
      cls.data.assets_developable_below_ = elvationBasedMap.clone
      cls.data.assets_developed_below_ = elvationBasedMap.clone
      cls.data.pop_below_ = elvationBasedMap.clone
      cls.data.area_below_h = rpBasedMap.clone
      cls.data.assets_below_h = rpBasedMap.clone
      cls.data.pop_below_h = rpBasedMap.clone
      cls.data.seafloodcost_h = rpBasedMap.clone
    }

    var admins = initialState.getFeatures[Admin]
    admins.foreach { admin =>
      admin.data.area_below_ = elvationBasedMap.clone
      admin.data.assets_below_ = elvationBasedMap.clone
      admin.data.assets_developable_below_ = elvationBasedMap.clone
      admin.data.assets_developed_below_ = elvationBasedMap.clone
      admin.data.pop_below_ = elvationBasedMap.clone
      admin.data.area_below_h = rpBasedMap.clone
      admin.data.assets_below_h = rpBasedMap.clone
      admin.data.pop_below_h = rpBasedMap.clone
      admin.data.seafloodcost_h = rpBasedMap.clone
    }

    var cities = initialState.getFeatures[City]
    cities.foreach { city =>
      city.data.area_below_ = elvationBasedMap.clone
      city.data.assets_below_ = elvationBasedMap.clone
      city.data.assets_developable_below_ = elvationBasedMap.clone
      city.data.assets_developed_below_ = elvationBasedMap.clone
      city.data.pop_below_ = elvationBasedMap.clone
      city.data.area_below_h = rpBasedMap.clone
      city.data.assets_below_h = rpBasedMap.clone
      city.data.pop_below_h = rpBasedMap.clone
      city.data.seafloodcost_h = rpBasedMap.clone
    }

    var deltas = initialState.getFeatures[Delta]
    deltas.foreach { delta =>
      delta.data.area_below_ = elvationBasedMap.clone
      delta.data.assets_below_ = elvationBasedMap.clone
      delta.data.assets_developable_below_ = elvationBasedMap.clone
      delta.data.assets_developed_below_ = elvationBasedMap.clone
      delta.data.pop_below_ = elvationBasedMap.clone
      delta.data.area_below_h = rpBasedMap.clone
      delta.data.assets_below_h = rpBasedMap.clone
      delta.data.pop_below_h = rpBasedMap.clone
      delta.data.seafloodcost_h = rpBasedMap.clone
    }

    var countries = initialState.getFeatures[Country]
    countries.foreach { country =>
      country.data.area_below_ = elvationBasedMap.clone
      country.data.assets_below_ = elvationBasedMap.clone
      country.data.assets_developable_below_ = elvationBasedMap.clone
      country.data.assets_developed_below_ = elvationBasedMap.clone
      country.data.pop_below_ = elvationBasedMap.clone
      country.data.area_below_h = rpBasedMap.clone
      country.data.assets_below_h = rpBasedMap.clone
      country.data.pop_below_h = rpBasedMap.clone
      country.data.seafloodcost_h = rpBasedMap.clone
    }

    var regions = initialState.getFeatures[Region]
    regions.foreach { region =>
      region.data.area_below_ = elvationBasedMap.clone
      region.data.assets_below_ = elvationBasedMap.clone
      region.data.assets_developable_below_ = elvationBasedMap.clone
      region.data.assets_developed_below_ = elvationBasedMap.clone
      region.data.pop_below_ = elvationBasedMap.clone
      region.data.area_below_h = rpBasedMap.clone
      region.data.assets_below_h = rpBasedMap.clone
      region.data.pop_below_h = rpBasedMap.clone
      region.data.seafloodcost_h = rpBasedMap.clone
    }

    var globals = initialState.getFeatures[Global]
    globals.foreach { global =>
      global.data.area_below_ = elvationBasedMap.clone
      global.data.assets_below_ = elvationBasedMap.clone
      global.data.assets_developable_below_ = elvationBasedMap.clone
      global.data.assets_developed_below_ = elvationBasedMap.clone
      global.data.pop_below_ = elvationBasedMap.clone
      global.data.area_below_h = rpBasedMap.clone
      global.data.assets_below_h = rpBasedMap.clone
      global.data.pop_below_h = rpBasedMap.clone
      global.data.seafloodcost_h = rpBasedMap.clone
    }
  }

  private def checkParamterConfigurations(initialState: divaState) {
    if ((!populationCorrection) && (submergenceMigration)) {
      logger.warn("Migration due to submergence has been set, but population correction not. This might lead to strange results.")
    }

    if (popdensUninhabitedThreshold >= popdensUrbanThreshold) {
      logger.error("popdensUninhabitedThreshold (" + popdensUninhabitedThreshold + ") must be smaller than popdensUrbanThreshold(" + popdensUrbanThreshold + ").")
    }

    if (popdensUninhabitedThreshold >= popdensRuralThreshold) {
      logger.error("popdensUninhabitedThreshold (" + popdensUninhabitedThreshold + ") must be smaller than popdensRuralThreshold(" + popdensRuralThreshold + ").")
    }

    if (popdensRuralThreshold >= popdensUrbanThreshold) {
      logger.error("popdensURuralThreshold (" + popdensRuralThreshold + ") must be smaller than popdensUrbanThreshold(" + popdensUrbanThreshold + ").")
    }

    if (popdensUninhabitedThreshold >= popdensHalf) {
      logger.error("popdensUninhabitedThreshold (" + popdensUninhabitedThreshold + ") must be smaller than popdensHalf(" + popdensHalf + ").")
    }

    if (integrationMaxSteps < 5) {
      logger.error("integrationMaxSteps (" + integrationMaxSteps + ") must be at least 5, otherwise results get numerically unreliable.")
    }

    if (integrationMaxSteps > 100) {
      logger.warn("integrationMaxSteps (" + integrationMaxSteps + ") is bigger than 100. This slows down computation a lot.")
    }

    if (dikeRaisingStartYear < initialState.startTime) {
      logger.warn("dikeRaisingStartYear (" + dikeRaisingStartYear + ") is before diva.startTime (" + initialState.startTime + "). Dikes are initialised in the first timestep and raised from then on.")
    }

    if (dikeRaisingStartYear > initialState.endTime) {
      logger.warn("dikeRaisingStartYear (" + dikeRaisingStartYear + ") is after diva.endTime (" + initialState.endTime + "). Dikes are initialised but not raised.")
    }
    /*
    if ((dikeRaisingMode == DIAZ_DIKES) && (retreatMode != DIAZ_RETREAT)) {
      logger.error("dikeRaisingMode DIAZ_DIKES requires retreatMode DIAZ_RETREAT")
    }

    if ((dikeRaisingMode != DIAZ_DIKES) && (retreatMode == DIAZ_RETREAT)) {
      logger.error("retreatMode DIAZ_RETREAT requires dikeRaisingMode DIAZ_DIKES")
    }
    */

    if ((dikeRaisingMode == OPTIMAL_DIKE_TRAJECTORY) && (retreatMode != NONE)) {
      logger.error("dikeRaisingMode OPTIMAL_DIKE_TRAJEKTORY does not work with retreatMode different from NONE")
    }

  }

  private def getCurrentExposure(cls: Cls) {
    cls.czs.foreach { cz => getCurrentExposure(cz, cls) }
  }

  private def getCurrentExposure(cz: Cz, cls: Cls) {
    cz.data.area_below_.keys.foreach { e: Float => cz.data.area_below_(e) = cz.coastalPlane.areaExposure.cumulativeExposure(e + cls.data.rslr).asInstanceOf[Float] }
    cz.data.area_below_h.keys.foreach { rp: Int => cz.data.area_below_h(rp) = cz.coastalPlane.areaExposure.cumulativeExposureAttenuation(cls.floodHazard.returnPeriodHeight(rp)).asInstanceOf[Float] }
    cz.data.assets_below_.keys.foreach { e: Float => cz.data.assets_below_(e) = cz.coastalPlane.assetsExposure.cumulativeExposure(e + cls.data.rslr).asInstanceOf[Float] }
    cz.data.assets_developable_below_.keys.foreach { e: Float => cz.data.assets_developable_below_(e) = cz.coastalPlane.assetsExposureDevelopable.cumulativeExposure(e + cls.data.rslr).asInstanceOf[Float] }
    cz.data.assets_developed_below_.keys.foreach { e: Float => cz.data.assets_developed_below_(e) = cz.coastalPlane.assetsExposureDeveloped.cumulativeExposure(e + cls.data.rslr).asInstanceOf[Float] }
    cz.data.assets_below_h.keys.foreach { rp: Int => cz.data.assets_below_h(rp) = cz.coastalPlane.assetsExposure.cumulativeExposureAttenuation(cls.floodHazard.returnPeriodHeight(rp)).asInstanceOf[Float] }
    cz.data.pop_below_.keys.foreach { e: Float => cz.data.pop_below_(e) = cz.coastalPlane.populationExposure.cumulativeExposure(e + cls.data.rslr).asInstanceOf[Float] }
    cz.data.pop_below_h.keys.foreach { rp: Int => cz.data.pop_below_h(rp) = cz.coastalPlane.populationExposure.cumulativeExposureAttenuation((cls.floodHazard.returnPeriodHeight(rp))).asInstanceOf[Float] }
  }

  // TODO: besserer Name!!
  //private def pointwiseProduct(f: Double => Double, g: Double => Double)(x: Double): Double = {
  //f(x) * g(x)
  //}

  private def getCurrentFloodimpacts(cls: Cls) {

    val impacts = impactComputation.getCurrentImpacts(cls)
    cls.data.par = impacts._1
    cls.data.seafloodcost = impacts._2

    cls.czs.foreach { cz =>
      {
        cz.data.seafloodcost_h.keys.foreach { rp: Int => cz.data.seafloodcost_h(rp) = cz.coastalPlane.assetsExposure.cumulativeDamageWithDike(cls.data.seadike_height, cls.data.seadikebase_elevation)(cls.floodHazard.returnPeriodHeight(rp)).asInstanceOf[Float] }
        cz.data.seafloodcost_min = cz.coastalPlane.assetsExposure.cumulativeDamageWithDike(cls.data.seadike_height, cls.data.seadikebase_elevation)(cls.data.seadike_height + 0.001).asInstanceOf[Float]
        cz.data.seafloodcost_relative = if (cz.data.localgdp > 0) (cz.data.seafloodcost / cz.data.localgdp) else 0f
      }
    }

    cls.data.seafloodcost_relative = if (cls.data.localgdp > 0) (cls.data.seafloodcost / cls.data.localgdp) else 0f

  }

  private def accumulateInits(state: divaState) {

    var clss: List[Cls] = state.getFeatures[Cls]
    clss.par.foreach {
      (cls: Cls) =>
        {
          cls.data.landloss_initial_land_below_S1 = cls.czs.foldLeft(0f)((sum, cz) => sum + cz.data.landloss_initial_land_below_S1)
        }
    }

    var cities: List[City] = state.getFeatures[City]
    cities.par.foreach {
      (city: City) =>
        {
          city.data.seadike_height_initial = city.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0) cls.data.seadike_height_initial * cls.data.length else 0)) / city.data.coastlength
          city.data.seadike_height_initial = divideControlled(city.data.seadike_height_initial, city.data.length_protected)
        }
    }

    var deltas: List[Delta] = state.getFeatures[Delta]
    deltas.par.foreach {
      (delta: Delta) =>
        {
          delta.data.seadike_height_initial = delta.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0) cls.data.seadike_height_initial * cls.data.length else 0)) / delta.data.coastlength
          delta.data.seadike_height_initial = divideControlled(delta.data.seadike_height_initial, delta.data.length_protected)
        }
    }

    var admins: List[Admin] = state.getFeatures[Admin]
    admins.par.foreach {
      (admin: Admin) =>
        {
          admin.data.seadike_height_initial = admin.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0) cls.data.seadike_height_initial * cls.data.length else 0))
          admin.data.seadike_height_initial = divideControlled(admin.data.seadike_height_initial, admin.data.length_protected)
          admin.data.landloss_initial_land_below_S1 = admin.czs.foldLeft(0f)((sum, cz) => sum + cz.data.landloss_initial_land_below_S1)
        }
    }

    var countries: List[Country] = state.getFeatures[Country]
    countries.par.foreach {
      (country: Country) =>
        {
          country.data.seadike_height_initial = country.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0) cls.data.seadike_height_initial * cls.data.length else 0))
          country.data.seadike_height_initial = divideControlled(country.data.seadike_height_initial, country.data.length_protected)
          country.data.landloss_initial_land_below_S1 = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.landloss_initial_land_below_S1)
        }
    }

    var globals: List[Global] = state.getFeatures[Global]
    globals.foreach {
      (global: Global) =>
        {
          global.data.seadike_height_initial = global.countries.foldLeft(0f)((sum, country) => sum + (if (country.data.length_protected > 0) country.data.seadike_height_initial * country.data.length_protected else 0))
          global.data.seadike_height_initial = divideControlled(global.data.seadike_height_initial, global.data.length_protected)
          global.data.landloss_initial_land_below_S1 = global.countries.foldLeft(0f)((sum, country) => sum + country.data.landloss_initial_land_below_S1)
        }
    }
  }

  private def accumulate(state: divaState) {
    var clss: List[Cls] = state.getFeatures[Cls]
    clss.par.foreach {
      (cls: Cls) =>
        {
          cls.data.landloss_submergence = cls.czs.foldLeft(0f)((sum, cz) => sum + cz.data.landloss_submergence)
          cls.data.landlosscost_submergence = cls.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.landlosscost_submergence))
          cls.data.migration_submergence = cls.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.migration_submergence))
          cls.data.migrationcost_submergence = cls.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.migrationcost_submergence))
          cls.data.migration_retreat = cls.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.migration_retreat))
          cls.data.migrationcost_retreat = cls.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.migrationcost_retreat))
          cls.data.setbackzone_population = cls.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.setbackzone_population))
          cls.data.setbackzone_assets = cls.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.setbackzone_assets))
          cls.data.setbackzone_area = cls.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.setbackzone_area))
          cls.data.population_correction = cls.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.population_correction))
          cls.data.area_below_.keys.foreach { e: Float => cls.data.area_below_(e) = cls.czs.foldLeft(0f)((sum, cz) => sum + cz.data.area_below_(e)) }
          cls.data.assets_below_.keys.foreach { e: Float => cls.data.assets_below_(e) = cls.czs.foldLeft(0f)((sum, cz) => sum + cz.data.assets_below_(e)) }
          cls.data.assets_developable_below_.keys.foreach { e: Float => cls.data.assets_developable_below_(e) = cls.czs.foldLeft(0f)((sum, cz) => sum + cz.data.assets_developable_below_(e)) }
          cls.data.assets_developed_below_.keys.foreach { e: Float => cls.data.assets_developed_below_(e) = cls.czs.foldLeft(0f)((sum, cz) => sum + cz.data.assets_developed_below_(e)) }
          cls.data.pop_below_.keys.foreach { e: Float => cls.data.pop_below_(e) = cls.czs.foldLeft(0f)((sum, cz) => sum + cz.data.pop_below_(e)) }
          cls.data.area_below_h.keys.foreach { rp: Int => cls.data.area_below_h(rp) = cls.czs.foldLeft(0f)((sum, cz) => sum + cz.data.area_below_h(rp)) }
          cls.data.assets_below_h.keys.foreach { rp: Int => cls.data.assets_below_h(rp) = cls.czs.foldLeft(0f)((sum, cz) => sum + cz.data.assets_below_h(rp)) }
          cls.data.pop_below_h.keys.foreach { rp: Int => cls.data.pop_below_h(rp) = cls.czs.foldLeft(0f)((sum, cz) => sum + cz.data.pop_below_h(rp)) }
          cls.data.seafloodcost_h.keys.foreach { rp: Int => cls.data.seafloodcost_h(rp) = cls.czs.foldLeft(0f)((sum, cz) => sum + cz.data.seafloodcost_h(rp)) }
          cls.data.seafloodcost_min = cls.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.seafloodcost_min))
        }
    }

    var cities: List[City] = state.getFeatures[City]
    cities.par.foreach {
      (city: City) =>
        {
          city.data.par = city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.par / 1000))
          city.data.length_protected = city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.length_protected))
          city.data.seadike_cost = city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seadike_cost))
          city.data.seafloodcost = city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seafloodcost / 1000000))
          city.data.seadike_maintenance_cost = city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seadike_maintenance_cost))
          city.data.seadike_increase_maintenance_cost = city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seadike_increase_maintenance_cost))
          city.data.seadike_height = city.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.seadike_height * cls.data.length else 0))
          city.data.seadike_height = divideControlled(city.data.seadike_height, city.data.length_protected)
          city.data.protlevel_implemented = city.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.protlevel_implemented * cls.data.length else 0))
          city.data.protlevel_implemented = divideControlled(city.data.protlevel_implemented, city.data.length_protected)
          city.data.protlevel_implemented_before_dikeraise = city.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.protlevel_implemented_before_dikeraise * cls.data.length else 0))
          city.data.protlevel_implemented_before_dikeraise = divideControlled(city.data.protlevel_implemented_before_dikeraise, city.data.length_protected)
          city.data.area_below_.keys.foreach { e: Float => city.data.area_below_(e) = city.clss.foldLeft(0f)((sum, cls) => sum + cls.data.area_below_(e)) }
          city.data.assets_below_.keys.foreach { e: Float => city.data.assets_below_(e) = city.clss.foldLeft(0f)((sum, cls) => sum + cls.data.assets_below_(e)) / 1000000 }
          city.data.assets_developable_below_.keys.foreach { e: Float => city.data.assets_developable_below_(e) = city.clss.foldLeft(0f)((sum, cls) => sum + cls.data.assets_developable_below_(e)) / 1000000 }
          city.data.assets_developed_below_.keys.foreach { e: Float => city.data.assets_developed_below_(e) = city.clss.foldLeft(0f)((sum, cls) => sum + cls.data.assets_developed_below_(e)) / 1000000 }
          city.data.pop_below_.keys.foreach { e: Float => city.data.pop_below_(e) = city.clss.foldLeft(0f)((sum, cls) => sum + cls.data.pop_below_(e)) / 1000 }
          city.data.area_below_h.keys.foreach { rp: Int => city.data.area_below_h(rp) = city.clss.foldLeft(0f)((sum, cls) => sum + cls.data.area_below_h(rp)) }
          city.data.assets_below_h.keys.foreach { rp: Int => city.data.assets_below_h(rp) = city.clss.foldLeft(0f)((sum, cls) => sum + cls.data.assets_below_h(rp)) / 1000000 }
          city.data.pop_below_h.keys.foreach { rp: Int => city.data.pop_below_h(rp) = city.clss.foldLeft(0f)((sum, cls) => sum + cls.data.pop_below_h(rp)) / 1000 }
          city.data.seafloodcost_h.keys.foreach { rp: Int => city.data.seafloodcost_h(rp) = city.clss.foldLeft(0f)((sum, cls) => sum + cls.data.seafloodcost_h(rp)) / 1000000 }
          city.data.seafloodcost_relative = if (city.data.localgdp > 0) { city.data.seafloodcost / city.data.localgdp } else 0
        }
    }

    var deltas: List[Delta] = state.getFeatures[Delta]
    deltas.par.foreach {
      (delta: Delta) =>
        {
          delta.data.par = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.par / 1000))
          delta.data.length_protected = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.length_protected))
          delta.data.seadike_cost = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seadike_cost))
          delta.data.seafloodcost = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seafloodcost / 1000000))
          delta.data.seadike_maintenance_cost = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seadike_maintenance_cost))
          delta.data.seadike_increase_maintenance_cost = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seadike_increase_maintenance_cost))
          delta.data.protlevel_implemented = delta.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.protlevel_implemented * cls.data.length else 0)) / delta.data.coastlength
          delta.data.landloss_submergence = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.landloss_submergence))
          delta.data.landlosscost_submergence = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.landlosscost_submergence)) / 1000000
          delta.data.seadike_height = delta.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.seadike_height * cls.data.length else 0))
          delta.data.seadike_height = divideControlled(delta.data.seadike_height, delta.data.length_protected)
          delta.data.protlevel_implemented = delta.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.protlevel_implemented * cls.data.length else 0))
          delta.data.protlevel_implemented = divideControlled(delta.data.protlevel_implemented, delta.data.length_protected)
          delta.data.protlevel_implemented_before_dikeraise = delta.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.protlevel_implemented_before_dikeraise * cls.data.length else 0))
          delta.data.protlevel_implemented_before_dikeraise = divideControlled(delta.data.protlevel_implemented_before_dikeraise, delta.data.length_protected)
          delta.data.migration_retreat = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.migration_retreat / 1000))
          delta.data.migrationcost_retreat = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.migrationcost_retreat / 1000000))
          delta.data.migration_submergence = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.migration_submergence / 1000))
          delta.data.migrationcost_submergence = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.migrationcost_submergence / 1000000))
          delta.data.area_below_.keys.foreach { e: Float => delta.data.area_below_(e) = delta.clss.foldLeft(0f)((sum, cls) => sum + cls.data.area_below_(e)) }
          delta.data.assets_below_.keys.foreach { e: Float => delta.data.assets_below_(e) = delta.clss.foldLeft(0f)((sum, cls) => sum + cls.data.assets_below_(e)) / 1000000 }
          delta.data.assets_developable_below_.keys.foreach { e: Float => delta.data.assets_developable_below_(e) = delta.clss.foldLeft(0f)((sum, cls) => sum + cls.data.assets_developable_below_(e)) / 1000000 }
          delta.data.assets_developed_below_.keys.foreach { e: Float => delta.data.assets_developed_below_(e) = delta.clss.foldLeft(0f)((sum, cls) => sum + cls.data.assets_developed_below_(e)) / 1000000 }
          delta.data.pop_below_.keys.foreach { e: Float => delta.data.pop_below_(e) = delta.clss.foldLeft(0f)((sum, cls) => sum + cls.data.pop_below_(e)) / 1000 }
          delta.data.area_below_h.keys.foreach { rp: Int => delta.data.area_below_h(rp) = delta.clss.foldLeft(0f)((sum, cls) => sum + cls.data.area_below_h(rp)) }
          delta.data.assets_below_h.keys.foreach { rp: Int => delta.data.assets_below_h(rp) = delta.clss.foldLeft(0f)((sum, cls) => sum + cls.data.assets_below_h(rp)) / 1000000 }
          delta.data.pop_below_h.keys.foreach { rp: Int => delta.data.pop_below_h(rp) = delta.clss.foldLeft(0f)((sum, cls) => sum + cls.data.pop_below_h(rp)) / 1000 }
          delta.data.seafloodcost_h.keys.foreach { rp: Int => delta.data.seafloodcost_h(rp) = delta.clss.foldLeft(0f)((sum, cls) => sum + cls.data.seafloodcost_h(rp)) / 1000000 }
          delta.data.seafloodcost_relative = if (delta.data.localgdp > 0) { delta.data.seafloodcost / delta.data.localgdp } else 0
        }
    }

    var admins: List[Admin] = state.getFeatures[Admin]
    admins.par.foreach {
      (admin: Admin) =>
        {
          admin.data.population_correction = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.population_correction))
          admin.data.par = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.par / 1000))
          admin.data.length_protected = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.length_protected))
          admin.data.seadike_cost = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seadike_cost))
          admin.data.seafloodcost = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.seafloodcost)) / 1000000
          admin.data.seadike_maintenance_cost = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seadike_maintenance_cost))
          admin.data.seadike_increase_maintenance_cost = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seadike_increase_maintenance_cost))
          admin.data.seadike_height = admin.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.seadike_height * cls.data.length else 0))
          admin.data.seadike_height = divideControlled(admin.data.seadike_height, admin.data.length_protected)
          admin.data.protlevel_implemented = admin.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.protlevel_implemented * cls.data.length else 0))
          admin.data.protlevel_implemented = divideControlled(admin.data.protlevel_implemented, admin.data.length_protected)
          admin.data.protlevel_implemented_before_dikeraise = admin.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.protlevel_implemented_before_dikeraise * cls.data.length else 0))
          admin.data.protlevel_implemented_before_dikeraise = divideControlled(admin.data.protlevel_implemented_before_dikeraise, admin.data.length_protected)
          admin.data.landloss_submergence = admin.czs.foldLeft(0f)((sum, cz) => sum + cz.data.landloss_submergence)
          admin.data.landlosscost_submergence = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.landlosscost_submergence)) / 1000000
          admin.data.migration_retreat = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.migration_retreat)) / 1000
          admin.data.migrationcost_retreat = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.migrationcost_retreat)) / 1000000
          admin.data.migration_submergence = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.migration_submergence)) / 1000
          admin.data.migrationcost_submergence = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.migrationcost_submergence / 1000000))
          admin.data.setbackzone_area = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.setbackzone_area))
          admin.data.setbackzone_population = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.setbackzone_population)) / 1000
          admin.data.setbackzone_assets = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.setbackzone_assets)) / 1000000
          admin.data.setback_length = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.setback_length))
          admin.data.setbackzone_administrative_cost = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.setbackzone_administrative_cost)) / 1000000
          admin.data.area_below_.keys.foreach { e: Float => admin.data.area_below_(e) = admin.czs.foldLeft(0f)((sum, cz) => sum + cz.data.area_below_(e)) }
          admin.data.assets_below_.keys.foreach { e: Float => admin.data.assets_below_(e) = admin.czs.foldLeft(0f)((sum, cz) => sum + cz.data.assets_below_(e)) / 1000000 }
          admin.data.assets_developable_below_.keys.foreach { e: Float => admin.data.assets_developable_below_(e) = admin.czs.foldLeft(0f)((sum, cz) => sum + cz.data.assets_developable_below_(e)) / 1000000 }
          admin.data.assets_developed_below_.keys.foreach { e: Float => admin.data.assets_developed_below_(e) = admin.czs.foldLeft(0f)((sum, cz) => sum + cz.data.assets_developed_below_(e)) / 1000000 }
          admin.data.pop_below_.keys.foreach { e: Float => admin.data.pop_below_(e) = admin.czs.foldLeft(0f)((sum, cz) => sum + cz.data.pop_below_(e)) / 1000 }
          admin.data.area_below_h.keys.foreach { rp: Int => admin.data.area_below_h(rp) = admin.czs.foldLeft(0f)((sum, cz) => sum + cz.data.area_below_h(rp)) }
          admin.data.assets_below_h.keys.foreach { rp: Int => admin.data.assets_below_h(rp) = admin.czs.foldLeft(0f)((sum, cz) => sum + cz.data.assets_below_h(rp)) / 1000000 }
          admin.data.pop_below_h.keys.foreach { rp: Int => admin.data.pop_below_h(rp) = admin.czs.foldLeft(0f)((sum, cz) => sum + cz.data.pop_below_h(rp)) / 1000 }
          admin.data.seafloodcost_h.keys.foreach { rp: Int => admin.data.seafloodcost_h(rp) = admin.czs.foldLeft(0f)((sum, cz) => sum + cz.data.seafloodcost_h(rp)) / 1000000 }
          admin.data.seafloodcost_relative = if (admin.data.localgdp > 0) { admin.data.seafloodcost / admin.data.localgdp } else 0
        }
    }

    var countries: List[Country] = state.getFeatures[Country]
    countries.par.foreach {
      (country: Country) =>
        {
          country.data.population_correction = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.population_correction))
          country.data.par = country.admins.foldLeft(0f)((sum, admins) => sum + (admins.data.par))
          country.data.length_protected = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.length_protected))
          country.data.seadike_cost = country.clss.foldLeft(0f)((sum, cls) => sum + cls.data.seadike_cost)
          country.data.seafloodcost = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.seafloodcost))
          country.data.seadike_maintenance_cost = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seadike_maintenance_cost))
          country.data.seadike_increase_maintenance_cost = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.seadike_increase_maintenance_cost))
          country.data.seadike_height = country.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.seadike_height * cls.data.length else 0))
          country.data.seadike_height = divideControlled(country.data.seadike_height, country.data.length_protected)
          country.data.protlevel_implemented = country.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.protlevel_implemented * cls.data.length else 0))
          country.data.protlevel_implemented = divideControlled(country.data.protlevel_implemented, country.data.length_protected)
          country.data.protlevel_implemented_before_dikeraise = country.clss.foldLeft(0f)((sum, cls) => sum + (if (cls.data.length_protected > 0f) cls.data.protlevel_implemented_before_dikeraise * cls.data.length else 0))
          country.data.protlevel_implemented_before_dikeraise = divideControlled(country.data.protlevel_implemented_before_dikeraise, country.data.length_protected)
          country.data.landloss_submergence = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.landloss_submergence)
          country.data.landlosscost_submergence = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.landlosscost_submergence))
          country.data.migration_retreat = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.migration_retreat))
          country.data.migrationcost_retreat = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.migrationcost_retreat))
          country.data.migration_submergence = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.migration_submergence))
          country.data.migrationcost_submergence = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.migrationcost_submergence)
          country.data.setbackzone_area = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.setbackzone_area)
          country.data.setbackzone_assets = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.setbackzone_assets))
          country.data.setbackzone_population = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.setbackzone_population))
          country.data.setback_length = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.setback_length))
          country.data.setbackzone_administrative_cost = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.setbackzone_administrative_cost))
          country.data.area_below_.keys.foreach { e: Float => country.data.area_below_(e) = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.area_below_(e)) }
          country.data.assets_below_.keys.foreach { e: Float => country.data.assets_below_(e) = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.assets_below_(e)) }
          country.data.assets_developable_below_.keys.foreach { e: Float => country.data.assets_developable_below_(e) = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.assets_developable_below_(e)) }
          country.data.assets_developed_below_.keys.foreach { e: Float => country.data.assets_developed_below_(e) = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.assets_developed_below_(e)) }
          country.data.pop_below_.keys.foreach { e: Float => country.data.pop_below_(e) = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.pop_below_(e)) }
          country.data.area_below_h.keys.foreach { rp: Int => country.data.area_below_h(rp) = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.area_below_h(rp)) }
          country.data.assets_below_h.keys.foreach { rp: Int => country.data.assets_below_h(rp) = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.assets_below_h(rp))) }
          country.data.pop_below_h.keys.foreach { rp: Int => country.data.pop_below_h(rp) = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.pop_below_h(rp)) }
          country.data.seafloodcost_h.keys.foreach { rp: Int => country.data.seafloodcost_h(rp) = country.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.seafloodcost_h(rp))) }
          country.data.seafloodcost_relative = country.data.seafloodcost / country.data.gdp
        }
    }

    var regions: List[Region] = state.getFeatures[Region]
    regions.par.foreach {
      (region: Region) =>
        {
          region.data.population_correction = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.population_correction)
          region.data.par = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.par)
          region.data.length_protected = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.length_protected))
          region.data.seadike_cost = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.seadike_cost)
          region.data.seafloodcost = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.seafloodcost)
          region.data.seadike_maintenance_cost = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.seadike_maintenance_cost))
          region.data.seadike_increase_maintenance_cost = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.seadike_increase_maintenance_cost))
          region.data.seadike_height = region.admins.foldLeft(0f)((sum, admin) => sum + (if (admin.data.length_protected > 0f) admin.data.seadike_height * admin.data.length_protected else 0))
          region.data.seadike_height = divideControlled(region.data.seadike_height, region.data.length_protected)
          region.data.protlevel_implemented = region.admins.foldLeft(0f)((sum, admin) => sum + (if (admin.data.length_protected > 0f) admin.data.protlevel_implemented * admin.data.length_protected else 0))
          region.data.protlevel_implemented = divideControlled(region.data.protlevel_implemented, region.data.length_protected)
          region.data.protlevel_implemented_before_dikeraise = region.admins.foldLeft(0f)((sum, admin) => sum + (if (admin.data.length_protected > 0f) admin.data.protlevel_implemented_before_dikeraise * admin.data.length_protected else 0))
          region.data.protlevel_implemented_before_dikeraise = divideControlled(region.data.protlevel_implemented_before_dikeraise, region.data.length_protected)
          region.data.landloss_submergence = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.landloss_submergence)
          region.data.landlosscost_submergence = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.landlosscost_submergence)
          region.data.migration_retreat = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.migration_retreat)
          region.data.migrationcost_retreat = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.migrationcost_retreat)
          region.data.migration_submergence = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.migration_submergence)
          region.data.migrationcost_submergence = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.migrationcost_submergence)
          region.data.area_below_.keys.foreach { e: Float => region.data.area_below_(e) = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.area_below_(e)) }
          region.data.assets_below_.keys.foreach { e: Float => region.data.assets_below_(e) = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.assets_below_(e)) }
          region.data.assets_developed_below_.keys.foreach { e: Float => region.data.assets_developed_below_(e) = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.assets_developed_below_(e)) }
          region.data.assets_developable_below_.keys.foreach { e: Float => region.data.assets_developable_below_(e) = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.assets_developable_below_(e)) }
          region.data.pop_below_.keys.foreach { e: Float => region.data.pop_below_(e) = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.pop_below_(e)) }
          region.data.area_below_h.keys.foreach { rp: Int => region.data.area_below_h(rp) = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.area_below_h(rp)) }
          region.data.assets_below_h.keys.foreach { rp: Int => region.data.assets_below_h(rp) = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.assets_below_h(rp)) }
          region.data.pop_below_h.keys.foreach { rp: Int => region.data.pop_below_h(rp) = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.pop_below_h(rp)) }
          region.data.seafloodcost_h.keys.foreach { rp: Int => region.data.seafloodcost_h(rp) = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.seafloodcost_h(rp)) }
        }
    }

    var globals: List[Global] = state.getFeatures[Global]
    globals.foreach {
      (global: Global) =>
        {
          global.data.population_correction = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.population_correction))
          global.data.par = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.par))
          global.data.length_protected = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.length_protected))
          global.data.seadike_cost = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.seadike_cost))
          global.data.seafloodcost = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.seafloodcost))
          global.data.seadike_maintenance_cost = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.seadike_maintenance_cost))
          global.data.seadike_increase_maintenance_cost = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.seadike_increase_maintenance_cost))
          global.data.seadike_height = global.countries.foldLeft(0f)((sum, country) => sum + (if (country.data.length_protected > 0f) country.data.seadike_height * country.data.length_protected else 0))
          global.data.seadike_height = divideControlled(global.data.seadike_height, global.data.length_protected)
          global.data.protlevel_implemented = global.countries.foldLeft(0f)((sum, country) => sum + (if (country.data.length_protected > 0f) country.data.protlevel_implemented * country.data.length_protected else 0))
          global.data.protlevel_implemented = divideControlled(global.data.protlevel_implemented, global.data.length_protected)
          global.data.protlevel_implemented_before_dikeraise = global.countries.foldLeft(0f)((sum, country) => sum + (if (country.data.length_protected > 0f) country.data.protlevel_implemented_before_dikeraise * country.data.length_protected else 0))
          global.data.protlevel_implemented_before_dikeraise = divideControlled(global.data.protlevel_implemented_before_dikeraise, global.data.length_protected)
          global.data.landloss_submergence = global.countries.foldLeft(0f)((sum, country) => sum + country.data.landloss_submergence)
          global.data.landlosscost_submergence = global.countries.foldLeft(0f)((sum, country) => sum + country.data.landlosscost_submergence)
          global.data.migration_retreat = global.countries.foldLeft(0f)((sum, country) => sum + country.data.migration_retreat)
          global.data.migrationcost_retreat = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.migrationcost_retreat))
          global.data.migration_submergence = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.migration_submergence))
          global.data.migrationcost_submergence = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.migrationcost_submergence))
          global.data.riverdike_cost = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.riverdike_cost))
          global.data.riverdike_maintenance_cost = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.riverdike_maintenance_cost))
          global.data.riverdike_increase_maintenance_cost = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.riverdike_increase_maintenance_cost))
          global.data.riverdike_length = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.riverdike_length))
          global.data.setback_length = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.setback_length))
          global.data.setbackzone_administrative_cost = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.setbackzone_administrative_cost))
          global.data.area_below_.keys.foreach { e: Float => global.data.area_below_(e) = global.countries.foldLeft(0f)((sum, country) => sum + country.data.area_below_(e)) }
          global.data.assets_below_.keys.foreach { e: Float => global.data.assets_below_(e) = global.countries.foldLeft(0f)((sum, country) => sum + country.data.assets_below_(e)) }
          global.data.assets_developable_below_.keys.foreach { e: Float => global.data.assets_developable_below_(e) = global.countries.foldLeft(0f)((sum, country) => sum + country.data.assets_developable_below_(e)) }
          global.data.assets_developed_below_.keys.foreach { e: Float => global.data.assets_developed_below_(e) = global.countries.foldLeft(0f)((sum, country) => sum + country.data.assets_developed_below_(e)) }
          global.data.pop_below_.keys.foreach { e: Float => global.data.pop_below_(e) = global.countries.foldLeft(0f)((sum, country) => sum + country.data.pop_below_(e)) }
          global.data.area_below_h.keys.foreach { rp: Int => global.data.area_below_h(rp) = global.countries.foldLeft(0f)((sum, country) => sum + country.data.area_below_h(rp)) }
          global.data.assets_below_h.keys.foreach { rp: Int => global.data.assets_below_h(rp) = global.countries.foldLeft(0f)((sum, country) => sum + country.data.assets_below_h(rp)) }
          global.data.pop_below_h.keys.foreach { rp: Int => global.data.pop_below_h(rp) = global.countries.foldLeft(0f)((sum, country) => sum + country.data.pop_below_h(rp)) }
          global.data.seafloodcost_h.keys.foreach { rp: Int => global.data.seafloodcost_h(rp) = global.countries.foldLeft(0f)((sum, country) => sum + country.data.seafloodcost_h(rp)) }
        }
    }
  }

  private def dikeInitialisation(cls: Cls, state: divaState) {
    cls.data.protlevel_target = adaptationManager.computeTargetProtection(cls, state, true)

    if ((cls.data.protlevel_target > 0.0) && (cls.data.protlevel_target < 1.0)) {
      // Case 1: we compute a protection level > 0 but below 1: below protection level 1.0 is ignored
      logger.info("cls " + cls.locationid + " has protection-level " + cls.data.protlevel_target + " in year " + state.time + " which is ignored")
      cls.coastalPlane.dikeHeigth = 0f
    } else if (cls.data.protlevel_target >= 1.0) {
      // Case 2: There is no dike, but the target protection is high enough to construct one
      constructDike(cls, state.time, (dikeInitilisationMode == DEMAND_FOR_SAFETY_ORIGINAL_ONLY_SED))
    } else {
      // Case 3: protlevel == 0 => no Dike
      cls.coastalPlane.dikeHeigth = 0f
    }

    cls.data.seadike_height_initial = cls.coastalPlane.dikeHeigth
    // annual sea dike maintenance costs are 1% of the cost of the total dike Heigth (i.e. the capital stock)
    cls.data.seadike_maintenance_cost = 0.01f * cls.data.seadike_cost
    computeImplementedProtection(cls, state)

  }

  private def dikeRaising(cls: Cls, state: divaState) {

    computeImplementedProtection(cls, state, true)
    cls.data.protlevel_target = adaptationManager.computeTargetProtection(cls, state, (state.time < dikeRaisingStartYear))

    if (!cls.data.retreat) {
      val slrHorizon: Float = computeSLRHorizon(cls, state.time, state.timeStep, state.endTime)

      val targetDikeHeight: Float =
        if (cls.data.protlevel_target == 0) {
          0f
        } else {
          // This case implements the do not raise the dike option.
          if (cls.data.protlevel_target == cls.data.protlevel_implemented_before_dikeraise) {
            cls.data.seadike_height
          } else {
            if (dikeRaisingMode != DEMAND_FOR_SAFETY_ORIGINAL_ONLY_SED)
              cls.floodHazard.returnPeriodHeight(cls.data.protlevel_target).asInstanceOf[Float] + (slrHorizon - cls.data.rslr) - cls.data.seadikebase_elevation
            else
              (cls.floodHazard.returnPeriodHeight(cls.data.protlevel_target).asInstanceOf[Float] - cls.data.rslr) - cls.data.seadikebase_elevation
          }
        }

      if ((cls.data.protlevel_target > 0.0) && (cls.data.protlevel_target < 1.0) && (cls.coastalPlane.dikeHeigth == 0)) {
        // Case 1: There is no dike, and target protection is not high enough to construct one (but there is target protection)
        logger.info("cls " + cls.locationid + " has protection-level " + cls.data.protlevel_target + " in year " + state.time + " which is ignored")
        cls.data.seadike_cost = 0f
      } else if ((cls.data.protlevel_target > 1.0) && (cls.coastalPlane.dikeHeigth == 0f)) {
        // Case 2: There is no dike, but the target protection is high enough to construct one
        constructDike(cls, state.time, (dikeRaisingMode == DEMAND_FOR_SAFETY_ORIGINAL_ONLY_SED))
      } else if ((cls.data.protlevel_target > 0.0) && (cls.data.protlevel_target < 1.0) && (cls.coastalPlane.dikeHeigth > 0f)) {
        // Case 3: There is already a dike, but target protection is not high enough to keep it ... so it is overtopped and disappears
        logger.info("cls " + cls.locationid + " has protection-level " + cls.data.protlevel_target + " in year " + state.time + " which is ignored. Dike is abandoned.")
        abandonDike(cls, state.time)
      } else if ((cls.data.protlevel_target > 1.0) && (targetDikeHeight >= cls.coastalPlane.dikeHeigth)) {
        // Case 4: There is already a dike and it has to be raised
        raiseDike(cls, targetDikeHeight)
      } else if ((cls.data.protlevel_target == 0.0) || (targetDikeHeight < cls.coastalPlane.dikeHeigth)) {
        // Case 5: There is already a dike and it has to be lowered
        lowerDike(cls, targetDikeHeight)
      } else {
        // Case 6: No Dike building at all => no Cost.
        cls.data.seadike_cost = 0f
      }
      cls.data.seadike_cost = cls.data.seadike_cost / state.timeStep

    }
    computeImplementedProtection(cls, state)

    if (cls.data.length_protected > 0f) {
      cls.data.seadike_maintenance_cost = adaptationManager.getDikeMaintenanceCost(cls, (cls.data.seadike_height - cls.data.seadikebase_elevation))
      cls.data.seadike_increase_maintenance_cost = adaptationManager.getDikeMaintenanceCost(cls, (cls.data.seadike_height - cls.data.seadike_height_initial))
    } else {
      cls.data.seadike_maintenance_cost = 0.0f
      cls.data.seadike_increase_maintenance_cost = 0.0f
    }

  }

  private def dikeRaisingPerCountry(clss: Array[Cls], country: Country, state: divaState) {
    adaptationManager.computeTargetProtectionPerCountry(clss, country, state, (state.time < dikeRaisingStartYear))
    country.clss.par.foreach(cls => {
      computeImplementedProtection(cls, state, true)
      val targetDikeHeight: Float = (cls.floodHazard.returnPeriodHeight(cls.data.protlevel_target).asInstanceOf[Float] - cls.data.rslr) - cls.data.seadikebase_elevation

      if ((cls.data.protlevel_target > 1.0) && (cls.data.protlevel_implemented > 0.0)) {
        raiseDike(cls, targetDikeHeight)
      } else if ((cls.data.protlevel_target >= 1.0) && (cls.data.protlevel_implemented == 0.0)) {
        constructDike(cls, state.time)
      } else if ((cls.data.protlevel_target < 1.0) && (cls.data.protlevel_implemented > 0.0)) {
        // Case 3: There is already a dike, but target protection is not high enough to keep it ... so it is overtopped and disappears
        logger.info("cls " + cls.locationid + " has protection-level " + cls.data.protlevel_target + " in year " + state.time + " which is ignored. Dike is abandoned.")
        abandonDike(cls, state.time)
      } else {
        // No Dike building at all => no Cost.
        cls.data.seadike_cost = 0f
      }
      cls.data.seadike_cost = cls.data.seadike_cost / state.timeStep
      computeImplementedProtection(cls, state)
      if (cls.data.length_protected > 0f) {
        cls.data.seadike_maintenance_cost = adaptationManager.getDikeMaintenanceCost(cls, (cls.data.seadike_height - cls.data.seadikebase_elevation))
        cls.data.seadike_increase_maintenance_cost = adaptationManager.getDikeMaintenanceCost(cls, (cls.data.seadike_height - cls.data.seadike_height_initial))
      } else {
        cls.data.seadike_maintenance_cost = 0.0f
        cls.data.seadike_increase_maintenance_cost = 0.0f
      }
    })
  }

  private def keepDikeInitialisation(cls: Cls, state: divaState) {
    computeImplementedProtection(cls, state, true)
    cls.data.protlevel_target = adaptationManager.computeTargetProtection(cls, state, true)
    if (!cls.data.retreat) {
      val targetDikeHeight: Float = cls.floodHazard.returnPeriodHeight(cls.data.protlevel_target).asInstanceOf[Float] - cls.data.seadikebase_elevation
      if ((cls.data.protlevel_target > 0.0) && (cls.data.protlevel_target < 1.0) && (cls.coastalPlane.dikeHeigth == 0f)) {
        // Case 1: There is no dike, and target protection is not high enough to construct one (but there is target protection)
        logger.info("cls " + cls.locationid + " has protection-level " + cls.data.protlevel_target + " in year " + state.time + " which is ignored")
        cls.data.seadike_cost = 0f
      } else if ((cls.data.protlevel_target > 1.0) && (targetDikeHeight >= cls.coastalPlane.dikeHeigth)) {
        // Case 2: There is a dike, and we have to raise it
        raiseDike(cls, targetDikeHeight)
      } else if ((cls.data.protlevel_target > 1.0) && (targetDikeHeight < cls.coastalPlane.dikeHeigth)) {
        // Case 3: There is a dike, and we have to lower it
        lowerDike(cls, targetDikeHeight)
      } else {
        // Case 4: No Dike building at all => no Cost.
        cls.data.seadike_cost = 0f
      }
      cls.data.seadike_height_initial = cls.data.seadike_height
      cls.data.seadike_cost = cls.data.seadike_cost / state.timeStep
    }
    computeImplementedProtection(cls, state)
    if (cls.data.length_protected > 0f) {
      cls.data.seadike_maintenance_cost = adaptationManager.getDikeMaintenanceCost(cls, cls.data.seadike_height - cls.data.seadikebase_elevation)
      cls.data.seadike_increase_maintenance_cost = adaptationManager.getDikeMaintenanceCost(cls, (cls.data.seadike_height - cls.data.seadikebase_elevation) - cls.data.seadike_height_initial)
    } else {
      cls.data.seadike_maintenance_cost = 0.0f
      cls.data.seadike_increase_maintenance_cost = 0.0f
    }
  }

  private def constructDike(cls: Cls, time: Long, ignoreSLR: Boolean = false) {
    cls.data.seadikebase_elevation = adaptationManager.computeDikePosition(cls, dikePositionMode)

    val lTargetHeigth: Float =
      if (ignoreSLR)
        (cls.floodHazard.returnPeriodHeight(cls.data.protlevel_target).asInstanceOf[Float] - cls.data.rslr) - cls.data.seadikebase_elevation
      else
        cls.floodHazard.returnPeriodHeight(cls.data.protlevel_target).asInstanceOf[Float] - cls.data.seadikebase_elevation

    cls.coastalPlane.dikeHeigth =
      if (lTargetHeigth < 0f) {
        0f
      } else {
        lTargetHeigth
      }
    // if the seadike would be so far away that it is at higher elevation than the dike should be, we dont build a dike at all
    if (cls.coastalPlane.dikeHeigth == 0) {
      cls.data.seadikebase_elevation = 0f
    } else {
      cls.data.seadike_construction_year = time.asInstanceOf[Int]
      cls.data.seadike_height = cls.coastalPlane.dikeHeigth
      cls.data.seadike_cost = adaptationManager.getDikeConstructionCost(cls, (cls.data.seadike_height - cls.data.seadikebase_elevation))
    }
  }

  private def abandonDike(cls: Cls, time: Long) {
    cls.data.seadike_cost = 0f
    cls.data.seadike_abandon_year = time.asInstanceOf[Int]
    cls.data.length_protected = 0f
  }

  private def raiseDike(cls: Cls, targetDikeHeight: Float) {

    val lTargetDikeHeight: Float =
      if (targetDikeHeight > cls.floodHazard.returnPeriodHeight(1000000).asInstanceOf[Float])
        cls.floodHazard.returnPeriodHeight(1000000).asInstanceOf[Float]
      else targetDikeHeight

    val additionalDikeHeigth: Float =
      if ((lTargetDikeHeight - cls.coastalPlane.dikeHeigth) < minimalDikeRaise)
        minimalDikeRaise
      else
        (lTargetDikeHeight - cls.coastalPlane.dikeHeigth)
    cls.coastalPlane.dikeHeigth += additionalDikeHeigth
    cls.data.seadike_height = cls.coastalPlane.dikeHeigth
    cls.data.seadike_cost = adaptationManager.getDikeConstructionCost(cls, additionalDikeHeigth)
  }

  private def lowerDike(cls: Cls, targetDikeHeight: Float) {

    val lTargetDikeHeight: Float =
      if (targetDikeHeight > cls.floodHazard.returnPeriodHeight(1000000).asInstanceOf[Float])
        cls.floodHazard.returnPeriodHeight(1000000).asInstanceOf[Float]
      else targetDikeHeight

    val excessiveDikeHeigth: Float =
      if ((cls.coastalPlane.dikeHeigth - lTargetDikeHeight) < minimalDikeRaise)
        minimalDikeRaise
      else
        (cls.coastalPlane.dikeHeigth - lTargetDikeHeight)

    cls.coastalPlane.dikeHeigth -= excessiveDikeHeigth
    if (cls.coastalPlane.dikeHeigth < 0f) {
      cls.coastalPlane.dikeHeigth = 0f
    }
    cls.data.seadike_height = cls.coastalPlane.dikeHeigth
    cls.data.seadike_cost = adaptationManager.getDikeConstructionCost(cls, -excessiveDikeHeigth)
  }

  private def computeImplementedProtection(cls: Cls, state: divaState, before: Boolean = false) {
    // Attention: If rslr is negative (happens in the first time step(s) with regular scenarios,
    // cls.data.seadike_height=0.0 can have a positive protection level
    // Therefore we need to check in there is a dike at all

    if (before)
      cls.data.protlevel_implemented_before_dikeraise =
        if ((cls.floodHazard.returnPeriod((cls.data.seadike_height + cls.data.seadikebase_elevation)) <= 1.0) || (cls.data.seadike_height == 0.0)) 0
        else cls.floodHazard.returnPeriod((cls.data.seadike_height + cls.data.seadikebase_elevation)).asInstanceOf[Float]
    else
      cls.data.protlevel_implemented =
        if ((cls.floodHazard.returnPeriod((cls.data.seadike_height + cls.data.seadikebase_elevation)) <= 1.0) || (cls.data.seadike_height == 0.0)) 0
        else cls.floodHazard.returnPeriod((cls.data.seadike_height + cls.data.seadikebase_elevation)).asInstanceOf[Float]

    if ((cls.data.protlevel_implemented <= 1.0f) && (cls.data.length_protected > 0f)) {
      logger.info("cls " + cls.locationid + " has implemented protection-level " + cls.floodHazard.returnPeriod((cls.data.seadike_height + cls.data.seadikebase_elevation)) + " in year " + state.time + " which is ignored. Dike is abandoned.")
      abandonDike(cls, state.time)
    }

    cls.data.length_protected = if (cls.data.protlevel_implemented > 1.0f) cls.data.length else 0f
  }

  private def computePopulationCorrection(cls: Cls) {

    if (populationCorrection) {
      val populationCorrectionHeigth: Float = cls.floodHazard.returnPeriodHeight(populationCorrectionReturnPeriod).asInstanceOf[Float]
      val populationCorrectionDistributionHeigth: Float = cls.floodHazard.returnPeriodHeight(populationCorrectionDistributionReturnPeriod).asInstanceOf[Float]
      if (populationCorrectionHeigth > cls.data.seadike_height) {
        cls.czs.foreach { cz =>
          cz.data.population_correction = cz.coastalPlane.populationCorrection(populationCorrectionHeigth, populationCorrectionDistributionHeigth)
          cz.data.nopopbelow = populationCorrectionHeigth
        }
        cls.data.nopopbelow = populationCorrectionHeigth
        cls.coastalPlane.noPopulationBelow = populationCorrectionHeigth
      }
      // OPTIMIZE: recompute area, population, assets
      cls.recomputeCoastalPlane
    }
  }

  private def computeSLRHorizon(cls: Cls, time: Int, timestep: Int, endTime: Int): Float = {
    val timeHorizon: Int = time + dikeConstructionHorizon
    val timeStepsToConsider: scala.collection.Set[Int] = cls.data.rslr_memory.keySet.filter { x => ((time <= x) && (x <= timeHorizon)) }
    var valuesToConsider: scala.collection.Set[Float] = timeStepsToConsider.map { t => cls.data.rslr_memory(t) }
    var vTime = timeStepsToConsider.max + timestep

    while ((vTime <= timeHorizon)) {
      valuesToConsider = valuesToConsider + (valuesToConsider.max + (cls.data.slr_current_annual_rate * timestep))
      vTime = vTime + timestep
    }

    valuesToConsider.max
  }

  private def landLoss(cls: Cls, initial: Boolean, time: Int, timestep: Int) {
    cls.czs.foreach { cz => landLoss(cz, initial, time, timestep) }
  }

  private def landLoss(cz: Cz, initial: Boolean, time: Int, timestep: Int) {
    initial match {
      case true =>
        cz.data.landloss_initial_land_below_S1 =
          if (cz.cls.data.length_protected == 0f)
            cz.coastalPlane.areaExposure.cumulativeExposure(cz.cls.floodHazard.returnPeriodHeight(1.0f)).asInstanceOf[Float]
          else
            0f
      case false =>
        // if we want to have landloss_initial_land_below_S1 = 0 for all timesteps but the initial timestep
        // cz.data.landloss_initial_land_below_S1 = 0f
        val sl_change_current: Float =
          if (cz.cls.data.seadike_abandon_year == time) {
            cz.cls.data.rslr_memory(time)
          } else {
            cz.cls.data.rslr_memory(time) - cz.cls.data.rslr_memory(time - timestep)
          }
        cz.data.landloss_submergence =
          if (cz.cls.data.length_protected == 0f)
            (cz.coastalPlane.areaExposure.cumulativeExposure(cz.cls.floodHazard.returnPeriodHeight(1.0f)) -
              cz.coastalPlane.areaExposure.cumulativeExposure(cz.cls.floodHazard.returnPeriodHeight(1.0f) - sl_change_current)).asInstanceOf[Float]
          else
            0

        cz.data.landloss_submergence = cz.data.landloss_submergence / timestep
        cz.data.landlosscost_submergence = cz.data.landloss_submergence * cz.data.landvalue_unit

        if (submergenceMigration && (cz.data.landloss_submergence > 0)) {
          cz.data.migration_submergence = cz.coastalPlane.migrateBelow(cz.cls.floodHazard.returnPeriodHeight(submergenceMigrationLevel).asInstanceOf[Float], pAwayRate = migrationAwayRate)._1.asInstanceOf[Float] / timestep
          cz.data.migrationcost_submergence = cz.data.migration_submergence * migrcostToGdpcRatio * cz.data.gdpc
          //println(cz.locationid + ": migration_submergence = " + cz.data.migration_submergence)
        } else {
          cz.data.migration_submergence = 0
          cz.data.migrationcost_submergence = 0
        }

    }
  }
  private def retreatManagement(cls: Cls, timeStep: Float) = retreatMode match {
    case SETBACK_ZONE_ALSO_DECONSTRUCTION => setback(cls, true)
    case SETBACK_ZONE_CONSTRUCTION_RESTRICTION => setback(cls, false)
    case RETREAT => retreat(cls, timeStep)
    case NONE => doNothing
  }

  private def retreat(cls: Cls, timeStep: Float) {
    if (((cls.data.protlevel_implemented <= retreatProtectionLevel) || ((cls.data.protlevel_implemented > retreatProtectionLevel) && (cls.data.seadikebase_elevation > 0f))) && (retreatMode == RETREAT)) {
      cls.czs.foreach { cz => retreat(cz, timeStep, (cls.data.protlevel_implemented <= retreatProtectionLevel)) }
      cls.data.retreat = true
    }
  }

  private def retreat(cz: Cz, timeStep: Float, complete: Boolean) {
    val retreated = if (complete) {
      cz.coastalPlane.computeRetreatWithoutDike(cz.cls.floodHazard.returnPeriodHeight(retreatLevel).asInstanceOf[Float])
    } else {
      cz.coastalPlane.computeRetreatInFrontOfDike(cz.cls.floodHazard.returnPeriodHeight(retreatLevel).asInstanceOf[Float], cz.cls.data.seadikebase_elevation, cz.cls.data.seadike_height)
    }
    cz.data.migration_retreat = (retreated._1 / timeStep).toFloat
    cz.data.migrationcost_retreat = (retreated._2 / timeStep).toFloat
  }

  private def setback(cls: Cls, alsoDeconstruction: Boolean) {

    val retreatPossible: Boolean = (cls.city == None) || (cityRetreat)
    if (((cls.data.length_protected == 0f) || ((cls.data.length_protected != 0f) && (cls.data.seadikebase_elevation > 0f))) && ((retreatMode == SETBACK_ZONE_CONSTRUCTION_RESTRICTION) || (retreatMode == SETBACK_ZONE_ALSO_DECONSTRUCTION)) && retreatPossible) {
      cls.czs.foreach {
        cz =>
          if (cz.data.coastalzone) {
            setback(cz, alsoDeconstruction)
          } else {
            cz.data.setbackzone_population = 0
          }
      }
    } else {
      cls.data.setback_height = 0f
      cls.data.setback_width = 0f
      cls.data.setback_returnperiod = 0
    }
  }

  private def setback(cz: Cz, alsoDeconstruction: Boolean) {
    if (cz.cls.data.length_protected == 0f)
      cz.coastalPlane.computeSetbackWithoutDike(cz.cls.data.maxhwater + cz.cls.data.rslr)
    else
      cz.coastalPlane.computeSetbackInFrontOfDike(cz.cls.data.maxhwater + cz.cls.data.rslr, cz.cls.data.seadikebase_elevation)

    cz.cls.data.setback_height = cz.coastalPlane.setbackHeight
    cz.cls.data.setback_width = cz.coastalPlane.setbackWidth
    // really?

    cz.cls.data.setback_returnperiod = cz.cls.floodHazard.returnPeriod(cz.coastalPlane.setbackHeight).asInstanceOf[Float]
    var setback_length_old = cz.cls.data.setback_length;

    if (alsoDeconstruction) {
      cz.coastalPlane.noAssetDevelopmentDevelopedBelow = cz.cls.data.setback_height
      cz.coastalPlane.noAssetDevelopmentDevelopableBelow = cz.cls.data.setback_height
      cz.cls.data.setback_length =
        if ((cz.data.fraction_developable == None) || (cz.data.fraction_developed == None)) cz.cls.data.length
        else cz.cls.data.length * (cz.data.fraction_developable.get + cz.data.fraction_developed.get)
    } else {
      cz.coastalPlane.noAssetDevelopmentDevelopedBelow = 0f
      cz.coastalPlane.noAssetDevelopmentDevelopableBelow = cz.cls.data.setback_height
      cz.cls.data.setback_length =
        if ((cz.data.fraction_developable == None)) cz.cls.data.length
        else cz.cls.data.length * cz.data.fraction_developable.get
    }
    // should never happen!
    if (cz.cls.data.setback_height.isNaN()) {
      cz.cls.coastalPlane.analyseSetback(cz.cls.data.maxhwater + cz.cls.data.rslr)
    }
    // can happen!
    if ((cz.cls.data.setback_returnperiod.isInfinity) || (cz.cls.data.setback_returnperiod > 100000)) {
      cz.cls.data.setback_returnperiod = 100000
    }
    // can happen!
    if (cz.cls.data.setback_height > 20.5f) {
      cz.coastalPlane.setbackHeight = 20.5f
      cz.cls.data.setback_height = 20.5f
    }

    cz.data.setbackzone_area = cz.coastalPlane.getSetbackzoneArea(alsoDeconstruction)
    cz.data.setbackzone_area =
      if (alsoDeconstruction) {
        cz.data.setbackzone_area.asInstanceOf[Float] - cz.cls.data.landloss_initial_land_below_S1.asInstanceOf[Float] * (1.0f - cz.coastalPlane.gUndevlopedFrac.asInstanceOf[Float])
      } else {
        cz.data.setbackzone_area - cz.cls.data.landloss_initial_land_below_S1 * cz.coastalPlane.gDevlopableFrac
      }
    // Typen anpassen!
    cz.data.setbackzone_population = cz.coastalPlane.getSetbackzonePopulation(alsoDeconstruction).toFloat
    cz.data.setbackzone_assets = cz.coastalPlane.getSetbackzoneAssets(alsoDeconstruction).toFloat
    cz.cls.data.setbackzone_administrative_cost =
      if (setback_length_old < cz.cls.data.setback_length) cz.cls.data.setback_length * setbackZoneAdministrativeCostInitial
      else cz.cls.data.setback_length * setbackZoneAdministrativeCost
  }

} // END OF CLASS

