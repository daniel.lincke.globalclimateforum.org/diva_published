package diva.types

import scala.language.implicitConversions

object DikeMode extends Enumeration {
  type DikeMode = Value

  val DEMAND_FOR_SAFETY_ORIGINAL = Value("DEMAND_FOR_SAFETY_ORIGINAL")
  val DEMAND_FOR_SAFETY_ORIGINAL_ONLY_SED = Value("DEMAND_FOR_SAFETY_ORIGINAL_ONLY_SED")
  val DEMAND_FOR_SAFETY_NEW = Value("DEMAND_FOR_SAFETY_NEW")
  val GLOBAL_EXPERT_TABLE = Value("GLOBAL_EXPERT_TABLE")
  val CONSTANT_FLOOD_RISK = Value("CONSTANT_FLOOD_RISK")
  val CONSTANT_RELATIVE_FLOOD_RISK = Value("CONSTANT_RELATIVE_FLOOD_RISK")
  val TARGET_RELATIVE_FLOOD_RISK = Value("TARGET_RELATIVE_FLOOD_RISK")
  val CONSTANT_PROTECTION_LEVELS = Value("CONSTANT_PROTECTION_LEVELS")
  
  val CONSTANT_FLOOD_RISK_INHABITED_AREAS = Value("CONSTANT_FLOOD_RISK_INHABITED_AREAS")
  val CONSTANT_RELATIVE_FLOOD_RISK_INHABITED_AREAS = Value("CONSTANT_RELATIVE_FLOOD_RISK_INHABITED_AREAS")
  val TARGET_RELATIVE_FLOOD_RISK_INHABITED_AREAS = Value("TARGET_RELATIVE_FLOOD_RISK_INHABITED_AREAS")

  val COUNTRY_TARGET_RELATIVE_FLOOD_RISK = Value("COUNTRY_TARGET_RELATIVE_FLOOD_RISK")
  val COUNTRY_CONSTANT_RELATIVE_FLOOD_RISK = Value("COUNTRY_CONSTANT_RELATIVE_FLOOD_RISK")

  val GLOBAL_TARGET_RELATIVE_FLOOD_RISK = Value("GLOBAL_TARGET_RELATIVE_FLOOD_RISK")
  val GLOBAL_CONSTANT_RELATIVE_FLOOD_RISK = Value("GLOBAL_CONSTANT_RELATIVE_FLOOD_RISK")

  val NO_DIKES = Value("NO_DIKES")
  val NO_DIKE_RAISE = Value("NO_DIKE_RAISE")
  val TOTAL_PROTECTION_LEVEL_100 = Value("TOTAL_PROTECTION_LEVEL_100")
  val INHABITED_PROTECTION_LEVEL_100 = Value("INHABITED_PROTECTION_LEVEL_100")
  val PROTECTION_LEVEL_UD = Value("PROTECTION_LEVEL_UD")

  val OPTIMAL_DISCRETE_DIKES = Value("OPTIMAL_DISCRETE_DIKES")
  val OPTIMAL_DIKES = Value("OPTIMAL_DIKES")
  val OPTIMAL_DIKE_TRAJECTORY = Value("OPTIMAL_DIKE_TRAJECTORY")

  implicit def valueToInterpolationMode(v: Value): DikeMode = v.asInstanceOf[DikeMode]
  implicit def stringToInterpolationMode(s: String): DikeMode = DikeMode.withName(s)

}