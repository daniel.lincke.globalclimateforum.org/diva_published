package diva.model

object divaFeatures {

  val features: List[diva.model.FeatureI] =
    List(new diva.feature.Cls,
      new diva.feature.Cz,
      new diva.feature.City,
      new diva.feature.Admin,
      new diva.feature.Country,
      new diva.feature.Delta,
      new diva.feature.Basin,
      new diva.feature.River,
      new diva.feature.Riverdistributary,
      new diva.feature.Surgebarrier,
      new diva.feature.Region,
      new diva.feature.Global)

  implicit def idendifyClsWithGlobal(cls: diva.feature.Cls, global: diva.feature.Global): Boolean = true
  implicit def idendifyClsWithCountry(cls: diva.feature.Cls, country: diva.feature.Country): Boolean = (cls.data.countryid == country.locationid)
  implicit def idendifyClsWithAdmin(cls: diva.feature.Cls, admin: diva.feature.Admin): Boolean = (cls.data.adminid == admin.locationid)

  implicit def idendifyAdminWithRegion(admin: diva.feature.Admin, region: diva.feature.Region): Boolean = admin.data.regionid match {
    case None     => false
    case Some(id) => (id == region.locationid)
  }

  implicit def idendifyCountryWithGlobal(country: diva.feature.Country, global: diva.feature.Global): Boolean = true

  implicit def idendifyClsWithCity(cls: diva.feature.Cls, city: diva.feature.City): Boolean = cls.data.cityid match {
    case None     => false
    case Some(id) => (id == city.locationid)
  }

  implicit def idendifyClsWithDelta(cls: diva.feature.Cls, delta: diva.feature.Delta): Boolean = cls.data.deltaid match {
    case None     => false
    case Some(id) => (id == delta.locationid)
  }

  implicit def idendifyRiverWithDelta(river: diva.feature.River, delta: diva.feature.Delta): Boolean = river.data.deltaid match {
    case None     => false
    case Some(id) => (id == delta.locationid)
  }

  implicit def idendifyRiverdistributaryWithCls(riverdistributary: diva.feature.Riverdistributary, cls: diva.feature.Cls): Boolean = (riverdistributary.data.clsid == cls.locationid)
  implicit def idendifyRiverdistributaryWithSurgebarrier(riverdistributary: diva.feature.Riverdistributary, surgebarrier: diva.feature.Surgebarrier): Boolean = (riverdistributary.data.surgebarrierid == surgebarrier.locationid)
  implicit def idendifyRiverdistributaryWithRiver(riverdistributary: diva.feature.Riverdistributary, river: diva.feature.River): Boolean = (riverdistributary.data.riverid == river.locationid) 
  
  implicit def idendifyClsWithBasin(cls: diva.feature.Cls, basin: diva.feature.Basin): Boolean = cls.data.basinid match {
    case None     => false
    case Some(id) => (id == basin.locationid)
  }
    
  
  implicit def idendifyCzWithCls(cz: diva.feature.Cz, cls: diva.feature.Cls): Boolean = (cz.data.clsid == cls.locationid)
  
  implicit def idendifyCzWithAdmin(cz: diva.feature.Cz, admin: diva.feature.Admin): Boolean = (cz.data.adminid == admin.locationid)

  implicit def idendifyAdminWithCountry(admin: diva.feature.Admin, country: diva.feature.Country): Boolean = (admin.data.countryid == country.locationid)

  implicit def idendifyCityWithCountry(city: diva.feature.City, country: diva.feature.Country): Boolean = (city.data.countryid == country.locationid)
}
