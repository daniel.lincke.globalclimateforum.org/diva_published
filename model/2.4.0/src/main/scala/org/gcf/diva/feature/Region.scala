package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class Region extends FeatureI {
  type Data = RegionData
  type DataIO = RegionDataIO

  val dataTypeTag: ru.TypeTag[RegionData] = ru.typeTag[RegionData]
  val dataClassTag: ClassTag[RegionData] = classTag[RegionData]
  val dataTypeSave: ru.Type = ru.typeOf[RegionData]

  var data: RegionData = null
  var dataIO: RegionDataIO = null
  
  var clss: Array[Cls] = Array()
  var admins: Array[Admin] = Array()

  def init() {
    data = new RegionData
    dataIO = new RegionDataIO
  }

  var mustHaveInputFile = false

  def newInstanceCopy: Region = {
    val ret = new Region
    ret.data = new RegionData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }
}