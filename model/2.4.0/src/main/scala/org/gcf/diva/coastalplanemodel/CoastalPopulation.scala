package diva.coastalplanemodel

import math._
import diva.toolbox.PartialLinearInterpolator

class CoastalPopulation(val pHeights: Array[Float], val pHeightsAttenuation: Array[Float], pExposedPeople: Array[Float])
  extends CoastalExposureI {

  lLevels = pHeights.clone
  lLevelsAttenuation = pHeightsAttenuation.clone
  lExposure = pExposedPeople.clone

  // pExposure values are given in noncumulative manner,
  // so we first cumulate them
  lCumulativeExposure = pExposedPeople.scanLeft(0.0f)(_ + _).drop(1)
  lPartialLinearInterpolator = new PartialLinearInterpolator(lLevels.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })
  lPartialLinearInterpolatorAttenuation = new PartialLinearInterpolator(lLevelsAttenuation.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })

  // we implement a more efficient version of cumulativeDamageWithDike for people
  // as vul=1 and vul'=0 the whole integral is zero, so we do not have to compute it every time
  /*
  override def cumulativeDamageWithDikeOld(d: Level, pDikePosition: Level = 0)(x: Level): Float = {
    if (x <= d) return 0
    // For people we have vulnerability=1 and therefore damage=exposure (the integral is zero, see ExposureI)
    depthDamage(0) * cumulativeExposure(x)
  } */

  override def cumulativeDamageInFrontOfDike(pDikeHeight: Level, pDikePosition: Level)(pWaterLevel: Level): Float = {
    if (pDikePosition == 0)
      0
    else
      depthDamage(0) * cumulativeExposureOnlyBelow(pDikePosition)(pWaterLevel)
  }

  override def cumulativeDamageBehindDike(pDikeHeight: Level, pDikePosition: Level)(pWaterLevel: Level): Float = {
    val realDikeHeight = pDikeHeight + pDikePosition
    if (pWaterLevel <= realDikeHeight)
      0
    else
      depthDamage(0) * cumulativeExposureOnlyAbove(pDikePosition)(pWaterLevel)
  }

  override def clone: CoastalPopulation = {
    val ret = new CoastalPopulation(lLevels.map { x => x.toFloat }, lLevelsAttenuation.map { x => x.toFloat }, lExposure.map { x => x.toFloat })
    ret.deepCopy(this)
    ret
  }
}