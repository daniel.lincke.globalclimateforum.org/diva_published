package diva.io.psqlcode

import diva.io.divaOutput
import diva.io.logging.LoggerI
import diva.types.TypeComputations._
import scala.collection.mutable.ArrayBuffer
import scala.reflect.runtime.{ universe => ru }
import java.io.FileOutputStream
import java.io.FileWriter
import java.io.BufferedWriter

class divaPSQLOutput(fi: FileOutputStream, val fname: String, val pLogger: LoggerI, val tableName: String) extends divaOutput {

  // CREATE TABLE IF NOT EXISTS 
  /*  CREATE TABLE IF NOT EXISTS distributors (
    did     integer,
    name    varchar(40),
    PRIMARY KEY(did)
);

INSERT INTO products (product_no, name, price) VALUES
    (1, 'Cheese', 9.99),
    (2, 'Bread', 1.99),
    (3, 'Milk', 2.99);

CREATE TABLE OUT(locationid INT NOT NULL, locationname VARCHAR(30),time INT NOT NULL , area_below_0 real, area_below_1 real, area_below_2 real, area_below_h100 real, 
area_below_h1000 real, assets_below_1 real, assets_below_10 real, assets_below_2 real, assets_below_h1 real, assets_below_h100 real, bnourcost real, bnourlengthbeach real, 
bnourlengthshore real, bnourvolume real, coastlength real, gdp real, gslr real, h1 real, h10 real, h100 real, h1000 real, landlcostero real, landleroind real, 
landlerotot real, landlsub real, length_protected real, migrcostero real, migrcostsub real, migrero real, migrsub real, openwater real, par real, pop real, 
pop_below_1 real, pop_below_10 real, pop_below_2 real, pop_below_h1 real, pop_below_h100 real, protlevel real, riverdikecost real, riverdikelength real, 
riverdikemain real, riverdikemain_incr real, riverfloodcost real, rslr real, salcost real, sandlind real, sandltot real, seadikecost real, seadikehght real, 
seadikehght_init real, seadikemain real, seadikemain_incr real, seafloodcost real, seafloodcost_s1 real, seafloodcost_s100 real, tourlosscost real, wetarea real, 
wetforst real, wetforst_value real, wetfresh real, wetfresh_value real, wethigh real, wethigh_value real, wetlow real, wetlow_value real, wetmang real, 
wetmang_value real, wetsalt real, wetsalt_value real, wetvalue real, wetvalueloss real, wnourcost real, PRIMARY KEY (locationid, time));


*/

  logger = pLogger
  var writer: FileWriter = new FileWriter(fi.getFD)
  var bufferedWriter: BufferedWriter = new BufferedWriter(writer);
  var columns: Array[String] = _
  private var initLine: Boolean = true
  private var initData: Boolean = true

  def writeInit = {
    bufferedWriter.write("INSERT INTO " + tableName + " (");
    if (columns.length > 0) {
      bufferedWriter.write(columns.head)
      columns.tail.foreach { x => bufferedWriter.write(", " + x) }
    }
    bufferedWriter.write(") VALUES");
    bufferedWriter.flush
    bufferedWriter.newLine
    initLine = true
    initData = true
  }

  def write[T: toPSQLString](t: T) = {
    if ((!initLine) && (initData)) { bufferedWriter.write(", (") }
    if ((initLine) && (initData)) { bufferedWriter.write("  (") }
    if (!initData) { bufferedWriter.write(", ") }
    bufferedWriter.write(convertToPSQLString[T](t))
    initData = false
    initLine = false
  }

  def writeNewLine() {
    bufferedWriter.write(")");
    bufferedWriter.flush
    bufferedWriter.newLine
    initData = true
  }

  def writeFinish = {
    bufferedWriter.write(";");
    bufferedWriter.flush
    bufferedWriter.newLine
  }
  def writeStringLine(v: Array[String], types: scala.collection.mutable.ArraySeq[scala.reflect.runtime.universe.Type]) = {}

  def writeHeaderLine(c: Array[String], types: scala.collection.mutable.ArraySeq[scala.reflect.runtime.universe.Type]) {
    columns = c
    bufferedWriter.write("CREATE TABLE IF NOT EXISTS " + tableName + " (");
    val valuesAndType = (c zip types)
    if (valuesAndType.length > 0) {
      bufferedWriter.write(valuesAndType.head._1 + " " + getPSQLType(valuesAndType.head._2))
      valuesAndType.tail.foreach { x => bufferedWriter.write(", " + x._1 + " " + getPSQLType(x._2)) }
    }
    bufferedWriter.write(");");
    bufferedWriter.flush
    bufferedWriter.newLine
  }

  private def getPSQLType(pType: scala.reflect.runtime.universe.Type): String = {
    if (pType =:= ru.typeOf[String]) "text"
    else if (pType =:= ru.typeOf[Byte]) "integer"
    else if (pType =:= ru.typeOf[Short]) "integer"
    else if (pType =:= ru.typeOf[Int]) "integer"
    else if (pType =:= ru.typeOf[Long]) "integer"
    else if (pType =:= ru.typeOf[Float]) "real"
    else if (pType =:= ru.typeOf[Double]) "real"
    else if (pType =:= ru.typeOf[Boolean]) "boolean"
    else if (pType.typeConstructor =:= ru.typeOf[Option[_]].typeConstructor) {
      val lInnerType = pType.typeArgs.head
      getPSQLType(lInnerType)
    } else {
      "text"
    }
  }

}
