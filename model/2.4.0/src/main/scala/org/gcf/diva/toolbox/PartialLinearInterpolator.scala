package diva.toolbox

import diva.toolbox.exceptions.arrayLengthsNotEqualException
import diva.toolbox.exceptions.xArrayNotSortedException

class PartialLinearInterpolator[T](pX: Array[T], pY: Array[T])(implicit frac: Fractional[T], ev: scala.reflect.ClassTag[T], ord: Ordering[T]) {

  import ord.mkOrderingOps
  import frac.mkNumericOps

  var lX = pX
  var lY = pY

  if (pX.length != pY.length) throw new arrayLengthsNotEqualException("linear interpolator needs two arrays of the same length as input (" + pX.length + " != " + pY.length + ")")
  //if (!pX_sorted) throw new arrayLengthsNotEqualException("linear interpolator expects sorted x-array , but found unsorted " + pX.toList)

  var lInterceptions: Array[T] = new Array[T](pY.length)
  var lSlopes: Array[T] = new Array[T](pY.length)

  var lZero: T = frac.zero

  finalise

  private def finalise = {
    for (i <- 0 to pY.length - 2) {
      val lX1 = if ((pX(i) <= lZero) && (lZero < pX(i + 1))) lZero else pX(i)
      val lX2 = pX(i + 1)
      val lY1 = pY(i)
      val lY2 = pY(i + 1)

      lSlopes(i) = frac.div((lY2 - lY1), (lX2 - lX1))
      lInterceptions(i) = lY1 - (lSlopes(i) * lX1)
    }
    lSlopes(pY.length - 1) = lSlopes(pY.length - 2)
    lInterceptions(pY.length - 1) = lInterceptions(pY.length - 2)
  }

  def rescale(pFactor: T) {
    lSlopes = lSlopes.map(x => x * pFactor)
    lInterceptions = lInterceptions.map(x => x * pFactor)
  }

  def rescaleAbove(pFactor: T, pAbove: T) {
    for (i <- 0 until pX.length) {
      if (pX(i) >= pAbove) {
        lSlopes(i) = lSlopes(i) * pFactor
        lInterceptions(i) = lInterceptions(i) * pFactor
      }
    }
  }

  def addPoint(x: T, y: T) {
    val ind = index(x)
    if (ind < lSlopes.length - 1) {

      val slope = frac.div((lX(ind) - y), (lX(ind) - x))
      val intercept = y - (slope * x)

      lSlopes = (lSlopes.slice(0, ind) :+ slope) ++ lSlopes.slice(ind, lSlopes.length)
      lInterceptions = (lInterceptions.slice(0, ind) :+ intercept) ++ lInterceptions.slice(ind, lInterceptions.length)

      lX = (lX.slice(0, ind) :+ x) ++ lX.slice(ind, lX.length)
      lY = (lY.slice(0, ind) :+ x) ++ lY.slice(ind, lY.length)

      if (ind > 0) {
        lSlopes(ind-1) = frac.div((lY(ind) - lY(ind-1)), (lX(ind) - lX(ind-1)))
        lInterceptions(ind-1) = lY(ind) - (lSlopes(ind) * lX(ind))
      }
    } else {
      
      val slope = frac.div((lX(lSlopes.length - 1) - y), (lX(lSlopes.length - 1) - x))
      val intercept = y - (slope * x)

      lSlopes = (lSlopes :+ slope)
      lInterceptions = (lInterceptions :+ intercept)      
    }
  }

  def value(x: T): T = {
    val lIndex = index(x)
    val ret =
      if (lIndex < lSlopes.length)
        lSlopes(lIndex) * x + lInterceptions(lIndex)
      else
        lSlopes(lSlopes.length - 1) * x + lInterceptions(lSlopes.length - 1)
    if (ret < frac.zero) frac.zero else ret
  }

  def index(x: T): Int = {
    //if (x <= frac.zero) return 0
    //if (pX.length == 0) return -1
    //if (pX.length == 1) return -1
    if (x <= pX(0)) return 0
    if (x > pX(pX.length - 1)) return (pX.length - 1)

    var index_f = index_find_fast(x)

    if ((index_f >= pX.length) || (index_f < 0)) { index_f = pX.length - 1 }
    index_f
  }

  def index_find_fast(x: T): Int = {
    var start: Int = 0
    var end: Int = pX.length - 1

    var ret: Int = -1
    while (start <= end) {
      val mid: Int = (start + end) / 2

      // Move to the left side if the target is smaller
      if (pX(mid) > x) {
        end = mid - 1;
      } // Move right side  
      else {
        ret = mid
        start = mid + 1
      }
    }
    return ret
  }

  def integral(x_low: T, x_high: T): T = {
    if (x_high <= frac.zero) return frac.zero
    if (x_high <= x_low) return frac.zero
    var lIndex1 = index(x_low)
    var lIndex2 = index(x_high)
    var area: T = frac.zero
    //println("ind: " + lIndex1 + " - " + lIndex2)
    for (i <- (lIndex1 - 1) to (lIndex2 + 1)) {
      if ((x_low <= pX(i)) && (pX(i + 1) <= x_high)) {
        //println("case1: " + i)
        var y1 = lSlopes(i) * pX(i) + lInterceptions(i)
        var y2 = lSlopes(i + 1) * pX(i + 1) + lInterceptions(i + 1)
        area = area + ((y1 + y2) / 2.asInstanceOf[T]) * (pX(i + 1) - pX(i))
      }
      if ((pX(i) <= x_low) && (x_low <= pX(i + 1)) && (pX(i) <= x_high) && (x_high <= pX(i + 1))) {
        //println("case2: " + i)
        var y1 = lSlopes(i) * x_low + lInterceptions(i)
        var y2 = lSlopes(i + 1) * x_high + lInterceptions(i + 1)
        area = area + (((y1 + y2) / 2.asInstanceOf[T]) * (x_high - x_low))
      }
      if ((x_low <= pX(i)) && (pX(i) <= x_high) && (pX(i + 1) <= x_high)) {
        //println("case3: " + i)
        var y1 = (lSlopes(i) * pX(i)) + lInterceptions(i)
        var y2 = (lSlopes(i + 1) * x_high) + lInterceptions(i + 1)
        area = area + (((y1 + y2) / 2.asInstanceOf[T]) * (x_high - pX(i)))
      }
      if ((pX(i) <= x_low) && (x_low <= pX(i + 1)) && (pX(i + 1) <= x_high)) {
        //println("case4: " + i)
        var y1 = (lSlopes(i) * x_low) + lInterceptions(i)
        var y2 = (lSlopes(i + 1) * pX(i + 1)) + lInterceptions(i + 1)
        area = area + (((y1 + y2) / 2.asInstanceOf[T]) * (pX(i + 1) - x_low))
      }
    }

    return area;
  }

  private def pX_sorted: Boolean = {
    for (i <- 0 until pX.length - 2) {
      if (pX(i) > pX(i + 1)) return false;
    }
    true
  }

  override def clone: PartialLinearInterpolator[T] = {
    var ret: PartialLinearInterpolator[T] = new PartialLinearInterpolator[T](pX, pY)
    ret.lInterceptions = lInterceptions.clone
    ret.lSlopes = lSlopes.clone
    ret.lZero = lZero
    ret.finalise
    ret
  }

}
