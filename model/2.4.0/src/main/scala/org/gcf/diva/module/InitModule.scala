/*---

$Header: /home/lincke/Software/projects/gcf/diva/CSV/cvsroot/genial/src/diva/module/RSLR.java,v 1.23 2011-09-01 12:37:57 hinkel Exp $

DIVA model
----------

Copyright (C) 2003 Jochen Hinkel and The Dinas Coast Consortium
http://pik-potsdam.de/dinas-coast

This is a pre-release of the DIVA model for circulation within the
DINAS Coast Consortium. The algorithms are not finalized
nor have they been validated. Please do not cite.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See COPYING.TXT for details.


---*/
package diva.module

import diva.model.ModuleI
import diva.model.divaState
import diva.model.divaFeatures._
import diva.model.Config
import diva.feature._
import diva.coastalplanemodel.CoastalPlaneModel
import diva.module.flooding._
import diva.types.AssetPopulationCorrectionMode._
import diva.types.Distribution._

import scala.collection.parallel.immutable.ParSeq

//import org.apache.commons.math3.stat.regression._

class InitModule extends ModuleI {

  def moduleName: String = "General Initialisation Module"
  def moduleVersion: String = "1.0"
  def moduleDescription: String = "Performes initial computations for DIVA, such as coastlength calculations."
  def moduleAuthor: String = "Daniel Lincke"

  // ...
  var assetsToGDPCRatio: Float = 2.8f
  var wetlandCorrection: Boolean = true

  var assetPopulationCorrectionMode: AssetPopulationCorrectionMode = POPULATION_TO_ASSETS

  // parameter: assumed width over which the assets of the tourist industry
  // are distributed in km [defalut value 1km]
  var beachWidth = 1f

  def initModule(state: divaState) {
    logger.info("initialising module")
    var clss: List[Cls] = state.getFeatures[Cls]
    var czs: List[Cz] = state.getFeatures[Cz]
    var admins: List[Admin] = state.getFeatures[Admin]

    var global: Global = state.getFeatures[Global].head
    var countries: List[Country] = state.getFeatures[Country]
    var riverdistributaries: List[Riverdistributary] = state.getFeatures[Riverdistributary]
    var rivers: List[River] = state.getFeatures[River]
    var cities: List[City] = state.getFeatures[City]

    attachClsAndAdmin(czs, state)
    attachRegionAndCountry(admins, state)

    clss.foreach { cls =>
      checkClsCz(cls)
      attachCountry(cls, state)
      attachAdmin(cls, state)
      attachDelta(cls, state)
      attachCity(cls, state)
      attachBasin(cls, state)
      setInitialH(cls)
    }

    czs.foreach { cz =>
      constructCoastalPlain(cz)
      attachCountry(cz, state)
    }

    //clss.par.foreach { cls =>
    clss.foreach { cls =>
      constructCoastalPlain(cls)
      constructFloodHazard(cls)
    }

    riverdistributaries.foreach {
      riverdistributary =>
        {
          attachCls(riverdistributary, state)
          attachSurgebarrier(riverdistributary, state)
          attachRiver(riverdistributary, state)
        }

    }
    rivers.foreach { river => attachDelta(river, state) }

    cities.foreach { city => attachCountry(city, state) }

    // countries.par.foreach { country => computeArea1(country, state) }
    global.countries = state.getFeatures[Country]
    global.basins = state.getFeatures[Basin]

    accumulate(state);
  }

  // check somewhere: S1, S10, ... >= mhws

  def computeScenario(currentState: diva.model.divaState) = shouldNotBeInvoked
  def init(state: divaState) = doNothing
  def invoke(currentState: divaState) = doNothing

  private def accumulate(state: divaState) {
    var admins: List[Admin] = state.getFeatures[Admin]
    var countries: List[Country] = state.getFeatures[Country]
    var cities: List[City] = state.getFeatures[City]
    var deltas: List[Delta] = state.getFeatures[Delta]
    var regions: List[Region] = state.getFeatures[Region]
    var global: List[Global] = state.getFeatures[Global]

    cities.foreach { (c: City) => c.data.coastlength = c.clss.foldLeft(0f)((sum, cls) => sum + cls.data.length) }
    admins.foreach { (a: Admin) => a.data.coastlength = a.clss.foldLeft(0f)((sum, cls) => sum + cls.data.length) }
    countries.foreach { (c: Country) => c.data.coastlength = c.admins.foldLeft(0f)((sum, admin) => sum + admin.data.coastlength) }
    deltas.foreach { (d: Delta) => d.data.coastlength = d.clss.foldLeft(0f)((sum, cls) => sum + cls.data.length) }
    regions.foreach { (r: Region) => r.data.coastlength = r.admins.foldLeft(0f)((sum, admin) => sum + admin.data.coastlength) }
    global.foreach { (g: Global) => g.data.coastlength = g.countries.foldLeft(0f)((sum, country) => sum + country.data.coastlength) }
  }

  private def attachClsAndAdmin(czs: List[Cz], state: divaState) {
    var clss: List[Cls] = state.getFeatures[Cls]
    var admins: List[Admin] = state.getFeatures[Admin]
    czs.par.foreach { cz =>
      var clssMatching: List[Cls] = clss.filter(cls => idendifyCzWithCls(cz, cls))

      if (clssMatching.length == 0)
        logger.error("No cls found for cz " + cz.locationid + ". Clsid = " + cz.data.clsid)
      else {
        var clsMatch: Cls = clssMatching.head
        cz.cls = clsMatch
      }
      var adminsMatching: List[Admin] = admins.filter(admin => idendifyCzWithAdmin(cz, admin))
      if (adminsMatching.length == 0)
        logger.error("No admin found for cz " + cz.locationid + ". Adminid = " + cz.data.adminid)
      else {
        var adminMatch: Admin = adminsMatching.head
        cz.admin = adminMatch
      }
    }

    czs.foreach { cz =>
      cz.cls.czs = cz.cls.czs :+ cz
      cz.admin.czs = cz.admin.czs :+ cz
    }

  }

  private def checkClsCz(cls: Cls) {
    var czs: Array[Cz] = cls.czs
    if (czs.length == 0) {
      logger.error("No cz attached to cls " + cls.locationid + ". Thats not allowed. Every cls need at least one cz.")
    }
    if (czs.filter(_.data.coastalzone == true).length == 0) {
      logger.error("No coastal cz attached to cls " + cls.locationid + ". Every cls needs exactly one coastal cz.")
    }
    if (czs.filter(_.data.coastalzone == true).length > 1) {
      logger.error("More than one coastal cz attached to cls " + cls.locationid + " (" + czs.filter(_.data.coastalzone == true).map(_.locationid).toList + "). Every cls needs exactly one coastal cz.")
    }
  }

  private def attachCountry(cls: Cls, state: divaState) {

    var countries: List[Country] = state.getFeaturesWith[Country](country => idendifyClsWithCountry(cls, country))
    if (countries.length == 0)
      logger.error("No country found for cls " + cls.locationid + ". Countryid = " + cls.data.countryid)
    else if (countries.length > 1) {
      logger.error("Found several countries for cls " + cls.locationid + ". Countries found: " + countries.map { x => x.locationid })
    } else {
      countries.head.clss = countries.head.clss :+ cls
      cls.country = countries.head
    }
  }

  private def attachCountry(cz: Cz, state: divaState) {

    var admins: List[Admin] = state.getFeaturesWith[Admin](admin => idendifyCzWithAdmin(cz, admin))
    if (admins.length == 0)
      logger.error("No adminunit found for cz " + cz.locationid + ". Adminid = " + cz.data.adminid)
    else if (admins.length > 1) {
      logger.error("Found several adminunit for cz " + cz.locationid + ". Adminids found: " + admins.map { x => x.locationid })
    } else {
      admins.head.country.czs = admins.head.country.czs :+ cz
      cz.country = admins.head.country
    }
  }

  private def attachRegionAndCountry(admins: List[Admin], state: divaState) {
    var countries: List[Country] = state.getFeatures[Country]
    var regions: List[Region] = state.getFeatures[Region]
    admins.foreach { admin =>
      var countriesMatching: List[Country] = countries.filter(country => idendifyAdminWithCountry(admin, country))
      if (countriesMatching.length == 0) {
        logger.error("No country found for admin unit " + admin.locationid + ". Countryid = " + admin.data.countryid)
      } else {
        var countryMatch: Country = countriesMatching.head
        countryMatch.admins = countryMatch.admins :+ admin
        admin.country = countryMatch
      }
      var regionsMatching: List[Region] = regions.filter(region => idendifyAdminWithRegion(admin, region))
      if ((regionsMatching.length == 0) && (!admin.data.regionid.isEmpty)) {
        logger.error("No region found for admin unit " + admin.locationid + ". Regionid = " + admin.data.regionid.get)
      }
      admin.region = if (regionsMatching.length == 0) None else Some(regionsMatching.head)
      if (regionsMatching.length > 0) {
        regionsMatching.head.admins = regionsMatching.head.admins :+ admin
      }
    }
  }

  private def attachAdmin(cls: Cls, state: divaState) {
    var admins: List[Admin] = state.getFeaturesWith[Admin](admin => idendifyClsWithAdmin(cls, admin))
    if (admins.length == 0)
      logger.error("No admin found for cls " + cls.locationid + ". Adminid = " + cls.data.adminid)
    else {
      admins.head.clss = admins.head.clss :+ cls
      admins.head.country = cls.country
      // Attention: DOES not work ... is NULL after this module!
      cls.admin = admins.head
    }
    if (cls.admin.data.countryid != cls.data.countryid) {
      logger.error("Data inconsistency: country for cls " + cls.locationid + " (" + cls.data.countryid + ") and connected admin " + cls.data.adminid + " (" + cls.admin.data.countryid + ") do not match.")
    }
  }

  private def attachCity(cls: Cls, state: divaState) {
    var cities: List[City] = state.getFeaturesWith[City](city => idendifyClsWithCity(cls, city))

    if ((cities.length == 0) && (!cls.data.cityid.isEmpty)) {
      logger.error("No city found for cls " + cls.locationid + ". Cityid = " + cls.data.cityid.get)
    }
    cls.city = if (cities.length == 0) None else Some(cities.head)
    if (cities.length > 0) {
      cities.head.clss = cities.head.clss :+ cls
      cities.head.country = cls.country
    }
  }

  private def attachDelta(cls: Cls, state: divaState) {
    var deltas: List[Delta] = state.getFeaturesWith[Delta](delta => idendifyClsWithDelta(cls, delta))
    if ((deltas.length == 0) && (cls.data.deltaid != None)) {
      logger.error("No delta found for cls " + cls.locationid + ". Deltaid = " + cls.data.deltaid.get)
    }
    cls.delta = if (deltas.length == 0) None else Some(deltas.head)
    if (deltas.length > 0) {
      deltas.head.clss = deltas.head.clss :+ cls
    }
  }

  private def attachDelta(river: River, state: divaState) {
    var deltas: List[Delta] = state.getFeaturesWith[Delta](delta => idendifyRiverWithDelta(river, delta))
    if ((deltas.length == 0) && (river.data.deltaid != None)) {
      logger.error("No delta found for river " + river.locationid + ". Deltaid = " + river.data.deltaid.get)
    }
    river.delta = if (deltas.length == 0) None else Some(deltas.head)
  }

  private def attachCountry(city: City, state: divaState) {
    var countries: List[Country] = state.getFeaturesWith[Country](country => idendifyCityWithCountry(city, country))
    if (countries.length == 0) {
      logger.error("No country found for city " + city.locationid + ". countryid = " + city.data.countryid)
    }
    city.country = (countries.head)
  }

  private def attachCls(riversdistributary: Riverdistributary, state: divaState) {
    var clss: List[Cls] = state.getFeaturesWith[Cls](cls => idendifyRiverdistributaryWithCls(riversdistributary, cls))
    if (clss.length == 0) {
      logger.error("No cls found for river distributary " + riversdistributary.locationid + ". Every river distributary needs exactly one cls. clsid: " + riversdistributary.data.clsid)
    }
    if (clss.length > 1) {
      logger.error("More than one cls found for river distributary " + riversdistributary.locationid + ". Every river distributary needs exactly one cls.")
    }

    riversdistributary.cls = clss.head
  }

  private def attachSurgebarrier(riversdistributary: Riverdistributary, state: divaState) {
    var surgebarriers: List[Surgebarrier] = state.getFeaturesWith[Surgebarrier](barrier => idendifyRiverdistributaryWithSurgebarrier(riversdistributary, barrier))
    if (surgebarriers.length == 0) {
      logger.error("No barrier found for river distributary " + riversdistributary.locationid + ". Every river distributary needs exactly one barrier. barrierid: " + riversdistributary.data.surgebarrierid)
    }
    if (surgebarriers.length > 1) {
      logger.error("More than one barrier found for river distributary " + riversdistributary.locationid + ". Every river distributary needs exactly one barrier.")
    }

    riversdistributary.surgeBarrier = surgebarriers.head
  }

  private def attachRiver(riversdistributary: Riverdistributary, state: divaState) {
    var rivers: List[River] = state.getFeaturesWith[River](river => idendifyRiverdistributaryWithRiver(riversdistributary, river))
    if (rivers.length == 0) {
      logger.error("No river found for river distributary " + riversdistributary.locationid + ". Every river distributary needs exactly one river. riverid: " + riversdistributary.data.riverid)
    }
    if (rivers.length > 1) {
      logger.error("More than one river found for river distributary " + riversdistributary.locationid + ". Every river distributary needs exactly one river.")
    }

    riversdistributary.river = rivers.head
    rivers.head.riverdistributarys = rivers.head.riverdistributarys :+ riversdistributary
  }

  private def attachBasin(cls: Cls, state: divaState) {
    var basins: List[Basin] = state.getFeaturesWith[Basin](basin => idendifyClsWithBasin(cls, basin))
    if ((basins.length == 0) && (cls.data.basinid != None)) {
      logger.error("No basin found for cls " + cls.locationid + ". Basinid = " + cls.data.basinid.get)
    }
    cls.basin = if (basins.length == 0) None else Some(basins.head)
    if (basins.length > 0) {
      basins.head.clss = basins.head.clss :+ cls
    }
  }

  private def checkRiver(river: River) {
    //if (river.clss.length == 0) {
    //logger.warn("No segments found for river " + river.locationid + ".")
    //}
  }

  private def setInitialH(cls: Cls) {
    cls.data.h1 = cls.data.s0001
    cls.data.h10 = cls.data.s0010
    cls.data.h100 = cls.data.s0100
    cls.data.h1000 = cls.data.s1000
  }

  private def computeArea1(country: Country, state: divaState) {
    country.data.touristic_area = country.clss.foldLeft(0f)((sum, cls) => sum + cls.data.brf * cls.data.length * beachWidth)
  }

  private def constructCoastalPlain(cz: Cz) {
    val data: (Array[Float], Array[Float], Array[Float], Array[Float], Array[Int]) = buildCoastalExposureArrays(cz)

    val levels: Array[Float] = data._1
    val areas: Array[Float] = data._2
    var population: Array[Float] = data._3
    var landuse: Array[Int] = data._5

    val areasAndPopulation: Array[(Float, Float)] = areas zip population
    if (areasAndPopulation.filter { x => ((x._1 == 0f) && (x._2 > 0)) }.length > 0) {
      logger.info("Population correction necessary for cz : " + cz.locationid)
      logger.info("Areas before: " + areas.toList)
      logger.info("Populations before: " + population.toList)
      population = correctAreaPopulationInconsistencies(areasAndPopulation)
      logger.info("Areas afterwards: " + areas.toList)
      logger.info("Populations afterwards: " + population.toList)
    }

    val assets: Array[Float] =
      if (data._4.length == 0)
        population.map(_ * cz.admin.data.gdpcmult * assetsToGDPCRatio * cz.cls.country.data.gdpc)
      else {
        if (assetPopulationCorrectionMode == POPULATION_TO_ASSETS) { correctAssetData(data._4, population, cz.admin.data.gdpcmult * assetsToGDPCRatio * cz.cls.country.data.gdpc) }
        else data._4
      }

    if (assetPopulationCorrectionMode == ASSETS_TO_POPULATION) {
      population = projectPopulationData(data._4, population, cz.admin.data.gdpcmult * assetsToGDPCRatio * cz.cls.country.data.gdpc)
    }

    if (areas.filter { x => (x < 0f) }.length > 0) {
      logger.error("Found negative exposure values for areas: " + areas.toList)
    }

    if (population.filter { x => (x < 0) }.length > 0) {
      logger.error("Found negative exposure values for populations: " + population.toList)
    }

    if (assets.filter { x => (x < 0) }.length > 0) {
      logger.error("Found negative exposure values for assets: " + assets.toList)
    }

    //logger.debug("Cls " + cls.locationid + ": Using the following levels for the coastal plane model: " + levels.toList)
    //logger.debug("Cls " + cls.locationid + ": Using the following area data for the coastal plane model: " + areas.toList)
    //logger.debug("Cls " + cls.locationid + ": Using the following population data for the coastal plane model: " + population.toList)
    //logger.debug("Cls " + cls.locationid + ": Using the following asset data for the coastal plane model: " + assets.toList)

    cz.coastalPlane = (cz.data.fraction_developed, cz.data.fraction_developable) match {
      case (Some(frac_devel), Some(frac_undevel)) => new CoastalPlaneModel(cz.cls.data.length, levels, areas, population, assets, landuse, frac_devel, frac_undevel)
      case (_, _) => new CoastalPlaneModel(cz.cls.data.length, levels, areas, population, assets, landuse)
    }

    cz.coastalPlane.logger = cz.logger.copy
    cz.coastalPlane.logger.id = "diva.CoastalPlaneModel for cz " + cz.locationid
    cz.coastalPlane.assetsPerCapita = cz.admin.data.gdpcmult * assetsToGDPCRatio * cz.cls.country.data.gdpc

    if ((wetlandCorrection) && (cz.data.coastalzone)) {
      cz.coastalPlane.wetlandCorrection(cz)
    }

  }

  private def constructCoastalPlain(cls: Cls) {
    /*  val data: (Array[Float], Array[Float], Array[Float], Array[Float], Array[Int]) = buildCoastalExposureArrays(cls)
    val levels: Array[Float] = data._1
    val areas: Array[Float] = data._2
    val population: Array[Float] = data._3
    val assets: Array[Float] = data._4
    val landuse: Array[Int] = data._5
    */
    val levels: Array[Float] = getExistingLevels(cls)
    /*
    if (levels.length != areas.length) {
      logger.error("Diffent array length for elevation levels and areas: " + levels.length + " != " + areas.length)
    }

    if (levels.length != population.length) {
      logger.error("Diffent array length for elevation levels and population: " + levels.length + " != " + population.length)
    }

    if (levels.length != landuse.length) {
      logger.error("Diffent array length for elevation levels and landuse: " + levels.length + " != " + landuse.length)
    }
*/
    cls.coastalPlane = new CoastalPlaneModel(cls.data.length, levels, levels.map(x => 0f), levels.map(x => 0f), levels.map(x => 0f), levels.map(x => 0))
    cls.coastalPlane.logger = cls.logger.copy
    cls.coastalPlane.logger.id = "diva.CoastalPlaneModel for cls " + cls.locationid

    cls.czs.foreach { cz =>
      cls.coastalPlane.add(cz.coastalPlane)
    }

    //if (wetlandCorrection) {
    //  cls.coastalPlane.wetlandCorrection(cls)
    //}
  }

  private def constructFloodHazard(cls: Cls) {

    cls.floodHazard = stringToDistribution(cls.data.surgedist_type) match {
      case GUMBEL => new EDFloodHazard(cls.data.surgedist_location, cls.data.surgedist_scale)
      case GEV => new GEVFloodHazard(cls.data.surgedist_location, cls.data.surgedist_scale, cls.data.surgedist_shape, cls.data.surgedist_number_events)
      case GPD => new GPDFloodHazard(cls.data.surgedist_location, cls.data.surgedist_scale, cls.data.surgedist_shape, cls.data.surgedist_number_events)
    }
  }

  private def correctAreaPopulationInconsistencies(areasAndPopulation: Array[(Float, Float)]): Array[Float] = {
    var ret: Array[Float] = areasAndPopulation.unzip._2
    var populationToMove: Float = 0f

    for (i <- 0 until areasAndPopulation.length) {
      if ((areasAndPopulation(i)._1 == 0f) && (ret(i) > 0)) {
        populationToMove += ret(i)
        ret(i) = 0f
      }
      if ((areasAndPopulation(i)._1 > 0f) && (populationToMove > 0)) {
        ret(i) = ret(i) + populationToMove
        populationToMove = 0f
      }
    }
    ret
  }

  private def addMissingLanduse(cz: Cz, levels: List[Float], landuselevels: List[Float], landuse: List[Int]): List[Int] = {

    if (landuse.size == 0) {
      landuse
    } else {

      val missingLanduseLevels: Set[Float] = levels.toSet.filterNot(landuselevels.toSet)
      val missingLevels: Set[Float] = landuselevels.toSet.filterNot(levels.toSet)

      if (missingLevels.size > 0) {
        logger.error("Found inconsistent landuse level data for cz with id " + cz.locationid + ". (Found landuselevels not present in levels: " + missingLevels + ")")
      }

      var landuse_new: Array[Int] = Array.empty[Int]
      var recent_landuse: Int = landuse(landuse.size - 1)
      var recent_landuse_index: Int = landuse.size - 1

      for (i <- (0 until levels.size).reverse) {
        if (missingLanduseLevels.contains(levels(i))) {
          landuse_new :+= recent_landuse
        } else {
          if ((i < (levels.size - 1)) && (i > 0)) {
            recent_landuse_index = recent_landuse_index - 1
            recent_landuse = landuse(recent_landuse_index)
          }
          landuse_new :+= recent_landuse
        }
      }
      landuse_new.reverse.toList
    }
  }

  private def buildCoastalExposureArrays(cz: Cz): (Array[Float], Array[Float], Array[Float], Array[Float], Array[Int]) = {
    val explicitAssetData: Boolean =
      (cz.data.assets01_0 != None) || (cz.data.assets02_0 != None) || (cz.data.assets03_0 != None) || (cz.data.assets04_0 != None) ||
        (cz.data.assets05_0 != None) || (cz.data.assets06_0 != None) || (cz.data.assets07_0 != None) || (cz.data.assets08_0 != None) ||
        (cz.data.assets09_0 != None) || (cz.data.assets10_0 != None) || (cz.data.assets11_0 != None) || (cz.data.assets12_0 != None) ||
        (cz.data.assets13_0 != None) || (cz.data.assets14_0 != None) || (cz.data.assets15_0 != None) || (cz.data.assets16_0 != None) ||
        (cz.data.assets17_0 != None) || (cz.data.assets18_0 != None) || (cz.data.assets19_0 != None) || (cz.data.assets20_0 != None) ||
        (cz.data.assets01_5 != None) || (cz.data.assets02_5 != None) || (cz.data.assets03_5 != None) || (cz.data.assets04_5 != None) ||
        (cz.data.assets05_5 != None) || (cz.data.assets06_5 != None) || (cz.data.assets07_5 != None) || (cz.data.assets08_5 != None) ||
        (cz.data.assets09_5 != None) || (cz.data.assets10_5 != None) || (cz.data.assets11_5 != None) || (cz.data.assets12_5 != None) ||
        (cz.data.assets13_5 != None) || (cz.data.assets14_5 != None) || (cz.data.assets15_5 != None) || (cz.data.assets16_5 != None) ||
        (cz.data.assets17_5 != None) || (cz.data.assets18_5 != None) || (cz.data.assets19_5 != None)

    val explicitLanduseData: Boolean =
      (cz.data.dluc01_0 != None) || (cz.data.dluc02_0 != None) || (cz.data.dluc03_0 != None) || (cz.data.dluc04_0 != None) ||
        (cz.data.dluc05_0 != None) || (cz.data.dluc06_0 != None) || (cz.data.dluc07_0 != None) || (cz.data.dluc08_0 != None) ||
        (cz.data.dluc09_0 != None) || (cz.data.dluc10_0 != None) || (cz.data.dluc11_0 != None) || (cz.data.dluc12_0 != None) ||
        (cz.data.dluc13_0 != None) || (cz.data.dluc14_0 != None) || (cz.data.dluc15_0 != None) || (cz.data.dluc16_0 != None) ||
        (cz.data.dluc17_0 != None) || (cz.data.dluc18_0 != None) || (cz.data.dluc19_0 != None) || (cz.data.dluc20_0 != None) ||
        (cz.data.dluc01_5 != None) || (cz.data.dluc02_5 != None) || (cz.data.dluc03_5 != None) || (cz.data.dluc04_5 != None) ||
        (cz.data.dluc05_5 != None) || (cz.data.dluc06_5 != None) || (cz.data.dluc07_5 != None) || (cz.data.dluc08_5 != None) ||
        (cz.data.dluc09_5 != None) || (cz.data.dluc10_5 != None) || (cz.data.dluc11_5 != None) || (cz.data.dluc12_5 != None) ||
        (cz.data.dluc13_5 != None) || (cz.data.dluc14_5 != None) || (cz.data.dluc15_5 != None) || (cz.data.dluc16_5 != None) ||
        (cz.data.dluc17_5 != None) || (cz.data.dluc18_5 != None) || (cz.data.dluc19_5 != None)

    var listlevel00_0: List[Float] = List(0.0f)
    var listarea00_0: List[Float] = List(0.0f)
    var listpop00_0: List[Float] = List(0.0f)
    var listassets00_0: List[Float] = if (explicitAssetData) List(0.0f); else List()
    var listlevellanduse00_0: List[Float] = List(0.0f)
    var listlanduse00_0: List[Int] = if (explicitLanduseData) List(0); else List()

    var listarea00_5: List[Float] = optionToList(cz.data.area00_5)
    var listpop00_5: List[Float] = optionToList(cz.data.pop00_5)
    var listassets00_5: List[Float] = optionToList(cz.data.assets00_5)
    checkIfLevelDataIsConsistent(cz, listarea00_5, listpop00_5, listassets00_5, explicitAssetData, 0.5f)
    var listlanduse00_5: List[Int] = optionToList(cz.data.dluc00_5)
    var listlevellanduse00_5: List[Float] = if (listlanduse00_5.isEmpty) List() else List(0.5f)
    var listlevel00_5: List[Float] = if (listarea00_5.isEmpty) List() else List(0.5f)

    var listarea01_0: List[Float] = optionToList(cz.data.area01_0)
    var listpop01_0: List[Float] = optionToList(cz.data.pop01_0)
    var listassets01_0: List[Float] = optionToList(cz.data.assets01_0)
    checkIfLevelDataIsConsistent(cz, listarea01_0, listpop01_0, listassets01_0, explicitAssetData, 1.0f)
    var listlanduse01_0: List[Int] = optionToList(cz.data.dluc01_0)
    var listlevellanduse01_0: List[Float] = if (listlanduse01_0.isEmpty) List() else List(1.0f)
    var listlevel01_0: List[Float] = if (listarea01_0.isEmpty) List() else List(1.0f)

    var listarea01_5: List[Float] = optionToList(cz.data.area01_5)
    var listpop01_5: List[Float] = optionToList(cz.data.pop01_5)
    var listassets01_5: List[Float] = optionToList(cz.data.assets01_5)
    checkIfLevelDataIsConsistent(cz, listarea01_5, listpop01_5, listassets01_5, explicitAssetData, 1.5f)
    var listlanduse01_5: List[Int] = optionToList(cz.data.dluc01_5)
    var listlevellanduse01_5: List[Float] = if (listlanduse01_5.isEmpty) List() else List(1.5f)
    var listlevel01_5: List[Float] = if (listarea01_5.isEmpty) List() else List(1.5f)

    var listarea02_0: List[Float] = optionToList(cz.data.area02_0)
    var listpop02_0: List[Float] = optionToList(cz.data.pop02_0)
    var listassets02_0: List[Float] = optionToList(cz.data.assets02_0)
    checkIfLevelDataIsConsistent(cz, listarea02_0, listpop02_0, listassets02_0, explicitAssetData, 2.0f)
    var listlanduse02_0: List[Int] = optionToList(cz.data.dluc02_0)
    var listlevellanduse02_0: List[Float] = if (listlanduse02_0.isEmpty) List() else List(2.0f)
    var listlevel02_0: List[Float] = if (listarea02_0.isEmpty) List() else List(2.0f)

    var listarea02_5: List[Float] = optionToList(cz.data.area02_5)
    var listpop02_5: List[Float] = optionToList(cz.data.pop02_5)
    var listassets02_5: List[Float] = optionToList(cz.data.assets02_5)
    checkIfLevelDataIsConsistent(cz, listarea02_5, listpop02_5, listassets02_5, explicitAssetData, 2.5f)
    var listlanduse02_5: List[Int] = optionToList(cz.data.dluc02_5)
    var listlevellanduse02_5: List[Float] = if (listlanduse02_5.isEmpty) List() else List(2.5f)
    var listlevel02_5: List[Float] = if (listarea02_5.isEmpty) List() else List(2.5f)

    var listarea03_0: List[Float] = optionToList(cz.data.area03_0)
    var listpop03_0: List[Float] = optionToList(cz.data.pop03_0)
    var listassets03_0: List[Float] = optionToList(cz.data.assets03_0)
    checkIfLevelDataIsConsistent(cz, listarea03_0, listpop03_0, listassets03_0, explicitAssetData, 3.0f)
    var listlanduse03_0: List[Int] = optionToList(cz.data.dluc03_0)
    var listlevellanduse03_0: List[Float] = if (listlanduse03_0.isEmpty) List() else List(3.0f)
    var listlevel03_0: List[Float] = if (listarea03_0.isEmpty) List() else List(3.0f)

    var listarea03_5: List[Float] = optionToList(cz.data.area03_5)
    var listpop03_5: List[Float] = optionToList(cz.data.pop03_5)
    var listassets03_5: List[Float] = optionToList(cz.data.assets03_5)
    checkIfLevelDataIsConsistent(cz, listarea03_5, listpop03_5, listassets03_5, explicitAssetData, 3.5f)
    var listlanduse03_5: List[Int] = optionToList(cz.data.dluc03_5)
    var listlevellanduse03_5: List[Float] = if (listlanduse03_5.isEmpty) List() else List(3.5f)
    var listlevel03_5: List[Float] = if (listarea03_5.isEmpty) List() else List(3.5f)

    var listarea04_0: List[Float] = optionToList(cz.data.area04_0)
    var listpop04_0: List[Float] = optionToList(cz.data.pop04_0)
    var listassets04_0: List[Float] = optionToList(cz.data.assets04_0)
    checkIfLevelDataIsConsistent(cz, listarea04_0, listpop04_0, listassets04_0, explicitAssetData, 4.0f)
    var listlanduse04_0: List[Int] = optionToList(cz.data.dluc04_0)
    var listlevellanduse04_0: List[Float] = if (listlanduse04_0.isEmpty) List() else List(4.0f)
    var listlevel04_0: List[Float] = if (listarea04_0.isEmpty) List() else List(4.0f)

    var listarea04_5: List[Float] = optionToList(cz.data.area04_5)
    var listpop04_5: List[Float] = optionToList(cz.data.pop04_5)
    var listassets04_5: List[Float] = optionToList(cz.data.assets04_5)
    checkIfLevelDataIsConsistent(cz, listarea04_5, listpop04_5, listassets04_5, explicitAssetData, 4.5f)
    var listlanduse04_5: List[Int] = optionToList(cz.data.dluc04_5)
    var listlevellanduse04_5: List[Float] = if (listlanduse04_5.isEmpty) List() else List(4.5f)
    var listlevel04_5: List[Float] = if (listarea04_5.isEmpty) List() else List(4.5f)

    var listarea05_0: List[Float] = optionToList(cz.data.area05_0)
    var listpop05_0: List[Float] = optionToList(cz.data.pop05_0)
    var listassets05_0: List[Float] = optionToList(cz.data.assets05_0)
    checkIfLevelDataIsConsistent(cz, listarea05_0, listpop05_0, listassets05_0, explicitAssetData, 5.0f)
    var listlanduse05_0: List[Int] = optionToList(cz.data.dluc05_0)
    var listlevellanduse05_0: List[Float] = if (listlanduse05_0.isEmpty) List() else List(5.0f)
    var listlevel05_0: List[Float] = if (listarea05_0.isEmpty) List() else List(5.0f)

    var listarea05_5: List[Float] = optionToList(cz.data.area05_5)
    var listpop05_5: List[Float] = optionToList(cz.data.pop05_5)
    var listassets05_5: List[Float] = optionToList(cz.data.assets05_5)
    checkIfLevelDataIsConsistent(cz, listarea05_5, listpop05_5, listassets05_5, explicitAssetData, 5.5f)
    var listlanduse05_5: List[Int] = optionToList(cz.data.dluc05_5)
    var listlevellanduse05_5: List[Float] = if (listlanduse05_5.isEmpty) List() else List(5.5f)
    var listlevel05_5: List[Float] = if (listarea05_5.isEmpty) List() else List(5.5f)

    var listarea06_0: List[Float] = optionToList(cz.data.area06_0)
    var listpop06_0: List[Float] = optionToList(cz.data.pop06_0)
    var listassets06_0: List[Float] = optionToList(cz.data.assets06_0)
    checkIfLevelDataIsConsistent(cz, listarea06_0, listpop06_0, listassets06_0, explicitAssetData, 6.0f)
    var listlanduse06_0: List[Int] = optionToList(cz.data.dluc06_0)
    var listlevellanduse06_0: List[Float] = if (listlanduse06_0.isEmpty) List() else List(6.0f)
    var listlevel06_0: List[Float] = if (listarea06_0.isEmpty) List() else List(6.0f)

    var listarea06_5: List[Float] = optionToList(cz.data.area06_5)
    var listpop06_5: List[Float] = optionToList(cz.data.pop06_5)
    var listassets06_5: List[Float] = optionToList(cz.data.assets06_5)
    checkIfLevelDataIsConsistent(cz, listarea06_5, listpop06_5, listassets06_5, explicitAssetData, 6.5f)
    var listlanduse06_5: List[Int] = optionToList(cz.data.dluc06_5)
    var listlevellanduse06_5: List[Float] = if (listlanduse06_5.isEmpty) List() else List(6.5f)
    var listlevel06_5: List[Float] = if (listarea06_5.isEmpty) List() else List(6.5f)

    var listarea07_0: List[Float] = optionToList(cz.data.area07_0)
    var listpop07_0: List[Float] = optionToList(cz.data.pop07_0)
    var listassets07_0: List[Float] = optionToList(cz.data.assets07_0)
    checkIfLevelDataIsConsistent(cz, listarea07_0, listpop07_0, listassets07_0, explicitAssetData, 7.0f)
    var listlanduse07_0: List[Int] = optionToList(cz.data.dluc07_0)
    var listlevellanduse07_0: List[Float] = if (listlanduse07_0.isEmpty) List() else List(7.0f)
    var listlevel07_0: List[Float] = if (listarea07_0.isEmpty) List() else List(7.0f)

    var listarea07_5: List[Float] = optionToList(cz.data.area07_5)
    var listpop07_5: List[Float] = optionToList(cz.data.pop07_5)
    var listassets07_5: List[Float] = optionToList(cz.data.assets07_5)
    checkIfLevelDataIsConsistent(cz, listarea07_5, listpop07_5, listassets07_5, explicitAssetData, 7.5f)
    var listlanduse07_5: List[Int] = optionToList(cz.data.dluc07_5)
    var listlevellanduse07_5: List[Float] = if (listlanduse07_5.isEmpty) List() else List(7.5f)
    var listlevel07_5: List[Float] = if (listarea07_5.isEmpty) List() else List(7.5f)

    var listarea08_0: List[Float] = optionToList(cz.data.area08_0)
    var listpop08_0: List[Float] = optionToList(cz.data.pop08_0)
    var listassets08_0: List[Float] = optionToList(cz.data.assets08_0)
    checkIfLevelDataIsConsistent(cz, listarea08_0, listpop08_0, listassets08_0, explicitAssetData, 8.0f)
    var listlanduse08_0: List[Int] = optionToList(cz.data.dluc08_0)
    var listlevellanduse08_0: List[Float] = if (listlanduse08_0.isEmpty) List() else List(8.0f)
    var listlevel08_0: List[Float] = if (listarea08_0.isEmpty) List() else List(8.0f)

    var listarea08_5: List[Float] = optionToList(cz.data.area08_5)
    var listpop08_5: List[Float] = optionToList(cz.data.pop08_5)
    var listassets08_5: List[Float] = optionToList(cz.data.assets08_5)
    checkIfLevelDataIsConsistent(cz, listarea08_5, listpop08_5, listassets08_5, explicitAssetData, 8.5f)
    var listlanduse08_5: List[Int] = optionToList(cz.data.dluc08_5)
    var listlevellanduse08_5: List[Float] = if (listlanduse08_5.isEmpty) List() else List(8.5f)
    var listlevel08_5: List[Float] = if (listarea08_5.isEmpty) List() else List(8.5f)

    var listarea09_0: List[Float] = optionToList(cz.data.area09_0)
    var listpop09_0: List[Float] = optionToList(cz.data.pop09_0)
    var listassets09_0: List[Float] = optionToList(cz.data.assets09_0)
    checkIfLevelDataIsConsistent(cz, listarea09_0, listpop09_0, listassets09_0, explicitAssetData, 9.0f)
    var listlanduse09_0: List[Int] = optionToList(cz.data.dluc09_0)
    var listlevellanduse09_0: List[Float] = if (listlanduse09_0.isEmpty) List() else List(9.0f)
    var listlevel09_0: List[Float] = if (listarea09_0.isEmpty) List() else List(9.0f)

    var listarea09_5: List[Float] = optionToList(cz.data.area09_5)
    var listpop09_5: List[Float] = optionToList(cz.data.pop09_5)
    var listassets09_5: List[Float] = optionToList(cz.data.assets09_5)
    checkIfLevelDataIsConsistent(cz, listarea09_5, listpop09_5, listassets09_5, explicitAssetData, 9.5f)
    var listlanduse09_5: List[Int] = optionToList(cz.data.dluc09_5)
    var listlevellanduse09_5: List[Float] = if (listlanduse09_5.isEmpty) List() else List(9.5f)
    var listlevel09_5: List[Float] = if (listarea09_5.isEmpty) List() else List(9.5f)

    var listarea10_0: List[Float] = optionToList(cz.data.area10_0)
    var listpop10_0: List[Float] = optionToList(cz.data.pop10_0)
    var listassets10_0: List[Float] = optionToList(cz.data.assets10_0)
    checkIfLevelDataIsConsistent(cz, listarea10_0, listpop10_0, listassets10_0, explicitAssetData, 10.0f)
    var listlanduse10_0: List[Int] = optionToList(cz.data.dluc10_0)
    var listlevellanduse10_0: List[Float] = if (listlanduse10_0.isEmpty) List() else List(10.0f)
    var listlevel10_0: List[Float] = if (listarea10_0.isEmpty) List() else List(10.0f)

    var listarea10_5: List[Float] = optionToList(cz.data.area10_5)
    var listpop10_5: List[Float] = optionToList(cz.data.pop10_5)
    var listassets10_5: List[Float] = optionToList(cz.data.assets10_5)
    checkIfLevelDataIsConsistent(cz, listarea10_5, listpop10_5, listassets10_5, explicitAssetData, 10.5f)
    var listlanduse10_5: List[Int] = optionToList(cz.data.dluc10_5)
    var listlevellanduse10_5: List[Float] = if (listlanduse10_5.isEmpty) List() else List(10.5f)
    var listlevel10_5: List[Float] = if (listarea10_5.isEmpty) List() else List(10.5f)

    var listarea11_0: List[Float] = optionToList(cz.data.area11_0)
    var listpop11_0: List[Float] = optionToList(cz.data.pop11_0)
    var listassets11_0: List[Float] = optionToList(cz.data.assets11_0)
    checkIfLevelDataIsConsistent(cz, listarea11_0, listpop11_0, listassets11_0, explicitAssetData, 11.0f)
    var listlanduse11_0: List[Int] = optionToList(cz.data.dluc11_0)
    var listlevellanduse11_0: List[Float] = if (listlanduse11_0.isEmpty) List() else List(11.0f)
    var listlevel11_0: List[Float] = if (listarea11_0.isEmpty) List() else List(11.0f)

    var listarea11_5: List[Float] = optionToList(cz.data.area11_5)
    var listpop11_5: List[Float] = optionToList(cz.data.pop11_5)
    var listassets11_5: List[Float] = optionToList(cz.data.assets11_5)
    checkIfLevelDataIsConsistent(cz, listarea11_5, listpop11_5, listassets11_5, explicitAssetData, 11.5f)
    var listlanduse11_5: List[Int] = optionToList(cz.data.dluc11_5)
    var listlevellanduse11_5: List[Float] = if (listlanduse11_5.isEmpty) List() else List(11.5f)
    var listlevel11_5: List[Float] = if (listarea11_5.isEmpty) List() else List(11.5f)

    var listarea12_0: List[Float] = optionToList(cz.data.area12_0)
    var listpop12_0: List[Float] = optionToList(cz.data.pop12_0)
    var listassets12_0: List[Float] = optionToList(cz.data.assets12_0)
    checkIfLevelDataIsConsistent(cz, listarea12_0, listpop12_0, listassets12_0, explicitAssetData, 12.0f)
    var listlanduse12_0: List[Int] = optionToList(cz.data.dluc12_0)
    var listlevellanduse12_0: List[Float] = if (listlanduse12_0.isEmpty) List() else List(12.0f)
    var listlevel12_0: List[Float] = if (listarea12_0.isEmpty) List() else List(12.0f)

    var listarea12_5: List[Float] = optionToList(cz.data.area12_5)
    var listpop12_5: List[Float] = optionToList(cz.data.pop12_5)
    var listassets12_5: List[Float] = optionToList(cz.data.assets12_5)
    checkIfLevelDataIsConsistent(cz, listarea12_5, listpop12_5, listassets12_5, explicitAssetData, 12.5f)
    var listlanduse12_5: List[Int] = optionToList(cz.data.dluc12_5)
    var listlevellanduse12_5: List[Float] = if (listlanduse12_5.isEmpty) List() else List(12.5f)
    var listlevel12_5: List[Float] = if (listarea12_5.isEmpty) List() else List(12.5f)

    var listarea13_0: List[Float] = optionToList(cz.data.area13_0)
    var listpop13_0: List[Float] = optionToList(cz.data.pop13_0)
    var listassets13_0: List[Float] = optionToList(cz.data.assets13_0)
    checkIfLevelDataIsConsistent(cz, listarea13_0, listpop13_0, listassets13_0, explicitAssetData, 13.0f)
    var listlanduse13_0: List[Int] = optionToList(cz.data.dluc13_0)
    var listlevellanduse13_0: List[Float] = if (listlanduse13_0.isEmpty) List() else List(13.0f)
    var listlevel13_0: List[Float] = if (listarea13_0.isEmpty) List() else List(13.0f)

    var listarea13_5: List[Float] = optionToList(cz.data.area13_5)
    var listpop13_5: List[Float] = optionToList(cz.data.pop13_5)
    var listassets13_5: List[Float] = optionToList(cz.data.assets13_5)
    checkIfLevelDataIsConsistent(cz, listarea13_5, listpop13_5, listassets13_5, explicitAssetData, 13.5f)
    var listlanduse13_5: List[Int] = optionToList(cz.data.dluc13_5)
    var listlevellanduse13_5: List[Float] = if (listlanduse13_5.isEmpty) List() else List(13.5f)
    var listlevel13_5: List[Float] = if (listarea13_5.isEmpty) List() else List(13.5f)

    var listarea14_0: List[Float] = optionToList(cz.data.area14_0)
    var listpop14_0: List[Float] = optionToList(cz.data.pop14_0)
    var listassets14_0: List[Float] = optionToList(cz.data.assets14_0)
    checkIfLevelDataIsConsistent(cz, listarea14_0, listpop14_0, listassets14_0, explicitAssetData, 14.0f)
    var listlanduse14_0: List[Int] = optionToList(cz.data.dluc14_0)
    var listlevellanduse14_0: List[Float] = if (listlanduse14_0.isEmpty) List() else List(14.0f)
    var listlevel14_0: List[Float] = if (listarea14_0.isEmpty) List() else List(14.0f)

    var listarea14_5: List[Float] = optionToList(cz.data.area14_5)
    var listpop14_5: List[Float] = optionToList(cz.data.pop14_5)
    var listassets14_5: List[Float] = optionToList(cz.data.assets14_5)
    checkIfLevelDataIsConsistent(cz, listarea14_5, listpop14_5, listassets14_5, explicitAssetData, 14.5f)
    var listlanduse14_5: List[Int] = optionToList(cz.data.dluc14_5)
    var listlevellanduse14_5: List[Float] = if (listlanduse14_5.isEmpty) List() else List(14.5f)
    var listlevel14_5: List[Float] = if (listarea14_5.isEmpty) List() else List(14.5f)

    var listarea15_0: List[Float] = optionToList(cz.data.area15_0)
    var listpop15_0: List[Float] = optionToList(cz.data.pop15_0)
    var listassets15_0: List[Float] = optionToList(cz.data.assets15_0)
    checkIfLevelDataIsConsistent(cz, listarea15_0, listpop15_0, listassets15_0, explicitAssetData, 15.0f)
    var listlanduse15_0: List[Int] = optionToList(cz.data.dluc15_0)
    var listlevellanduse15_0: List[Float] = if (listlanduse15_0.isEmpty) List() else List(15.0f)
    var listlevel15_0: List[Float] = if (listarea15_0.isEmpty) List() else List(15.0f)

    var listarea15_5: List[Float] = optionToList(cz.data.area15_5)
    var listpop15_5: List[Float] = optionToList(cz.data.pop15_5)
    var listassets15_5: List[Float] = optionToList(cz.data.assets15_5)
    checkIfLevelDataIsConsistent(cz, listarea15_5, listpop15_5, listassets15_5, explicitAssetData, 15.5f)
    var listlanduse15_5: List[Int] = optionToList(cz.data.dluc15_5)
    var listlevellanduse15_5: List[Float] = if (listlanduse15_5.isEmpty) List() else List(15.5f)
    var listlevel15_5: List[Float] = if (listarea15_5.isEmpty) List() else List(15.5f)

    var listarea16_0: List[Float] = optionToList(cz.data.area16_0)
    var listpop16_0: List[Float] = optionToList(cz.data.pop16_0)
    var listassets16_0: List[Float] = optionToList(cz.data.assets16_0)
    checkIfLevelDataIsConsistent(cz, listarea16_0, listpop16_0, listassets16_0, explicitAssetData, 16.0f)
    var listlanduse16_0: List[Int] = optionToList(cz.data.dluc16_0)
    var listlevellanduse16_0: List[Float] = if (listlanduse16_0.isEmpty) List() else List(16.0f)
    var listlevel16_0: List[Float] = if (listarea16_0.isEmpty) List() else List(16.0f)

    var listarea16_5: List[Float] = optionToList(cz.data.area16_5)
    var listpop16_5: List[Float] = optionToList(cz.data.pop16_5)
    var listassets16_5: List[Float] = optionToList(cz.data.assets16_5)
    checkIfLevelDataIsConsistent(cz, listarea16_5, listpop16_5, listassets16_5, explicitAssetData, 16.5f)
    var listlanduse16_5: List[Int] = optionToList(cz.data.dluc16_5)
    var listlevellanduse16_5: List[Float] = if (listlanduse16_5.isEmpty) List() else List(16.5f)
    var listlevel16_5: List[Float] = if (listarea16_5.isEmpty) List() else List(16.5f)

    var listarea17_0: List[Float] = optionToList(cz.data.area17_0)
    var listpop17_0: List[Float] = optionToList(cz.data.pop17_0)
    var listassets17_0: List[Float] = optionToList(cz.data.assets17_0)
    checkIfLevelDataIsConsistent(cz, listarea17_0, listpop17_0, listassets17_0, explicitAssetData, 17.0f)
    var listlanduse17_0: List[Int] = optionToList(cz.data.dluc17_0)
    var listlevellanduse17_0: List[Float] = if (listlanduse17_0.isEmpty) List() else List(17.0f)
    var listlevel17_0: List[Float] = if (listarea17_0.isEmpty) List() else List(17.0f)

    var listarea17_5: List[Float] = optionToList(cz.data.area17_5)
    var listpop17_5: List[Float] = optionToList(cz.data.pop17_5)
    var listassets17_5: List[Float] = optionToList(cz.data.assets17_5)
    checkIfLevelDataIsConsistent(cz, listarea17_5, listpop17_5, listassets17_5, explicitAssetData, 17.5f)
    var listlanduse17_5: List[Int] = optionToList(cz.data.dluc17_5)
    var listlevellanduse17_5: List[Float] = if (listlanduse17_5.isEmpty) List() else List(17.5f)
    var listlevel17_5: List[Float] = if (listarea17_5.isEmpty) List() else List(17.5f)

    var listarea18_0: List[Float] = optionToList(cz.data.area18_0)
    var listpop18_0: List[Float] = optionToList(cz.data.pop18_0)
    var listassets18_0: List[Float] = optionToList(cz.data.assets18_0)
    checkIfLevelDataIsConsistent(cz, listarea18_0, listpop18_0, listassets18_0, explicitAssetData, 18.0f)
    var listlanduse18_0: List[Int] = optionToList(cz.data.dluc18_0)
    var listlevellanduse18_0: List[Float] = if (listlanduse18_0.isEmpty) List() else List(18.0f)
    var listlevel18_0: List[Float] = if (listarea18_0.isEmpty) List() else List(18.0f)

    var listarea18_5: List[Float] = optionToList(cz.data.area18_5)
    var listpop18_5: List[Float] = optionToList(cz.data.pop18_5)
    var listassets18_5: List[Float] = optionToList(cz.data.assets18_5)
    checkIfLevelDataIsConsistent(cz, listarea18_5, listpop18_5, listassets18_5, explicitAssetData, 18.5f)
    var listlanduse18_5: List[Int] = optionToList(cz.data.dluc18_5)
    var listlevellanduse18_5: List[Float] = if (listlanduse18_5.isEmpty) List() else List(18.5f)
    var listlevel18_5: List[Float] = if (listarea18_5.isEmpty) List() else List(18.5f)

    var listarea19_0: List[Float] = optionToList(cz.data.area19_0)
    var listpop19_0: List[Float] = optionToList(cz.data.pop19_0)
    var listassets19_0: List[Float] = optionToList(cz.data.assets19_0)
    checkIfLevelDataIsConsistent(cz, listarea19_0, listpop19_0, listassets19_0, explicitAssetData, 19.0f)
    var listlanduse19_0: List[Int] = optionToList(cz.data.dluc19_0)
    var listlevellanduse19_0: List[Float] = if (listlanduse19_0.isEmpty) List() else List(19.0f)
    var listlevel19_0: List[Float] = if (listarea19_0.isEmpty) List() else List(19.0f)

    var listarea19_5: List[Float] = optionToList(cz.data.area19_5)
    var listpop19_5: List[Float] = optionToList(cz.data.pop19_5)
    var listassets19_5: List[Float] = optionToList(cz.data.assets19_5)
    checkIfLevelDataIsConsistent(cz, listarea19_5, listpop19_5, listassets19_5, explicitAssetData, 19.5f)
    var listlanduse19_5: List[Int] = optionToList(cz.data.dluc19_5)
    var listlevellanduse19_5: List[Float] = if (listlanduse19_5.isEmpty) List() else List(19.5f)
    var listlevel19_5: List[Float] = if (listarea19_5.isEmpty) List() else List(19.5f)

    var listarea20_0: List[Float] = optionToList(cz.data.area20_0)
    var listpop20_0: List[Float] = optionToList(cz.data.pop20_0)
    var listassets20_0: List[Float] = optionToList(cz.data.assets20_0)
    checkIfLevelDataIsConsistent(cz, listarea20_0, listpop20_0, listassets20_0, explicitAssetData, 20.0f)
    var listlanduse20_0: List[Int] = optionToList(cz.data.dluc20_0)
    var listlevellanduse20_0: List[Float] = if (listlanduse20_0.isEmpty) List() else List(20.0f)
    var listlevel20_0: List[Float] = if (listarea20_0.isEmpty) List() else List(20.0f)

    var listarea20_5: List[Float] = optionToList(cz.data.area20_5)
    var listpop20_5: List[Float] = optionToList(cz.data.pop20_5)
    var listassets20_5: List[Float] = optionToList(cz.data.assets20_5)
    checkIfLevelDataIsConsistent(cz, listarea20_5, listpop20_5, listassets20_5, explicitAssetData, 20.5f)
    var listlanduse20_5: List[Int] = optionToList(cz.data.dluc20_5)
    var listlevellanduse20_5: List[Float] = if (listlanduse20_5.isEmpty) List() else List(20.5f)
    var listlevel20_5: List[Float] = if (listarea20_5.isEmpty) List() else List(20.5f)

    var levelList: List[Float] =
      listlevel00_0 ++ listlevel00_5 ++ listlevel01_0 ++ listlevel01_5 ++ listlevel02_0 ++ listlevel02_5 ++ listlevel03_0 ++ listlevel03_5 ++ listlevel04_0 ++ listlevel04_5 ++ listlevel05_0 ++ listlevel05_5 ++ listlevel06_0 ++ listlevel06_5 ++ listlevel07_0 ++ listlevel07_5 ++ listlevel08_0 ++ listlevel08_5 ++ listlevel09_0 ++ listlevel09_5 ++ listlevel10_0 ++ listlevel10_5 ++ listlevel11_0 ++ listlevel11_5 ++ listlevel12_0 ++ listlevel12_5 ++ listlevel13_0 ++ listlevel13_5 ++ listlevel14_0 ++ listlevel14_5 ++ listlevel15_0 ++ listlevel15_5 ++ listlevel16_0 ++ listlevel16_5 ++ listlevel17_0 ++ listlevel17_5 ++ listlevel18_0 ++ listlevel18_5 ++ listlevel19_0 ++ listlevel19_5 ++ listlevel20_0 ++ listlevel20_5
    var areaList: List[Float] =
      listarea00_0 ++ listarea00_5 ++ listarea01_0 ++ listarea01_5 ++ listarea02_0 ++ listarea02_5 ++ listarea03_0 ++ listarea03_5 ++ listarea04_0 ++ listarea04_5 ++ listarea05_0 ++ listarea05_5 ++ listarea06_0 ++ listarea06_5 ++ listarea07_0 ++ listarea07_5 ++ listarea08_0 ++ listarea08_5 ++ listarea09_0 ++ listarea09_5 ++ listarea10_0 ++ listarea10_5 ++ listarea11_0 ++ listarea11_5 ++ listarea12_0 ++ listarea12_5 ++ listarea13_0 ++ listarea13_5 ++ listarea14_0 ++ listarea14_5 ++ listarea15_0 ++ listarea15_5 ++ listarea16_0 ++ listarea16_5 ++ listarea17_0 ++ listarea17_5 ++ listarea18_0 ++ listarea18_5 ++ listarea19_0 ++ listarea19_5 ++ listarea20_0 ++ listarea20_5
    var populationList: List[Float] =
      listpop00_0 ++ listpop00_5 ++ listpop01_0 ++ listpop01_5 ++ listpop02_0 ++ listpop02_5 ++ listpop03_0 ++ listpop03_5 ++ listpop04_0 ++ listpop04_5 ++ listpop05_0 ++ listpop05_5 ++ listpop06_0 ++ listpop06_5 ++ listpop07_0 ++ listpop07_5 ++ listpop08_0 ++ listpop08_5 ++ listpop09_0 ++ listpop09_5 ++ listpop10_0 ++ listpop10_5 ++ listpop11_0 ++ listpop11_5 ++ listpop12_0 ++ listpop12_5 ++ listpop13_0 ++ listpop13_5 ++ listpop14_0 ++ listpop14_5 ++ listpop15_0 ++ listpop15_5 ++ listpop16_0 ++ listpop16_5 ++ listpop17_0 ++ listpop17_5 ++ listpop18_0 ++ listpop18_5 ++ listpop19_0 ++ listpop19_5 ++ listpop20_0 ++ listpop20_5
    var assetList: List[Float] =
      listassets00_0 ++ listassets00_5 ++ listassets01_0 ++ listassets01_5 ++ listassets02_0 ++ listassets02_5 ++ listassets03_0 ++ listassets03_5 ++ listassets04_0 ++ listassets04_5 ++ listassets05_0 ++ listassets05_5 ++ listassets06_0 ++ listassets06_5 ++ listassets07_0 ++ listassets07_5 ++ listassets08_0 ++ listassets08_5 ++ listassets09_0 ++ listassets09_5 ++ listassets10_0 ++ listassets10_5 ++ listassets11_0 ++ listassets11_5 ++ listassets12_0 ++ listassets12_5 ++ listassets13_0 ++ listassets13_5 ++ listassets14_0 ++ listassets14_5 ++ listassets15_0 ++ listassets15_5 ++ listassets16_0 ++ listassets16_5 ++ listassets17_0 ++ listassets17_5 ++ listassets18_0 ++ listassets18_5 ++ listassets19_0 ++ listassets19_5 ++ listassets20_0 ++ listassets20_5
    var landuseList: List[Int] =
      listlanduse00_0 ++ listlanduse00_5 ++ listlanduse01_0 ++ listlanduse01_5 ++ listlanduse02_0 ++ listlanduse02_5 ++ listlanduse03_0 ++ listlanduse03_5 ++ listlanduse04_0 ++ listlanduse04_5 ++ listlanduse05_0 ++ listlanduse05_5 ++ listlanduse06_0 ++ listlanduse06_5 ++ listlanduse07_0 ++ listlanduse07_5 ++ listlanduse08_0 ++ listlanduse08_5 ++ listlanduse09_0 ++ listlanduse09_5 ++ listlanduse10_0 ++ listlanduse10_5 ++ listlanduse11_0 ++ listlanduse11_5 ++ listlanduse12_0 ++ listlanduse12_5 ++ listlanduse13_0 ++ listlanduse13_5 ++ listlanduse14_0 ++ listlanduse14_5 ++ listlanduse15_0 ++ listlanduse15_5 ++ listlanduse16_0 ++ listlanduse16_5 ++ listlanduse17_0 ++ listlanduse17_5 ++ listlanduse18_0 ++ listlanduse18_5 ++ listlanduse19_0 ++ listlanduse19_5 ++ listlanduse20_0 ++ listlanduse20_5
    var levellanduseList: List[Float] =
      listlevellanduse00_0 ++ listlevellanduse00_5 ++ listlevellanduse01_0 ++ listlevellanduse01_5 ++ listlevellanduse02_0 ++ listlevellanduse02_5 ++ listlevellanduse03_0 ++ listlevellanduse03_5 ++ listlevellanduse04_0 ++ listlevellanduse04_5 ++ listlevellanduse05_0 ++ listlevellanduse05_5 ++ listlevellanduse06_0 ++ listlevellanduse06_5 ++ listlevellanduse07_0 ++ listlevellanduse07_5 ++ listlevellanduse08_0 ++ listlevellanduse08_5 ++ listlevellanduse09_0 ++ listlevellanduse09_5 ++ listlevellanduse10_0 ++ listlevellanduse10_5 ++ listlevellanduse11_0 ++ listlevellanduse11_5 ++ listlevellanduse12_0 ++ listlevellanduse12_5 ++ listlevellanduse13_0 ++ listlevellanduse13_5 ++ listlevellanduse14_0 ++ listlevellanduse14_5 ++ listlevellanduse15_0 ++ listlevellanduse15_5 ++ listlevellanduse16_0 ++ listlevellanduse16_5 ++ listlevellanduse17_0 ++ listlevellanduse17_5 ++ listlevellanduse18_0 ++ listlevellanduse18_5 ++ listlevellanduse19_0 ++ listlevellanduse19_5 ++ listlevellanduse20_0 ++ listlevellanduse20_5

    //ToDo: consistency check ?
    var landuseListNew = addMissingLanduse(cz, levelList, levellanduseList, landuseList)
    if (landuseListNew.size != landuseList.size) {
      logger.debug("Changed landuse list for CZ " + cz.locationid)
      logger.debug("Old: " + landuseList)
      logger.debug("New: " + landuseListNew)
    }

    if (landuseListNew.size == 0) {
      landuseListNew = cz.data.landuse match {
        case None => levelList.map(_ => -999)
        case Some(x) => levelList.map(_ => x)
      }
    }

    (levelList.toArray, areaList.toArray, populationList.toArray, assetList.toArray, landuseListNew.toArray)
  }

  private def getAssetDataForLevel(cz: Cz, assetData: Option[Float], level: Float): Float = assetData match {
    case None =>
      logger.error("Found no explicit asset data for cz with id " + cz.locationid + " for elevation level " + level + ", but this data is required in the current configuration"); 0f
    case Some(v) => v
  }

  private def optionToList[T](data: Option[T]): List[T] = data match {
    case None => List()
    case Some(v) => List(v)
  }

  private def checkIfLevelDataIsConsistent(cz: Cz, listarea: List[Float], listpop: List[Float], listassets: List[Float], explicitAssetData: Boolean, level: Float) {
    if (explicitAssetData) {
      if ((listarea.length != listpop.length) || (listassets.length != listpop.length)) {
        logger.error("Found inconsistent optional coastal area/population/asset data for cz with id " + cz.locationid + " at level " + level +
          ". (Found configuation: area => " + listarea.length + ", pop => " + listpop.length + ", assets => " + listassets.length + ". Expected all three or none of them.)")
      }
    } else {
      if ((listarea.length != listpop.length) || ((listarea.length == 0) && (listpop.length == 0) && (listassets.length > 0))) {
        logger.error("Found inconsistent optional coastal area/population/asset data for cz with id " + cz.locationid + " at level " + level +
          ". (Found configuation: area => " + listarea.length + ", pop => " + listpop.length + ", assets => " + listassets.length + ". Expected area and pop and maybe assets.)")
      }
    }
  }

  private def correctAssetData(assets: Array[Float], population: Array[Float], assetsPerPerson: Float): Array[Float] = {
    //(assets zip population).map ( x => x ).unzip._1

    for (i <- 0 until assets.length) {
      if (assets(i) < population(i) * assetsPerPerson)
        assets(i) = population(i) * assetsPerPerson
    }
    assets
  }

  private def projectPopulationData(assets: Array[Float], population: Array[Float], assetsPerPerson: Float): Array[Float] = {
    //(assets zip population).map ( x => x ).unzip._1

    for (i <- 0 until population.length) {
      population(i) = assets(i) / assetsPerPerson
    }
    population
  }

  private def getExistingLevels(cls: Cls): Array[Float] = {
    var levels: Array[Float] = Array()
    cls.czs.foreach { cz => levels = levels ++ cz.coastalPlane.areaExposure.lLevels }
    levels.sortWith(_ < _).distinct
  }

}

