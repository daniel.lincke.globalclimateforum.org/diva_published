/* Automatically generated data features for Whs
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data

import diva.annotations._
import diva.types._
import diva.types.TypeComputations._
import scala.collection.mutable._


class WhsData {

  @Parameter
  @Description(value = "")
  var clsid :  Int = default[Int]

  @Input
  @Parameter
  @Description(value = "")
  var id :  Int = default[Int]

  @Input
  @Parameter
  @Description(value = "")
  var lati :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "")
  var longi :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Indicates that this site cannot be threatened by sea-level rise, because of its altitude")
  var mask :  Int = 1

  @Output
  @Variable
  @Description(value = "Indicates weather the site is threatened by sea-level rise")
  var threatened :  Int = default[Int]

  @Input
  @Parameter
  @Description(value = "")
  var unescu :  Int = default[Int]

  override def clone : WhsData = {
    var ret: WhsData  = new WhsData
    ret.clsid = clsid
    ret.id = id
    ret.lati = lati
    ret.longi = longi
    ret.mask = mask
    ret.threatened = threatened
    ret.unescu = unescu
    ret
  }

}
