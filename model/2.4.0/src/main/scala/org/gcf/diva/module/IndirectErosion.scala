package diva.module

import scala.collection.parallel.immutable.ParSeq

import diva.model.ModuleI
import diva.model.divaState
import diva.model.divaFeatures._
import diva.module.erosion.AsmitaCoefficients
import diva.toolbox.Toolbox._
import diva.feature._


class IndirectErosion extends ModuleI {

  // configurable parameters
  var startAtDynEquilibrium: Boolean = true;
  var initialGMSLRateInMMperYear: Float = 2.0f;

  //private AsmitaCoefficients[] asmitaCoefficients;

  def moduleName: String = "Indirect Erosion Module"
  def moduleVersion: String = "2.0 (migrated to scala from old Version 1.5)"
  def moduleDescription: String = "This is a reduced ASMITA model of Delft Hydraulics. It calculates the loss of land, the loss of sand and the demand for nourishment due to indirect erosion in tidal basins."
  def moduleAuthor: String = "Luc Bijsterbosch, Zheng Bing Wang, Gerben Boot, Daniel Lincke"

  def getInput: Array[java.lang.String] = Array("cls.waveclim", "cls.tidalrng", "cls.rslr", "cls.rslr_last", "cls.tnour", "basin.clsid", "basin.ce", "basin.dcf", "basin.dcd", "basin.ddo", "basin.wsf", "basin.wsc", "basin.wsd", "basin.nf", "basin.nc", "basin.nd", "basin.lagoona", "basin.numinlet")
  def getOutput: Array[java.lang.String] = Array("basin.landleroind", "basin.sandlind", "basin.nourbasin", "basin.nbbenefit", "admin.landleroind", "admin.sandlind", "admin.nourbasin", "country.landleroind", "country.sandlind", "country.nourbasin")

  def initModule(initialState: divaState) = doNothing

  def computeScenario(currentState: diva.model.divaState) = shouldNotBeInvoked

  def init(initialState: divaState) = {

    var basins: ParSeq[Basin] = initialState.getFeatures[Basin].par
    basins.foreach { basin: Basin =>
      {
        basin.asmitaCoefficients = new AsmitaCoefficients
        var ce: Float = basin.data.ce;
        var d_cf: Float = basin.data.dcf;
        var d_cd: Float = basin.data.dcd;
        var d_do: Float = basin.data.ddo;
        var wsf: Float = basin.data.wsf;
        var wsc: Float = basin.data.wsc;
        var wsd: Float = basin.data.wsd;
        basin.asmitaCoefficients.nf = basin.data.nf;
        basin.asmitaCoefficients.nc = basin.data.nc;
        basin.asmitaCoefficients.nd = basin.data.nd;

        // Lagoon details
        var ab: Float = basin.data.lagoona / basin.data.numinlet;
        basin.asmitaCoefficients.af = ab - 2.5E-05f * scala.math.pow(ab, 1.5).asInstanceOf[Float]

        if (basin.asmitaCoefficients.af < (0.1f * ab))
          basin.asmitaCoefficients.af = 0.1f * ab

        basin.asmitaCoefficients.ac = ab - basin.asmitaCoefficients.af;

        basin.clss.foreach { cls: Cls =>
          {
            var alphad: Float =
              if (cls.data.waveclim == 1) 8.64E-03f
              else if ((cls.data.waveclim == 2) || (cls.data.waveclim == 3)) 6.44E-03f
              else if ((cls.data.waveclim == 4) || (cls.data.waveclim == 5)) 5.33E-03f
              else 0f

            basin.asmitaCoefficients.hf = 0.5f * cls.data.mtidalrng
            basin.asmitaCoefficients.vfe = basin.asmitaCoefficients.af * basin.asmitaCoefficients.hf
            var vtidal: Float = ab * cls.data.mtidalrng - basin.asmitaCoefficients.vfe
            basin.asmitaCoefficients.vce = 6.5E-05f * scala.math.pow(vtidal, 1.5).asInstanceOf[Float]
            basin.asmitaCoefficients.vde = alphad * scala.math.pow(vtidal, 1.23).asInstanceOf[Float]
            basin.asmitaCoefficients.ad = 3.46E+03f * scala.math.pow(vtidal, 0.51).asInstanceOf[Float]

            var dcf: Double = d_cf * (basin.asmitaCoefficients.af * cls.data.mtidalrng - basin.asmitaCoefficients.vfe) / scala.math.sqrt(ab)
            var dcd: Double = (d_cd) * vtidal / scala.math.sqrt(ab)
            var ddo: Double = (d_do * vtidal) / scala.math.sqrt(basin.asmitaCoefficients.ad)

            //Help variables (no explanation)
            var waf: Float = wsf * basin.asmitaCoefficients.af
            var wac: Float = wsc * basin.asmitaCoefficients.ac
            var wad: Float = wsd * basin.asmitaCoefficients.ad
            var x: Double = dcd * waf * wac + wad * dcd * dcf + wad * wac * waf + wad * wac * dcf + wad * dcf * waf + wad * dcd * waf + dcd * dcf * waf + dcd * dcf * wac + wac * ddo * dcf + wac * ddo * waf + ddo * dcf * waf + ddo * dcd * dcf + ddo * dcd * waf

            //Working variables (no explanation)
            basin.asmitaCoefficients.m1 = (-waf * ce * dcf / x * (ddo * dcd + dcd * wac + ddo * wac + wad * dcd + wad * wac)).asInstanceOf[Float]
            basin.asmitaCoefficients.m2 = (waf * ce * dcf / x * (dcd * wac + wac * ddo + wac * wad)).asInstanceOf[Float]
            basin.asmitaCoefficients.m3 = (waf * ce * dcf / x * (wad * dcd)).asInstanceOf[Float]
            basin.asmitaCoefficients.m4 = (waf * ce * dcf / x * (ddo * dcd)).asInstanceOf[Float]
            basin.asmitaCoefficients.m5 = (-wac * ce / x * (dcd * dcf * waf + waf * ddo * dcf + waf * wad * dcf)).asInstanceOf[Float]
            basin.asmitaCoefficients.m6 = (wac * ce / x * (ddo * dcd * waf + wad * dcd * waf + wad * dcd * dcf + ddo * dcd * dcf + dcd * dcf * waf + ddo * dcf * waf + wad * dcf * waf)).asInstanceOf[Float]
            basin.asmitaCoefficients.m7 = (-wac * ce / x * (wad * dcd * dcf + wad * dcd * waf)).asInstanceOf[Float]
            basin.asmitaCoefficients.m8 = (-wac * ce / x * (ddo * dcd * dcf + ddo * dcd * waf)).asInstanceOf[Float]
            basin.asmitaCoefficients.m9 = (wad * ce / x * (dcd * dcf * waf)).asInstanceOf[Float]
            basin.asmitaCoefficients.m10 = (wad * ce / x * (dcd * waf * wac + dcd * dcf * wac)).asInstanceOf[Float]
            basin.asmitaCoefficients.m11 = (-wad * ce / x * (dcd * waf * wac + dcd * dcf * wac + dcd * dcf * waf + ddo * dcf * waf + wac * ddo * dcf + wac * ddo * waf + ddo * dcd * dcf + ddo * dcd * waf)).asInstanceOf[Float]
            basin.asmitaCoefficients.m12 = (wad * ce / x * (ddo * dcd * dcf + ddo * dcd * waf + wac * ddo * dcf + ddo * dcf * waf + wac * ddo * waf)).asInstanceOf[Float]

            if (startAtDynEquilibrium) {
              // rate of sea level rise in m/day, the defaut assumption is 2 mm/year in 1995
              var dslr: Float = ((initialGMSLRateInMMperYear) / 1000) / 365
              var t1: Double = (basin.asmitaCoefficients.af) / dcf
              var t2: Double = (basin.asmitaCoefficients.af + basin.asmitaCoefficients.ac) / dcd
              var t3: Double = (basin.asmitaCoefficients.af + basin.asmitaCoefficients.ac + basin.asmitaCoefficients.ad) / ddo
              basin.asmitaCoefficients.vf = (basin.asmitaCoefficients.vfe * scala.math.pow(1 - dslr * 1 / ce * (t1 + t2 + t3 + 1 / wsf), 1 / basin.asmitaCoefficients.nf)).asInstanceOf[Float]
              basin.asmitaCoefficients.vc = (basin.asmitaCoefficients.vce * scala.math.pow(1 - dslr * 1 / ce * (t2 + t3 + 1 / wsc), 1 / basin.asmitaCoefficients.nc)).asInstanceOf[Float]
              basin.asmitaCoefficients.vd = (basin.asmitaCoefficients.vde * scala.math.pow(1 - dslr * 1 / ce * (t3 + 1 / wsd), 1 / basin.asmitaCoefficients.nd)).asInstanceOf[Float]
            } else {
              basin.asmitaCoefficients.vf = basin.asmitaCoefficients.vfe
              basin.asmitaCoefficients.vc = basin.asmitaCoefficients.vce
              basin.asmitaCoefficients.vd = basin.asmitaCoefficients.vde
            }
          }
        }
      }
    }

  }

  def invoke(state: divaState) {

    var clss: ParSeq[Cls] = state.getFeatures[Cls].par
    clss.foreach { cls: Cls => computeIndirectErosion(cls, state) }
    accumulate(state)

  }

  def computeIndirectErosion(cls: Cls, state: divaState) =
    cls.data.basinid match {
      case None     => doNothing;
      case Some(id) => applyAstimaModel(cls, state)
    }

  def applyAstimaModel(cls: Cls, state: divaState) {

    // this function is only called for cls which HAVE a basin attached!
    if (cls.basin == None) {
      logger.fatal("Programmer fucked it up: applyAstimaModel called for a Cls (" + cls.locationid + ") without Basin!")
    }
    var basin: Basin = cls.basin.get

    // set nodata items
    var missing = -999.999f;
    var eps = 1.0e-6f;

    // calculation per 5 years! (loop over timesteps 2200, 2300, 2400, 2500)
    var tot_daf = 0f;
    var tot_dvci = 0f;
    var tot_dvsup = 0f;
    var sum = 0d;

    //dt = 1; //hard coded: set to 5 years
    //long cp = (state.getCurrentTime() - state.getLastTime()) / dt; // = 5year
    for (j <- 0 until state.timeStep) {

      var drsl = cls.data.slr_current_annual_rate // in [m/year] 
      // ?? if current slr is 0 we set it to the default (2 mm/yr)
      if (drsl <= 0) drsl = 2E-3f;

      // calculate change in volume and new volumes

      var dvf =
        (basin.asmitaCoefficients.m1 * Math.pow((basin.asmitaCoefficients.vf / basin.asmitaCoefficients.vfe), basin.asmitaCoefficients.nf)
          + basin.asmitaCoefficients.m2 * Math.pow((basin.asmitaCoefficients.vc / basin.asmitaCoefficients.vce), basin.asmitaCoefficients.nc)
          + basin.asmitaCoefficients.m3 * Math.pow((basin.asmitaCoefficients.vd / basin.asmitaCoefficients.vde), basin.asmitaCoefficients.nd)
          + basin.asmitaCoefficients.m4) * 365 - basin.asmitaCoefficients.af * drsl;

      var dvc =
        (basin.asmitaCoefficients.m5 * Math.pow((basin.asmitaCoefficients.vf / basin.asmitaCoefficients.vfe), basin.asmitaCoefficients.nf)
          + basin.asmitaCoefficients.m6 * Math.pow((basin.asmitaCoefficients.vc / basin.asmitaCoefficients.vce), basin.asmitaCoefficients.nc)
          + basin.asmitaCoefficients.m7 * Math.pow((basin.asmitaCoefficients.vd / basin.asmitaCoefficients.vde), basin.asmitaCoefficients.nd)
          + basin.asmitaCoefficients.m8) * 365 + basin.asmitaCoefficients.ac * drsl;

      var dvd =
        (basin.asmitaCoefficients.m9 * Math.pow((basin.asmitaCoefficients.vf / basin.asmitaCoefficients.vfe), basin.asmitaCoefficients.nf)
          + basin.asmitaCoefficients.m10 * Math.pow((basin.asmitaCoefficients.vc / basin.asmitaCoefficients.vce), basin.asmitaCoefficients.nc)
          + basin.asmitaCoefficients.m11 * Math.pow((basin.asmitaCoefficients.vd / basin.asmitaCoefficients.vde), basin.asmitaCoefficients.nd)
          + basin.asmitaCoefficients.m12) * 365 - basin.asmitaCoefficients.ad * drsl;

      //Nourishment correction for 100 years intervals ???
      // float tnour = (cls.tnour / basin.numinlet);
      var tnour = 0f;

      sum = dvc - dvd - dvf;
      basin.asmitaCoefficients.vf = (basin.asmitaCoefficients.vf + dvf - tnour * dvf / sum).asInstanceOf[Float]
      basin.asmitaCoefficients.vc = (basin.asmitaCoefficients.vc + dvc - tnour * dvc / sum).asInstanceOf[Float]
      basin.asmitaCoefficients.vd = (basin.asmitaCoefficients.vd + dvd - tnour * dvd / sum).asInstanceOf[Float]

      //negative volumes is impossible
      if (basin.asmitaCoefficients.vf < 0) basin.asmitaCoefficients.vf = 1
      if (basin.asmitaCoefficients.vc < 0) basin.asmitaCoefficients.vc = 1
      if (basin.asmitaCoefficients.vd < 0) basin.asmitaCoefficients.vd = 1

      var daf = ((-dvf + dvf * tnour / sum) / basin.asmitaCoefficients.hf) * basin.data.numinlet
      var dvci = (drsl * (basin.asmitaCoefficients.af + basin.asmitaCoefficients.ac + basin.asmitaCoefficients.ad) + dvf - dvc + dvd) * basin.data.numinlet
      var dvsup = (-dvf + dvc - dvd) * basin.data.numinlet
      //var year = state.time - state.timeStep + j + 1

      if (daf.isNaN) {
        daf = missing
      }
      if (dvci.isNaN) {
        dvci = missing
      }
      if (dvsup.isNaN) {
        dvsup = missing
      }
      if (daf.isInfinite) {
        System.out.println("daf=" + daf + "-dvf=" + dvf + ", dvf=" + dvf + ", tnour=" + tnour + ", basin.asmitaCoefficients.hf=" + basin.asmitaCoefficients.hf + ", basin.numinlet=" + basin.data.numinlet + ", sum=" + sum);
      }

      //innerloop variables
      tot_daf = (tot_daf + daf).asInstanceOf[Float]
      tot_dvci = (tot_dvci + dvci).asInstanceOf[Float]
      tot_dvsup = (tot_dvsup + dvsup).asInstanceOf[Float]

      // RICHARD T WAS HERE; USE LAST YEAR OF INNERLOOP ONLY
      basin.data.nbbenefit = ((-dvf / basin.asmitaCoefficients.hf) * (basin.data.numinlet / dvsup)).asInstanceOf[Float]

    }

    //Calculating output variables
    //27.08.2003 Adjusted for Adaptation: basin.landleroind
    //basin.landleroind = tot_daf / sum / basin.asmitaCoefficients.hf;
    //29.09.2004 Restored original situation:
    //22.11.2004 Output parameters per year (version 1.3.5)
    cls.data.flatloss_erosion_indirect = tot_daf / state.timeStep
    cls.data.sandloss_erosion_indirect = tot_dvci / state.timeStep
    basin.data.sandloss_erosion_indirect = cls.data.sandloss_erosion_indirect
    basin.data.flatloss_erosion_indirect = cls.data.flatloss_erosion_indirect
    // reintroduce basin.data.nourbasin
    //basin.data.nourbasin =tot_dvsup/(state.getCurrentTime() - state.getLastTime());

  }

  private def accumulate(state: divaState) {

    var deltas: List[Delta] = state.getFeatures[Delta]
    deltas.foreach {
      (delta: Delta) =>
        {
          delta.data.sandloss_erosion_indirect = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_indirect))
        }
    }

    var admins: List[Admin] = state.getFeatures[Admin]
    admins.foreach {
      (admin: Admin) =>
        {
          admin.data.sandloss_erosion_indirect = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_indirect))
        }
    }

    var countries: List[Country] = state.getFeatures[Country]
    countries.foreach {
      (country: Country) =>
        {
          country.data.sandloss_erosion_indirect = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_indirect))
        }
    }
    
    var regions: List[Region] = state.getFeatures[Region]
    regions.foreach {
      (region: Region) =>
        {
          region.data.sandloss_erosion_indirect = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.sandloss_erosion_indirect))
        }
    }

    var globals: List[Global] = state.getFeatures[Global]
    globals.foreach {
      (global: Global) =>
        {
          global.data.sandloss_erosion_indirect = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.sandloss_erosion_indirect))
          global.data.flatloss_erosion_indirect = global.basins.foldLeft(0f)((sum, basin) => sum + (basin.data.flatloss_erosion_indirect))
        }
    }
  }
}