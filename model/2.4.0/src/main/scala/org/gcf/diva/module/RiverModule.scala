package diva.module

import scala.collection.parallel.immutable.ParSeq

import diva.model.ModuleI

import diva.model.divaState
import diva.feature._
import diva.types.RiverProtectionMode._
import diva.toolbox.Toolbox._

class RiverModule extends ModuleI {

  def moduleName: String = "River Module"
  def moduleVersion: String = "3.0.0 (June 01, 2017)"
  def moduleDescription: String = "Calculates the distance from the river mouth over which variations in sea level are noticeable. Calculates river dikes and surge barriers."
  def moduleAuthor: String = "Daniel Lincke, Robert Nicholls"

  def getInput: Array[java.lang.String] = Array("cls.rslr", "cls.s1", "cls.s10", "cls.s100", "cls.s1000", "cls.landval_unit", "river.clsid", "river.rivdpth", "river.rivslop", "river.sal_angle")
  def getOutput: Array[java.lang.String] = Array("cls.salcost", "river.initimp", "river.incrimp_s1", "river.incrimp_s10", "river.incrimp_s100", "river.incrimp_s1000", "river.salint", "river.sal_area")

  def initModule(initialState: divaState) = doNothing
  def computeScenario(state: divaState) = shouldNotBeInvoked

  var riverProtectionMode: RiverProtectionMode = CBA_PROTECTION

  // Achtung: das irgendwann global machen!
  var seadikeConstructionCostFactor: Float = 1.0f
  var surgeBarrierUnitCost: Float = 0.16f // Million US$ (2014) per m width and m height
  var riverDikeRaisingStartYear: Int = 1995

  val defaultRiverDepth: Float = 4.5f
  val defaultRiverSlope: Float = 0.000095f

  def init(state: divaState) {
    var riverdistributaries: List[Riverdistributary] = state.getFeatures[Riverdistributary]
    riverdistributaries.foreach {
      riverdistributary: Riverdistributary =>
        {
          computeRiverEffects(riverdistributary, true)
          computeRiverdistributaryProtection(riverdistributary, state, true)
        }
        accumulate(state)
    }
  }

  def invoke(state: divaState) {
    var riverdistributaries: List[Riverdistributary] = state.getFeatures[Riverdistributary]
    riverdistributaries.foreach {
      riverdistributary: Riverdistributary =>
        {
          computeRiverEffects(riverdistributary, false)
          computeRiverdistributaryProtection(riverdistributary, state, state.time < riverDikeRaisingStartYear)
        }
    }

    accumulate(state)
  }

  private def computeRiverEffects(riverdistributary: Riverdistributary, init: Boolean = false) {
    riverImpactLengths(riverdistributary, init)
    riverdistributary.data.salinity_intrusion_length =
      if (riverdistributary.data.river_distributary_is_root) {
        riverSalinityIntrusionLength(riverdistributary)
      } else {
        riverSalinityIntrusionLength(riverdistributary) min riverdistributary.data.length
      }
    riverdistributary.data.salinity_area = riverSalinityIntrusionArea(riverdistributary)
    riverdistributary.cls.data.salinity_cost += riverSalinityIntrusionCost(riverdistributary)
  }

  private def riverImpactLengths(riverdistributary: Riverdistributary, init: Boolean) {

    var cls: Cls = riverdistributary.cls
    var river: River = riverdistributary.river
    var river_slope: Float = if (river.data.river_slope < 0) defaultRiverSlope else river.data.river_slope
    var river_depth: Float = if (river.data.river_depth <= 0f) defaultRiverDepth else river.data.river_depth
    // Definitions: River impact length
    // Distance from the river mouth over which variations in sea level are noticeable.
    if (init) {
      riverdistributary.data.river_impact_length_initial_tide = 0.001f * (cls.data.meanhwater) / river_slope
      riverdistributary.data.river_impact_length_initial_s1 = 0.001f * (cls.data.s0001) / river_slope
      riverdistributary.data.river_impact_length_initial_s10 = 0.001f * (cls.data.s0010) / river_slope
      riverdistributary.data.river_impact_length_initial_s100 = 0.001f * (cls.data.s0100) / river_slope
      riverdistributary.data.river_impact_length_initial_s1000 = 0.001f * (cls.data.s1000) / river_slope
    }
    riverdistributary.data.river_impact_length_tide = 0.001f * (cls.data.meanhwater + cls.data.slr_climate_induced) / river_slope
    riverdistributary.data.river_impact_length_s1 = 0.001f * (cls.data.h1 - cls.data.slr_nonclimate_induced) / river_slope
    riverdistributary.data.river_impact_length_s10 = 0.001f * (cls.data.h10 - cls.data.slr_nonclimate_induced) / river_slope
    riverdistributary.data.river_impact_length_s100 = 0.001f * (cls.data.h100 - cls.data.slr_nonclimate_induced) / river_slope
    riverdistributary.data.river_impact_length_s1000 = 0.001f * (cls.data.h1000 - cls.data.slr_nonclimate_induced) / river_slope

    if (!riverdistributary.data.river_distributary_is_root) {
      riverdistributary.data.river_impact_length_initial_tide = riverdistributary.data.river_impact_length_initial_tide min riverdistributary.data.length
      riverdistributary.data.river_impact_length_initial_s1 = riverdistributary.data.river_impact_length_initial_s1 min riverdistributary.data.length
      riverdistributary.data.river_impact_length_initial_s10 = riverdistributary.data.river_impact_length_initial_s10 min riverdistributary.data.length
      riverdistributary.data.river_impact_length_initial_s100 = riverdistributary.data.river_impact_length_initial_s100 min riverdistributary.data.length
      riverdistributary.data.river_impact_length_initial_s1000 = riverdistributary.data.river_impact_length_initial_s1000 min riverdistributary.data.length

      riverdistributary.data.river_impact_length_tide = riverdistributary.data.river_impact_length_tide min riverdistributary.data.length
      riverdistributary.data.river_impact_length_s1 = riverdistributary.data.river_impact_length_s1 min riverdistributary.data.length
      riverdistributary.data.river_impact_length_s10 = riverdistributary.data.river_impact_length_s10 min riverdistributary.data.length
      riverdistributary.data.river_impact_length_s100 = riverdistributary.data.river_impact_length_s100 min riverdistributary.data.length
      riverdistributary.data.river_impact_length_s1000 = riverdistributary.data.river_impact_length_s1000 min riverdistributary.data.length
    }

  }

  // returns salinty intrusion length in km
  // the algorithm is described in the following book chapter (schoenfeld), GVA
  private def riverSalinityIntrusionLength(riverdistributary: Riverdistributary): Float = {

    var cls: Cls = riverdistributary.cls
    var river: River = riverdistributary.river

    var river_slope: Float = if (river.data.river_slope < 0) defaultRiverSlope else river.data.river_slope
    var river_depth: Float = if (river.data.river_depth <= 0f) defaultRiverDepth else river.data.river_depth

    //Calculation of salt intrusion length
    var riv_fv: Float = 0.7f * 40f * scala.math.pow(((river_depth + cls.data.rslr) * river_slope), 0.5).asInstanceOf[Float]
    var dens_froude: Float = riv_fv / scala.math.pow((0.025f * 9.81f * (river_depth + cls.data.rslr)), 0.5).asInstanceOf[Float]
    //Maximum of Densimetric Froude Number = 1
    if (dens_froude > 1.0) dens_froude = 1.0f
    var df1: Float = scala.math.pow(dens_froude, -2f).asInstanceOf[Float];
    var df2: Float = scala.math.pow(dens_froude, 0.666f).asInstanceOf[Float];
    var df3: Float = scala.math.pow(dens_froude, 1.333f).asInstanceOf[Float];

    10f * (river_depth + cls.data.rslr.asInstanceOf[Float]) * ((0.2f * df1) - 2.0f + (3.0f * df2) - (1.2f * df3))
  }

  //Calculation of saline area (based on triangle approach) NEW in VERSION 1.2
  private def riverSalinityIntrusionArea(riverdistributary: Riverdistributary): Float = {
    var rad_angle = ((riverdistributary.data.sal_angle / 180f) * scala.math.Pi);
    ((scala.math.tan(rad_angle) * riverdistributary.data.salinity_intrusion_length) * riverdistributary.data.salinity_intrusion_length).asInstanceOf[Float]
  }

  private def riverSalinityIntrusionCost(riverdistributary: Riverdistributary): Float = {
    riverdistributary.data.salinity_area * 0.5f * riverdistributary.cls.data.landvalue_unit
  }

  private def getRiverDikeUnitCost(cls: Cls): Float = {
    cls.country.data.seadike_unit_cost_rural
  }

  private def getRiverDikeCapitalCost(riverdistributary: Riverdistributary): Float = {
    riverdistributary.data.riverdike_length * riverdistributary.data.riverdike_height * getRiverDikeUnitCost(riverdistributary.cls)
  }

  private def computeRiverdistributaryProtection(riverdistributary: Riverdistributary, state: divaState, init: Boolean = false) {
    var cls: Cls = riverdistributary.cls
    var river: River = riverdistributary.river

    if (init) {
      if (state.time == state.startTime) {
        riverDikeInitialisation(riverdistributary)
      } else {
        buildRiverdikes(state, riverdistributary)
      }
    } else {
      if (cls.data.protlevel_target > 0) {
        riverProtectionMode match {
          case OPEN_PROTECTION     => buildRiverdikes(state, riverdistributary)
          case CLOSED_PROTECTION   => buildSurgeBarrier(state, riverdistributary, init)
          case CBA_PROTECTION      => buildCBARiverProtection(riverdistributary, state, init)
          case STYLIZED_PROTECTION => buildStylizedRiverProtection(riverdistributary, state, init)
        }
      }
    }
  }

  private def riverDikeInitialisation(riverdistributary: Riverdistributary) {
    riverdistributary.data.riverdike_height_initial = 0.5f * riverdistributary.cls.data.seadike_height
    riverdistributary.data.riverdike_height = riverdistributary.data.riverdike_height_initial
    if (riverdistributary.data.riverdike_height > 0f) {
      riverdistributary.data.riverdike_length_initial = riverdistributary.data.river_impact_length_s1000 * 2
      riverdistributary.data.riverdike_length = riverdistributary.data.riverdike_length_initial
    }

    riverdistributary.data.riverdike_cost = riverdistributary.cls.country.data.seadike_unit_cost_rural * seadikeConstructionCostFactor * riverdistributary.data.riverdike_length * (riverdistributary.data.riverdike_height)
    // river dike maintenace costs are 0.5% of the cost of the total dike Heigth (i.e. the 
    // capital stock). Costs are cheaper compared to sea dikes because there are no waves
    riverdistributary.data.riverdike_maintenance_cost = 0.005f * riverdistributary.cls.country.data.seadike_unit_cost_rural * seadikeConstructionCostFactor * riverdistributary.data.river_impact_length_s1000 * 2 * riverdistributary.data.riverdike_height

    riverdistributary.cls.data.riverdike_cost += riverdistributary.data.riverdike_cost
    riverdistributary.cls.data.riverdike_maintenance_cost += riverdistributary.data.riverdike_maintenance_cost

    // later: check if we need to build initial surge barriers?
  }

  private def buildRiverdikes(state: divaState, riverdistributary: Riverdistributary) {
    var riverdike_length_old: Float = riverdistributary.data.riverdike_length
    var riverdike_height_old: Float = riverdistributary.data.riverdike_height

    riverdistributary.data.riverdike_height = riverdistributary.cls.data.seadike_height * 0.5f
    if (riverdistributary.data.riverdike_height > 0f) {
      riverdistributary.data.riverdike_length = riverdistributary.data.river_impact_length_s1000 * 2
    }

    val riverdike_length_increase: Float = bounded_low(0f, riverdistributary.data.riverdike_length - riverdike_length_old)
    val riverdike_height_increase: Float = bounded_low(0f, riverdistributary.data.riverdike_height - riverdike_height_old)

    riverdistributary.data.riverdike_cost = getRiverDikeUnitCost(riverdistributary.cls) * seadikeConstructionCostFactor *
      ((riverdike_length_old * riverdike_height_increase) + (riverdike_length_increase * riverdistributary.data.riverdike_height)) / state.timeStep

    // river dike maintenace costs are 0.5% of the cost of the total dike Heigth (i.e. the 
    // capital stock). Costs are cheaper compared to sea dikes because there are no waves
    riverdistributary.data.riverdike_maintenance_cost = 0.005f * getRiverDikeCapitalCost(riverdistributary) * seadikeConstructionCostFactor
    var initial_riverdike_maintenance_cost: Float = 0.005f * riverdistributary.cls.country.data.seadike_unit_cost_rural * seadikeConstructionCostFactor * riverdistributary.data.riverdike_length_initial * riverdistributary.data.riverdike_height_initial
    riverdistributary.data.riverdike_increase_maintenance_cost = riverdistributary.data.riverdike_maintenance_cost - initial_riverdike_maintenance_cost

    riverdistributary.cls.data.riverdike_cost += riverdistributary.data.riverdike_cost
    riverdistributary.cls.data.riverdike_maintenance_cost += riverdistributary.data.riverdike_maintenance_cost
    riverdistributary.cls.data.riverdike_increase_maintenance_cost += riverdistributary.data.riverdike_increase_maintenance_cost

    if (riverdistributary.locationid == "") {
      //println(state.time + ", " + riverdistributary.cls.data.rslr + ", " + (riverdistributary.cls.data.seadike_height * 0.5f))
      //println("riverdistributary.cls.data.seadike_height = " + riverdistributary.cls.data.seadike_height)
      //println("getRiverDikeCapitalCost(riverdistributary) * seadikeConstructionCostFactor = " + getRiverDikeCapitalCost(riverdistributary) * seadikeConstructionCostFactor)
    }

  }

  private def buildSurgeBarrier(state: divaState, riverdistributary: Riverdistributary, init: Boolean) {
    var river: River = riverdistributary.river
    var river_depth: Float = if (river.data.river_depth <= 0f) defaultRiverDepth else river.data.river_depth

    var barrier: Surgebarrier = riverdistributary.surgeBarrier
    var barrier_width: Float = barrier.data.surge_barrier_width

    if (!init) {
      val surge_barrier_height_old: Float = riverdistributary.surgeBarrier.data.surge_barrier_height
      riverdistributary.surgeBarrier.data.surge_barrier_height = riverdistributary.cls.data.seadike_height + river_depth
      riverdistributary.surgeBarrier.data.surge_barrier_cost =
        if (riverdistributary.surgeBarrier.data.surge_barrier_height > surge_barrier_height_old) {
          (riverdistributary.surgeBarrier.data.surge_barrier_height - surge_barrier_height_old) * barrier_width * surgeBarrierUnitCost
        } else {
          0f
        } / state.timeStep
      riverdistributary.cls.data.surge_barrier_cost += riverdistributary.surgeBarrier.data.surge_barrier_cost
      riverdistributary.surgeBarrier.data.surge_barrier_maintenance_cost = 0.01f * riverdistributary.surgeBarrier.data.surge_barrier_height * barrier_width * surgeBarrierUnitCost
      riverdistributary.cls.data.surge_barrier_maintenance_cost += riverdistributary.surgeBarrier.data.surge_barrier_maintenance_cost
      riverdistributary.data.surge_barrier_maintenance_cost = riverdistributary.surgeBarrier.data.surge_barrier_maintenance_cost
    }

  }

  private def buildCBARiverProtection(riverdistributary: Riverdistributary, state: divaState, init: Boolean) {
    if (!init) {
      val potential_barrier_cost: Float = computeAccumulatedSurgebarrierCost(riverdistributary, state)
      val potential_riverdike_cost: Float = computeAccumulatedRiverDikeCost(riverdistributary, state)

      if (riverdistributary.locationid == "") {
        println("Surge barrier cost: " + potential_barrier_cost)
        println("River dike cost: " + potential_riverdike_cost)
      }
      if (potential_barrier_cost > potential_riverdike_cost) {
        buildRiverdikes(state, riverdistributary)
      } else {
        buildSurgeBarrier(state, riverdistributary, init)
      }
    }
  }

  private def buildStylizedRiverProtection(riverdistributary: Riverdistributary, state: divaState, init: Boolean) {
    if (!init) {
      val initialSLR: Float = riverdistributary.cls.data.rslr_memory(state.time)
      val finalSLR: Float = riverdistributary.cls.data.rslr_memory(state.endTime)
      val rslrOverTime: Float = finalSLR - initialSLR;

      val s1000: Float = riverdistributary.cls.data.s1000
      val s1: Float = riverdistributary.cls.data.s0001
      val s_diff: Float = s1000 - s1

      if (s_diff > finalSLR) {
        buildSurgeBarrier(state, riverdistributary, init)
      } else {
        buildRiverdikes(state, riverdistributary)
      }
    }
  }

  private def computeAccumulatedSurgebarrierCost(riverdistributary: Riverdistributary, state: divaState): Float = {

    if (riverdistributary.locationid == "") {
      println("time,locationid,existing_barrier_height,target_barrier_height,barrier_height_to_add,barrier_update_cost,barrier_maintenance_cost,discounted_barrier_update_cost,discounted_barrier_maintenance_cost")
    }
    var river: River = riverdistributary.river
    var riverDepth: Float = if (river.data.river_depth <= 0f) defaultRiverDepth else river.data.river_depth

    var updateCost: Float = 0f
    var maintenanceCost: Float = 0f
    var lSurgeBarrierHeight: Float = riverdistributary.surgeBarrier.data.surge_barrier_height
    val initialSLR: Float = riverdistributary.cls.data.rslr_memory(state.time)
    // this should be in meter!
    val surgeBarrierWidth: Float = riverdistributary.surgeBarrier.data.surge_barrier_width

    if (riverdistributary.locationid == "") {
      println(riverdistributary.cls.data.protlevel_target + "  ---  " + riverdistributary.cls.floodHazard.returnPeriodHeight(1000))
    }

    for (t <- state.time to state.endTime by state.timeStep) {
      val lYearsInFuture: Int = t - state.time;
      val discountFactor: Float = scala.math.pow(1 - (state.discountRate / 100.0f), lYearsInFuture).asInstanceOf[Float]
      val recentSLR: Float = if ((riverdistributary.cls.data.rslr_memory(t) - initialSLR) > 0f) (riverdistributary.cls.data.rslr_memory(t) - initialSLR) else 0f

      val surgeBarrierTargetHeight: Float = riverdistributary.cls.data.seadike_height + riverDepth + recentSLR
      val surgeBarrierHeightToAdd: Float = bounded_low(0f, (surgeBarrierTargetHeight - lSurgeBarrierHeight))

      val surgeBarrierUpdateCost: Float = surgeBarrierHeightToAdd * surgeBarrierWidth * surgeBarrierUnitCost
      val surgeBarrierMaintenanceCost: Float = surgeBarrierTargetHeight * surgeBarrierWidth * surgeBarrierUnitCost * 0.01f * state.timeStep

      if (riverdistributary.locationid == "") {
        println(t + "," + riverdistributary.locationid + "," + lSurgeBarrierHeight + "," + surgeBarrierTargetHeight + ","
          + surgeBarrierHeightToAdd + "," + surgeBarrierUpdateCost + "," + surgeBarrierMaintenanceCost + "," + (surgeBarrierUpdateCost * discountFactor) + ","
          + (surgeBarrierMaintenanceCost * discountFactor))
      }

      updateCost += surgeBarrierUpdateCost * discountFactor
      maintenanceCost += surgeBarrierMaintenanceCost * discountFactor
      //if ((surgeBarrierTargetHeight - lSurgeBarrierHeight) > 0) { lSurgeBarrierHeight = surgeBarrierTargetHeight }
    }

    (updateCost + maintenanceCost)
  }

  private def computeAccumulatedRiverDikeCost(riverdistributary: Riverdistributary, state: divaState): Float = {

    if (riverdistributary.locationid == "") {
      println("time,locationid,existing_riverdike_length,target_riverdike_length,riverdike_length_to_add,existing_riverdike_height,target_riverdike_height,riverdike_height_to_add,riverdike_update_cost,riverdike_maintenance_cost,discounted_riverdike_update_cost,discounted_riverdike_maintenance_cost")

    }

    var river: River = riverdistributary.river
    var river_slope: Float = if (river.data.river_slope < 0) defaultRiverSlope else river.data.river_slope

    var lRiverdikeLength: Float = riverdistributary.data.riverdike_length
    var lRiverdikeHeight: Float = riverdistributary.data.riverdike_height

    var lUpdateCost: Float = 0f
    var lMaintenanceCost: Float = 0f

    if (riverdistributary.locationid == "") {
      println(", initial river dike length and height, " + lRiverdikeLength + "," + lRiverdikeHeight)
    }

    val initialSLR: Float = riverdistributary.cls.data.rslr_memory(state.time)

    for (t <- state.time to state.endTime by state.timeStep) {
      val lYearsInFuture: Int = t - state.time;
      val discountFactor: Float = scala.math.pow(1 - (state.discountRate / 100.0f), lYearsInFuture).asInstanceOf[Float]
      val recentSLR: Float = if ((riverdistributary.cls.data.rslr_memory(t) - initialSLR) > 0f) (riverdistributary.cls.data.rslr_memory(t) - initialSLR) else 0f

      // unbedingt mal checken!!!
      // WIESO ist riverdistributary.cls.data.h1000 + recentSLR - riverdistributary.cls.data.slr_nonclimate_induced_memory(t) nicht gleich 
      // riverdistributary.cls.data.s1000 + riverdistributary.cls.data.slr_climate_induced_memory(t) ???

      var riverImpactTargetLength: Float = (0.001f * ((riverdistributary.cls.data.h1000 + recentSLR) - riverdistributary.cls.data.slr_nonclimate_induced_memory(t)) / river_slope)
      if (!riverdistributary.data.river_distributary_is_root) {
        riverImpactTargetLength = riverImpactTargetLength min riverdistributary.data.length
      }

      if (riverdistributary.locationid == "") {
        println(riverdistributary.data.river_distributary_is_root + " -- " + (0.001f * ((riverdistributary.cls.data.h1000 + recentSLR) - riverdistributary.cls.data.slr_nonclimate_induced_memory(t)) / river_slope))
        println(riverdistributary.cls.data.h1000 + " + " + recentSLR + " - " + riverdistributary.cls.data.slr_nonclimate_induced_memory(t))
        println(riverdistributary.data.length)
        println(riverImpactTargetLength)
      }

      val riverdikeTargetLength: Float = riverImpactTargetLength * 2
      val riverdikeLengthToAdd: Float = bounded_low(0, (riverdikeTargetLength - lRiverdikeLength))

      val riverdikeTargetHeight: Float = (riverdistributary.cls.data.seadike_height + recentSLR) * 0.5f
      val riverdikeHeightToAdd: Float = bounded_low(0f, (riverdikeTargetHeight - lRiverdikeHeight))

      val riverdikeUpdateCost: Float = ((riverdikeLengthToAdd * riverdikeTargetHeight) + (lRiverdikeLength * riverdikeHeightToAdd)) * getRiverDikeUnitCost(riverdistributary.cls)

      val riverdikeMaintenanceCost: Float = (riverdikeTargetHeight * riverdikeTargetLength) * getRiverDikeUnitCost(riverdistributary.cls) * 2 * 0.005f * state.timeStep

      if (riverdistributary.locationid == "") {
        println(t + "," + riverdistributary.locationid + "," + lRiverdikeLength + "," + riverdikeTargetLength + "," + riverdikeLengthToAdd + "," +
          lRiverdikeHeight + "," + riverdikeTargetHeight + "," + riverdikeHeightToAdd + "," +
          riverdikeUpdateCost + "," + riverdikeMaintenanceCost + "," + (riverdikeUpdateCost * discountFactor) + "," + (riverdikeMaintenanceCost * discountFactor))
      }

      lUpdateCost += riverdikeUpdateCost * discountFactor
      lMaintenanceCost += riverdikeMaintenanceCost * discountFactor
      if ((riverdikeTargetHeight - lRiverdikeHeight) > 0f) { lRiverdikeHeight = riverdikeTargetHeight }
      if ((riverdikeTargetLength - lRiverdikeLength) > 0f) { lRiverdikeLength = riverdikeTargetLength }
    }

    (lUpdateCost + lMaintenanceCost)
  }

  private def computeDeltaRiverProtection(river: River, init: Boolean = false) {
  }

  private def computeNonDeltaRiverProtection(river: River, init: Boolean = false) {
  }

  private def accumulate(state: divaState) {

    var globals: List[Global] = state.getFeatures[Global]
    var rivers: List[River] = state.getFeatures[River]

    var admins: List[Admin] = state.getFeatures[Admin]
    admins.par.foreach {
      (admin: Admin) =>
        {
          admin.data.salinity_cost = admin.clss.foldLeft(0f)((sum, cls) => sum + cls.data.salinity_cost)
          admin.data.riverdike_cost = admin.clss.foldLeft(0f)((sum, cls) => sum + cls.data.riverdike_cost)
          admin.data.riverdike_maintenance_cost = admin.clss.foldLeft(0f)((sum, cls) => sum + cls.data.riverdike_maintenance_cost)
          admin.data.riverdike_increase_maintenance_cost = admin.clss.foldLeft(0f)((sum, cls) => sum + cls.data.riverdike_increase_maintenance_cost)
          admin.data.surge_barrier_cost = admin.clss.foldLeft(0f)((sum, cls) => sum + cls.data.surge_barrier_cost)
          admin.data.surge_barrier_maintenance_cost = admin.clss.foldLeft(0f)((sum, cls) => sum + cls.data.surge_barrier_maintenance_cost)
          admin.data.riverdike_length_initial = 0f
          admin.data.riverdike_length = 0f
        }
    }
    
    var riverdistributaries: List[Riverdistributary] = state.getFeatures[Riverdistributary]
    riverdistributaries.foreach {
      riverdistributary =>
        {
          var cls: Cls = riverdistributary.cls
          cls.admin.data.riverdike_length_initial += riverdistributary.data.riverdike_length_initial
          cls.admin.data.riverdike_length += riverdistributary.data.riverdike_length

          if (riverdistributary.surgeBarrier.data.surge_barrier_height > 0f)
            cls.admin.data.surge_barriers_number += 1
        }
    }

    var countries: List[Country] = state.getFeatures[Country]
    countries.par.foreach {
      (country: Country) =>
        {
          country.data.salinity_cost = country.clss.foldLeft(0f)((sum, cls) => sum + cls.data.salinity_cost)
          country.data.riverdike_cost = country.clss.foldLeft(0f)((sum, cls) => sum + cls.data.riverdike_cost)
          country.data.riverdike_maintenance_cost = country.clss.foldLeft(0f)((sum, cls) => sum + cls.data.riverdike_maintenance_cost)
          country.data.riverdike_increase_maintenance_cost = country.clss.foldLeft(0f)((sum, cls) => sum + cls.data.riverdike_increase_maintenance_cost)
          country.data.surge_barrier_cost = country.clss.foldLeft(0f)((sum, cls) => sum + cls.data.surge_barrier_cost)
          country.data.surge_barrier_maintenance_cost = country.clss.foldLeft(0f)((sum, cls) => sum + cls.data.surge_barrier_maintenance_cost)
          country.data.riverdike_length_initial = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.riverdike_length_initial)
          country.data.riverdike_length = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.riverdike_length)
          country.data.surge_barriers_number = country.admins.foldLeft(0f)((sum, admin) => sum + admin.data.surge_barriers_number)
        }
    }

    var regions: List[Region] = state.getFeatures[Region]
    regions.par.foreach {
      (region: Region) =>
        {
          region.data.salinity_cost = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.salinity_cost)
          region.data.riverdike_cost = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.riverdike_cost)
          region.data.riverdike_maintenance_cost = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.riverdike_maintenance_cost)
          region.data.riverdike_increase_maintenance_cost = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.riverdike_increase_maintenance_cost)
          region.data.surge_barrier_cost = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.surge_barrier_cost)
          region.data.surge_barrier_maintenance_cost = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.surge_barrier_maintenance_cost)
          region.data.riverdike_length_initial = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.riverdike_length_initial)
          region.data.riverdike_length = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.riverdike_length)
          region.data.surge_barriers_number = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.surge_barriers_number)
        }
    }

    rivers.foreach {
      river =>
        river.data.riverdike_cost = river.riverdistributarys.foldLeft(0f)((sum, riverdistributary) => sum + (riverdistributary.data.riverdike_cost))
        river.data.riverdike_maintenance_cost = river.riverdistributarys.foldLeft(0f)((sum, riverdistributary) => sum + (riverdistributary.data.riverdike_maintenance_cost))
        river.data.riverdike_increase_maintenance_cost = river.riverdistributarys.foldLeft(0f)((sum, riverdistributary) => sum + (riverdistributary.data.riverdike_increase_maintenance_cost))
        river.data.riverdike_length = river.riverdistributarys.foldLeft(0f)((sum, riverdistributary) => sum + (riverdistributary.data.riverdike_length))
        river.data.surge_barrier_cost = river.riverdistributarys.foldLeft(0f)((sum, riverdistributary) => sum + (riverdistributary.data.surge_barrier_cost))
        river.data.surge_barrier_maintenance_cost = river.riverdistributarys.foldLeft(0f)((sum, riverdistributary) => sum + (riverdistributary.data.surge_barrier_maintenance_cost))

        river.data.river_impact_length_initial_tide = river.riverdistributarys.foldLeft(0f)((m, riverdistributary) => m max (riverdistributary.data.river_impact_length_initial_tide))
        river.data.river_impact_length_initial_s1 = river.riverdistributarys.foldLeft(0f)((m, riverdistributary) => m max (riverdistributary.data.river_impact_length_initial_s1))
        river.data.river_impact_length_initial_s10 = river.riverdistributarys.foldLeft(0f)((m, riverdistributary) => m max (riverdistributary.data.river_impact_length_initial_s10))
        river.data.river_impact_length_initial_s100 = river.riverdistributarys.foldLeft(0f)((m, riverdistributary) => m max (riverdistributary.data.river_impact_length_initial_s100))
        river.data.river_impact_length_initial_s1000 = river.riverdistributarys.foldLeft(0f)((m, riverdistributary) => m max (riverdistributary.data.river_impact_length_initial_s1000))
        river.data.river_impact_length_tide = river.riverdistributarys.foldLeft(0f)((m, riverdistributary) => m max (riverdistributary.data.river_impact_length_tide))
        river.data.river_impact_length_s1 = river.riverdistributarys.foldLeft(0f)((m, riverdistributary) => m max (riverdistributary.data.river_impact_length_s1))
        river.data.river_impact_length_s10 = river.riverdistributarys.foldLeft(0f)((m, riverdistributary) => m max (riverdistributary.data.river_impact_length_s10))
        river.data.river_impact_length_s100 = river.riverdistributarys.foldLeft(0f)((m, riverdistributary) => m max (riverdistributary.data.river_impact_length_s100))
        river.data.river_impact_length_s1000 = river.riverdistributarys.foldLeft(0f)((m, riverdistributary) => m max (riverdistributary.data.river_impact_length_s1000))
    }

    globals.foreach {
      global =>
        // TODO: check wo die surge barrier cost verloren gehen wenn wir hier über rivers accumulieren
        global.data.salinity_cost = countries.foldLeft(0f)((sum, country) => sum + (country.data.salinity_cost))
        global.data.riverdike_cost = rivers.foldLeft(0f)((sum, river) => sum + (river.data.riverdike_cost))
        global.data.riverdike_increase_maintenance_cost = rivers.foldLeft(0f)((sum, river) => sum + (river.data.riverdike_increase_maintenance_cost))
        global.data.riverdike_maintenance_cost = rivers.foldLeft(0f)((sum, river) => sum + (river.data.riverdike_maintenance_cost))
        global.data.surge_barrier_cost = countries.foldLeft(0f)((sum, country) => sum + (country.data.surge_barrier_cost))
        global.data.surge_barrier_maintenance_cost = countries.foldLeft(0f)((sum, river) => sum + (river.data.surge_barrier_maintenance_cost))
        global.data.riverdike_length_initial = countries.foldLeft(0f)((sum, country) => sum + country.data.riverdike_length_initial)
        global.data.riverdike_length = countries.foldLeft(0f)((sum, country) => sum + country.data.riverdike_length)
        global.data.surge_barriers_number = countries.foldLeft(0f)((sum, country) => sum + country.data.surge_barriers_number)
    }
  }

}