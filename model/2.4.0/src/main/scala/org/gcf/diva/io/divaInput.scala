package diva.io

import scala.collection.mutable.HashMap
import scala.collection.mutable.AnyRefMap
import diva.model.hasLogger
import diva.types.InterpolationMode._
import diva.toolbox.InterpolationTools._

// A diva input should serve for parameter files as well as for scenario files
trait divaInput extends hasLogger {

  // locationid, locationname, time
  var metainformation: List[String]

  // parameter Names
  var parameter: Array[String]

  // location IDs
  var locations: Array[String]

  // timesteps
  // def timesteps: List[Int]
  def timesteps: AnyRefMap[String, List[Int]]

  var noValueString: String

  var locationWildcard: String

  // protected def isTimeInTimesteps(time: Int): Boolean = timesteps.contains(time)
  protected def isTimeInTimesteps(time: Int, pTimesteps: List[Int]): Boolean = pTimesteps.contains(time)


  protected def lastTimestepBefore(time: Int, pTimesteps: List[Int]): Int = pTimesteps.filter(_ < time).reduceLeft(_ max _)
  protected def firstTimestepAfter(time: Int, pTimesteps: List[Int]): Int = pTimesteps.filter(_ > time).reduceLeft(_ min _)

  def readMetainformation
  def readParameter

  def hasNextTime: Boolean
  def readCompletely(check: Boolean)

  var numberLocations: Int

  //def readCurrentParameterValue[T](parName: String)(implicit ev1: diva.types.TypeComputations.Parsable[T], ev2: diva.types.TypeComputations.Floatable[T]): T
  //def readCurrentOptionParameterValue[T](parName: String, pNoValue: String)(implicit ev1: diva.types.TypeComputations.Parsable[T], ev2: diva.types.TypeComputations.Floatable[T]): Option[T]

  def getInputValue[T](parName: String, locid: String, time: Int)(implicit ev1: diva.types.TypeComputations.StringParseOp[T], ev2: diva.types.TypeComputations.Floatable[T]): T
  def getOptionInputValue[T](parName: String, locid: String, time: Int)(implicit ev1: diva.types.TypeComputations.StringParseOp[T], ev2: diva.types.TypeComputations.Floatable[T]): Option[T]

  def getScenarioValue[T](parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode)(implicit ev1: diva.types.TypeComputations.StringParseOp[T], ev2: diva.types.TypeComputations.Floatable[T], num: Interpolatable[T]): T = {
    var pTimesteps: List[Int] = timesteps.get(parName).get
    if (isTimeInTimesteps(time, pTimesteps)) {
      getInputValue[T](parName, locid, time)
    } else {
      val timeBefore: Int = lastTimestepBefore(time, pTimesteps)
      val timeAfter: Int = firstTimestepAfter(time, pTimesteps)
      val valueBefore: T = getInputValue[T](parName, locid, timeBefore)
      val valueAfter: T = getInputValue[T](parName, locid, timeAfter)

      pInterpolationMode match {
        case LINEAR              => interpolatLinear(valueBefore, valueAfter, timeBefore, timeAfter, time)
        case LEFT_INTERPOLATION  => interpolatLeft(valueBefore, valueAfter, timeBefore, timeAfter, time)
        case RIGHT_INTERPOLATION => interpolatRight(valueBefore, valueAfter, timeBefore, timeAfter, time)
      }
    }
  }

  def getOptionScenarioValue[T](parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode)(implicit ev1: diva.types.TypeComputations.StringParseOp[T], ev2: diva.types.TypeComputations.Floatable[T], num: Fractional[T]): Option[T] = {
    if (isTimeInTimesteps(time, timesteps.get(parName).get)) {
      getOptionInputValue[T](parName, locid, time)
    } else {
      getOptionScenarioValue[T](parName, locid, time, pInterpolationMode)
    }
  }

}