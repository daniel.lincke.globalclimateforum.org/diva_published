/* Automatically generated data features for Delta
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data

import diva.annotations._
import diva.types._
import diva.types.TypeComputations._
import scala.collection.mutable._


class DeltaData {

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Land area below ? m (ignoring see dikes)")
  var area_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Land area below the one in ? flood level (ignoring see dikes)")
  var area_below_h :  Map[Int ,Float] = default[Map[Int ,Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets below ? m (ignoring see dikes)")
  var assets_below_ :  Map[Float,Float] = default[Map[Float,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Assets below the one in ? flood level (ignoring see dikes)")
  var assets_below_h :  Map[Int ,Float] = default[Map[Int ,Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets in developable areas below ? m (ignoring see dikes)")
  var assets_developable_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets in developed areas below ? m (ignoring see dikes)")
  var assets_developed_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Output
  @Variable
  @Description(value = "")
  var coastlength :  Float = 0f

  @Output
  @Variable
  @Description(value = "Average annual income")
  var gdpc :  Float = default[Float]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Storm surge height for a ? in 1 year surge, allowing for relative sea-level rise.")
  var h :  Map[Int ,Float] = default[Map[Int ,Float]]

  @Variable
  @Description(value = "Storm surge height for a 1 in 1 year surge, allowing for relative sea-level rise.")
  var h1 :  Float = default[Float]

  @Variable
  @Description(value = "Storm surge height for a 1 in 10 year surge, allowing for relative sea-level rise.")
  var h10 :  Float = default[Float]

  @Variable
  @Description(value = "Storm surge height for a 1 in 100 year surge, allowing for relative sea-level rise.")
  var h100 :  Float = default[Float]

  @Variable
  @Description(value = "Storm surge height for a 1 in 1000 year surge, allowing for relative sea-level rise.")
  var h1000 :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Flag to indicate if the delta is an ericsson delta")
  var is_ericson :  Boolean = default[Boolean]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total land loss due to direct and indirect erosion, including the benefits of beach nourishment")
  var landloss_erosion_total :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Change in land area below the one in one flood level, taking Into account see dikes")
  var landloss_submergence :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total costs of land loss due to erosion")
  var landlosscost_erosion_total :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Change in land area below the one in one flood level, taking Into account see dikes")
  var landlosscost_submergence :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Length of the proctected coastline.")
  var length_protected :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Local GDP below 20.5m")
  var localgdp :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to land loss (erosion) (per year)")
  var migration_erosion :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to retreat (per year)")
  var migration_retreat :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to land loss (flooding/submergence) (per year)")
  var migration_submergence :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration costs")
  var migrationcost_erosion :  Float = 0f

  @Output
  @Variable
  @Description(value = "Forced migration costs")
  var migrationcost_retreat :  Float = 0f

  @Output
  @Variable
  @Description(value = "Forced migration costs")
  var migrationcost_submergence :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "People at risk of flooding: average number of people flooded per year by storm surge allowing for the effect of flood defences (segment level).")
  var par :  Float = default[Float]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Exposure (people) that are located below ? m")
  var pop_below_ :  Map[Float,Float] = default[Map[Float,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Exposure (people) that are located below the h? flood")
  var pop_below_h :  Map[Int,Float] = default[Map[Int,Float]]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Forced migration due to the initial population correction")
  var population_correction :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Implemented protection level/ return period")
  var protlevel_implemented :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Implemented protection level/ return period (before dike is raised)")
  var protlevel_implemented_before_dikeraise :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "")
  var riverdike_cost :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of incremental riverdike maIntenace")
  var riverdike_increase_maintenance_cost :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of riverdike maIntenace")
  var riverdike_maintenance_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Change in relative sea level compared to the reference period, taking account of (1) human-induced climate change under the selected scenario, (2) uplift/subsidence due to glacial-isostatic adjustment and (3) natural subsidence of deltaic areas (segment level)")
  var rslr :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Sand loss direct on the open coast ")
  var sandloss_erosion_direct :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total sand loss due to erosion, allowing for direct and indirect erosion, and beach nourishment as appropriate")
  var sandloss_erosion_including_nourishment :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Sand loss indirect on the open coast ")
  var sandloss_erosion_indirect :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total sand loss due to erosion, allowing for direct and indirect erosion, ignoring beach nourishment")
  var sandloss_erosion_total :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total cost of seadike built")
  var seadike_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Average dike height")
  var seadike_height :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Average initial sea dike height")
  var seadike_height_initial :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of incremental seadike maIntenace")
  var seadike_increase_maintenance_cost :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of seadike maIntenace")
  var seadike_maintenance_cost :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "total expected costs of sea floods")
  var seafloodcost :  Float = 0f

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Damage (assets) caused by the h? flood")
  var seafloodcost_h :  Map[Int,Float] = default[Map[Int,Float]]

  @Output
  @Variable
  @Description(value = "total expected costs of sea floods as fractionof local gdp")
  var seafloodcost_relative :  Float = 0f

  @Output
  @Variable
  @Description(value = "Change in relative sea level compared to the reference period, only  human-induced climate change under the selected scenario")
  var slr_climate_induced :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Change in relative sea level compared to the referenceperiod, taking account only subsidence of deltaic and city areas (segment level) and isostatic adjustment")
  var slr_nonclimate_induced :  Float = default[Float]

  @Variable
  @Description(value = "stores the normalisation value for slr accumulation")
  var slr_normalizer :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "")
  var subsidence_ericson_natural :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "")
  var subsidence_ericson_total :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "")
  var subsidence_non_ericson :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total area of freshwater marsh (administrative level).  Aggregation rule, sum of coastline segments within administrative unit.")
  var wetland_area_freshmarshes :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total area of mangrove (segment level).")
  var wetland_area_mangroves :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total area of saltmarsh (segment level).")
  var wetland_area_saltmarshes :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total sum of all wetland areas")
  var wetland_area_total :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total area of low unvegetated wetland (country level).  Aggregate rule, sum of coastline segments within country unit.")
  var wetlow :  Float = default[Float]

  override def clone : DeltaData = {
    var ret: DeltaData  = new DeltaData
    ret.area_below_ = area_below_
    ret.area_below_h = area_below_h
    ret.assets_below_ = assets_below_
    ret.assets_below_h = assets_below_h
    ret.assets_developable_below_ = assets_developable_below_
    ret.assets_developed_below_ = assets_developed_below_
    ret.coastlength = coastlength
    ret.gdpc = gdpc
    ret.h = h
    ret.h1 = h1
    ret.h10 = h10
    ret.h100 = h100
    ret.h1000 = h1000
    ret.is_ericson = is_ericson
    ret.landloss_erosion_total = landloss_erosion_total
    ret.landloss_submergence = landloss_submergence
    ret.landlosscost_erosion_total = landlosscost_erosion_total
    ret.landlosscost_submergence = landlosscost_submergence
    ret.length_protected = length_protected
    ret.localgdp = localgdp
    ret.migration_erosion = migration_erosion
    ret.migration_retreat = migration_retreat
    ret.migration_submergence = migration_submergence
    ret.migrationcost_erosion = migrationcost_erosion
    ret.migrationcost_retreat = migrationcost_retreat
    ret.migrationcost_submergence = migrationcost_submergence
    ret.par = par
    ret.pop_below_ = pop_below_
    ret.pop_below_h = pop_below_h
    ret.population_correction = population_correction
    ret.protlevel_implemented = protlevel_implemented
    ret.protlevel_implemented_before_dikeraise = protlevel_implemented_before_dikeraise
    ret.riverdike_cost = riverdike_cost
    ret.riverdike_increase_maintenance_cost = riverdike_increase_maintenance_cost
    ret.riverdike_maintenance_cost = riverdike_maintenance_cost
    ret.rslr = rslr
    ret.sandloss_erosion_direct = sandloss_erosion_direct
    ret.sandloss_erosion_including_nourishment = sandloss_erosion_including_nourishment
    ret.sandloss_erosion_indirect = sandloss_erosion_indirect
    ret.sandloss_erosion_total = sandloss_erosion_total
    ret.seadike_cost = seadike_cost
    ret.seadike_height = seadike_height
    ret.seadike_height_initial = seadike_height_initial
    ret.seadike_increase_maintenance_cost = seadike_increase_maintenance_cost
    ret.seadike_maintenance_cost = seadike_maintenance_cost
    ret.seafloodcost = seafloodcost
    ret.seafloodcost_h = seafloodcost_h
    ret.seafloodcost_relative = seafloodcost_relative
    ret.slr_climate_induced = slr_climate_induced
    ret.slr_nonclimate_induced = slr_nonclimate_induced
    ret.slr_normalizer = slr_normalizer
    ret.subsidence_ericson_natural = subsidence_ericson_natural
    ret.subsidence_ericson_total = subsidence_ericson_total
    ret.subsidence_non_ericson = subsidence_non_ericson
    ret.wetland_area_freshmarshes = wetland_area_freshmarshes
    ret.wetland_area_mangroves = wetland_area_mangroves
    ret.wetland_area_saltmarshes = wetland_area_saltmarshes
    ret.wetland_area_total = wetland_area_total
    ret.wetlow = wetlow
    ret
  }

}
