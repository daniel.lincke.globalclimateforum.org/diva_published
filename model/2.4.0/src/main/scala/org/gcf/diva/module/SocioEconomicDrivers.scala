/*---

$Header: /home/lincke/Software/projects/gcf/diva/CSV/cvsroot/genial/src/diva/module/InternalDrivers.java,v 1.24 2012-01-03 11:31:50 hinkel Exp $

DIVA model
----------

Copyright (C) 2003 Jochen Hinkel and The Dinas Coast Consortium
http://pik-potsdam.de/dinas-coast

This is a pre-release of the DIVA model for circulation within the
DINAS Coast Consortium. The algorithms are not finalized
nor have they been validated. Please do not cite.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See COPYING.TXT for details.


---*/
/* to-do:
*/

package diva.module;

import scala.collection.parallel.immutable.ParSeq

import diva.model.ModuleI
import diva.model.divaState
import diva.model.divaFeatures._
import diva.model.Config
import diva.toolbox.Toolbox._
import diva.feature._

import scala.math._


class SocioEconomicDrivers extends ModuleI {

  private val ginipar1: Float = -0.07f
  private val litpar1: Float = 0.19f
  private val clpar1: Float = -0.24f
  private val lepar1: Float = 0.09f
  private val corrpar1: Float = -3.43f
  private val corrpar2: Float = 0.45f
  private val dempar1: Float = -4.90f
  private val dempar2: Float = 1.34f
  private val gqpar1: Float = 0.18f
  private val stabpar1: Float = -3.22f
  private val stabpar2: Float = 0.42f
  private val ecfreepar1: Float = -0.13f
  private val indpar1: Float = 0.21f
  private val rate: Float = 0.05f

  //constant for agricultural land value min 105.0 mean 180.4 max 310.0
  private val agvalalpha: Float = 180.4f

  //income density elasticity of agricultural land value min 0.50 mean 0.53 max 0.57
  private val agvalbeta: Float = 0.53f

  // parameter
  var globalGdpcAdditionalAnnualGrowthRate: Float = 0.0f
  var globalPopAdditionalAnnualGrowthRate: Float = 0.0f

  var intenseCoastalDevelopableConstructionStartYear: Int = 2015
  var intenseCoastalDevelopableConstructionEndYear: Int = 2025
  var intenseCoastalDevelopableConstructionPeriods: Int = 0

  // One dollar back in 1995 is equivalent to ... dollar in current dollar base year
  // Introduced to switch to another dollar base year.
  private val moneyForwardsFrom1995ConverionFactor: Float = 1.56f
  private def moneyBackwardsTo1995ConverionFactor: Float = 1 / moneyForwardsFrom1995ConverionFactor

  val elevationLimit: Float = 20.5f

  override val scenarioModule: Boolean = true

  def moduleName: String = "Socio economic drivers"
  def moduleVersion: String = "1.0 (20 August 2015)"
  def moduleDescription: String = "Produces socio-economic scenarios."
  def moduleAuthor: String = "Richard Tol, Daniel Lincke, Jochen Hinkel"

  /*
  def getInput: Array[java.lang.String] = Array()
  def getOutput: Array[java.lang.String] = Array()
  */

  def initModule(initialState: divaState) = {
    var countries: ParSeq[Country] = initialState.getFeatures[Country].par
    countries.foreach { country => computeDevelopedArea(country) }
    var intenseCoastalDevelopableConstructionYears: Int = intenseCoastalDevelopableConstructionEndYear - intenseCoastalDevelopableConstructionStartYear
    if (intenseCoastalDevelopableConstructionYears < initialState.timeStep) intenseCoastalDevelopableConstructionYears = initialState.timeStep
    intenseCoastalDevelopableConstructionPeriods = intenseCoastalDevelopableConstructionYears / initialState.timeStep
  }

  def computeScenario(currentState: diva.model.divaState) {
    var countries: ParSeq[Country] = currentState.getFeatures[Country].par
    countries.foreach { country => computeSocioeconomicGrowthScenario(currentState, country) }

    var czs: ParSeq[Cz] = currentState.getFeatures[Cz].par
    czs.foreach { cz => computeSocioeconomicGrowthScenario(currentState, cz) }
  }

  def init(state: divaState) = {
    var clss: ParSeq[Cls] = state.getFeatures[Cls].par
    clss.foreach { cls => cls.data.gdpc = cls.country.data.gdpc * cls.admin.data.gdpcmult }

    var czs: ParSeq[Cz] = state.getFeatures[Cz].par
    czs.foreach { cz =>
      cz.data.gdpc = cz.admin.country.data.gdpc * cz.admin.data.gdpcmult
      cz.data.localgdp = cz.coastalPlane.populationExposure.cumulativeExposure(elevationLimit).asInstanceOf[Float] * cz.data.gdpc
    }

    var countries: ParSeq[Country] = state.getFeatures[Country].par
    countries.foreach { country => computeCountryDrivers(state, country) }

    var admins: ParSeq[Admin] = state.getFeatures[Admin].par
    admins.foreach { admin => computeAdminDrivers(state, admin) }

    clss.foreach {
      cls =>
        cls.czs.foreach { cz => cls.data.localgdp += cz.data.localgdp }
    }

    accumulate(state)
  }

  def invoke(state: divaState) {
    var countries: ParSeq[Country] = state.getFeatures[Country].par
    countries.foreach {
      country =>
        computeCountryDrivers(state, country)
        computeSocioEconomics(state, country)
    }

    var admins: ParSeq[Admin] = state.getFeatures[Admin].par
    admins.foreach { admin => computeAdminDrivers(state, admin) }

    var cities: ParSeq[City] = state.getFeatures[City].par
    cities.foreach { city => computeCityDrivers(state, city) }

    var clss: ParSeq[Cls] = state.getFeatures[Cls].par
    clss.foreach {
      cls =>
        cls.czs.foreach { cz =>
          if (cz.data.coastalzone) {
            cls.data.landvalue_unit = cz.data.landvalue_unit
            cls.data.gdpc = cz.data.gdpc
          }
          cls.data.localgdp += cz.data.localgdp
        }
      //cls.recomputeCoastalPlane
    }

    accumulate(state)
  }

  private def computeDevelopedArea(country: Country) {
    var clss = country.clss
    var czs: List[Cz] = List()
    clss.foreach { cls => czs = cls.czs.toList ++ czs }
    var czs2 = czs.filter(cz => (cz.data.fraction_developed != None))
    var coastalArea = czs.map(x => x.coastalPlane.areaExposure.cumulativeExposure(elevationLimit)).foldLeft(0f)((sum, x) => sum + x.asInstanceOf[Float]).asInstanceOf[Float]
    country.data.fraction_developed = if (czs2.length > 0) { Some((czs2.map(x => (x.coastalPlane.areaExposure.cumulativeExposure(elevationLimit) * x.data.fraction_developed.get)).foldLeft(0f)((sum, x) => sum + x.asInstanceOf[Float]) / coastalArea).toFloat) } else { None }
    country.data.fraction_developable = if (czs2.length > 0) { Some((czs2.map(x => (x.coastalPlane.areaExposure.cumulativeExposure(elevationLimit) * x.data.fraction_developable.get)).foldLeft(0f)((sum, x) => sum + x.asInstanceOf[Float]) / coastalArea).toFloat) } else { None }
  }

  private def computeSocioeconomicGrowthScenario(currentState: divaState, cz: Cz) {
    var popgrowth: Float = cz.data.popgrowth match {
      case Some(x) => x + globalPopAdditionalAnnualGrowthRate
      case None => cz.admin.country.data.popgrowth + globalPopAdditionalAnnualGrowthRate
    }

    cz.data.popgrowth_total = ((1 + cz.data.popgrowth_total) * pow((1 + popgrowth), currentState.timeStep).asInstanceOf[Float] - 1.0f)
    cz.data.popgrowth_memory += (currentState.time -> Some(popgrowth))
    cz.data.popgrowth_total_memory += (currentState.time -> cz.data.popgrowth_total)

    cz.data.gdpcgrowth = cz.admin.country.data.gdpcgrowth + globalGdpcAdditionalAnnualGrowthRate
    cz.data.gdpcgrowth_total = ((1 + cz.data.gdpcgrowth_total) * pow((1 + cz.admin.country.data.gdpcgrowth + globalGdpcAdditionalAnnualGrowthRate), currentState.timeStep).asInstanceOf[Float] - 1.0f)
    cz.data.gdpcgrowth_memory += (currentState.time -> cz.data.gdpcgrowth)
    cz.data.gdpcgrowth_total_memory += (currentState.time -> cz.data.gdpcgrowth_total)
  }

  private def computeSocioeconomicGrowthScenario(currentState: divaState, country: Country) {
    country.data.popgrowth_memory += (currentState.time -> (country.data.popgrowth + globalPopAdditionalAnnualGrowthRate))
    country.data.gdpcgrowth_memory += (currentState.time -> (country.data.gdpcgrowth + globalPopAdditionalAnnualGrowthRate))
    country.data.urbansharegrowth_memory += (currentState.time -> country.data.urbansharegrowth)
  }

  private def computeCountryDrivers(currentState: divaState, country: Country) {


    country.data.totalpop = country.data.totalpop * pow((1.0f + country.data.popgrowth_memory(currentState.time)), currentState.timeStep).asInstanceOf[Float]

    if (country.data.totalpop < 0) {
      logger.warn("All population disapeared for country " + country.locationname + " (" + country.locationid + ")")
      country.data.totalpop = 0
    }

    country.data.gdpc = country.data.gdpc * pow((1.0f + country.data.gdpcgrowth_memory(currentState.time)), currentState.timeStep).asInstanceOf[Float]
    if (country.data.gdpc < 0) {
      logger.error("No more GDP per capita left for country " + country.locationname + " (" + country.locationid + ")")
      country.data.gdpc = 0
    }

    country.data.urbanshare = country.data.urbanshare * pow((1.0f + country.data.urbansharegrowth_memory(currentState.time)), currentState.timeStep).asInstanceOf[Float]
    if (country.data.urbanshare < 0) {
      logger.info("Urban share declined below 0.0 for country " + country.locationname + " (" + country.locationid + ")")
      country.data.urbanshare = 0
    }

    if (country.data.urbanshare > 100) {
      logger.info("Urban share raised above 100.0 for country " + country.locationname + " (" + country.locationid + ")")
      country.data.urbanshare = 100
    }

    // country gdp: in million (moneyUnit)
    country.data.gdp = ((country.data.totalpop * country.data.gdpc) / 1000)

    var gdpcRelChange: Float = country.data.gdpcgrowth_memory(currentState.time)
    var weight: Float = pow((1 - rate), currentState.timeStep).asInstanceOf[Float]

    // Calculates the Gini Index
    // gini(0) = observed
    country.data.gini = limited(country.data.gini * (1 + ginipar1 * gdpcRelChange), 0, 100)

    // Calculates the percentage literate people
    // literacy(0) = observed
    country.data.literacy = limited(country.data.literacy * (1 + litpar1 * gdpcRelChange), 0, 100)

    // Calculates the civil liberty index
    // civlib(0) = observed
    country.data.civlib = limited(country.data.civlib * (1 + clpar1 * gdpcRelChange), 1, 7)

    // Calculates the expected life time at birth
    // lifeexp(0) = observed
    country.data.lifeexp = country.data.lifeexp * (1 + lepar1 * gdpcRelChange)
    if (country.data.lifeexp < 0) country.data.lifeexp = 0

    //Calculates the corruption index
    // corruption(0) = observed
    country.data.corrupt = limited(weight * country.data.corrupt + (1 - weight) * (corrpar1 + corrpar2 * math.log(country.data.gdpc).asInstanceOf[Float]), -2.5f, 2.5f)

    // Calculates the democracy index
    // democracy(0) = observed
    country.data.democracy = limited(weight * country.data.democracy + (1 - weight) * (dempar1 + dempar2 * math.log(country.data.gdpc).asInstanceOf[Float]), 0, 10)

    //Calculates the government quality index
    // govqual(0) = observed
    country.data.govqual = limited(country.data.govqual * (1 + gqpar1 * gdpcRelChange), 1, 10)

    //Calculates the index of political stability
    //stability(0) = observed
    country.data.stability = limited(weight * country.data.stability + (1 - weight) * (stabpar1 + stabpar2 * math.log(country.data.gdpc).asInstanceOf[Float]), -3.0f, 3.0f)

    //Calculates the economic freedom index
    // ecfreedom(0) = observed
    country.data.ecfreedom = limited(country.data.ecfreedom * (1 + ecfreepar1 * gdpcRelChange), 1, 5)

    // Calculates the individualism index
    // individ(0) = observed
    country.data.individ = limited(country.data.individ * (1 + indpar1 * gdpcRelChange), 0, 100)

    // Calculate Tourism added value
    country.data.tourtav = country.data.tourtav * pow(1 + country.data.gdpcgrowth, 0.195592).asInstanceOf[Float]

  }

  private def computeAdminDrivers(currentState: divaState, admin: Admin) {
    admin.data.totalpop = admin.data.totalpop * pow((1.0f + admin.country.data.popgrowth_memory(currentState.time)), currentState.timeStep).asInstanceOf[Float]
    if (admin.data.totalpop < 0) {
      logger.warn("All population disapeared for country " + admin.locationname + " (" + admin.locationid + ")")
      admin.data.totalpop = 0
    }

    // country gdp: in million (moneyUnit)
    admin.data.gdp = ((admin.country.data.totalpop * admin.country.data.gdpc * admin.data.gdpcmult) / 1000)
  }

  private def computeCityDrivers(currentState: divaState, city: City) {
    city.data.totalpop = city.data.totalpop * pow((1.0f + city.country.data.popgrowth_memory(currentState.time)), currentState.timeStep).asInstanceOf[Float]
    city.data.totalpop = city.data.totalpop * pow((1.0f + city.country.data.urbansharegrowth_memory(currentState.time)), currentState.timeStep).asInstanceOf[Float]
    if (city.data.totalpop < 0) {
      logger.warn("All population disapeared for city " + city.locationname + " (" + city.locationid + ")")
      city.data.totalpop = 0
    }
  }

  private def computeSocioEconomics(currentState: divaState, country: Country) {

    //intenseCoastalDevelopableConstructionPeriods
    //country.data.fraction_developed
    //country.data.fraction_developable

    var czs: Array[Cz] = country.czs

    if ((country.data.fraction_developed != None) && (currentState.time <= (intenseCoastalDevelopableConstructionStartYear + intenseCoastalDevelopableConstructionPeriods * currentState.timeStep)) && (currentState.time >= intenseCoastalDevelopableConstructionStartYear)) {
      //
      var czsOnlyDevelopable = czs.filter { x => ((x.data.fraction_developed.get == 0) && (x.data.fraction_developable.get > 0)) }

      val remainingIntenseCoastalDevelopableConstructionPeriods: Int = ((intenseCoastalDevelopableConstructionEndYear - currentState.time) / currentState.timeStep) + 1
      val countryCoastalArea: Float = (czs.map(x => x.coastalPlane.areaExposure.cumulativeExposure(elevationLimit))).foldLeft(0f)((sum, x) => sum + x.asInstanceOf[Float])
      //val countryCoastalDevelopedArea: Float = countryCoastalArea * country.data.fraction_developed.get
      val countryCoastalOnlyDevelopableArea: Float = (czsOnlyDevelopable.map(x => x.coastalPlane.areaExposure.cumulativeExposure(elevationLimit) * x.data.fraction_developable.get)).foldLeft(0f)((sum, x) => sum + x.asInstanceOf[Float])

      val countryCoastalAssets: Float = (czs.map(x => x.coastalPlane.assetsExposure.cumulativeExposure(elevationLimit))).foldLeft(0f)((sum, x) => sum + x.asInstanceOf[Float])
      val countryCoastalAssetDensity: Float = countryCoastalAssets / countryCoastalArea

      val existingCoastalOnlyDevelopableAssets: Float = (czsOnlyDevelopable.map(x => x.coastalPlane.assetsExposure.cumulativeExposure(elevationLimit))).foldLeft(0f)((sum, x) => sum + x.asInstanceOf[Float])
      val targetCoastalOnlyDevelopableAssets: Float = countryCoastalOnlyDevelopableArea * countryCoastalAssetDensity
      val thisPeriodAdditionalCoastalOnlyDevelopableAssets: Float = (targetCoastalOnlyDevelopableAssets - existingCoastalOnlyDevelopableAssets) / remainingIntenseCoastalDevelopableConstructionPeriods
      val targetCoastalOnlyDevelopableAssetsDensity: Float = (existingCoastalOnlyDevelopableAssets + thisPeriodAdditionalCoastalOnlyDevelopableAssets) / countryCoastalOnlyDevelopableArea

      val countryCostalAssetsTotalTarget: Float = countryCoastalAssets * pow((1.0f + country.data.gdpcgrowth_memory(currentState.time)).asInstanceOf[Float] * (1.0f + country.data.popgrowth_memory(currentState.time)), currentState.timeStep).asInstanceOf[Float]
      val countryAdditionalCostalAssets = countryCostalAssetsTotalTarget - countryCoastalAssets

      // Think about tomorrow ...
      val developedCoastalAssetsGrowthRate = pow(((countryCoastalAssets + (countryAdditionalCostalAssets - thisPeriodAdditionalCoastalOnlyDevelopableAssets)) / countryCoastalAssets), 1 / currentState.timeStep.toFloat)
      val developedCoastalAssetsGrowthRateAsGDPCGrowthRate = developedCoastalAssetsGrowthRate / (1.0f + country.data.popgrowth_memory(currentState.time))

      val countryCostalAssetsDeveloped1: Float = (czs.map(x => x.coastalPlane.assetsExposureDeveloped.cumulativeExposure(elevationLimit))).foldLeft(0f)((sum, x) => sum + x.asInstanceOf[Float])
      val countryCostalAssetsDevelopable1: Float = (czs.map(x => x.coastalPlane.assetsExposureDevelopable.cumulativeExposure(elevationLimit))).foldLeft(0f)((sum, x) => sum + x.asInstanceOf[Float])

      /*
      println(currentState.time + " intense coastal construction: " + (currentState.time <= (currentState.startTime + intenseCoastalDevelopableConstructionPeriods * currentState.timeStep)) + "  remainingIntenseCoastalDevelopableConstructionPeriods: " + remainingIntenseCoastalDevelopableConstructionPeriods)
      println("countryCostalArea: " + countryCoastalArea)
      println("countryCostalAreaDevelopable: " + countryCoastalArea * country.data.fraction_developable.get)
      println("countryCoastalOnlyDevelopableArea: " + countryCoastalOnlyDevelopableArea)
      println("countryCostalAssets: " + countryCoastalAssets)
      println("countryCostalAssetsDeveloped: " + countryCostalAssetsDeveloped1)
      println("countryCostalAssetsDevelopable: " + countryCostalAssetsDevelopable1)
      println("countryCostalAssetsTotalTarget: " + countryCostalAssetsTotalTarget)
      println("developedCoastalAssetsGrowthRate: " + developedCoastalAssetsGrowthRate)
      println("totalCoastalAssetsGrowthRate: " + (1.0 + country.data.gdpcgrowth_memory(currentState.time)))
      println()
      */

      /*
      czs.foreach {
        cz =>
          if (cz.locationid == "CZ_HRV_00411") {
            cz.coastalPlane.assetsExposureDevelopable.printForDebug()
            println("CZ_HRV_00411 noAssetDevelopmentDevelopedBelow = " + cz.coastalPlane.noAssetDevelopmentDevelopedBelow)
            println("CZ_HRV_00411 noAssetDevelopmentDevelopableBelow = " + cz.coastalPlane.noAssetDevelopmentDevelopableBelow)
            println("CZ_HRV_00411 setbackHeight = " + cz.coastalPlane.setbackHeight)
            println("CZ_HRV_00411 SetbackzoneAssets = " + cz.coastalPlane.getSetbackzoneAssets(true).toFloat)
            println("CZ_HRV_00411 SetbackzoneAssets = " + cz.coastalPlane.getSetbackzoneAssets(false).toFloat)
          }
      }*/

      czs.foreach {
        cz =>
          val noNewConstructionBelow: Float = if (cz.cls.data.seadike_height > 0f) 0f else cz.cls.floodHazard.returnPeriodHeight(1.0f).asInstanceOf[Float]
          cz.coastalPlane.noAssetDevelopmentDevelopableBelow = if (cz.coastalPlane.noAssetDevelopmentDevelopableBelow < noNewConstructionBelow) noNewConstructionBelow else cz.coastalPlane.noAssetDevelopmentDevelopableBelow
          val depr: (Float, Float) = cz.coastalPlane.intenseCoastalDevelopment(1.0f + cz.getAnnualPopGrowthAtTime(currentState.time), developedCoastalAssetsGrowthRateAsGDPCGrowthRate.toFloat, targetCoastalOnlyDevelopableAssetsDensity.toFloat, currentState.timeStep, cz.locationid == "CZ_HRV_00411")
          cz.data.setbackzone_deprecation = depr._2
      }

    } else {

      czs.foreach {
        cz =>
          {
            val depr: (Float, Float) = cz.coastalPlane.socioEconomicDevelopment(1.0f + cz.getAnnualPopGrowthAtTime(currentState.time), (1.0f + cz.data.gdpcgrowth_memory(currentState.time)), currentState.timeStep)
            cz.data.setbackzone_deprecation = depr._2
          }
      }

      /*
      czs.foreach {
        cz =>
          if (cz.locationid == "CZ_HRV_00411") {
            cz.coastalPlane.assetsExposureDevelopable.printForDebug()
            println("CZ_HRV_00411 SetbackzoneAssets = " + cz.coastalPlane.getSetbackzoneAssets(true).toFloat)
            println("CZ_HRV_00411 SetbackzoneAssets = " + cz.coastalPlane.getSetbackzoneAssets(false).toFloat)
          }
      } */
    }

    czs.foreach {
      cz =>
        {
          cz.data.localgdp = cz.coastalPlane.populationExposure.cumulativeExposure(elevationLimit).asInstanceOf[Float] * cz.data.gdpc
          computeLandval(cz)
        }
    }
  }

  private def accumulate(state: divaState) {
    var global: List[Global] = state.getFeatures[Global]
    global.foreach { (g: Global) => g.data.totalpop = g.countries.foldLeft(0f)((sum, country) => sum + country.data.totalpop) }
    global.foreach { (g: Global) => g.data.gdp = g.countries.foldLeft(0f)((sum, country) => sum + country.data.gdp) }
    global.foreach { (g: Global) => g.data.gdpc = g.data.gdp / (g.data.totalpop / 1000) }

    var cities: ParSeq[City] = state.getFeatures[City].par
    cities.foreach {
      (city: City) =>
        city.data.gdpc = computeCityGDPC(city)
        city.data.localgdp = city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.localgdp / 1000000))
    }

    var deltas: ParSeq[Delta] = state.getFeatures[Delta].par
    deltas.foreach {
      (delta: Delta) =>
        delta.data.gdpc = computeDeltaGDPC(delta)
        delta.data.localgdp = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.localgdp / 1000000))
    }

    var admins: ParSeq[Admin] = state.getFeatures[Admin].par
    admins.foreach {
      (admin: Admin) =>
        {
          admin.data.gdpc = admin.country.data.gdpc * admin.data.gdpcmult
          admin.data.setbackzone_deprecation = admin.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.setbackzone_deprecation / 1000000))
          admin.data.localgdp = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.localgdp / 1000000))
        }
    }

    var countries: ParSeq[Country] = state.getFeatures[Country].par
    countries.foreach {
      (country: Country) => country.data.setbackzone_deprecation = country.czs.foldLeft(0f)((sum, cz) => sum + (cz.data.setbackzone_deprecation / 1000000))
    }

  }

  private def computeCityGDPC(city: City): Float =
    if (city.clss.length > 0)
      city.clss.foldLeft(0f)((sum, cls) => sum + cls.data.gdpc) / city.clss.length
    else
      0

  private def computeDeltaGDPC(delta: Delta): Float =
    if (delta.clss.length > 0)
      delta.clss.foldLeft(0f)((sum, cls) => sum + cls.data.gdpc) / delta.clss.length
    else
      0

  private def computeLandval(cz: Cz) {
    val landval_agri: Float = (agvalalpha * Math.pow(cz.coastalPlane.populationDensityBelow(16.5f) * cz.data.gdpc * moneyBackwardsTo1995ConverionFactor, agvalbeta).asInstanceOf[Float]) * moneyForwardsFrom1995ConverionFactor
    /*
    if (cz.locationid == "CZ_RUS_00148") {
      println("computing landval_agri:")
      println(agvalalpha + " * Math.pow(" + cz.coastalPlane.populationDensityBelow(16.5f) + " * " + cz.data.gdpc + " * " + moneyBackwardsTo1995ConverionFactor + "," + agvalbeta + ") * " + moneyForwardsFrom1995ConverionFactor);
      println("= " + landval_agri)
    }*/

    cz.data.landvalue_unit =
      if (cz.data.landuse == 1) landval_agri else (landval_agri / 2)

    /*
      case 2 =>
        val landval_grass = landval_agri / 2; cls.landval_unit = landval_grass
      case 3 =>
        val landval_carbon = landval_agri / 2; cls.landval_unit = landval_carbon
      case 4 =>
        val landval_abantimber = landval_agri / 2; cls.landval_unit = landval_abantimber
      case 5 =>
        val landval_timber = landval_agri / 2; cls.landval_unit = landval_timber
      case 6 =>
        val landval_ice = landval_agri / 2; cls.landval_unit = landval_ice
      case 7 =>
        val landval_tundra = landval_agri / 2; cls.landval_unit = landval_tundra
      case 8 =>
        val landval_woodtundra = landval_agri / 2; cls.landval_unit = landval_woodtundra
      case 9 =>
        val landval_borealforest = landval_agri / 2; cls.landval_unit = landval_borealforest
      case 10 =>
        val landval_coolneedle = landval_agri / 2; cls.landval_unit = landval_coolneedle
      case 11 =>
        val landval_tempmixed = landval_agri / 2; cls.landval_unit = landval_tempmixed
      case 12 =>
        val landval_tempbroad = landval_agri / 2; cls.landval_unit = landval_tempbroad
      case 13 =>
        val landval_warmmixed = landval_agri / 2; cls.landval_unit = landval_warmmixed
      case 14 =>
        val landval_steppe = landval_agri / 2; cls.landval_unit = landval_steppe
      case 15 =>
        val landval_hotdesert = landval_agri / 2; cls.landval_unit = landval_hotdesert
      case 16 =>
        val landval_scrub = landval_agri / 2; cls.landval_unit = landval_scrub
      case 17 =>
        val landval_savannah = landval_agri / 2; cls.landval_unit = landval_savannah
      case 18 =>
        val landval_tropicalwood = landval_agri / 2; cls.landval_unit = landval_tropicalwood
      case 19 =>
        val landval_tropicalforest = landval_agri / 2; cls.landval_unit = landval_tropicalforest
     */
  }

}
