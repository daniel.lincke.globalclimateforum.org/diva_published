package diva.types

import scala.language.implicitConversions

object SlrNormalisationMode extends Enumeration {
  type SlrNormalisationMode = Value

  val CLS_LENGTH = Value("CLS_LENGTH")
  val CLS_POP_H100 = Value("CLS_POP_H100")
  val CLS_POP_10M = Value("CLS_POP_10M")
  
  implicit def valueToInterpolationMode(v: Value): SlrNormalisationMode = v.asInstanceOf[SlrNormalisationMode]
  implicit def stringToInterpolationMode(s: String): SlrNormalisationMode = SlrNormalisationMode.withName(s)
}