package diva.datagenerator

import diva.io.logging.Logger
import java.io.FileReader
import com.opencsv.CSVReader
import scala.collection.mutable.HashMap
import java.io.File
import java.io.FileWriter
import java.util.Calendar

object DataGenerator {

  var logger: Logger = new Logger
  var syspropsFileName: String = ""
  var csvreader: CSVReader = null
  var header: List[String] = List()
  var datatable: HashMap[String, List[DataForOneProperty]] = new HashMap[String, List[DataForOneProperty]]

  private def readHeader: List[String] = {
    val nextLine = csvreader.readNext()
    if (nextLine != null) nextLine.toList else List()
  }

  private def readNextLine: Option[HashMap[String, String]] = {
    val nextLine = csvreader.readNext()
    if (nextLine == null)
      None
    else {
      val nextLineList = nextLine.toList
      if (nextLineList.length != header.length) {
        logger.warn("line " + nextLine + " in " + syspropsFileName + " has wrong number of elements: " + nextLine.length + " instead of " + header.length + " (header)")
      }
      val m: HashMap[String, String] = new HashMap[String, String]
      val nextLineWithHeader = header zip nextLineList
      nextLineWithHeader foreach ((p: (String, String)) => m(p._1) = p._2)
      Some(m)
    }
  }

  private def addProperty(f: String, nl: HashMap[String, String], spfn: Option[String]) {
    var data: DataForOneProperty = new DataForOneProperty
    data.Output = nl.getOrElse("Output", "") == "1"
    data.Input = nl.getOrElse("Input", "") == "1"
    data.Accumulate = nl.getOrElse("Accumulate", "") == "1"
    data.Memory = nl.getOrElse("Memory", "") == "1"
    data.resetEveryPeriod = nl.getOrElse("ResetEveryPeriod", "") == "1"
    data.Unit = nl.getOrElse("Unit", "")
    data.Max = nl.getOrElse("Max", "")
    data.Min = nl.getOrElse("Min", "")
    data.Default = nl.getOrElse("Default", "")
    data.Description = nl.getOrElse("Description", "")
    data.propName = nl.getOrElse("Property Acronym", "")
    data.propType = nl.getOrElse("Property Type", "")
    data.rawType = nl.getOrElse("rawType", "")
    data.divaType = nl.getOrElse("divaType", "")
    data.Interpolation = nl.getOrElse("Interpolation", "")
    data.Configuration = nl.getOrElse("Configuration", "")

    // for optional inputs, change the type to option type
    if (((data.propType == "OP") || (data.propType == "OD")) && (!(data.rawType contains "Option[")) && (data.divaType == "")) {
      data.rawType = "Option[" + data.rawType + "]"
    }

    // for optional inputs, change the type to option type
    if (((data.propType == "OP") || (data.propType == "OD")) && (!(data.rawType contains "Option[")) && (data.divaType != "")) {
      data.divaType = "Option[" + data.divaType + "]"
    }

    if (data.rawType contains "Option[") {
      data.dataRawType = data.rawType.replace("Option[", "").dropRight(1)
    } else {
      data.dataRawType = data.rawType
    }

    // set flag if the type is an option type
    if ((data.Input) || (data.propType == "OD") || (data.propType == "D")) {
      data.OptionType = (data.rawType contains "Option[") || (data.divaType contains "Option[")
    }

    if (!data.Input && data.propType == "IV") {
      logger.error(data.propName + " for feature " + f + " is decalared as initialised variable (IV) but not as input.")
    }

    if (data.Input && data.propType == "D") {
      logger.error(data.propName + " for feature " + f + " is decalared as driver, so it should not be declared as input.")
    }

    if ((data.propType == "D") && (data.Interpolation == "")) {
      logger.error("no interpolationmode given for driver property " + data.propName + " in " + spfn.get)
    }

    datatable.get(f) match {
      case None    => datatable += (f -> List(data))
      case Some(l) => datatable.update(f, l :+ data)
    }

  }

  private def addProperties(nl: HashMap[String, String], spfn: Option[String]) {
    val feats = nl.getOrElse("Feature Name", "") split ","
    if (feats.length == 0) {
      logger.warn("no feature name found in line " + nl.toList.toString() + " in " + spfn.get);
    } else {
      feats.foreach { (s: String) => addProperty(s.trim, nl, spfn) }
    }
  }

  private def generateOneProperty(dat: DataForOneProperty, fw: FileWriter, indention: Int) {
    val indentionString: String = "  " * indention

    if (dat.Output) { fw.write(indentionString + "@Output\n") }
    if (dat.Input) { fw.write(indentionString + "@Input\n") }
    if (dat.resetEveryPeriod) { fw.write(indentionString + "@ResetEveryPeriod\n") }
    if (dat.propType == "P") { fw.write(indentionString + "@Parameter\n") }
    if (dat.propType == "OP") { fw.write(indentionString + "@OptionalParameter\n") }
    if (dat.propType == "V") { fw.write(indentionString + "@Variable\n") }
    if (dat.propType == "IV") { fw.write(indentionString + "@Variable\n") }
    if (dat.propType == "V") {
      if (dat.Configuration != "") {
        fw.write(indentionString + "@Configure(mode = \"" + dat.Configuration + "\")\n")
      }
    }
    if (dat.propType == "D") {
      fw.write(indentionString + "@Driver\n")
      if (dat.Interpolation != "") {
        fw.write(indentionString + "@Interpolate(mode = \"" + dat.Interpolation + "\")\n")
      } else {

      }
    }
    if (dat.propType == "OD") {
      fw.write(indentionString + "@OptionalDriver\n")
      if (dat.Interpolation != "") {
        fw.write(indentionString + "@Interpolate(mode = \"" + dat.Interpolation + "\")\n")
      } else {

      }
    }
    fw.write(indentionString + "@Description(value = \"" + dat.Description + "\")\n")
    if (dat.divaType == "") {
      fw.write(indentionString + "var " + dat.propName + " :  " + dat.rawType)
      if (dat.Default != "") {
        fw.write(" = " + dat.Default)
        if (dat.rawType == "Float") { fw.write("f") }
        if (dat.rawType == "Double") { fw.write("d") }
      } else {
        fw.write(" = default[" + dat.rawType + "]")
      }
      if (dat.Memory) {
        fw.write("\n" + indentionString + "var " + dat.propName + "_memory: Map[Int ," + dat.rawType + "] = Map()")
      }
    } else {
      fw.write(indentionString + "var " + dat.propName + " :  " + dat.divaType)
      if (dat.Default != "") {
        fw.write(" = " + dat.Unit + "(" + dat.Default)
        if (dat.rawType == "Float") { fw.write("f") }
        if (dat.rawType == "Double") { fw.write("d") }
        fw.write(")")
      } else {
        if (dat.OptionType) {
          fw.write(" = default[" + dat.divaType + "]")
        } else {
          fw.write(" = " + dat.Unit + "(default[" + dat.rawType + "])")
        }
      }
      if (dat.Memory) {
        fw.write("\n" + indentionString + "var " + dat.propName + "_memory: Map[Int ," + dat.divaType + "] = Map()")
      }
    }

    fw.write("\n\n")
  }

  private def generateDataCodeForFeature(dir: String, f: String, l: List[DataForOneProperty]) {
    var canonicalFeatureName = f.toLowerCase().capitalize

    var filename = dir + "/" + canonicalFeatureName + "Data.scala"
    var file: File = new File(filename)
    var writer: FileWriter = new FileWriter(file)

    writer.write("/* Automatically generated data features for " + canonicalFeatureName + "\n")
    writer.write("*  generated on " + Calendar.getInstance().getTime() + "\n")
    writer.write("*/\n\n")

    writer.write("package diva.feature.data\n\n")

    writer.write("import diva.annotations._\n")
    writer.write("import diva.types._\n")
    writer.write("import diva.types.TypeComputations._\n")

    writer.write("import scala.collection.mutable._\n\n\n")

    writer.write("class " + canonicalFeatureName + "Data {\n")
    writer.write("\n")
    l.sortWith((x1: DataForOneProperty, x2: DataForOneProperty) => (x1.propName < x2.propName)).foreach { (dat: DataForOneProperty) => generateOneProperty(dat, writer, 1) }

    writer.write("  override def clone : " + canonicalFeatureName + "Data = {\n")
    writer.write("    var ret: " + canonicalFeatureName + "Data  = new " + canonicalFeatureName + "Data\n")
    l.sortWith((x1: DataForOneProperty, x2: DataForOneProperty) => (x1.propName < x2.propName)).foreach {
      (dat: DataForOneProperty) =>
        writer.write("    ret." + dat.propName + " = " + dat.propName + "\n")
        if (dat.Memory) {
          writer.write("    ret." + dat.propName + "_memory = " + dat.propName + "_memory\n")
        }
    }
    writer.write("    ret\n")
    writer.write("  }\n\n")

    writer.write("}\n")

    writer.flush();
    writer.close();
    logger.info("generating feature data " + canonicalFeatureName + "Data: " + l.size + " data fields")
  }

  private def generateDataIOCodeForWriteData(writer: FileWriter, indentionString: String, canonicalFeatureName: String, l: List[DataForOneProperty]) {
    writer.write(indentionString + "def writeData(out: divaOutput, d: " + canonicalFeatureName + "Data) {\n")
    l.sortWith((x1: DataForOneProperty, x2: DataForOneProperty) => (x1.propName < x2.propName)).foreach {
      (dat: DataForOneProperty) =>
        {
          if ((dat.Output) && (dat.Configuration == "") && (dat.divaType == "")) writer.write(indentionString * 2 + "out.write(d." + dat.propName + ")\n")
          // divaTypes:
          if ((dat.Output) && (dat.Configuration == "") && (dat.divaType != "")) writer.write(indentionString * 2 + "out.write(d." + dat.propName + ".to" + dat.Unit + ")\n")
          // configurable outputs:
          if ((dat.Output) && (dat.Configuration != "")) {
            writer.write(indentionString * 2 + "d." + dat.propName + ".keys.toList.sorted.foreach { x => out.write(d." + dat.propName + "(x)) }" + "\n")
          }
        }
    }
    writer.write(indentionString + "}\n\n")

    writer.write(indentionString + "def writeSelectedData(out: divaOutput, d: " + canonicalFeatureName + "Data, ignoreThisOutput: Array[Boolean]) {\n")
    writer.write(indentionString * 2 + "var c: Int = 0\n")
    l.sortWith((x1: DataForOneProperty, x2: DataForOneProperty) => (x1.propName < x2.propName)).foreach {
      (dat: DataForOneProperty) =>
        {
          if ((dat.Output) && (dat.Configuration == "") && (dat.divaType == "")) writer.write(indentionString * 2 + "if (!ignoreThisOutput(c)) {out.write(d." + dat.propName + ")}; c+=1\n")
          // divaTypes:
          if ((dat.Output) && (dat.Configuration == "") && (dat.divaType != "")) writer.write(indentionString * 2 + "if (!ignoreThisOutput(c)) {out.write(d." + dat.propName + ".value)}; c+=1\n")
          // configurable outputs:
          if ((dat.Output) && (dat.Configuration != "")) {
            writer.write(indentionString * 2 + "d." + dat.propName + ".keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d." + dat.propName + "(x))}; c+=1 } } \n")
          }
        }
    }
    writer.write(indentionString + "}\n\n")
  }

  private def generateDataIOCodeForReadInputValue(writer: FileWriter, indentionString: String, canonicalFeatureName: String, l: List[DataForOneProperty]) {
    writer.write(indentionString + "def readInputValue(in: divaInput, d:" + canonicalFeatureName + "Data, parName: String, locid: String, time: Int) {\n")
    var first = true;
    l.sortWith((x1: DataForOneProperty, x2: DataForOneProperty) => (x1.propName < x2.propName)).foreach {
      (dat: DataForOneProperty) =>
        {
          if (dat.Input && !dat.OptionType && !dat.OptionalInput) {
            if (first) {
              if (dat.divaType == "") {
                writer.write(indentionString * 2 + "if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = in.getInputValue[" + dat.rawType + "](\"" + dat.propName + "\", locid, time)}\n")
              } else {
                writer.write(indentionString * 2 + "if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = " + dat.Unit + "(in.getInputValue[" + dat.rawType + "](\"" + dat.propName + "\", locid, time))}\n")
              }
              first = false
            } else {
              if (dat.divaType == "") {
                writer.write(indentionString * 2 + "else if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = in.getInputValue[" + dat.rawType + "](\"" + dat.propName + "\", locid, time)}\n")
              } else {
                writer.write(indentionString * 2 + "else if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = " + dat.Unit + "(in.getInputValue[" + dat.rawType + "](\"" + dat.propName + "\", locid, time))}\n")
              }
            }
          }
        }

    }
    if (!first) {
      // we actually should log an error here ... 
      writer.write(indentionString * 2 + "// else println(\"error:\" + parName + \" not found in " + canonicalFeatureName + "\")\n");
    }
    writer.write(indentionString + "}\n\n")
  }

  private def generateDataIOCodeForReadOptionInputValue(writer: FileWriter, indentionString: String, canonicalFeatureName: String, l: List[DataForOneProperty]) {
    writer.write(indentionString + "def readOptionInputValue(in: divaInput, d:" + canonicalFeatureName + "Data, parName: String, locid: String, time: Int) {\n")
    var first = true;

    l.sortWith((x1: DataForOneProperty, x2: DataForOneProperty) => (x1.propName < x2.propName)).foreach { (dat: DataForOneProperty) =>
      { //println(dat.propName + " - " + dat.propType + "(" + dat.OptionType + "," + dat.OptionalInput + ")")
        if ((dat.OptionType || dat.OptionalInput) && (dat.propType != "D") && (dat.propType != "OD")) {
          if (first) {
            if (dat.divaType == "") {
              writer.write(indentionString * 2 + "if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = in.getOptionInputValue[" + dat.dataRawType + "](\"" + dat.propName + "\", locid, time)}\n")
            } else {
              writer.write(indentionString * 2 + "if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = in.getOptionInputValue[" + dat.dataRawType + "](\"" + dat.propName + "\", locid, time).map(" + dat.Unit + "(_))}\n")
            }
            first = false
          } else {
            if (dat.divaType == "") {
              writer.write(indentionString * 2 + "else if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = in.getOptionInputValue[" + dat.dataRawType + "](\"" + dat.propName + "\", locid, time)}\n")
            } else {
              writer.write(indentionString * 2 + "else if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = in.getOptionInputValue[" + dat.dataRawType + "](\"" + dat.propName + "\", locid, time).map(" + dat.Unit + "(_))}\n")
            }
          }
        }
      }
    }

    if (!first) {
      // we actually should log an error here ... 
      writer.write(indentionString * 2 + "// else println(\"error:\" + parName + \" not found in " + canonicalFeatureName + "\")\n");
    }
    writer.write(indentionString + "}\n\n")
  }

  private def generateDataIOCodeForReadScenarioValue(writer: FileWriter, indentionString: String, canonicalFeatureName: String, l: List[DataForOneProperty]) {

    writer.write(indentionString + "def readScenarioValue(in: divaInput, d:" + canonicalFeatureName + "Data, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {\n")
    var first = true
    l.sortWith((x1: DataForOneProperty, x2: DataForOneProperty) => (x1.propName < x2.propName)).foreach { (dat: DataForOneProperty) =>
      {
        if ((dat.propType == "D") && !dat.OptionType && !dat.OptionalInput) {
          if (first) {
            // check if raw type!
            writer.write(indentionString * 2 + "if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = in.getScenarioValue[" + dat.rawType + "](\"" + dat.propName + "\", locid, time, pInterpolationMode)}\n")
            first = false
          } else {
            // check if raw type!
            writer.write(indentionString * 2 + "else if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = in.getScenarioValue[" + dat.rawType + "](\"" + dat.propName + "\", locid, time, pInterpolationMode)}\n")
          }
        }
      }
    }

    if (!first) {
      // we actually should log an error here ... 
      writer.write(indentionString * 2 + "// else println(\"error:\" + parName + \" not found in " + canonicalFeatureName + "\")\n");
    }
    writer.write(indentionString + "}\n\n")
  }

  private def generateDataIOCodeForReadOptionScenarioValue(writer: FileWriter, indentionString: String, canonicalFeatureName: String, l: List[DataForOneProperty]) {

    writer.write(indentionString + "def readOptionScenarioValue(in: divaInput, d:" + canonicalFeatureName + "Data, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {\n")
    var first = true
    l.sortWith((x1: DataForOneProperty, x2: DataForOneProperty) => (x1.propName < x2.propName)).foreach { (dat: DataForOneProperty) =>
      {
        if (((dat.propType == "D") || (dat.propType == "OD")) && (dat.OptionType || dat.OptionalInput)) {
          if (first) {
            writer.write(indentionString * 2 + "if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = in.getOptionScenarioValue[" + dat.dataRawType + "](\"" + dat.propName + "\", locid, time, pInterpolationMode)}\n")
            first = false
          } else {
            writer.write(indentionString * 2 + "else if (parName==\"" + dat.propName + "\") {d." + dat.propName + " = in.getOptionScenarioValue[" + dat.dataRawType + "](\"" + dat.propName + "\", locid, time, pInterpolationMode)}\n")
          }
        }
      }
    }

    if (!first) {
      // we actually should log an error here ... 
      writer.write(indentionString * 2 + "// else println(\"error:\" + parName + \" not found in " + canonicalFeatureName + "\")\n");
    }
    writer.write(indentionString + "}\n\n")
  }

  private def generateDataIOCodeForFeature(dir: String, f: String, l: List[DataForOneProperty]) {
    var canonicalFeatureName = f.toLowerCase().capitalize

    var filename = dir + "/io/" + canonicalFeatureName + "DataIO.scala"
    var file: File = new File(filename)
    var writer: FileWriter = new FileWriter(file)
    val indentionString: String = "  "

    writer.write("/* Automatically generated data IO for " + canonicalFeatureName + "\n")
    writer.write("*  generated on " + Calendar.getInstance().getTime() + "\n")
    writer.write("*/\n\n")

    writer.write("package diva.feature.data.io\n\n")

    writer.write("import diva.io._\n")
    writer.write("import diva.types.InterpolationMode.InterpolationMode\n")
    writer.write("import diva.feature.data." + canonicalFeatureName + "Data\n\n")

    writer.write("class " + canonicalFeatureName + "DataIO extends FeatureDataIO[" + canonicalFeatureName + "Data] {\n")
    writer.write("\n")

    generateDataIOCodeForWriteData(writer, indentionString, canonicalFeatureName, l)
    generateDataIOCodeForReadInputValue(writer, indentionString, canonicalFeatureName, l)
    generateDataIOCodeForReadOptionInputValue(writer, indentionString, canonicalFeatureName, l)
    generateDataIOCodeForReadScenarioValue(writer, indentionString, canonicalFeatureName, l)
    generateDataIOCodeForReadOptionScenarioValue(writer, indentionString, canonicalFeatureName, l)

    writer.write("}\n")

    writer.flush();
    writer.close();
    logger.info("generating feature data io " + canonicalFeatureName + "DataIO")
  }

  private def generateCode(dir: String) {
    datatable.keySet.toList.sortWith(_ < _).foreach { (s: String) => generateDataCodeForFeature(dir, s, datatable(s)) }
    datatable.keySet.toList.sortWith(_ < _).foreach { (s: String) => generateDataIOCodeForFeature(dir, s, datatable(s)) }
  }

  def run(args: Array[String]) {

    // constructand set up the logger ...
    // ... first as a screen logger
    logger = new Logger
    logger.id = "dataGenerator"

    datatable = new HashMap[String, List[DataForOneProperty]]

    val syspropsFileName: Option[String] =
      if (args.length == 0) {
        logger.error("no sysprops file given")
        None
      } else {
        Some(args(0))
      }

    val targetDir: Option[String] = {
      if (args.length != 2) {
        logger.warn("no target directory given. Generating code in current directory.")
        Some(".")
      } else {
        Some(args(1))
      }
    }

    var filereader: Option[FileReader] = try {
      Some(new FileReader(syspropsFileName.get))
    } catch {
      case fnfe: java.io.FileNotFoundException =>
        logger.error("file " + syspropsFileName.get + " not found"); None
      case ioe: java.io.IOException =>
        logger.error("file " + syspropsFileName.get + ": i/o error"); None
      case _: Throwable => logger.error("file " + syspropsFileName.get + ": some unknown error"); None
    }

    csvreader = new CSVReader(filereader.get)
    header = readHeader

    var nextLine: Option[HashMap[String, String]] = readNextLine
    while (nextLine != None) {
      //println(nextLine.toList.toString())
      addProperties(nextLine.get, syspropsFileName)
      nextLine = readNextLine
    }

    generateCode(targetDir.get)
    logger.info("generated data and io for " + datatable.keySet.size + " features")

  }

}