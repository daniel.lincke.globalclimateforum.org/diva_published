package diva.toolbox

import diva.toolbox.SimpleNumericalIntegration._

import math._
import org.apache.commons.math3.analysis.integration.SimpsonIntegrator
import org.apache.commons.math3.analysis.UnivariateFunction
import org.apache.commons.math3.exception.TooManyEvaluationsException

object NumericalIntegration {

  var integrator: SimpsonIntegrator = new SimpsonIntegrator();

  def integrate[T](f: T => T, a: T, b: T, steps: Int)(implicit frac: Fractional[T], ord: Ordering[T]): T = {

    import ord.mkOrderingOps
    import frac.mkNumericOps
    
    var lSteps: Int = steps
    if (b <= a) return frac.zero
    if (lSteps < 4) lSteps = 4

    var integral: T = frac.zero
    integral = integrateSimple[T](f, a, b, lSteps, simpson)

    integral
  }

  def integratePieceWise(f: Double => Double, a: Double, b: Double, steps: Int, pieces: Array[Double]): Double = {
    var integrator: SimpsonIntegrator = new SimpsonIntegrator();
    var integral: Double = 0.0

    if (b <= a) return 0.0
    for (i <- 0 until pieces.length - 1) {
      // case 1: a and b in one segment ... just do normal integration
      if ((pieces(i) <= a) && (a <= pieces(i + 1)) && (pieces(i) <= b) && (b <= pieces(i + 1))) {
        return integrate(f, a, b, steps)
      }

      // case 2: the whole segment has to be integrated ... 
      if ((a <= pieces(i)) && (pieces(i + 1) <= b)) {
        integral = integral + integrate(f, pieces(i), pieces(i + 1), steps)
      }

      // case 3: a to the segment border segment has to be integrated ... 
      if ((pieces(i) <= a) && (a <= pieces(i + 1)) && (pieces(i + 1) <= b)) {
        integral = integral + integrate(f, a, pieces(i + 1), steps)
      }

      // case 4: segment border to b has to be integrated ... 
      if ((a <= pieces(i)) && (pieces(i) <= b) && (b <= pieces(i + 1))) {
        integral = integral + integrate(f, pieces(i), b, steps)
      }
    }
    integral
  }

}
