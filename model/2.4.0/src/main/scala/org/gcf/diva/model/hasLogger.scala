package diva.model

import diva.io.logging._

trait hasLogger {
  
  // everything that has a logger gets a standard (=screen) logger when constructed
  var logger: LoggerI = new Logger

}