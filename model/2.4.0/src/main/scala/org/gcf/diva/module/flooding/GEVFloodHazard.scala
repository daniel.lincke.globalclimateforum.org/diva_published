package diva.module.flooding

import diva.toolbox.exceptions.GEVWithNonPositiveScaleException

// gev_location = mue
// gev_scale = sigma
// gev_shape = xi
// gev_number_events = r
class GEVFloodHazard(pMue: Float, pSigma: Float, pXi: Float, pR: Float)
  extends FloodHazardI {

  // only check here - if we try to construct a GEV with pSigma<=0  we will fail.
  if (pSigma <= 0) throw new GEVWithNonPositiveScaleException()

  // helper for the more support boundary
  private def support_boundary: Double = {
    if (pR == 1)
      (pMue + shift) - (pSigma / pXi)
    else {
      if ((pR != 1) && (pXi == 0)) {
        -scala.math.log(-scala.math.log(1 - 1 / pR)) * pSigma + (pMue + shift)
      } else {
        (((scala.math.pow(-scala.math.log(1 - 1 / pR), -pXi) - 1) * pSigma) / pXi) + (pMue + shift)
      }

    }
  }

  // helper for the more complex formulas
  private def t(x: Double): Double = {
    if (pXi == 0d) {
      scala.math.exp(-(x - (pMue + shift)) / pSigma)
    } else {
      scala.math.pow((1 + pXi * ((x - (pMue + shift)) / pSigma)), (-1 / pXi))
    }
  }

  // Mathematically, this is
  // F(x) = P(X<=level)
  // As I understand the support depends on pXi
  def cdf(level: Level): Probability = {
    // lower and upper bound needs to be adjusted?
    //val lower_bound = if (pXi > 0) support_boundary else scala.Double.MinValue
    //val upper_bound = if (pXi < 0) support_boundary else scala.Double.MaxValue
    val lower_bound = scala.Double.MinValue
    val upper_bound = scala.Double.MaxValue
    if ((lower_bound <= level) && (level <= upper_bound)) {
      //scala.math.exp(-t(level))
      val mt: Double = -t(level)
      if (mt.isNaN()) {
        0
      } else {
        val r = 1 - ((1 - scala.math.exp(mt)) * pR)
        if (r < 0) 0 else r
      }
    } else {
      0
    }
  }

  def cdf_inverse(cuprob: Probability): Level = {
    /*
    if (pXi == 0d) {
      -((pMue + shift) - pSigma * (scala.math.log(-scala.math.log(cuprob))))
    } else {
      ((scala.math.pow(-scala.math.log(cuprob), -pXi) - 1) * pSigma / pXi) + (pMue + shift)
    }
    */
    if (pXi == 0d) {
      -((pMue + shift) - pSigma * (scala.math.log(-scala.math.log(1 - ((1 - cuprob) / pR))).toFloat))
    } else {
      ((scala.math.pow(-scala.math.log(1 - ((1 - cuprob) / pR)), -pXi) - 1).toFloat * pSigma / pXi) + (pMue + shift)
    }
  }

  // Mathematically this is
  // f(x) = F'(x)
  def cdf_prime(level: Level): Probability = {
    /*
    if (pXi == 0d) {
      scala.math.exp(-scala.math.exp(-(level - (pMue + shift)) / pSigma) - ((pMue + shift) / pSigma)) / pSigma
    } else {
      (scala.math.pow(((pXi * (level - (pMue + shift))) / pSigma + 1), (-1 / pXi - 1))
        * scala.math.exp(-1 / scala.math.pow(((pXi * (level - (pMue + shift))) / pSigma + 1), (1 / pXi))) / pSigma)
    }*/
    if (pXi == 0d) {
      (pR * scala.math.exp(-scala.math.exp(-(level.toDouble - (pMue + shift)) / pSigma) - (level.toDouble - (pMue + shift)) / pSigma)) / pSigma
    } else {
      val x1: Double = ((pXi * (level.toDouble - (pMue + shift))) / pSigma + 1)
      if (x1 < 0) {
        val eps: Double = 10e-5
        val f_x: Double = cdf(level)
        val f_x_minus_eps: Double = cdf(level - eps.toFloat)
        val f_x_plus_eps: Double = cdf(level + eps.toFloat)
        (f_x_plus_eps - f_x_minus_eps) / (2 * eps)
      } else {
        val x2: Double = (-1 / pXi - 1)
        (pR * scala.math.pow(x1, x2) * scala.math.exp(-1 / scala.math.pow(x1, (1 / pXi)))) / pSigma
      }
    }
  }


  def returnPeriodHeight(returnPeriod: TimePeriod): Level = {
    val rp: Double = if (returnPeriod == 1) returnPeriod + 1e-10d else returnPeriod
    (pMue + shift) + (pSigma / pXi * (scala.math.pow(-scala.math.log(1 - (1 / (rp * pR))), -pXi) - 1).toFloat)
  }

  def cloneHazard: GEVFloodHazard = {
    var ret: GEVFloodHazard = new GEVFloodHazard(pMue, pSigma, pXi, pR)
    ret.shift = shift
    return ret
  }

}