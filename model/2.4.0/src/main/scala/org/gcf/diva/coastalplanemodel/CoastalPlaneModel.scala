package diva.coastalplanemodel

import scala.math._

import diva.feature.Cls
import diva.feature.Cz
import diva.model.hasLogger
import diva.types.RetreatMode._

class CoastalPlaneModel(val pLength: Float, val pHeights: Array[Float], val pExposedArea: Array[Float], pExposedPeople: Array[Float], pExposedAssets: Array[Float], val pLanduse: Array[Int], pDevelopedFrac: Float = 1.0f, pDevelopableFrac: Float = 0.0f)
  extends hasLogger {

  var dikeHeigth: Float = 0f
  var dikeBase: Float = 0f
  var gUndevlopedFrac: Float = 1.0f - (pDevelopedFrac + pDevelopableFrac)
  var gDevelopedFrac: Float = pDevelopedFrac
  var gDevlopableFrac: Float = pDevelopableFrac
  val elevationLimit: Float = 20.5f
  var assetsPerCapita: Float = _

  var pHeightsAttenuation: Array[Float] = pHeights.clone

  // Exposures for the developed zone
  // maps: elevation to area below
  var areaExposureDeveloped: CoastalArea = new CoastalArea(pHeights, pHeightsAttenuation, pExposedArea.map { x => x * pDevelopedFrac })
  // maps: elevation to people below
  //private
  var populationExposureDeveloped: CoastalPopulation = new CoastalPopulation(pHeights, pHeightsAttenuation, pExposedPeople.map { x => { if (pDevelopedFrac > 0) x else 0f } })
  // maps: elevation to assets below
  var assetsExposureDeveloped: CoastalAssets = new CoastalAssets(pHeights, pHeightsAttenuation, pExposedAssets.map { x => { if (pDevelopedFrac > 0) x else 0f } })

  // maps: elevation to area below
  var areaExposureDevelopable: CoastalArea = new CoastalArea(pHeights, pHeightsAttenuation, pExposedArea.map { x => x * pDevelopableFrac })
  // maps: elevation to people below
  //private
  var populationExposureDevelopable: CoastalPopulation = new CoastalPopulation(pHeights, pHeightsAttenuation, pExposedPeople.map { x => 0f })
  // maps: elevation to assets below
  var assetsExposureDevelopable: CoastalAssets = new CoastalAssets(pHeights, pHeightsAttenuation, pExposedAssets.map { x => 0f })

  // maps: elevation to area below
  //private
  var areaExposureUndeveloped: CoastalArea = new CoastalArea(pHeights, pHeightsAttenuation, pExposedArea.map { x => x * gUndevlopedFrac })
  // maps: elevation to people below
  //private
  var populationExposureUndeveloped: CoastalPopulation = new CoastalPopulation(pHeights, pHeightsAttenuation, pExposedPeople.map { x => 0f })
  // maps: elevation to assets below
  //private
  var assetsExposureUndeveloped: CoastalAssets = new CoastalAssets(pHeights, pHeightsAttenuation, pExposedAssets.map { x => 0f })

  // maps: elevation to area below
  var areaExposure: CoastalArea = new CoastalArea(pHeights, pHeightsAttenuation, pExposedArea)
  // maps: elevation to people below
  var populationExposure: CoastalPopulation = new CoastalPopulation(pHeights, pHeightsAttenuation, pExposedPeople)
  // maps: elevation to assets below
  var assetsExposure: CoastalAssets = new CoastalAssets(pHeights, pHeightsAttenuation, pExposedAssets)

  var noPopulationBelow: Float = 0f
  var populationCorrectionDistributionHeigth: Float = 0f
  var noAssetDevelopmentDevelopedBelow: Float = 0f
  var noAssetDevelopmentDevelopableBelow: Float = 0f
  var setbackHeight: Float = 0f
  var setbackHeightInitial: Float = 0f
  var setbackWidth: Float = 0f
  var setbackReturnPeriod: Float = 0f
  var deprecationRate: Float = 0.0f

  // units: elevation given in m, length given in km, width is computed in km
  var lWidths: Array[Float] = pExposedArea.map(x => (x / pLength).asInstanceOf[Float])
  // maps: width to elevation
  var elevationExposure: CoastalWidth = _
  var elevationExposureAttenuation: CoastalWidth = _

  // current width of max high water strip
  var currentMaxHighWaterWidth: Float = 0f

  // slopes??
  var levelIntervals: Array[(Float, Float)] = _
  var lLevelSteps: Array[Float] = _
  var lSinusAngles: Array[Float] = _
  var lAngles: Array[Float] = _

  recomputeGeometries

  def populationDensityBelow(elevation: Float): Float = DensityBelow(elevation, populationExposure, areaExposure)
  def assetDensityBelow(elevation: Float): Float = DensityBelow(elevation, assetsExposure, areaExposure)

  private def DensityBelow[Exposure](elevation: Float, exposure: CoastalExposureI, pAreaExposure: CoastalArea = areaExposure): Float =
    if (elevation > pHeights.last)
      densityBelowComputation(pHeights.last, exposure, pAreaExposure)
    else
      densityBelowComputation(elevation, exposure, pAreaExposure)

  def populationDensityBetween(elevationLow: Float, elevationHigh: Float, pAreaExposure: CoastalArea): Float =
    if ((pAreaExposure.cumulativeExposure(elevationHigh) - pAreaExposure.cumulativeExposure(elevationLow)) < 0)
      0
    else
      (scala.math.abs(populationExposure.cumulativeExposure(elevationHigh) - populationExposure.cumulativeExposure(elevationLow)) / scala.math.abs((pAreaExposure.cumulativeExposure(elevationHigh) - pAreaExposure.cumulativeExposure(elevationLow)))).asInstanceOf[Float]

  private def densityBelowComputation[Exposure](elevation: Float, pExposure: CoastalExposureI, pAreaExposure: CoastalArea): Float =
    if (pAreaExposure.cumulativeExposure(elevation) == 0)
      0
    else
      (pExposure.cumulativeExposure(elevation) / pAreaExposure.cumulativeExposure(elevation)).asInstanceOf[Float]

  def wetlandCorrection(cz: Cz) {
    // Correct flood areas for wetland areas
    // reducing the area1 (the first elevation increment area) by the total wetland area

    // if cls.area1 is already 0, we have nothing to correct!!
    if (areaExposure.cumulativeExposure(pHeights(1)) > 0) {

      var wetlandAreaToCorrect: Float = cz.cls.data.wetsalt + cz.cls.data.wetfresh + cz.cls.data.wetmang
      if (wetlandAreaToCorrect > 0) {
        pExposedArea(1) = pExposedArea(1) - wetlandAreaToCorrect
        if (pExposedArea(1) < 0) {
          logger.debug("Wetland correction for cz " + cz.locationid + " (" + cz.locationname + "): we corrected all area below " + pHeights(1))
          pExposedArea(1) = 0
        }

        if ((pExposedArea(1) == 0) && (pExposedPeople(1) > 0)) {
          if (pExposedPeople(1) > 30)
            logger.debug("Wetland correction problematic for cls " + cz.locationid + " (" + cz.locationname + "): we corrected all area below 1.5m, but significant population remains (" + pExposedPeople(1) + ")")
          pExposedPeople(1) = 0
          pExposedAssets(1) = 0
        }

        areaExposureDeveloped = new CoastalArea(pHeights, pHeightsAttenuation, pExposedArea.map { x => x * pDevelopedFrac })
        populationExposureDeveloped = new CoastalPopulation(pHeights, pHeightsAttenuation, pExposedPeople.map { x => { if (pDevelopedFrac > 0) x else 0f } })
        assetsExposureDeveloped = new CoastalAssets(pHeights, pHeightsAttenuation, pExposedAssets.map { x => { if (pDevelopedFrac > 0) x else 0f } })

        areaExposureDevelopable = new CoastalArea(pHeights, pHeightsAttenuation, pExposedArea.map { x => x * pDevelopableFrac })
        populationExposureDevelopable = new CoastalPopulation(pHeights, pHeightsAttenuation, pExposedPeople.map { x => 0f })
        assetsExposureDevelopable = new CoastalAssets(pHeights, pHeightsAttenuation, pExposedAssets.map { x => 0f })
        areaExposureUndeveloped = new CoastalArea(pHeights, pHeightsAttenuation, pExposedArea.map { x => x * gUndevlopedFrac })
        populationExposureUndeveloped = new CoastalPopulation(pHeights, pHeightsAttenuation, pExposedPeople.map { x => 0f })
        assetsExposureUndeveloped = new CoastalAssets(pHeights, pHeightsAttenuation, pExposedAssets.map { x => 0f })

        areaExposure = new CoastalArea(pHeights, pHeightsAttenuation, pExposedArea)
        populationExposure = new CoastalPopulation(pHeights, pHeightsAttenuation, pExposedPeople)
        assetsExposure = new CoastalAssets(pHeights, pHeightsAttenuation, pExposedAssets)

        recomputeGeometries
      }
    }
  }

  def populationCorrection(lPopulationCorrectionHeigth: Float, lPopulationCorrectionDistributionHeigth: Float): Float = {
    noPopulationBelow = lPopulationCorrectionHeigth
    populationCorrectionDistributionHeigth = lPopulationCorrectionDistributionHeigth
    if ((gDevelopedFrac > 0) && (populationExposure.cumulativeExposure(lPopulationCorrectionHeigth) > 0)) {
      migrateBelowBetween(pMigriateFromBelowThisElevation = lPopulationCorrectionHeigth, pMigriateToBelowThisElevation = populationCorrectionDistributionHeigth, pAwayRate = 0.0f)._1.asInstanceOf[Float]
    } else {
      0f
    }
  }

  def computeHighWaterWidth(currentMaxHighWater: Float): Float = elevationExposure.cumulativeExposureInverse(currentMaxHighWater).asInstanceOf[Float]

  def computeSetbackWithoutDike(currentMaxHighWater: Float) {
    currentMaxHighWaterWidth = computeHighWaterWidth(currentMaxHighWater)
    setbackHeight = elevationExposure.cumulativeExposure(currentMaxHighWaterWidth + setbackWidth).asInstanceOf[Float]
    setbackHeightInitial = if (setbackHeightInitial == 0f) setbackHeight else setbackHeightInitial
  }

  def computeSetbackInFrontOfDike(currentMaxHighWater: Float, dikeElevation: Float) {
    currentMaxHighWaterWidth = computeHighWaterWidth(currentMaxHighWater)
    setbackHeight = elevationExposure.cumulativeExposure(currentMaxHighWaterWidth + setbackWidth).asInstanceOf[Float]
    if (setbackHeight > dikeElevation)
      setbackHeight = dikeElevation
    val realSetbackHeigth: Float = dikeElevation - currentMaxHighWater
    setbackWidth = if (realSetbackHeigth < 0f) 0f else elevationExposure.cumulativeExposureInverse(realSetbackHeigth).asInstanceOf[Float]
    setbackHeightInitial = if (setbackHeightInitial == 0f) setbackHeight else setbackHeightInitial
    //noPopulationBelow = setbackHeight
  }

  def computeRetreatWithoutDike(pRetreatHeight: Float): (Double, Double) = {
    //noPopulationBelow = pRetreatHeight
    migrateBelowBetween(pMigriateFromBelowThisElevation = pRetreatHeight, pMigriateToBelowThisElevation = 16.0f, pAwayRate = 0.0f)
  }

  def computeRetreatInFrontOfDike(pRetreatHeight: Float, dikeElevation: Float, dikeHeigth: Float): (Double, Double) = {
    var retreatHeight =
      if (pRetreatHeight > (dikeElevation + dikeHeigth))
        pRetreatHeight
      else
        dikeElevation
    //noPopulationBelow = retreatHeight
    migrateBelowBetween(pMigriateFromBelowThisElevation = pRetreatHeight, pMigriateToBelowThisElevation = 16.0f, pAwayRate = 0.0f)
  }

  def analyseSetback(currentMaxHighWater: Float) {
    currentMaxHighWaterWidth = elevationExposure.cumulativeExposureInverse(currentMaxHighWater).asInstanceOf[Float]
    setbackHeight = elevationExposure.cumulativeExposure(currentMaxHighWaterWidth + setbackWidth).asInstanceOf[Float]
    //noPopulationBelow = setbackHeight

    println("length = " + pLength)
    println("heigths")
    elevationExposure.lCumulativeExposure.foreach(x => print(x + "; ")); println;
    //widthExposure.lLevels.foreach(x => print(x + "; ")); println;
    pHeights.foreach(x => print(x + "; ")); println;
    println("widths")
    elevationExposure.lLevels.foreach(x => print(x + "; ")); println;
    //widthExposure.lCumulativeExposure.foreach(x => print(x + "; ")); println;
    lWidths.foreach(x => print(x + "; ")); println;
    println
    println("currentMaxHighWater = " + currentMaxHighWater)
    println("currentMaxHighWaterWidth = " + currentMaxHighWaterWidth)
    println("heigth(currentMaxHighWaterWidth) = " + elevationExposure.cumulativeExposure(currentMaxHighWaterWidth))
    println("setbackWidht = " + setbackWidth)
    //println("heigth(setbackWidth) = " + elevationExposure.cumulativeExposure2(setbackWidth))
    println("setbackWidhtTotal = " + (setbackWidth + currentMaxHighWaterWidth))
    println("setbackHeight = " + setbackHeight)
    println("heigth(10m) = " + elevationExposure.cumulativeExposure(10f))
    println("heigth(20m) = " + elevationExposure.cumulativeExposure(20f))
    println("heigth(30m) = " + elevationExposure.cumulativeExposure(30f))
    println("heigth(40m) = " + elevationExposure.cumulativeExposure(40f))
    println("heigth(50m) = " + elevationExposure.cumulativeExposure(50f))
    println
  }

  def socioEconomicDevelopment(annualPopulationGrowth: Float, annualGDPCGrowth: Float, years: Int): (Float, Float) = {
    if (gUndevlopedFrac == 1.0) {
      (0f, 0f)
    } else {
      // popgrowth = the growthrate for ONE year ... but here we apply for timeStep years
      val populationGrowthFactor: Float = pow(annualPopulationGrowth, years).toFloat
      // assetgrowth = popgrowth * gdpcgrowth
      val assetGrowthFactor: Float = pow(annualPopulationGrowth * annualGDPCGrowth, years).toFloat

      var populationGrowthFactorDeveloped: Float = populationGrowthFactor
      var assetGrowthFactorDeveloped: Float = assetGrowthFactor
      var populationGrowthFactorDevelopable: Float = populationGrowthFactor
      var assetGrowthFactorDevelopable: Float = assetGrowthFactor

      populationExposureDeveloped.rescaleAbove(populationGrowthFactorDeveloped, noAssetDevelopmentDevelopedBelow)
      assetsExposureDeveloped.rescaleAbove(assetGrowthFactorDeveloped, noAssetDevelopmentDevelopedBelow)
      populationExposureDevelopable.rescaleAbove(populationGrowthFactorDevelopable, noAssetDevelopmentDevelopableBelow)
      assetsExposureDevelopable.rescaleAbove(assetGrowthFactorDevelopable, noAssetDevelopmentDevelopableBelow)

      val r = depriciation(years)
      recompute
      r
    }
  }

  def intenseCoastalDevelopment(annualPopulationGrowth: Float, annualGDPCGrowth: Float, targetDevelopableAssetDensity: Float, years: Int, b: Boolean): (Float, Float) = {
    if (gUndevlopedFrac == 1.0) {
      (0f, 0f)
    } else {
      // popgrowth = the growthrate for ONE year ... but here we apply for timeStep years
      val populationGrowthFactor: Float = pow(annualPopulationGrowth, years).toFloat
      // assetgrowth
      val assetGrowthFactor: Float = pow(annualPopulationGrowth * annualGDPCGrowth, years).toFloat

      var populationGrowthFactorDeveloped: Float = populationGrowthFactor
      var assetGrowthFactorDeveloped: Float = assetGrowthFactor
      var populationGrowthFactorDevelopable: Float = populationGrowthFactor
      var assetGrowthFactorDevelopable: Float = assetGrowthFactor

      if ((pDevelopedFrac == 0) && (pDevelopableFrac > 0)) {
        // hier muss setback beachtet werden!

        var assetsDevelopable: Array[Float] = areaExposureDevelopable.lExposure.map { x => x.toFloat * targetDevelopableAssetDensity }
        assetsExposureDevelopable = new CoastalAssets(pHeights, pHeightsAttenuation, assetsDevelopable)
        val peopleDevelopable: Array[Float] = assetsDevelopable.map { x => x / assetsPerCapita }
        populationExposureDevelopable = new CoastalPopulation(pHeights, pHeightsAttenuation, peopleDevelopable)

        if (noAssetDevelopmentDevelopableBelow > 0f) {
          var pHeightsIndex = pHeights.indexWhere(_ > noAssetDevelopmentDevelopableBelow)
          if (pHeightsIndex < 0) pHeightsIndex = (pHeights.length - 1)
          //if (pHeightsIndex==(pHeights.length - 1)) pHeightsIndex = (pHeights.length - 2)
          assetsExposureDevelopable.migrateBelowBetween(noAssetDevelopmentDevelopableBelow, pHeights(pHeightsIndex), pAwayRate = 0.0f)
          populationExposureDevelopable.migrateBelowBetween(noAssetDevelopmentDevelopableBelow, pHeights(pHeightsIndex), pAwayRate = 0.0f)
        } else {
          // migrateBelowBetween(pMigriateFromBelowThisElevation = noPopulationBelow, pMigriateToBelowThisElevation = populationCorrectionDistributionHeigth, pAwayRate = 0.0f)._1.asInstanceOf[Float]
        }

        assetGrowthFactorDevelopable = 1.0f
        populationGrowthFactorDevelopable = 1.0f
      } else if ((pDevelopedFrac > 0) && (pDevelopableFrac > 0)) {

        val assetsDeveloped: Float = assetsExposureDeveloped.cumulativeExposure(elevationLimit).asInstanceOf[Float]
        val assetsDevelopable: Float = assetsExposureDevelopable.cumulativeExposure(elevationLimit).asInstanceOf[Float]
        val assetsTarget: Float = (assetsDeveloped + assetsDevelopable) * assetGrowthFactor
        val additionalAssetsTarget: Float = assetsTarget - (assetsDeveloped + assetsDevelopable)

        val developedAssetDensity: Float = DensityBelow(elevationLimit, assetsExposureDeveloped, areaExposureDeveloped)
        val developableAssetDensity: Float = DensityBelow(elevationLimit, assetsExposureDevelopable, areaExposureDevelopable)

        val assetsDevelopedTarget: Float = scala.math.max(developedAssetDensity, developableAssetDensity) * areaExposureDeveloped.cumulativeExposure(elevationLimit).asInstanceOf[Float]
        val assetsDevelopableTarget: Float = scala.math.max(developedAssetDensity, developableAssetDensity) * areaExposureDevelopable.cumulativeExposure(elevationLimit).asInstanceOf[Float]

        var assetsDevelopableToAdd: Float = scala.math.min(assetsDevelopableTarget - assetsDevelopable, additionalAssetsTarget)
        val remainingAdditionalAssetsTarget = additionalAssetsTarget - assetsDevelopableToAdd
        var assetsDevelopedToAdd: Float = scala.math.min(assetsDevelopedTarget - assetsDeveloped, remainingAdditionalAssetsTarget)

        var additionalAssetsToAdd: Float = scala.math.max(assetsTarget - (assetsDeveloped + assetsDevelopedToAdd + assetsDevelopable + assetsDevelopableToAdd), 0f)
        assetsDevelopedToAdd += ((additionalAssetsToAdd) * (pDevelopedFrac / (pDevelopedFrac + pDevelopableFrac)))
        assetsDevelopableToAdd += ((additionalAssetsToAdd) * (pDevelopableFrac / (pDevelopedFrac + pDevelopableFrac)))

        assetGrowthFactorDeveloped =
          if (assetsDeveloped == 0) {
            val areaDeveloped: Float = areaExposureDeveloped.cumulativeExposure(elevationLimit).toFloat
            if (areaDeveloped > 0d) {
              val areaDevelopedFractions: Array[Float] = areaExposureDeveloped.lExposure.map { x => x.toFloat / areaDeveloped }
              val assetsDevelopedArray: Array[Float] = areaDevelopedFractions.map { x => x.toFloat * assetsDevelopedToAdd.toFloat }
              assetsExposureDeveloped = new CoastalAssets(pHeights, pHeightsAttenuation, assetsDevelopedArray)
              val peopleDevelopedArray: Array[Float] = assetsDevelopedArray.map { x => x / assetsPerCapita }
              populationExposureDeveloped = new CoastalPopulation(pHeights, pHeightsAttenuation, peopleDevelopedArray)
              if (noAssetDevelopmentDevelopedBelow > 0f) {
                var pHeightsIndex = pHeights.indexWhere(_ > noAssetDevelopmentDevelopedBelow)
                if (pHeightsIndex < 0) pHeightsIndex = (pHeights.length - 1)
                //if (pHeightsIndex==(pHeights.length - 1)) pHeightsIndex = (pHeights.length - 2)
                assetsExposureDeveloped.migrateBelowBetween(noAssetDevelopmentDevelopedBelow, pHeights(pHeightsIndex), pAwayRate = 0.0f)
                populationExposureDeveloped.migrateBelowBetween(noAssetDevelopmentDevelopedBelow, pHeights(pHeightsIndex), pAwayRate = 0.0f)
              } else {
                // migrateBelowBetween(pMigriateFromBelowThisElevation = noPopulationBelow, pMigriateToBelowThisElevation = populationCorrectionDistributionHeigth, pAwayRate = 0.0f)._1.asInstanceOf[Float]
              }
            }
            1.0f
          } else (assetsDeveloped + assetsDevelopedToAdd).toFloat / assetsDeveloped.toFloat

        assetGrowthFactorDevelopable =
          if (assetsDevelopable == 0) {
            val areaDevelopable: Float = areaExposureDevelopable.cumulativeExposure(elevationLimit).asInstanceOf[Float]
            if (areaDevelopable > 0f) {
              val areaDevelopableFractions: Array[Float] = areaExposureDevelopable.lExposure.map { x => x.toFloat / areaDevelopable }
              val assetsDevelopableArray: Array[Float] = areaDevelopableFractions.map { x => x.toFloat * assetsDevelopableToAdd }
              assetsExposureDevelopable = new CoastalAssets(pHeights, pHeightsAttenuation, assetsDevelopableArray)
              val peopleDevelopableArray: Array[Float] = assetsDevelopableArray.map { x => x / assetsPerCapita }
              populationExposureDevelopable = new CoastalPopulation(pHeights, pHeightsAttenuation, peopleDevelopableArray)
              if (noAssetDevelopmentDevelopableBelow > 0f) {
                var pHeightsIndex = pHeights.indexWhere(_ > noAssetDevelopmentDevelopableBelow)
                if (pHeightsIndex < 0) pHeightsIndex = (pHeights.length - 1)
                //if (pHeightsIndex==(pHeights.length - 1)) pHeightsIndex = (pHeights.length - 2)
                assetsExposureDevelopable.migrateBelowBetween(noAssetDevelopmentDevelopableBelow, pHeights(pHeightsIndex), pAwayRate = 0.0f)
                populationExposureDevelopable.migrateBelowBetween(noAssetDevelopmentDevelopableBelow, pHeights(pHeightsIndex), pAwayRate = 0.0f)
              } else {
                // migrateBelowBetween(pMigriateFromBelowThisElevation = noPopulationBelow, pMigriateToBelowThisElevation = populationCorrectionDistributionHeigth, pAwayRate = 0.0f)._1.asInstanceOf[Float]
              }
            }
            1.0f
          } else (assetsDevelopable + assetsDevelopableToAdd).toFloat / assetsDevelopable.toFloat

      } else /* if ((pDevlopedFrac>0) && (pDevlopableFrac==0)) */ {
        // do nothing - just apply growth factors for Population and Assets
      }

      populationExposureDeveloped.rescaleAbove(populationGrowthFactorDeveloped, noAssetDevelopmentDevelopedBelow)
      assetsExposureDeveloped.rescaleAbove(assetGrowthFactorDeveloped, noAssetDevelopmentDevelopedBelow)
      populationExposureDevelopable.rescaleAbove(populationGrowthFactorDevelopable, noAssetDevelopmentDevelopableBelow)
      assetsExposureDevelopable.rescaleAbove(assetGrowthFactorDevelopable, noAssetDevelopmentDevelopableBelow)
    }

    val r = depriciation(years)
    recompute
    r
  }

  private def depriciation(years: Int): (Float, Float) = {
    if (deprecationRate != 0) {
      val timeStepDeprecationFactor: Float = pow((1.0 - (deprecationRate * 0.01)), years).toFloat

      val popBefore: Float = populationExposure.cumulativeExposure(20.5f).asInstanceOf[Float]
      val assetsBefore: Float = assetsExposure.cumulativeExposure(20.5f).asInstanceOf[Float]

      populationExposureDeveloped.rescaleBelow(timeStepDeprecationFactor, noAssetDevelopmentDevelopedBelow)
      assetsExposureDeveloped.rescaleBelow(timeStepDeprecationFactor, noAssetDevelopmentDevelopedBelow)
      populationExposureDevelopable.rescaleBelow(timeStepDeprecationFactor, noAssetDevelopmentDevelopableBelow)
      assetsExposureDevelopable.rescaleBelow(timeStepDeprecationFactor, noAssetDevelopmentDevelopableBelow)
      recompute

      val popAfter: Float = populationExposure.cumulativeExposure(20.5f).asInstanceOf[Float]
      val assetsAfter: Float = assetsExposure.cumulativeExposure(20.5f).asInstanceOf[Float]

      val populationDeprecation: Float = popBefore - popAfter
      val assetsDeprecation: Float = assetsBefore - assetsAfter
      (populationDeprecation, assetsDeprecation)
    } else {
      (0, 0)
    }
  }

  def migrateBelow(pBelowThisElevation: Float, pAwayRate: Float): (Double, Double) = {
    val migratedAssets: Double = assetsExposureDeveloped.migrateBelow(pBelowThisElevation, pAwayRate) + assetsExposureDevelopable.migrateBelow(pBelowThisElevation, pAwayRate)
    val migratedPopulation: Double = populationExposureDeveloped.migrateBelow(pBelowThisElevation, pAwayRate) + populationExposureDevelopable.migrateBelow(pBelowThisElevation, pAwayRate)
    recompute
    (migratedPopulation, migratedAssets)
  }

  //  ACHTUING : Ist falsch!! muss für die einzelteile neu geschrieben werden.
  def migrateBackBelow(pBelowThisElevation: Float, pNumberPeople: Float, pAssets: Float) {
    assetsExposure.migrateBackBelow(pBelowThisElevation, pAssets)
    populationExposure.migrateBackBelow(pBelowThisElevation, pNumberPeople)
  }

  def migrateBelowBetween(pMigriateFromBelowThisElevation: Float, pMigriateToBelowThisElevation: Float, pAwayRate: Float): (Double, Double) = {
    areaExposureDeveloped.migrateBelowBetween(pMigriateFromBelowThisElevation, pMigriateToBelowThisElevation, pAwayRate)
    areaExposureDevelopable.migrateBelowBetween(pMigriateFromBelowThisElevation, pMigriateToBelowThisElevation, pAwayRate)
    areaExposureUndeveloped.migrateBelowBetween(pMigriateFromBelowThisElevation, pMigriateToBelowThisElevation, pAwayRate)

    val migratedAssets: Double =
      assetsExposureDeveloped.migrateBelowBetween(pMigriateFromBelowThisElevation, pMigriateToBelowThisElevation, pAwayRate) +
        assetsExposureDevelopable.migrateBelowBetween(pMigriateFromBelowThisElevation, pMigriateToBelowThisElevation, pAwayRate)
    assetsExposureUndeveloped.migrateBelowBetween(pMigriateFromBelowThisElevation, pMigriateToBelowThisElevation, pAwayRate)

    val migratedPopulation: Double =
      populationExposureDeveloped.migrateBelowBetween(pMigriateFromBelowThisElevation, pMigriateToBelowThisElevation, pAwayRate) +
        populationExposureDevelopable.migrateBelowBetween(pMigriateFromBelowThisElevation, pMigriateToBelowThisElevation, pAwayRate)
    populationExposureUndeveloped.migrateBelowBetween(pMigriateFromBelowThisElevation, pMigriateToBelowThisElevation, pAwayRate)

    recompute
    (migratedPopulation, migratedAssets)
  }

  def getSetbackzoneArea(alsoDeconstruction: Boolean): Float =
    if (alsoDeconstruction) (areaExposureDeveloped.cumulativeExposure(setbackHeight) + areaExposureDevelopable.cumulativeExposure(setbackHeight)).asInstanceOf[Float] else areaExposureDevelopable.cumulativeExposure(setbackHeight).asInstanceOf[Float]

  def getSetbackzonePopulation(alsoDeconstruction: Boolean): Double =
    if (alsoDeconstruction) (populationExposureDeveloped.cumulativeExposure(setbackHeight) + populationExposureDevelopable.cumulativeExposure(setbackHeight)) else populationExposureDevelopable.cumulativeExposure(setbackHeight)

  def getSetbackzoneAssets(alsoDeconstruction: Boolean): Double =
    if (alsoDeconstruction) (assetsExposureDeveloped.cumulativeExposure(setbackHeight) + assetsExposureDevelopable.cumulativeExposure(setbackHeight)) else assetsExposureDevelopable.cumulativeExposure(setbackHeight)

  def add(cpm: CoastalPlaneModel) {
    areaExposureDeveloped.add(cpm.areaExposureDeveloped)
    populationExposureDeveloped.add(cpm.populationExposureDeveloped)
    assetsExposureDeveloped.add(cpm.assetsExposureDeveloped)

    areaExposureDevelopable.add(cpm.areaExposureDevelopable)
    populationExposureDevelopable.add(cpm.populationExposureDevelopable)
    assetsExposureDevelopable.add(cpm.assetsExposureDevelopable)

    areaExposureUndeveloped.add(cpm.areaExposureUndeveloped)
    populationExposureUndeveloped.add(cpm.populationExposureUndeveloped)
    assetsExposureUndeveloped.add(cpm.assetsExposureUndeveloped)

    recompute
  }

  def recompute {
    areaExposure = new CoastalArea(areaExposureDeveloped.lLevels, areaExposureDeveloped.lLevelsAttenuation, areaExposureDeveloped.lExposure.map(x => 0f))
    populationExposure = new CoastalPopulation(populationExposureDeveloped.lLevels, populationExposureDeveloped.lLevelsAttenuation, populationExposureDeveloped.lExposure.map(x => 0f))
    assetsExposure = new CoastalAssets(assetsExposureDeveloped.lLevels, assetsExposureDeveloped.lLevelsAttenuation, assetsExposureDeveloped.lExposure.map(x => 0f))

    areaExposure.add(areaExposureDeveloped)
    areaExposure.add(areaExposureDevelopable)
    areaExposure.add(areaExposureUndeveloped)
    populationExposure.add(populationExposureDeveloped)
    populationExposure.add(populationExposureDevelopable)
    assetsExposure.add(assetsExposureDeveloped)
    assetsExposure.add(assetsExposureDevelopable)
  }

  def recomputeGeometries {
    lWidths = areaExposure.lExposure.map(x => (x / pLength).asInstanceOf[Float])

    elevationExposure = new CoastalWidth(lWidths, areaExposure.lLevels.map(x => x.asInstanceOf[Float]))
    levelIntervals = areaExposure.lLevels.sliding(2).toArray.map(x => (x(0).asInstanceOf[Float], x(1).asInstanceOf[Float]))

    lLevelSteps = 0f +: levelIntervals.map(x => (x._2 - x._1))
    lSinusAngles = (lWidths zip lLevelSteps).map { (x: (Float, Float)) => if (x._1 == 0f) 0f else (x._2 / x._1).toFloat }
    lAngles = lSinusAngles.map(x => toDegrees(asin(x)).toFloat)
  }

  def attenuate(waterlevelAttenuationMap: Map[Int, Float]) {
    if (areaExposure.lLevels.size != areaExposureDeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != areaExposureDeveloped.lLevels.size(" + areaExposureDeveloped.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != areaExposureDevelopable.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != areaExposureDevelopable.lLevels.size(" + areaExposureDevelopable.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != areaExposureUndeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != areaExposureUndeveloped.lLevels.size(" + areaExposureUndeveloped.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != populationExposureDeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != populationExposureDeveloped.lLevels.size(" + populationExposureDeveloped.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != populationExposureDevelopable.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != populationExposureDevelopable.lLevels.size(" + populationExposureDevelopable.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != populationExposureUndeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != populationExposureUndeveloped.lLevels.size(" + populationExposureUndeveloped.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != assetsExposureDeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != assetsExposureDeveloped.lLevels.size(" + assetsExposureDeveloped.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != assetsExposureDevelopable.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != assetsExposureDevelopable.lLevels.size(" + assetsExposureDevelopable.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != assetsExposureUndeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != assetsExposureUndeveloped.lLevels.size(" + assetsExposureUndeveloped.lLevels.size + ")")
    }

    val pLanduseNew = addMissingLanduse(areaExposure.lLevels.toList, pHeights.toList, pLanduse.toList)

    lWidths = areaExposure.lExposure.map(x => (x / pLength).asInstanceOf[Float])
    pHeightsAttenuation = areaExposure.lLevels.map { x => x.toFloat }
    val lHorizontalWidths: Array[Float] = lWidths.clone()
    for (i <- 0 until lHorizontalWidths.length) {
      lHorizontalWidths(i) = if (lHorizontalWidths(i) >= pHeightsAttenuation(i) / 1000) {
        scala.math.sqrt((lHorizontalWidths(i) * lHorizontalWidths(i)) - (pHeightsAttenuation(i) / 1000 * pHeightsAttenuation(i) / 1000)).toFloat
      } else {
        lHorizontalWidths(i)
      }
      if (lHorizontalWidths(i).isNaN()) {
        println(i + "  " + lWidths(i) + " - " + pHeightsAttenuation(i)  + " :   " + ((lHorizontalWidths(i) * lHorizontalWidths(i)) - (pHeightsAttenuation(i) / 1000 * pHeightsAttenuation(i) / 1000)))
        
      }
    }

    var attenuation: Float = 0f

    for (i <- 0 until pLanduseNew.length) {
      waterlevelAttenuationMap.get(pLanduseNew(i)) match {
        case Some(x) => {
          attenuation = attenuation + (x * lHorizontalWidths(i))
          pHeightsAttenuation(i) = pHeightsAttenuation(i) + attenuation
        }
        case None => /* nothing */
      }
    }

    areaExposure = new CoastalArea(areaExposureDeveloped.lLevels, pHeightsAttenuation, areaExposure.lLevels.map(x => 0f))
    areaExposureDeveloped = new CoastalArea(areaExposureDeveloped.lLevels, pHeightsAttenuation, areaExposureDeveloped.lExposure.map(x => x * pDevelopedFrac))
    areaExposureDevelopable = new CoastalArea(areaExposureDevelopable.lLevels, pHeightsAttenuation, areaExposureDevelopable.lExposure.map(x => x * pDevelopableFrac))
    areaExposureUndeveloped = new CoastalArea(areaExposureUndeveloped.lLevels, pHeightsAttenuation, areaExposureUndeveloped.lExposure.map(x => x * gUndevlopedFrac))

    populationExposure = new CoastalPopulation(populationExposureDeveloped.lLevels, pHeightsAttenuation, populationExposureDeveloped.lLevels.map(x => 0f))
    assetsExposure = new CoastalAssets(assetsExposureDeveloped.lLevels, pHeightsAttenuation, assetsExposureDeveloped.lLevels.map(x => 0f))

    populationExposureDeveloped = new CoastalPopulation(populationExposureDeveloped.lLevels, pHeightsAttenuation, populationExposureDeveloped.lExposure.map(x => { if (pDevelopedFrac > 0) x else 0f }))
    assetsExposureDeveloped = new CoastalAssets(assetsExposureDeveloped.lLevels, pHeightsAttenuation, assetsExposureDeveloped.lExposure.map(x => { if (pDevelopedFrac > 0) x else 0f }))

    populationExposureDevelopable = new CoastalPopulation(populationExposureDevelopable.lLevels, pHeightsAttenuation, populationExposureDevelopable.lExposure.map(x => { if (pDevelopedFrac > 0) x else 0f }))
    assetsExposureDevelopable = new CoastalAssets(assetsExposureDevelopable.lLevels, pHeightsAttenuation, assetsExposureDevelopable.lExposure.map(x => { if (pDevelopedFrac > 0) x else 0f }))

    populationExposureUndeveloped = new CoastalPopulation(populationExposureUndeveloped.lLevels, pHeightsAttenuation, populationExposureUndeveloped.lLevels.map(x => 0f))
    assetsExposureUndeveloped = new CoastalAssets(assetsExposureUndeveloped.lLevels, pHeightsAttenuation, assetsExposureUndeveloped.lLevels.map(x => 0f))

    areaExposure.add(areaExposureDeveloped)
    areaExposure.add(areaExposureDevelopable)
    areaExposure.add(areaExposureUndeveloped)
    populationExposure.add(populationExposureDeveloped)
    assetsExposure.add(assetsExposureDeveloped)

    elevationExposureAttenuation = new CoastalWidth(lWidths, areaExposure.lLevelsAttenuation.map(x => x.asInstanceOf[Float]))

  }

  private def addMissingLanduse(levels: List[Float], landuselevels: List[Float], landuse: List[Int]): List[Int] = {

    if (landuse.size == 0) {
      landuse
    } else {

      val missingLanduseLevels: Set[Float] = levels.toSet.filterNot(landuselevels.toSet)
      val missingLevels: Set[Float] = landuselevels.toSet.filterNot(levels.toSet)

      var landuse_new: Array[Int] = Array.empty[Int]
      var recent_landuse: Int = landuse(landuse.size - 1)
      var recent_landuse_index: Int = landuse.size - 1

      for (i <- (0 until levels.size).reverse) {
        if (missingLanduseLevels.contains(levels(i))) {
          landuse_new :+= recent_landuse
        } else {
          if ((i < (levels.size - 1)) && (i > 0)) {
            recent_landuse_index = recent_landuse_index - 1
            recent_landuse = landuse(recent_landuse_index)
          }
          landuse_new :+= recent_landuse
        }
      }
      landuse_new.reverse.toList
    }
  }

  def attenuateDebug(waterlevelAttenuationMap: Map[Int, Float]) {
    println("all levels:")
    println(areaExposure.lLevels.toList)
    println(populationExposure.lLevels.toList)
    println(assetsExposure.lLevels.toList)
    println(areaExposureDeveloped.lLevels.toList)
    println(populationExposureDeveloped.lLevels.toList)
    println(assetsExposureDeveloped.lLevels.toList)
    println(areaExposureDevelopable.lLevels.toList)
    println(populationExposureDevelopable.lLevels.toList)
    println(assetsExposureDevelopable.lLevels.toList)

    println("all Attenuation levels:")
    println(areaExposure.lLevelsAttenuation.toList)
    println(populationExposure.lLevelsAttenuation.toList)
    println(assetsExposure.lLevelsAttenuation.toList)
    println(areaExposureDeveloped.lLevelsAttenuation.toList)
    println(populationExposureDeveloped.lLevelsAttenuation.toList)
    println(assetsExposureDeveloped.lLevelsAttenuation.toList)
    println(areaExposureDevelopable.lLevelsAttenuation.toList)
    println(populationExposureDevelopable.lLevelsAttenuation.toList)
    println(assetsExposureDevelopable.lLevelsAttenuation.toList)

    println("all exposures:")
    println(areaExposure.lExposure.length + " - " + areaExposure.lExposure.toList)
    println(populationExposure.lExposure.length + " - " + populationExposure.lExposure.toList)
    println(assetsExposure.lExposure.length + " - " + assetsExposure.lExposure.toList)
    println(areaExposureDeveloped.lExposure.length + " - " + areaExposureDeveloped.lExposure.toList)
    println(populationExposureDeveloped.lExposure.length + " - " + populationExposureDeveloped.lExposure.toList)
    println(assetsExposureDeveloped.lExposure.length + " - " + assetsExposureDeveloped.lExposure.toList)
    println(areaExposureDevelopable.lExposure.length + " - " + areaExposureDevelopable.lExposure.toList)
    println(populationExposureDevelopable.lExposure.length + " - " + populationExposureDevelopable.lExposure.toList)
    println(assetsExposureDevelopable.lExposure.length + " - " + assetsExposureDevelopable.lExposure.toList)

    if (areaExposure.lLevels.size != areaExposureDeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != areaExposureDeveloped.lLevels.size(" + areaExposureDeveloped.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != areaExposureDevelopable.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != areaExposureDevelopable.lLevels.size(" + areaExposureDevelopable.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != areaExposureUndeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != areaExposureUndeveloped.lLevels.size(" + areaExposureUndeveloped.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != populationExposureDeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != populationExposureDeveloped.lLevels.size(" + populationExposureDeveloped.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != populationExposureDevelopable.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != populationExposureDevelopable.lLevels.size(" + populationExposureDevelopable.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != populationExposureUndeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != populationExposureUndeveloped.lLevels.size(" + populationExposureUndeveloped.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != assetsExposureDeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != assetsExposureDeveloped.lLevels.size(" + assetsExposureDeveloped.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != assetsExposureDevelopable.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != assetsExposureDevelopable.lLevels.size(" + assetsExposureDevelopable.lLevels.size + ")")
    }
    if (areaExposure.lLevels.size != assetsExposureUndeveloped.lLevels.size) {
      logger.error("attenuate: areaExposure.lLevels.size(" + areaExposure.lLevels.size + ") != assetsExposureUndeveloped.lLevels.size(" + assetsExposureUndeveloped.lLevels.size + ")")
    }

    lWidths = pExposedArea.map(x => (x / pLength).asInstanceOf[Float])
    pHeightsAttenuation = pHeights.clone
    println(" pLength: " + pLength)
    println(" pHeights: " + pHeights.toList)
    println(" pHeightsAttenuation: " + pHeightsAttenuation.toList)
    println(" pLanduse: " + pLanduse.toList)
    println(" lWidths: " + lWidths.toList)
    println(" pExposedArea: " + pExposedArea.toList)

    var attenuation: Float = 0f
    for (i <- 0 until pLanduse.length) {
      //println(i + "; " + pLanduse(i) + "; " + waterlevelAttenuationMap.get(pLanduse(i)))

      waterlevelAttenuationMap.get(pLanduse(i)) match {
        case Some(x) => {
          attenuation = attenuation + (x * lWidths(i))
          pHeightsAttenuation(i) = pHeightsAttenuation(i) + attenuation
        }
        case None => /* nothing */
      }
    }
    println(" pHeightsAttenuation: " + pHeightsAttenuation.toList)

    areaExposure = new CoastalArea(areaExposureDeveloped.lLevels, areaExposureDeveloped.lLevelsAttenuation, areaExposure.lLevels.map(x => 0f))
    areaExposureDeveloped = new CoastalArea(areaExposureDeveloped.lLevels, areaExposureDeveloped.lLevelsAttenuation, areaExposure.lExposure.map(x => x * pDevelopedFrac))
    areaExposureDevelopable = new CoastalArea(areaExposureDevelopable.lLevels, areaExposureDevelopable.lLevelsAttenuation, areaExposure.lExposure.map(x => x * pDevelopableFrac))
    areaExposureUndeveloped = new CoastalArea(areaExposureUndeveloped.lLevels, areaExposureUndeveloped.lLevelsAttenuation, areaExposure.lExposure.map(x => x * gUndevlopedFrac))

    populationExposure = new CoastalPopulation(populationExposureDeveloped.lLevels, populationExposureDeveloped.lLevelsAttenuation, populationExposureDeveloped.lLevels.map(x => 0f))
    assetsExposure = new CoastalAssets(assetsExposureDeveloped.lLevels, assetsExposureDeveloped.lLevelsAttenuation, assetsExposureDeveloped.lLevels.map(x => 0f))

    populationExposureDeveloped = new CoastalPopulation(populationExposureDeveloped.lLevels, populationExposureDeveloped.lLevelsAttenuation, populationExposureDeveloped.lExposure.map(x => { if (pDevelopedFrac > 0) x else 0f }))
    assetsExposureDeveloped = new CoastalAssets(assetsExposureDeveloped.lLevels, assetsExposureDeveloped.lLevelsAttenuation, assetsExposureDeveloped.lExposure.map(x => { if (pDevelopedFrac > 0) x else 0f }))

    populationExposureDevelopable = new CoastalPopulation(populationExposureDevelopable.lLevels, populationExposureDevelopable.lLevelsAttenuation, populationExposureDevelopable.lExposure.map(x => { if (pDevelopedFrac > 0) x else 0f }))
    assetsExposureDevelopable = new CoastalAssets(assetsExposureDevelopable.lLevels, assetsExposureDevelopable.lLevelsAttenuation, assetsExposureDevelopable.lExposure.map(x => { if (pDevelopedFrac > 0) x else 0f }))

    populationExposureUndeveloped = new CoastalPopulation(populationExposureUndeveloped.lLevels, populationExposureUndeveloped.lLevelsAttenuation, populationExposureUndeveloped.lLevels.map(x => 0f))
    assetsExposureUndeveloped = new CoastalAssets(assetsExposureUndeveloped.lLevels, assetsExposureUndeveloped.lLevelsAttenuation, assetsExposureUndeveloped.lLevels.map(x => 0f))

    areaExposure.add(areaExposureDeveloped)
    areaExposure.add(areaExposureDevelopable)
    areaExposure.add(areaExposureUndeveloped)
    populationExposure.add(populationExposureDeveloped)
    assetsExposure.add(assetsExposureDeveloped)

  }

  def reset {
    areaExposure = new CoastalArea(areaExposureDeveloped.lLevels, areaExposureDeveloped.lLevelsAttenuation, areaExposure.lLevels.map(x => 0f))
    populationExposure = new CoastalPopulation(populationExposureDeveloped.lLevels, populationExposureDeveloped.lLevelsAttenuation, populationExposureDeveloped.lLevels.map(x => 0f))
    assetsExposure = new CoastalAssets(assetsExposureDeveloped.lLevels, assetsExposureDeveloped.lLevelsAttenuation, assetsExposureDeveloped.lLevels.map(x => 0f))

    areaExposureDeveloped = new CoastalArea(areaExposureDeveloped.lLevels, areaExposureDeveloped.lLevelsAttenuation, areaExposureDeveloped.lLevels.map(x => 0f))
    populationExposureDeveloped = new CoastalPopulation(populationExposureDeveloped.lLevels, populationExposureDeveloped.lLevelsAttenuation, populationExposureDeveloped.lLevels.map(x => 0f))
    assetsExposureDeveloped = new CoastalAssets(assetsExposureDeveloped.lLevels, assetsExposureDeveloped.lLevelsAttenuation, assetsExposureDeveloped.lLevels.map(x => 0f))

    areaExposureDevelopable = new CoastalArea(areaExposureDevelopable.lLevels, areaExposureDevelopable.lLevelsAttenuation, areaExposureDevelopable.lLevels.map(x => 0f))
    populationExposureDevelopable = new CoastalPopulation(populationExposureDevelopable.lLevels, populationExposureDevelopable.lLevelsAttenuation, populationExposureDevelopable.lLevels.map(x => 0f))
    assetsExposureDevelopable = new CoastalAssets(assetsExposureDevelopable.lLevels, assetsExposureDevelopable.lLevelsAttenuation, assetsExposureDevelopable.lLevels.map(x => 0f))

    areaExposureUndeveloped = new CoastalArea(areaExposureUndeveloped.lLevels, areaExposureUndeveloped.lLevelsAttenuation, areaExposureUndeveloped.lLevels.map(x => 0f))
    populationExposureUndeveloped = new CoastalPopulation(populationExposureUndeveloped.lLevels, populationExposureUndeveloped.lLevelsAttenuation, populationExposureUndeveloped.lLevels.map(x => 0f))
    assetsExposureUndeveloped = new CoastalAssets(assetsExposureUndeveloped.lLevels, assetsExposureUndeveloped.lLevelsAttenuation, assetsExposureUndeveloped.lLevels.map(x => 0f))
  }

  override def clone: CoastalPlaneModel = {
    val ret: CoastalPlaneModel = new CoastalPlaneModel(pLength, pHeights, pExposedArea, pExposedPeople, pExposedAssets, pLanduse)
    ret.dikeHeigth = dikeHeigth
    ret.dikeBase = dikeBase

    //ret.noPopulationBelow = noPopulationBelow
    ret.setbackHeight = setbackHeight
    ret.setbackWidth = setbackWidth
    ret.setbackReturnPeriod = setbackReturnPeriod
    ret.deprecationRate = deprecationRate

    // units: elevation given in m, length given in km, width is computed in km
    ret.lWidths = lWidths.clone
    ret.lLevelSteps = lLevelSteps.clone
    ret.lSinusAngles = lSinusAngles
    ret.lAngles = lAngles
    ret.currentMaxHighWaterWidth = currentMaxHighWaterWidth

    ret.areaExposure = areaExposure.clone
    ret.populationExposure = populationExposure.clone
    ret.assetsExposure = assetsExposure.clone

    ret.areaExposureDeveloped = areaExposureDeveloped.clone
    ret.populationExposureDeveloped = populationExposureDeveloped.clone
    ret.assetsExposureDeveloped = assetsExposureDeveloped.clone
    ret.areaExposureDevelopable = areaExposureDevelopable.clone
    ret.populationExposureDevelopable = populationExposureDevelopable.clone
    ret.assetsExposureDevelopable = assetsExposureDevelopable.clone
    ret.areaExposureUndeveloped = areaExposureUndeveloped.clone
    ret.populationExposureUndeveloped = populationExposureUndeveloped.clone
    ret.assetsExposureUndeveloped = assetsExposureUndeveloped.clone

    ret.elevationExposure = elevationExposure.clone

    ret
  }

  def printForDebug {
    println("pDevelopedFrac: " + pDevelopedFrac)
    println("pDevelopableFrac: " + pDevelopableFrac)

    println("Area:")
    areaExposure.printForDebug
    println("Population:")
    populationExposure.printForDebug
    println("Assets:")
    assetsExposure.printForDebug
    println()

    println("AreaDeveloped:")
    areaExposureDeveloped.printForDebug
    println("PopulationDeveloped:")
    populationExposureDeveloped.printForDebug
    println("AssetsDeveloped:")
    assetsExposureDeveloped.printForDebug
    println()

    println("AreaDevelopable:")
    areaExposureDevelopable.printForDebug
    println("PopulationDevelopable:")
    populationExposureDevelopable.printForDebug
    println("AssetsDevelopable:")
    assetsExposureDevelopable.printForDebug
    println()

    println("AreaUndeveloped:")
    areaExposureUndeveloped.printForDebug
    println("PopulationUndeveloped:")
    populationExposureUndeveloped.printForDebug
    println("AssetsUndeveloped:")
    assetsExposureUndeveloped.printForDebug
    println()
  }

}