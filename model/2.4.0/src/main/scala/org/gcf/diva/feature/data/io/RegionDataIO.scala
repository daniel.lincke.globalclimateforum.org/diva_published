/* Automatically generated data IO for Region
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data.io

import diva.io._
import diva.types.InterpolationMode.InterpolationMode
import diva.feature.data.RegionData

class RegionDataIO extends FeatureDataIO[RegionData] {

  def writeData(out: divaOutput, d: RegionData) {
    d.area_below_.keys.toList.sorted.foreach { x => out.write(d.area_below_(x)) }
    d.area_below_h.keys.toList.sorted.foreach { x => out.write(d.area_below_h(x)) }
    d.assets_below_.keys.toList.sorted.foreach { x => out.write(d.assets_below_(x)) }
    d.assets_below_h.keys.toList.sorted.foreach { x => out.write(d.assets_below_h(x)) }
    d.assets_developable_below_.keys.toList.sorted.foreach { x => out.write(d.assets_developable_below_(x)) }
    d.assets_developed_below_.keys.toList.sorted.foreach { x => out.write(d.assets_developed_below_(x)) }
    out.write(d.beachnourishment_cost)
    out.write(d.beachnourishment_length_beach)
    out.write(d.beachnourishment_length_shore)
    out.write(d.beachnourishment_volume)
    out.write(d.coastlength)
    d.h.keys.toList.sorted.foreach { x => out.write(d.h(x)) }
    out.write(d.landloss_erosion_total)
    out.write(d.landloss_submergence)
    out.write(d.landlosscost_erosion_total)
    out.write(d.landlosscost_submergence)
    out.write(d.length_protected)
    out.write(d.migration_erosion)
    out.write(d.migration_retreat)
    out.write(d.migration_submergence)
    out.write(d.migrationcost_erosion)
    out.write(d.migrationcost_retreat)
    out.write(d.migrationcost_submergence)
    out.write(d.par)
    d.pop_below_.keys.toList.sorted.foreach { x => out.write(d.pop_below_(x)) }
    d.pop_below_h.keys.toList.sorted.foreach { x => out.write(d.pop_below_h(x)) }
    out.write(d.population_correction)
    out.write(d.protlevel_implemented)
    out.write(d.protlevel_implemented_before_dikeraise)
    out.write(d.riverdike_cost)
    out.write(d.riverdike_increase_maintenance_cost)
    out.write(d.riverdike_length)
    out.write(d.riverdike_maintenance_cost)
    out.write(d.rslr)
    out.write(d.salinity_cost)
    out.write(d.sandloss_erosion_direct)
    out.write(d.sandloss_erosion_including_nourishment)
    out.write(d.sandloss_erosion_indirect)
    out.write(d.sandloss_erosion_total)
    out.write(d.seadike_cost)
    out.write(d.seadike_height)
    out.write(d.seadike_height_initial)
    out.write(d.seadike_increase_maintenance_cost)
    out.write(d.seadike_maintenance_cost)
    out.write(d.seafloodcost)
    d.seafloodcost_h.keys.toList.sorted.foreach { x => out.write(d.seafloodcost_h(x)) }
    out.write(d.seafloodcost_relative)
    out.write(d.slr_climate_induced)
    out.write(d.slr_nonclimate_induced)
    out.write(d.surge_barrier_cost)
    out.write(d.surge_barrier_maintenance_cost)
    out.write(d.surge_barriers_number)
    out.write(d.wetland_area_freshmarshes)
    out.write(d.wetland_area_mangroves)
    out.write(d.wetland_area_saltmarshes)
    out.write(d.wetland_area_total)
    out.write(d.wetland_nourishment_cost)
    out.write(d.wetland_value)
  }

  def writeSelectedData(out: divaOutput, d: RegionData, ignoreThisOutput: Array[Boolean]) {
    var c: Int = 0
    d.area_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.area_below_(x))}; c+=1 } } 
    d.area_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.area_below_h(x))}; c+=1 } } 
    d.assets_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_below_(x))}; c+=1 } } 
    d.assets_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_below_h(x))}; c+=1 } } 
    d.assets_developable_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_developable_below_(x))}; c+=1 } } 
    d.assets_developed_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_developed_below_(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_length_beach)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_length_shore)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_volume)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.coastlength)}; c+=1
    d.h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.landloss_erosion_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landloss_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landlosscost_erosion_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landlosscost_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.length_protected)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_erosion)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_retreat)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migrationcost_erosion)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migrationcost_retreat)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migrationcost_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.par)}; c+=1
    d.pop_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.pop_below_(x))}; c+=1 } } 
    d.pop_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.pop_below_h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.population_correction)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.protlevel_implemented)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.protlevel_implemented_before_dikeraise)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_increase_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_length)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.rslr)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.salinity_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_direct)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_including_nourishment)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_indirect)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_height)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_height_initial)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_increase_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost)}; c+=1
    d.seafloodcost_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_relative)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.slr_climate_induced)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.slr_nonclimate_induced)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barriers_number)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_area_freshmarshes)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_area_mangroves)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_area_saltmarshes)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_area_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_nourishment_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_value)}; c+=1
  }

  def readInputValue(in: divaInput, d:RegionData, parName: String, locid: String, time: Int) {
  }

  def readOptionInputValue(in: divaInput, d:RegionData, parName: String, locid: String, time: Int) {
  }

  def readScenarioValue(in: divaInput, d:RegionData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

  def readOptionScenarioValue(in: divaInput, d:RegionData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

}
