package diva.module.wetlands

trait WetlandI {

  //type Cubicmeter = Float

  var name: String
  val weight_current: Float
  val weight_last: Float
  var area: Float
  var actual_nourished: Float
  var ess: Option[Float]

  /* Ecological Sensitivity Score (ess) for each Wetland Type.  ESS calculations convert the vulnerability of the total 
   * wetland into forcing values that are sensitive to each wetland type.  ESS incorporates the time lag between a 
   * forcing event and the full impact to the wetland.
   */
  def ecologicalSensitivityScore(csvs_current: Float, csvs_last: Float) = {
    ess = Some((csvs_current * weight_current) + (csvs_last * weight_last))
  }
  
  /* Proportion of Wetland Change.  The proportion of each wetland type loss was calibrated with results from 
   * the ASMITA model and Landscape Modelling Literature.
   */
  def wetlandLossRate: Float

  
  /* Maximum nourishment (volume) required to maintain wetland areas as they are: cumulative losses per wetland type 
   * multiplied relative slr over the timestep divided by time interval of last time step relative slr over last 
   * timestep -- only +ve rslr values should initiate wetland nourishment
   */
  def maxWetlandNourishment(rslr_rate: Float): Float = {
    // area lost (in km^2)
    val wetland_area_lost = wetLandAreaLost
    // if we nourish all lost area with a heigth of 1m, we need area * 1 million cubicmeter of sand
    // so the maximal nourishment is the lost area time one million times slr rate
    val max_wetland_nourishment_volume = wetland_area_lost * rslr_rate * 1000000f
    max_wetland_nourishment_volume
  }

  def wetLandAreaLost: Float = {
    area * wetlandLossRate
  }

  def wetLandAreaNourished(rslr_rate: Float): Float = {
    // volume nourinshed (im m^3)
    val volume_nourished = if (rslr_rate > 0) (actual_nourished / rslr_rate) else 0
    // area nourished in km^2 (= volume_nourished / 1000000)
    val area_nourished = volume_nourished / 1000000f
    area_nourished
  }

  def deepCopy: WetlandI
}
