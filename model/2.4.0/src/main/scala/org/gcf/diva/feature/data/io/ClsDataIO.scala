/* Automatically generated data IO for Cls
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data.io

import diva.io._
import diva.types.InterpolationMode.InterpolationMode
import diva.feature.data.ClsData

class ClsDataIO extends FeatureDataIO[ClsData] {

  def writeData(out: divaOutput, d: ClsData) {
    out.write(d.adminid)
    d.area_below_.keys.toList.sorted.foreach { x => out.write(d.area_below_(x)) }
    d.area_below_h.keys.toList.sorted.foreach { x => out.write(d.area_below_h(x)) }
    d.assets_below_.keys.toList.sorted.foreach { x => out.write(d.assets_below_(x)) }
    d.assets_below_h.keys.toList.sorted.foreach { x => out.write(d.assets_below_h(x)) }
    d.assets_developable_below_.keys.toList.sorted.foreach { x => out.write(d.assets_developable_below_(x)) }
    d.assets_developed_below_.keys.toList.sorted.foreach { x => out.write(d.assets_developed_below_(x)) }
    out.write(d.basinid)
    out.write(d.beachnourishment_cost)
    out.write(d.beachnourishment_length_beach)
    out.write(d.beachnourishment_length_shore)
    out.write(d.beachnourishment_reason)
    out.write(d.beachnourishment_volume)
    out.write(d.cityid)
    out.write(d.countryid)
    out.write(d.deltaid)
    out.write(d.flatloss_erosion_indirect)
    out.write(d.gdpc)
    d.h.keys.toList.sorted.foreach { x => out.write(d.h(x)) }
    out.write(d.landloss_erosion_total)
    out.write(d.landloss_initial_land_below_S1)
    out.write(d.landloss_submergence)
    out.write(d.landlosscost_erosion_total)
    out.write(d.landlosscost_submergence)
    out.write(d.landuse)
    out.write(d.lati)
    out.write(d.length)
    out.write(d.length_protected)
    out.write(d.localgdp)
    out.write(d.longi)
    out.write(d.maxhwater)
    out.write(d.maxhwater_width)
    out.write(d.mhws)
    out.write(d.migration_erosion)
    out.write(d.migration_retreat)
    out.write(d.migration_submergence)
    out.write(d.migrationcost_erosion)
    out.write(d.migrationcost_retreat)
    out.write(d.migrationcost_submergence)
    out.write(d.nbbenefit)
    out.write(d.openwater)
    out.write(d.par)
    d.pop_below_.keys.toList.sorted.foreach { x => out.write(d.pop_below_(x)) }
    d.pop_below_h.keys.toList.sorted.foreach { x => out.write(d.pop_below_h(x)) }
    out.write(d.popdens)
    out.write(d.population_correction)
    out.write(d.protlevel_implemented)
    out.write(d.protlevel_implemented_before_dikeraise)
    out.write(d.protlevel_target)
    out.write(d.riverdike_cost)
    out.write(d.riverdike_increase_maintenance_cost)
    out.write(d.riverdike_maintenance_cost)
    out.write(d.riverfloodcost)
    out.write(d.rslr)
    out.write(d.salinity_cost)
    out.write(d.sandloss_erosion_direct)
    out.write(d.sandloss_erosion_including_nourishment)
    out.write(d.sandloss_erosion_indirect)
    out.write(d.sandloss_erosion_total)
    out.write(d.seadike_abandon_year)
    out.write(d.seadike_bcr)
    out.write(d.seadike_construction_year)
    out.write(d.seadike_cost)
    out.write(d.seadike_height)
    out.write(d.seadike_height_initial)
    out.write(d.seadike_increase_maintenance_cost)
    out.write(d.seadike_maintenance_cost)
    out.write(d.seadikebase_elevation)
    out.write(d.seadikepos_distance_orig_coastline)
    out.write(d.seadikepos_distance_recent_coastline)
    out.write(d.seafloodcost)
    d.seafloodcost_h.keys.toList.sorted.foreach { x => out.write(d.seafloodcost_h(x)) }
    out.write(d.seafloodcost_min)
    out.write(d.seafloodcost_relative)
    out.write(d.setback_height)
    out.write(d.setback_length)
    out.write(d.setback_returnperiod)
    out.write(d.setback_width)
    out.write(d.setbackzone_administrative_cost)
    out.write(d.setbackzone_area)
    out.write(d.setbackzone_assets)
    out.write(d.setbackzone_deprecation)
    out.write(d.setbackzone_population)
    out.write(d.slr_climate_induced)
    out.write(d.slr_current_annual_rate)
    out.write(d.slr_nonclimate_induced)
    out.write(d.surge_barrier_cost)
    out.write(d.surge_barrier_maintenance_cost)
    out.write(d.target_floodloss_risk)
    out.write(d.tourlosscost)
    out.write(d.wetfresh)
    out.write(d.wetfresh_value)
    out.write(d.wetland_area_total)
    out.write(d.wetland_length)
    out.write(d.wetland_nourishment_cost)
    out.write(d.wetland_value)
    out.write(d.wetland_width)
    out.write(d.wetlow)
    out.write(d.wetlow_value)
    out.write(d.wetmang)
    out.write(d.wetmang_value)
    out.write(d.wetsalt)
    out.write(d.wetsalt_value)
    out.write(d.wnourcost)
  }

  def writeSelectedData(out: divaOutput, d: ClsData, ignoreThisOutput: Array[Boolean]) {
    var c: Int = 0
    if (!ignoreThisOutput(c)) {out.write(d.adminid)}; c+=1
    d.area_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.area_below_(x))}; c+=1 } } 
    d.area_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.area_below_h(x))}; c+=1 } } 
    d.assets_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_below_(x))}; c+=1 } } 
    d.assets_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_below_h(x))}; c+=1 } } 
    d.assets_developable_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_developable_below_(x))}; c+=1 } } 
    d.assets_developed_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_developed_below_(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.basinid)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_length_beach)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_length_shore)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_reason)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_volume)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.cityid)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.countryid)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.deltaid)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.flatloss_erosion_indirect)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.gdpc)}; c+=1
    d.h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.landloss_erosion_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landloss_initial_land_below_S1)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landloss_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landlosscost_erosion_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landlosscost_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landuse)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.lati)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.length)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.length_protected)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.localgdp)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.longi)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.maxhwater)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.maxhwater_width)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.mhws)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_erosion)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_retreat)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migrationcost_erosion)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migrationcost_retreat)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migrationcost_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.nbbenefit)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.openwater)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.par)}; c+=1
    d.pop_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.pop_below_(x))}; c+=1 } } 
    d.pop_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.pop_below_h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.popdens)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.population_correction)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.protlevel_implemented)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.protlevel_implemented_before_dikeraise)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.protlevel_target)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_increase_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverfloodcost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.rslr)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.salinity_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_direct)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_including_nourishment)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_indirect)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_abandon_year)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_bcr)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_construction_year)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_height)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_height_initial)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_increase_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadikebase_elevation)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadikepos_distance_orig_coastline)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadikepos_distance_recent_coastline)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost)}; c+=1
    d.seafloodcost_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_min)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_relative)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setback_height)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setback_length)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setback_returnperiod)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setback_width)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_administrative_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_area)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_assets)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_deprecation)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_population)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.slr_climate_induced)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.slr_current_annual_rate)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.slr_nonclimate_induced)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.target_floodloss_risk)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.tourlosscost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetfresh)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetfresh_value)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_area_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_length)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_nourishment_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_value)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_width)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetlow)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetlow_value)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetmang)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetmang_value)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetsalt)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetsalt_value)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wnourcost)}; c+=1
  }

  def readInputValue(in: divaInput, d:ClsData, parName: String, locid: String, time: Int) {
    if (parName=="adminid") {d.adminid = in.getInputValue[String]("adminid", locid, time)}
    else if (parName=="brf") {d.brf = in.getInputValue[Float]("brf", locid, time)}
    else if (parName=="countryid") {d.countryid = in.getInputValue[String]("countryid", locid, time)}
    else if (parName=="cpc") {d.cpc = in.getInputValue[Int]("cpc", locid, time)}
    else if (parName=="cyclone") {d.cyclone = in.getInputValue[Boolean]("cyclone", locid, time)}
    else if (parName=="fetchlimited") {d.fetchlimited = in.getInputValue[Boolean]("fetchlimited", locid, time)}
    else if (parName=="lati") {d.lati = in.getInputValue[Float]("lati", locid, time)}
    else if (parName=="length") {d.length = in.getInputValue[Float]("length", locid, time)}
    else if (parName=="longi") {d.longi = in.getInputValue[Float]("longi", locid, time)}
    else if (parName=="maxhwater") {d.maxhwater = in.getInputValue[Float]("maxhwater", locid, time)}
    else if (parName=="meanhwater") {d.meanhwater = in.getInputValue[Float]("meanhwater", locid, time)}
    else if (parName=="mhwn") {d.mhwn = in.getInputValue[Float]("mhwn", locid, time)}
    else if (parName=="mhws") {d.mhws = in.getInputValue[Float]("mhws", locid, time)}
    else if (parName=="mtidalrng") {d.mtidalrng = in.getInputValue[Float]("mtidalrng", locid, time)}
    else if (parName=="s0001") {d.s0001 = in.getInputValue[Float]("s0001", locid, time)}
    else if (parName=="s0010") {d.s0010 = in.getInputValue[Float]("s0010", locid, time)}
    else if (parName=="s0100") {d.s0100 = in.getInputValue[Float]("s0100", locid, time)}
    else if (parName=="s1000") {d.s1000 = in.getInputValue[Float]("s1000", locid, time)}
    else if (parName=="slopecst") {d.slopecst = in.getInputValue[Float]("slopecst", locid, time)}
    else if (parName=="surgedist_location") {d.surgedist_location = in.getInputValue[Float]("surgedist_location", locid, time)}
    else if (parName=="surgedist_number_events") {d.surgedist_number_events = in.getInputValue[Float]("surgedist_number_events", locid, time)}
    else if (parName=="surgedist_scale") {d.surgedist_scale = in.getInputValue[Float]("surgedist_scale", locid, time)}
    else if (parName=="surgedist_shape") {d.surgedist_shape = in.getInputValue[Float]("surgedist_shape", locid, time)}
    else if (parName=="surgedist_type") {d.surgedist_type = in.getInputValue[String]("surgedist_type", locid, time)}
    else if (parName=="tsm") {d.tsm = in.getInputValue[Float]("tsm", locid, time)}
    else if (parName=="uplift") {d.uplift = in.getInputValue[Float]("uplift", locid, time)}
    else if (parName=="waveclim") {d.waveclim = in.getInputValue[Int]("waveclim", locid, time)}
    else if (parName=="wetfresh") {d.wetfresh = in.getInputValue[Float]("wetfresh", locid, time)}
    else if (parName=="wetlow") {d.wetlow = in.getInputValue[Float]("wetlow", locid, time)}
    else if (parName=="wetmang") {d.wetmang = in.getInputValue[Float]("wetmang", locid, time)}
    else if (parName=="wetsalt") {d.wetsalt = in.getInputValue[Float]("wetsalt", locid, time)}
    // else println("error:" + parName + " not found in Cls")
  }

  def readOptionInputValue(in: divaInput, d:ClsData, parName: String, locid: String, time: Int) {
    if (parName=="attenuation_slope") {d.attenuation_slope = in.getOptionInputValue[Float]("attenuation_slope", locid, time)}
    else if (parName=="basinid") {d.basinid = in.getOptionInputValue[String]("basinid", locid, time)}
    else if (parName=="cityid") {d.cityid = in.getOptionInputValue[String]("cityid", locid, time)}
    else if (parName=="deltaid") {d.deltaid = in.getOptionInputValue[String]("deltaid", locid, time)}
    else if (parName=="landuse") {d.landuse = in.getOptionInputValue[Int]("landuse", locid, time)}
    else if (parName=="s0002") {d.s0002 = in.getOptionInputValue[Float]("s0002", locid, time)}
    else if (parName=="s0005") {d.s0005 = in.getOptionInputValue[Float]("s0005", locid, time)}
    else if (parName=="s0025") {d.s0025 = in.getOptionInputValue[Float]("s0025", locid, time)}
    else if (parName=="s0050") {d.s0050 = in.getOptionInputValue[Float]("s0050", locid, time)}
    else if (parName=="s0250") {d.s0250 = in.getOptionInputValue[Float]("s0250", locid, time)}
    else if (parName=="s0500") {d.s0500 = in.getOptionInputValue[Float]("s0500", locid, time)}
    // else println("error:" + parName + " not found in Cls")
  }

  def readScenarioValue(in: divaInput, d:ClsData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
    if (parName=="sealevel_anomalie") {d.sealevel_anomalie = in.getScenarioValue[Float]("sealevel_anomalie", locid, time, pInterpolationMode)}
    else if (parName=="temperature_anomalie") {d.temperature_anomalie = in.getScenarioValue[Float]("temperature_anomalie", locid, time, pInterpolationMode)}
    // else println("error:" + parName + " not found in Cls")
  }

  def readOptionScenarioValue(in: divaInput, d:ClsData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

}
