package diva.model

import diva.io.logging.LogLevel._
import diva.io.logging.LoggerI
import diva.types.TypeComputations._
import scala.reflect.runtime.{ universe => ru }
import scala.collection.mutable.HashMap
import java.io.File
import java.io.FileInputStream
import java.io.PrintStream
import java.util.Properties

class Config(val file: Option[FileInputStream], var logger: LoggerI, var version: String) {

  private var prop: Properties = new Properties
  private val propsPrefix = "diva."
  private val featurePropsPrefix = "diva.feature."
  private val modulePropsPrefix = "diva.module."

  // all paths
  private var homeDir = "."
  private var etcDir = "etc"

  private var dataInBase = "../../data/his"
  private var dataScenBase = "../../scen"
  private var dataOutBase = "out"

  // Defaults ...
  var startTime = 1995
  var timeLimit = 2100
  var timeStep = 5
  var logLevel: LogLevel = INFO
  private var logFileName: String = "diva.log"
  private var logModules = true
  private var globalParameters = None

  def logFile = dataOutBase + "/" + logFileName

  private val inputFileEnding = "_input"
  private val scenarioFileEnding = "_scenario"
  private val outputFileEnding = "_output"
  private var ending = ".csv"

  var caseid: String = "singlerun"
  var tablePrefix: String = "singlerun_"

  var extendedInputCheck: Boolean = true

  var noValue: Float = -999.999f
  private def noValueString: String = noValue.toString

  var scenarioWildcard: String = "ALL_LOCATIONS"

  private val featureClasses: List[FeatureI] = diva.model.divaFeatures.features
  private val featureClassNames: List[String] = featureClasses.map { x => x.getClass.getName.toLowerCase }
  var featuresInputFilenames: HashMap[String, List[String]] = new HashMap[String, List[String]]
  var featuresScenarioFilenames: HashMap[String, List[String]] = new HashMap[String, List[String]]
  var featuresScenarioGlobal: HashMap[String, Boolean] = new HashMap[String, Boolean]
  var featuresOutputFilenames: HashMap[String, String] = new HashMap[String, String]
  var featuresOutputIgnore: HashMap[String, List[String]] = new HashMap[String, List[String]]

  var elevationValuesForExposureOutput: List[Float] = List(1.0f, 2.0f, 10.0f)
  var rpValuesForExposureOutput: List[Int] = List(1, 10, 100, 1000)

  var discountRate: Float = 0.0f
  var dikeConstructionCostFactor: Float = 1.0f
  var surgeBarrierUnitCost: Float = 0.16f // Million US$ (2014) per m width and m height
  var dikeRaisingStartYear: Int = 1995

  private var moduleClasses: List[ModuleI] = diva.model.divaModules.modules
  private val moduleClassNames: List[String] = moduleClasses.map { x => x.getClass.getName.toLowerCase }
  var moduleRequests: HashMap[String, String] = new HashMap[String, String]

  // TODO: think about better solution?
  //featureClasses.foreach { (f: FeatureI) => featuresInputFilenames += (f.name -> (dataInBase + "/" + f.name + inputFileEnding + ending) ) }
  //featureClasses.foreach { (f: FeatureI) => featuresOutputFilenames += (f.name -> (dataOutBase + "/" + f.name + outputFileEnding + ending) ) }
  featureClasses.foreach { (f: FeatureI) => featuresInputFilenames += (f.name -> (List[String](f.name + inputFileEnding + ending))) }
  featureClasses.foreach { (f: FeatureI) => featuresOutputFilenames += (f.name -> (f.name + outputFileEnding + ending)) }
  featureClasses.foreach { (f: FeatureI) => featuresOutputIgnore += (f.name -> List()) }
  featureClasses.foreach { (f: FeatureI) => featuresScenarioFilenames += (f.name -> (List[String](f.name + scenarioFileEnding + ending))) }
  featureClasses.foreach { (f: FeatureI) => featuresScenarioGlobal += (f.name -> false) }

  // parse external config file, if given
  file match {
    case None    => ;
    case Some(f) => parse(f)
  }

  logger.id = this.getClass.getName
  setConfig

  // Construct the logfile stream
  var logFileStream: Option[PrintStream] =
    try { Some(new PrintStream(logFile)) }
    catch {
      case _: Throwable => None
    }

  // Functions
  def parse(file: FileInputStream) {
    guessHomeDir()
    prop.load(file)
  }

  // get the homedir of diva ... should be the same as DIVA_HOME
  private def guessHomeDir() {
    // where does genial.home comes from?
    var divaHome = System.getProperty("diva.home")
    // if not take the current working dir
    if (divaHome == null)
      divaHome = System.getProperty("user.dir")
    homeDir = divaHome
    etcDir = divaHome + "/etc"
  }

  private def setConfig {
    //println(prop.toString())
    dataOutBase = getPropertyOrDefault[String](prop, propsPrefix + "out.directory", dataOutBase)
    dataInBase = getPropertyOrDefault[String](prop, propsPrefix + "in.directory", dataInBase)
    dataScenBase = getPropertyOrDefault[String](prop, propsPrefix + "scen.directory", dataScenBase)
    startTime = getPropertyOrDefault[Int](prop, propsPrefix + "time.start", startTime)
    timeLimit = getPropertyOrDefault[Int](prop, propsPrefix + "time.limit", timeLimit)
    timeStep = getPropertyOrDefault[Int](prop, propsPrefix + "time.step", timeStep)
    logFileName = getPropertyOrDefault[String](prop, propsPrefix + "logfile", logFileName)
    logLevel = getPropertyOrDefault[String](prop, propsPrefix + "loglevel", logLevel.toString())
    noValue = getPropertyOrDefault[Float](prop, propsPrefix + "noValue", noValue)
    discountRate = getPropertyOrDefault[Float](prop, propsPrefix + "discountRate", discountRate)

    dikeConstructionCostFactor = getPropertyOrDefault[Float](prop, propsPrefix + "dikeConstructionCostFactor", dikeConstructionCostFactor)
    surgeBarrierUnitCost = getPropertyOrDefault[Float](prop, propsPrefix + "surgeBarrierUnitCost", surgeBarrierUnitCost)
    dikeRaisingStartYear = getPropertyOrDefault[Int](prop, propsPrefix + "dikeRaisingStartYear", dikeRaisingStartYear)

    caseid = getPropertyOrDefault[String](prop, propsPrefix + "caseid", caseid)
    version = getPropertyOrDefault[String](prop, propsPrefix + "version", version)
    tablePrefix = getPropertyOrDefault[String](prop, propsPrefix + "tablePrefix", tablePrefix)
    extendedInputCheck = getPropertyOrDefault[Boolean](prop, propsPrefix + "extendedInputCheck", extendedInputCheck)

    elevationValuesForExposureOutput = getPropertyListOrDefault(prop, propsPrefix + "elevationExposureOutput", elevationValuesForExposureOutput)
    rpValuesForExposureOutput = getPropertyListOrDefault(prop, propsPrefix + "rpExposureOutput", rpValuesForExposureOutput)

    // global -> ../../data/his/global_input.his,
    // diva.feature.cls.input
    //println(featuresInputFilenames)
    featuresInputFilenames.foreach((v: (String, List[String])) => featuresInputFilenames(v._1) = getPropertyListOrDefault(prop, featurePropsPrefix + v._1 + ".input", (v._2)))
    featuresInputFilenames.foreach((v: (String, List[String])) => featuresInputFilenames(v._1) = v._2.map(x => dataInBase + "/" + x))
    //println(featuresInputFilenames)

    featuresOutputFilenames.foreach((v: (String, String)) => featuresOutputFilenames(v._1) = getPropertyOrDefault(prop, featurePropsPrefix + v._1 + ".output", v._2))
    featuresOutputFilenames.foreach((v: (String, String)) => featuresOutputFilenames(v._1) = dataOutBase + "/" + v._2)

    featuresScenarioFilenames.foreach((v: (String, List[String])) => featuresScenarioFilenames(v._1) = getPropertyListOrDefault(prop, featurePropsPrefix + v._1 + ".scenario", v._2))
    featuresScenarioFilenames.foreach((v: (String, List[String])) => featuresScenarioFilenames(v._1) = v._2.map(x => dataScenBase + "/" + x))

    featuresScenarioGlobal.foreach((v: (String, Boolean)) => featuresScenarioGlobal(v._1) = getPropertyOrDefault(prop, featurePropsPrefix + v._1 + ".scenarioIsGlobal", v._2))

    featuresOutputIgnore.foreach((v: (String, List[String])) => featuresOutputIgnore(v._1) = getPropertyListOrDefault(prop, featurePropsPrefix + v._1 + ".ignore_outputs", (v._2)))

  }

  def check {
    logger.info("elevation levels for exposure output: " + elevationValuesForExposureOutput)
    logger.info("return periods for exposure output: " + rpValuesForExposureOutput)

    featuresScenarioGlobal.foreach((v: (String, Boolean)) => if (v._2 == true) logger.info("setting global scenario for feature " + v._1))
    // check for feature request for unexisting features --> warn then!
    prop.keySet.toArray.foreach { x => checkIfExists(x.toString, featurePropsPrefix, featureClassNames, "feature") }
    // check for feature request for unexisting modules --> warn then!
    prop.keySet.toArray.foreach { x => checkIfExists(x.toString, modulePropsPrefix, moduleClassNames, "module") }
  }

  def setModuleParameter(m: diva.model.ModuleI) {
    prop.keySet.toArray.foreach { x => trySetModuleParameter(x.toString, m) }
  }

  private def getPropertyOrDefault[T: StringParseOp](p: Properties, s: String, default2: T)(implicit /*ev1: diva.types.TypeComputations.StringParseOp[T],*/ ev2: ru.TypeTag[T]): T = {

    val actualPropertyValue: String = prop.getProperty(s)
    if (actualPropertyValue == null)
      default2
    else
      getParsedValue[T](actualPropertyValue, s) match {
        case Some(v) => v
        case None =>
          logger.warn("Using default value " + default2 + " for property " + s + ": " + actualPropertyValue)
          default2
      }
  }

  // experimental: 
  private def getPropertyListOrDefault[T: StringParseOp](p: Properties, s: String, default: List[T])(implicit ev2: ru.TypeTag[T]): List[T] = {
    var propertyValues: List[String] = readAllPropertyValues(p, s)
    var ret: List[T] = List()
    propertyValues.foreach {
      (v: String) =>
        getParsedValue[T](v, s) match {
          case Some(v) => ret = ret ++ List(v)
          case None    =>
        }
    }
    if (ret.size > 0) ret else default
  }

  private def getParsedValue[T: StringParseOp](actualPropertyValue: String, s: String)(implicit ev2: ru.TypeTag[T]): Option[T] = {
    try {
      Some(diva.types.TypeComputations.parseString[T](actualPropertyValue))
    } catch {
      case nf: java.lang.NumberFormatException =>
        logger.warn("wrong number format for property " + s + ": expected " + ru.typeTag[T].tpe + ", found " + actualPropertyValue)
        None
      case iae: java.lang.IllegalArgumentException =>
        logger.warn("Wrong type for property " + s + ": expected " + ru.typeTag[T].tpe + " found " + actualPropertyValue)
        None
      case e: Throwable =>
        logger.warn("something wrong (" + e.toString() + ") with property " + s + ": " + actualPropertyValue)
        None
    }
  }

  private def checkIfExists(x: String, prefix: String, names: List[String], z: String) {
    val xPrefix = x.toString.take(prefix.length)
    if (xPrefix == prefix) {
      val xWords = x.toString().toLowerCase().split('.')
      if (!names.contains(xPrefix + xWords(2))) {
        logger.warn("Config requests for " + z + " " + xPrefix + xWords(2) + " (in " + x + " = " + prop.getProperty(x.toString) + ") which does not exist. Request Ignored.")
      }
    }
  }

  private def readAllPropertyValues(p: Properties, s: String): List[String] = {
    if (prop.getProperty(s) == null) {
      List()
    } else {
      prop.getProperty(s).split(" ").toList
    }
  }

  private def trySetModuleParameter(x: String, m: diva.model.ModuleI) {
    val xPrefix = x.toString.take(modulePropsPrefix.length)
    if (xPrefix == modulePropsPrefix) {
      val xWords = x.toString().split('.')
      if (xWords.length == 4) {
        val moduleName = xWords(2).toLowerCase
        if (modulePropsPrefix + moduleName == m.fileName.toLowerCase()) {
          val parameterName = xWords(3)
          val rm = ru.runtimeMirror(getClass.getClassLoader)
          val im = rm.reflect(m)
          val fieldSymbols: List[ru.Symbol] = im.symbol.typeSignature.members.filter(m => !m.isMethod).toList
          val fieldNames = fieldSymbols.map { x => x.name.toString.trim /*x.fullName.toLowerCase */ }
          //println(modulePropsPrefix + moduleName + "." + parameterName)
          //println(fieldNames)
          if (fieldNames.contains(parameterName)) {
            val fieldToSet: ru.Symbol = fieldSymbols(fieldNames.indexOf(parameterName))
            val fieldType: ru.Type = fieldToSet.typeSignature
            val field: ru.TermSymbol = fieldToSet.asTerm
            val fm: ru.FieldMirror = im.reflectField(field)
            val valueToSet: String = prop.getProperty(x.toString)
            try {
              if (fieldType =:= ru.typeOf[String]) { fm.set(valueToSet) }
              else if (fieldType =:= ru.typeOf[Byte]) { fm.set(parseString[Byte](valueToSet)) }
              else if (fieldType =:= ru.typeOf[Short]) { fm.set(parseString[Short](valueToSet)) }
              else if (fieldType =:= ru.typeOf[Int]) { fm.set(parseString[Int](valueToSet)) }
              else if (fieldType =:= ru.typeOf[Long]) { fm.set(parseString[Long](valueToSet)) }
              else if (fieldType =:= ru.typeOf[Float]) { fm.set(parseString[Float](valueToSet)) }
              else if (fieldType =:= ru.typeOf[Double]) { fm.set(parseString[Double](valueToSet)) }
              else if (fieldType =:= ru.typeOf[Boolean]) { fm.set(parseString[Boolean](valueToSet)) }
              else if (fieldType =:= ru.typeOf[diva.types.DikePositionMode.DikePositionMode]) { fm.set(diva.types.DikePositionMode.withName(valueToSet)) }
              else if (fieldType =:= ru.typeOf[diva.types.DikeMode.DikeMode]) { fm.set(diva.types.DikeMode.withName(valueToSet)) }
              else if (fieldType =:= ru.typeOf[diva.types.AssetPopulationCorrectionMode.AssetPopulationCorrectionMode]) { fm.set(diva.types.AssetPopulationCorrectionMode.withName(valueToSet)) }
              else if (fieldType =:= ru.typeOf[diva.types.RetreatMode.RetreatMode]) { fm.set(diva.types.RetreatMode.withName(valueToSet)) }
              else if (fieldType =:= ru.typeOf[diva.types.BeachNourishmentMode.BeachNourishmentMode]) { fm.set(diva.types.BeachNourishmentMode.withName(valueToSet)) }
              else if (fieldType =:= ru.typeOf[diva.types.SlrNormalisationMode.SlrNormalisationMode]) { fm.set(diva.types.SlrNormalisationMode.withName(valueToSet)) }
              else if (fieldType =:= ru.typeOf[diva.types.RiverProtectionMode.RiverProtectionMode]) { fm.set(diva.types.RiverProtectionMode.withName(valueToSet)) }
              //else if (fieldType.baseClasses.toString.contains("Value")) { fm.set(parseEnumeration(valueToSet,fieldType)) }
              else {
                logger.error("Could not set parameter " + parameterName + " in module " + m.fileName + "(" + x + " = " + prop.getProperty(x.toString) + ": unsupported type (" + fieldType.toString() + ")")
              }
            } catch {
              case e: Throwable => logger.error("Could not set parameter " + parameterName + " of type " + fieldType.toString + " in module " + m.fileName + " (" + x + " = " + prop.getProperty(x.toString) + "): unknown error (" + e.toString() + ")")
            }
            logger.info("Setting " + x + " to " + valueToSet + " of type " + fieldType.toString)
          } else {
            logger.fatal("Requested to set parameter " + parameterName + " in module " + m.fileName + " (" + x + " = " + prop.getProperty(x.toString) + ") but the parameter does not exist in the module.")
          }
        }
      } else {
        logger.error("Syntax error: " + x + " = " + prop.getProperty(x.toString))
      }
    }
  }
}