package diva.types

import scala.language.implicitConversions

object Distribution extends Enumeration {
  type Distribution = Value

  val GUMBEL = Value("GUMBEL")
  val GEV = Value("GEV")
  val GPD = Value("GPD")
  
  implicit def valueToDistributionString(v: Value): Distribution = v.asInstanceOf[Distribution]
  implicit def stringToDistribution(s: String): Distribution = Distribution.withName(s)

}