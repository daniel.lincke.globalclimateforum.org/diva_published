package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI
import diva.module.flooding._

import diva.coastalplanemodel.CoastalPlaneModel

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class Cz extends FeatureI {
  type Data = CzData
  type DataIO = CzDataIO

  val dataTypeTag: ru.TypeTag[CzData] = ru.typeTag[CzData]
  val dataClassTag: ClassTag[CzData] = classTag[CzData]
  val dataTypeSave: ru.Type = ru.typeOf[CzData]

  var data: CzData = null
  var dataIO: CzDataIO = null

  var mustHaveInputFile = true

  var coastalPlane: CoastalPlaneModel = null
  //var developedCoastalPlane: CoastalPlaneModel = null
  //var developableCoastalPlane: CoastalPlaneModel = null
  //var undevelopedCoastalPlane: CoastalPlaneModel = null

  var admin: Admin = _
  var country: Country = _
  var cls: Cls = _

  def getAnnualPopGrowthAtTime(t: Int): Float = data.popgrowth_memory(t).get

  def init() {
    data = new CzData
    dataIO = new CzDataIO
  }

  def newInstanceCopy: Cz = {
    val ret = new Cz
    ret.data = new CzData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }

  override def clone: Cz = {
    val ret = new Cz
    //ret.data = new ClsData
    ret.dataIO = dataIO
    ret.coastalPlane = coastalPlane.clone
    //ret.developedCoastalPlane = developedCoastalPlane.clone
    //ret.developableCoastalPlane = developableCoastalPlane.clone
    //ret.undevelopedCoastalPlane = undevelopedCoastalPlane.clone
    ret.admin = admin
    ret.cls = cls

    ret.deepCopy(this)
    ret.data = data.clone
    ret.locationid = locationid
    ret.locationname = locationname
    ret
  }

}