package diva.types

//import scala.reflect.runtime.{ universe => ru }

object TypeComputations {

  case class DefaultOp[T](op: () => T)
  implicit val defaultString = DefaultOp[String](() => "")
  implicit val defaultBoolean = DefaultOp[Boolean](() => false)
  implicit val defaultByte = DefaultOp[Byte](() => 0)
  implicit val defaultShort = DefaultOp[Short](() => 0)
  implicit val defaultInt = DefaultOp[Int](() => 0)
  implicit val defaultLong = DefaultOp[Long](() => 0)
  implicit val defaultFloat = DefaultOp[Float](() => 0.0f)
  implicit val defaultDouble = DefaultOp[Double](() => 0.0d)
  implicit def defaultOption[A] = DefaultOp[Option[A]](() => None)
  implicit def defaultMap[A, B] = DefaultOp[scala.collection.mutable.Map[A, B]](() => scala.collection.mutable.Map())

  def default[T]()(implicit ev: DefaultOp[T]): T = ev.op()

  case class StringParseOp[T](op: String => T)
  implicit val stringParseString = StringParseOp[String]((str: String) => str)
  implicit val stringParseBoolean = StringParseOp[Boolean]((str: String) => ((str.toLowerCase() == "true") || (str == "1") || (str == "T") || (str == "t")))
  implicit val stringParseByte = StringParseOp[Byte]((str: String) => str.toByte)
  implicit val stringParseShort = StringParseOp[Short]((str: String) => str.toShort)
  implicit val stringParseInt = StringParseOp[Int]((str: String) => str.toInt)
  implicit val stringParseLong = StringParseOp[Long]((str: String) => str.toLong)
  implicit val stringParseFloat = StringParseOp[Float]((str: String) => str.toFloat)
  implicit val stringParseDouble = StringParseOp[Double]((str: String) => str.toDouble)
  //implicit def stringParseEnumeration[T <: Enumeration] = StringParseOp[T]((str: String) => str)
  implicit def stringParseOption[A] = StringParseOp[Option[A]]((str: String) =>
    if (str.toLowerCase() == "none") None
    else None)

  def parseString[T](str: String)(implicit ev: StringParseOp[T]): T = ev.op(str)

  def parseEnumeration[T <: Enumeration](s: String, enumeration: T): T#Value = enumeration.withName(s)

  case class toPSQLString[T](op: (T) => String)
  implicit val StringToPSQLString = toPSQLString[String]((s: String) => "'" + s + "'")
  implicit val BooleanToPSQLString = toPSQLString[Boolean]((b: Boolean) => b.toString)
  implicit val ByteToPSQLString = toPSQLString[Byte]((b: Byte) => b.toString)
  implicit val ShortToPSQLString = toPSQLString[Short]((s: Short) => s.toString)
  implicit val IntToPSQLString = toPSQLString[Int]((i: Int) => i.toString)
  implicit val LongToPSQLString = toPSQLString[Long]((l: Long) => l.toString)
  implicit val FloatToPSQLString = toPSQLString[Float]((f: Float) => f.toString)
  implicit val DoubleToPSQLString = toPSQLString[Double]((d: Double) => d.toString)
  implicit def OptionToPSQLString[A] = toPSQLString[Option[A]]((o: Option[A]) => o match {
    case None => "NULL"
    case Some(x) => { "'" + x.toString + "'" }
  })

  implicit def MapToPSQLString[A, B] = toPSQLString[scala.collection.mutable.Map[A, B]]((m: scala.collection.mutable.Map[A, B]) => "")

  def convertToPSQLString[T](t: T)(implicit ev: toPSQLString[T]): String = ev.op(t)

  //Some(parseString[A](str.drop(5).dropRight(1))())

  // Float Parsing
  trait Floatable[T] {
    def parse(f: Float): T
  }

  implicit object IntIsFloatable extends Floatable[Int] {
    def parse(f: Float): Int = f.toInt
  }

  implicit object FloatIsFloatable extends Floatable[Float] {
    def parse(f: Float): Float = f
  }

  implicit object DoubleIsFloatable extends Floatable[Double] {
    def parse(f: Float): Double = f.toDouble
  }

  implicit object BooleanIsFloatable extends Floatable[Boolean] {
    def parse(f: Float): Boolean = f == 1.0
  }

  implicit object StringIsFloatable extends Floatable[String] {
    def parse(f: Float): String = f.toString
  }

  def floatTo[T: Floatable](f: Float): T = implicitly[Floatable[T]].parse(f)

  implicit def f2d(x: Float): Double = x.toDouble

  implicit def d2f(x: Double): Float = x.toFloat
}
