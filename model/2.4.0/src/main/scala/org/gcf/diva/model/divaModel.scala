package diva.model

import diva.model._
import diva.io.logging.Logger
import java.io.File
import java.io.FileInputStream
import java.io.PrintStream
import java.io.FileNotFoundException

object divaModel {

  def run(args: Array[String]) {

    // constructand set up the logger ...
    // ... first as a screen logger
    var logger: Logger = new Logger
    logger.id = this.getClass.getName
    
    val version: String = "dev"

    // constructing the config object ...
    // if no config file is given, just take the hard coded defaults
    var config: Config =
      if (args.length == 0) {
        logger.warn("no configuration file given, using default values")
        new Config(None, logger, version)
      } else {
        var configFile: Option[FileInputStream] =
          try { Some(new FileInputStream(args(0))) }
          catch {
            case fnfe: FileNotFoundException =>
              logger.error("configuration file " + args(0) + " not found"); None
            case e: Throwable => logger.error("configuration file " + args(0) + " i/o problem (" + e.toString() + ")"); None
          }
        new Config(configFile, logger, version)
      }

    // reset the logger to the settings given in the config file 
    config.logFileStream match {
      case Some(st) => {
        logger.info("logging redirected to " + config.logFile)
        logger = new Logger(config.logLevel, st)
        logger.id = "divaModel"
        logger.info("logging redirected to " + config.logFile)
        logger.info("loglevel set to " + config.logLevel)
      }
      case None => {
        logger.fatal("couldn't create logging file " + config.logFile)
      }
    }

    config.logger = logger
    config.check

    // get the DIVA modules ... 
    val DIVAModules: List[ModuleI] = diva.model.divaModules.modules
    // ... and connect them with the logger
    DIVAModules.foreach((m: ModuleI) => m.logger = logger.copy)
    DIVAModules.foreach((m: ModuleI) => m.logger.id = m.fileName)
    DIVAModules.foreach((m: ModuleI) => m.logger.info("connected to logfile"))
    DIVAModules.foreach((m: ModuleI) => config.setModuleParameter(m))

    // set the modules active/inactive

    // construct the DIVA state
    var state: divaState = new divaState(config, logger.copy)

    // precompute scenarios
    val DIVAScenarioModules: List[ModuleI] = DIVAModules.filter { x => x.scenarioModule }

    logger.info("initialising modules")
    DIVAModules.foreach((m: ModuleI) => m.initModule(state))

    while (!state.reachedFinalTime) {
      state.resetVariables
      state.readScenarios
      logger.info("calculating scenario modules for step " + state.time)
      DIVAScenarioModules.foreach((m: ModuleI) => m.computeScenario(state))
      state.nextTime
    }
    state.resetTime

    logger.info("initial step for modules")
    DIVAModules.foreach((m: ModuleI) => m.init(state))

    state.output
    state.nextTime

    // compute module iterations
    while (!state.reachedFinalTime) {
      state.resetVariables
      println("calculating year " + state.time)
      logger.info("calculating modules for step " + state.time)
      DIVAModules.foreach((m: ModuleI) => m.invoke(state))
      state.output
      state.nextTime
    }

    logger.info("finished computations")

    config.logFileStream match {
      case Some(v) => { v.flush(); v.close() }
      case None    =>
    }

  }

  /*
   1) read config
   2) set logger
   3) set modules
   4) build state
   5) set feature I/O
   6) iterate model
   */

}