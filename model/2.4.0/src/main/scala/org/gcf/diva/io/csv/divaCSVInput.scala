package diva.io.csv

import diva.io.divaInput
import diva.io.logging.LoggerI
import diva.types.TypeComputations._
import diva.types._
import scala.collection.mutable.HashMap
import scala.collection.JavaConversions.mapAsScalaMap
import scala.collection.mutable.ListBuffer
import scala.collection.immutable.SortedMap
import scala.collection.mutable.AnyRefMap
import java.io.FileInputStream
import java.io.FileReader
import com.opencsv.CSVReader

class divaCSVInput(fi: FileInputStream, val fname: String, val pLogger: LoggerI) extends divaInput {
  logger = pLogger
  var reader: CSVReader = new CSVReader(new FileReader(fi.getFD))
  var parameter: Array[String] = _
  var metainformation: List[String] = _
  var locations: Array[String] = _
  var indexMap: AnyRefMap[String, Int] = new AnyRefMap[String, Int]

  private var firstLine: Array[String] = _
  private var nextLine: Array[String] = _
  private var currentNextLine: Array[String] = _

  private var dataBuffer: SortedMap[Int, Array[Array[String]]] = SortedMap()
  private var locationIndices: AnyRefMap[String, Int] = new AnyRefMap[String, Int]

  var numberLocations: Int = 0

  var timesteps: AnyRefMap[String, List[Int]] = new AnyRefMap[String, List[Int]]

  var noValueString: String = ""

  var locationWildcard: String = ""

  def readMetainformation = {
    firstLine = reader.readNext
    if (firstLine == null) firstLine = new Array(0)
    val metaInformationisComplete: Boolean = (firstLine.length >= 3)
    val correctMetainformation: Boolean =
      if (metaInformationisComplete) (firstLine(0) == "locationid") && (firstLine(1) == "locationname") && (firstLine(2) == "time") else false
    if (correctMetainformation) {
      metainformation = List(firstLine(0), firstLine(1), firstLine(2))
      logger.info("Read metainformation from " + fname + ": Found " + metainformation.toList.toString())
    } else {
      logger.error("Wrong metainformation format in " + fname + ": Found " + firstLine.toList.toString() + "; expected something starting with locationid, locationname, time.")
    }
    firstLine.foreach { x => indexMap(x) = firstLine.indexOf(x) }
    //firstLine.foreach { x => indexMap.put(x,firstLine.indexOf(x)) }
  }

  def readParameter {
    parameter = if (firstLine.length > 0) firstLine.drop(3).toArray else Array()
    logger.info("Read parameter information from " + fname + ": Found " + parameter.toList.toString)
  }

  def readCompletely(check: Boolean) {
    if ((nextLine == null) || (nextLine.size == 0)) {
      nextLine = reader.readNext
    }

    if (checkLine(nextLine)) {
      while (nextLine != null) {
        var dataBufferTemp: ListBuffer[Array[String]] = ListBuffer()
        val cTime = nextLine(2)
        var time = cTime
        while ((time == cTime) && (nextLine != null) && (nextLine.size > 0)) {
          dataBufferTemp += nextLine
          nextLine = reader.readNext
          time = if (checkLine(nextLine)) nextLine(2) else time + 1
        }
        // TODO: check if cTime already in Map. 
        dataBuffer += (cTime.toInt -> dataBufferTemp.toArray[Array[String]])
      }
    }

    // TODO: check if keySet is sorted  
    parameter.foreach(p => timesteps.put(p, dataBuffer.keySet.toList))
    metainformation.foreach(p => timesteps.put(p, dataBuffer.keySet.toList))

    if (check)
      checkDatabufferConsistency
    else {
      val allLocations: SortedMap[Int, List[String]] = dataBuffer.map { case (k, v) => (k, v.map(x => x(0)).toList) }
      locations = allLocations.valuesIterator.toArray.distinct(0).toArray[String]
      locations.foreach { x => locationIndices += (x -> locations.indexWhere(_ == x)) }
      numberLocations = locations.length
    }
  }

  def getInputValue[T](parName: String, locid: String, time: Int)(implicit ev1: diva.types.TypeComputations.StringParseOp[T], ev2: diva.types.TypeComputations.Floatable[T]): T = {
    try {
      val locationIndex = getLocationIndex(locid, time)
      if (timesteps.apply(parName).contains(time)) {
        parseString[T](dataBuffer(time)(locationIndex)(indexMap(parName)))(ev1)
      } else {
        // fake to avoid error
        parseString[T](dataBuffer(time)(locationIndex)(indexMap(parName)))(ev1)
      }
    } catch {
      case e: Throwable => throw e
    }
  }

  def getOptionInputValue[T](parName: String, locid: String, time: Int)(implicit ev1: diva.types.TypeComputations.StringParseOp[T], ev2: diva.types.TypeComputations.Floatable[T]): Option[T] = {
    try {
      val locationIndex = getLocationIndex(locid, time)
      val v: String = dataBuffer(time)(locationIndex)(indexMap(parName))
      if (v == noValueString)
        None
      else
        Some(parseString[T](v)(ev1))
    } catch {
      case e: Throwable => throw e
    }
  }

  def readCurrentMetainformation[T](parName: String, locid: String, time: Int)(implicit ev1: diva.types.TypeComputations.StringParseOp[T], ev2: diva.types.TypeComputations.Floatable[T]): T = {
    val locationIndex = getLocationIndex(locid, time)
    if (parName == "locationname") {
      parseString[T](dataBuffer(time)(locationIndex)(0))(ev1)
    } else if (parName == "locationid") {
      parseString[T](dataBuffer(time)(locationIndex)(1))(ev1)
    } else if (parName == "time") {
      parseString[T](dataBuffer(time)(locationIndex)(2))(ev1)
    } else {
      null.asInstanceOf[T]
    }
  }

  private def checkLine(line: Array[String]): Boolean = {
    if (line != null) {
      if (line.length != (parameter.length + metainformation.length)) {
        val lineId =
          if (line.length >= 3)
            line(0) + "(" + line(1) + "," + line(2) + ")"
          else
            "non-idendifiable line"
        logger.error("line " + lineId + " in " + fname + " has wrong number of elements: " + line.length + " instead of " + (parameter.length + metainformation.length) + " (metainformation + parameter)")
      }
      true
    } else {
      false
    }
  }

  def hasNextTime: Boolean = fi.getFD.valid()

  private def checkDatabufferConsistency = {
    logger.info("found following timesteps in " + fname + ": " + timesteps)

    // check if all timesteps have the same number of locations
    // todo: better error message?
    if (dataBuffer.valuesIterator.toArray.map(_.length).distinct.size > 1) {
      logger.error("found different number of locations for different timesteps in " + fname + ": " + (SortedMap[Int, Any]() ++ (dataBuffer.map { case (k, v) => (k -> v.length) })).toString())
    }

    dataBuffer.foreach { case (k, v) => (k, v.sortWith((x1: Array[String], x2: Array[String]) => (x1(0) < x2(0)))) }
    val allLocations = dataBuffer.map { case (k, v) => (k, v.map(x => x(0)).toList) }

    if (allLocations.valuesIterator.toArray.distinct.size > 1) {
      val allLocationsList = allLocations.toSeq.toList
      val allLocationsListSlide = allLocationsList.iterator.sliding(2).toList
      for (i <- 0 until allLocationsListSlide.length) {
        val diff1 = allLocationsListSlide(i)(0)._2.filterNot(allLocationsListSlide(i)(1)._2.contains(_))
        val diff2 = allLocationsListSlide(i)(1)._2.filterNot(allLocationsListSlide(i)(0)._2.contains(_))
        if (diff1.size > 0) logger.error("found different locations for different timesteps in " + fname + ": the locations " + diff1 + " where available in " + allLocationsListSlide(i)(0)._1 + " but not in " + allLocationsListSlide(i)(1)._1)
        if (diff2.size > 0) logger.error("found different locations for different timesteps in " + fname + ": the locations " + diff2 + " where available in " + allLocationsListSlide(i)(1)._1 + " but not in " + allLocationsListSlide(i)(2)._1)
      }
      
      for (i <- 0 until allLocationsListSlide.length) {
        if (allLocationsListSlide(i)(0)._2 != allLocationsListSlide(i)(1)._2) {
          // Luxury variant: levensthein
          logger.error("found different location order for different timesteps in " + fname + ": in " + allLocationsListSlide(i)(0)._1 + " the locations are " + allLocationsListSlide(i)(0)._2 + " but in " + allLocationsListSlide(i)(1)._1 + " the locations are " + allLocationsListSlide(i)(1)._2)
        } 
      }
      println("ohoh")
      println(allLocationsList)
      logger.error("found different locations for different timesteps in " + fname)
    }

    if (allLocations.valuesIterator.toArray.distinct.length == 0) {
      logger.warn("no locations found " + fname)
      locations = Array()
    } else {
      locations = allLocations.valuesIterator.toArray.distinct(0).toArray[String]
      locations.foreach { x => locationIndices += (x -> locations.indexWhere(_ == x)) }
      numberLocations = locations.length

      val duplicateLocations = locations.diff(locations.distinct).distinct
      if (duplicateLocations.size > 0) {
        // To do: specify the differences?
        logger.error("found duplicate locationids in " + fname + ": " + duplicateLocations.toList + ". Locationids should be unique!")
      }
      if (allLocations.valuesIterator.toList.distinct(0).size != allLocations.valuesIterator.toList.distinct(0).distinct.size) {
        // To do: specify the differences?
        
        val diff1 = allLocations.valuesIterator.toList.distinct(0).filterNot(allLocations.valuesIterator.toList.distinct(0).distinct.contains(_))
        val diff2 = allLocations.valuesIterator.toList.distinct(0).distinct.filterNot(allLocations.valuesIterator.toList.distinct(0).contains(_))
        if (diff1.size > 0) logger.error("found different locations for different timesteps in " + fname + ": the locations " + diff1 + " where available in " + allLocations.valuesIterator.toList.distinct(0).distinct + " but not in " + allLocations.valuesIterator.toList.distinct(0))
        if (diff2.size > 0) logger.error("found different locations for different timesteps in " + fname + ": the locations " + diff2 + " where available in " + allLocations.valuesIterator.toList.distinct(0) + " but not in " + allLocations.valuesIterator.toList.distinct(0))
        //logger.error("found different locations for different timesteps in " + fname)
      }
    }
  }

  private def getLocationIndex(locid: String, time: Int): Int = {
    if (locationIndices.contains(locid)) locationIndices(locid)
    else if (locationIndices.contains(locationWildcard)) locationIndices(locationWildcard)
    else {
      logger.error("found neither location with id " + locid + " nor the wildcard location " + locationWildcard + " for timestep " + time + " in " + fname); 0
    }
  }
}