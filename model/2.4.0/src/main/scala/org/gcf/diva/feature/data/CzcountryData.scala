/* Automatically generated data features for Czcountry
*  generated on Thu Aug 16 14:08:59 CEST 2018
*/

package diva.feature.data

import diva.annotations._
import diva.types._
import diva.types.TypeComputations._

import scala.collection.mutable._


class CzcountryData {

  @Input
  @OptionalParameter
  @Description(value = "Fraction of undeveloped but buildable area in the coastal zone")
  var fraction_developable :  Option[Float] = default[Option[Float]]

  override def clone : CzcountryData = {
    var ret: CzcountryData  = new CzcountryData
    ret.fraction_developable = fraction_developable
    ret
  }

}
