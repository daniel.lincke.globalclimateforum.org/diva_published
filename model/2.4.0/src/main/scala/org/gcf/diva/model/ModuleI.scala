package diva.model

import diva.model._
import scala.reflect.runtime.{ universe => ru }

trait ModuleI extends hasLogger {

  var active: Boolean = true
  val scenarioModule: Boolean = false

  def fileName = this.getClass().getCanonicalName()
  def moduleName: String
  def moduleVersion: String
  def moduleDescription: String
  def moduleAuthor: String

  def initModule(initialState: divaState)
  def computeScenario(currentState: divaState)
  def init(initialState: divaState)
  def invoke(currentState: divaState)

  protected def shouldNotBeInvoked = {
    logger.error("Oops. Invoked method that should not be invoked.")
  }

  protected def doNothing = {

  }

}