package diva.model

// diva imports
import diva.model._
import diva.feature._
import diva.io._
import diva.io.csv._
import diva.io.multicsv._
import diva.io.psqlcode._
import diva.io.sql._
import diva.io.logging.LogLevel._
import diva.io.logging.LoggerI
// scala imports
import scala.collection.mutable.HashMap
// java imports
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.FileNotFoundException

//import java.sql._
import java.sql.Connection
import java.sql.DriverManager

class divaState(c: Config, var logger: LoggerI) {

  private val featureClasses: List[FeatureI] = diva.model.divaFeatures.features
  private var features: HashMap[String, List[FeatureI]] = new HashMap[String, List[FeatureI]]

  logger.id = this.getClass.getName
  featureClasses.foreach { (f: FeatureI) => logger.info("adding feature " + f.fileName) }
  featureClasses.foreach { (f: FeatureI) => f.logger = logger }
  //featureClasses.foreach { (f: FeatureI) => f.noValue = c.noValue.toString }
  //featureClasses.foreach { (f: FeatureI) => f.scenarioWildcard = c.scenarioWildcard }
  featureClasses.foreach { (f: FeatureI) => features += (f.name -> List()) }
  featureClasses.foreach { (f: FeatureI) => f.init }
  featureClasses.foreach { (f: FeatureI) => f.reflectClass }

  val startTime: Int = c.startTime
  val endTime: Int = c.timeLimit
  val timeStep: Int = c.timeStep
  val timesteps: Array[Int] = (c.startTime to c.timeLimit by c.timeStep).toArray.sorted

  var time = c.startTime
  def elapsedTime = time - startTime;

  val elevationValuesForExposureOutput: List[Float] = c.elevationValuesForExposureOutput
  val rpValuesForExposureOutput: List[Int] = c.rpValuesForExposureOutput

  val discountRate: Float = c.discountRate
  
  val dikeConstructionCostFactor: Float = c.dikeConstructionCostFactor
  val surgeBarrierUnitCost: Float = c.surgeBarrierUnitCost
  val dikeRaisingStartYear: Int = c.dikeRaisingStartYear

  // important: connectOutputs connects an output writer for each feature class (Cls, Country, ...)
  // initFeatures builds the instances of the feature classes (it creates a List with 12150 Cls etc.)
  // so we have to connect the outputs first, in order to reuse the same stream for all instances 
  configureFeatures
  connectOutputs
  configureIgnoreOutputs
  outputHeaders
  connectScenarios
  initFeatures

  def resetTime {
    time = c.startTime
  }

  def reachedFinalTime: Boolean = (time > c.timeLimit)

  def nextTime = {
    if (!reachedFinalTime) {
      time += c.timeStep
    }
  }

  def readScenarios() = featureClasses.foreach { (f: FeatureI) => readFeatureScenario(f) }

  def output() = featureClasses.foreach { (f: FeatureI) => outputFeatureData(f) }

  def resetVariables() = featureClasses.foreach { (f: FeatureI) => resetFeatureVariables(f) }

  def getFeatures[F <: FeatureI](implicit ev: Manifest[F]): List[F] = {
    val featurename: String = ev.runtimeClass.getSimpleName.toLowerCase
    try {
      // dirty hack in Order to solve the Problem of non-castability of the array
      /* val featureList: List[FeatureI] = features(featurename).toList
      val featureListCasted: List[F] = featureList.asInstanceOf[List[F]]
      featureListCasted.toArray*
      * 
      */
      features(featurename).asInstanceOf[List[F]]
    } catch {
      case e: Throwable =>
        logger.fatal("Request for features " + featurename + ", but they don't exist (" + e.toString + ")")
        List()
    }
  }

  def getFeaturesWith[F <: FeatureI](predicate: F => Boolean)(implicit ev: Manifest[F]): List[F] = {
    getFeatures[F].filter(predicate)
  }

  def apply[F <: FeatureI](functionToApply: F => Unit)(implicit ev1: Manifest[F]) {
    val featuresToApply: List[F] = getFeatures[F]
    featuresToApply.foreach(functionToApply)
  }

  // init all features
  private def configureFeatures = featureClasses.foreach { (f: FeatureI) => configureFeature(f) }

  // init one Feature
  private def configureFeature(feat: FeatureI) {
    feat.elevationValuesForExposureOutput = elevationValuesForExposureOutput
    feat.rpValuesForExposureOutput = rpValuesForExposureOutput
  }

  // init all features
  private def initFeatures = featureClasses.foreach { (f: FeatureI) => initFeature(f) }

  // init one Feature
  private def initFeature(feat: FeatureI) {
    val featName: String = feat.name
    var featInputFilenames: List[String] = c.featuresInputFilenames(featName)
    feat.inputFileNames = Some(c.featuresInputFilenames(featName))
    //println(featName + " --> " + featInputFilename)
    val endings: List[String] = featInputFilenames.map(_.reverse.takeWhile(_ != '.').reverse)

    var inputFiles: List[Option[FileInputStream]] = featInputFilenames.map {
      x =>
        try { Some(new FileInputStream(x)) }
        catch {
          case fnfe: FileNotFoundException =>
            logger.warn("feature input file " + x + " not found. ")
            None
          case e: Throwable =>
            logger.warn("feature input file " + x + " i/o problem(" + e.toString + "). ")
            None
        }
    }

    val filesNotThere: List[String] = featInputFilenames.zip(inputFiles).filter(x => (x._2 == None)).map(_._1)
    featInputFilenames = featInputFilenames.zip(inputFiles).filter(x => (x._2 != None)).map(_._1)
    inputFiles = inputFiles.filter { x => (x != None) }
    val countExistingFiles: Int = inputFiles.length

    if ((countExistingFiles == 0) && feat.mustHaveInputFile) {
      inputFiles = Nil
      logger.warn("No feature input file found for " + featName + ". No " + featName + " features initialized.")
    }
    if ((countExistingFiles == 0) && !feat.mustHaveInputFile) {
      inputFiles = Nil
      logger.warn("No feature input file found for " + featName + ". One " + featName + " feature initialized without input.")
    }
    
    if ((inputFiles.length>1) && (countExistingFiles!=inputFiles.length) ) {
      logger.error("Feature input files " + filesNotThere + " not found.")
    }

    inputFiles = inputFiles.filter { x => (x != None) }

    var distinctEndings: List[String] = endings.distinct

    if (distinctEndings.length > 1) {
      logger.error("Found different input file types (extensions) for feature " + featName + ": " + featInputFilenames + " - this is not supported.")
    }

    var ending: String = if (distinctEndings.length == 0) "" else distinctEndings(0)

    var featureInput: Option[divaInput] =
      (inputFiles, ending) match {
        case ((fis :: x :: nil), "csv") =>
          //logger.warn("multiple feature input files (" + featInputFilenames + ") are not yet supported. No " + featName + " features initialized."); None
          Some(new divaMultiCSVInput(inputFiles.map(_.get), featInputFilenames, logger))
        case ((fis :: x :: nil), _) =>
          logger.warn("feature input files (" + featInputFilenames + ") have unknown type. No " + featName + " features initialized."); None
        case ((fis :: nil), "csv") => Some(new divaCSVInput(fis.get, featInputFilenames(0), logger))
        case ((fis :: nil), _) =>
          logger.warn("feature input file " + featInputFilenames(0) + " has unknown type. No " + featName + " features initialized."); None

        case (Nil, _) => None
      }

    featureInput match {
      case Some(fis) => {
        fis.noValueString = c.noValue.toString
        fis.locationWildcard = c.scenarioWildcard
        fis.readMetainformation
        fis.readParameter
        feat.checkParameters(fis.parameter ++ fis.metainformation)
        fis.readCompletely(c.extendedInputCheck)

        var fArray: Array[FeatureI] = Array.fill(fis.numberLocations)(feat.newInstanceCopy)
        fArray.foreach { _.reflectInstance }

        for (locid <- fis.locations) {
          fArray(fis.locations.indexWhere(_ == locid)).initialise(fis, locid, time)
        }

        logger.info("initialised " + fArray.length + " features of type " + feat.name)
        features(feat.name) = fArray.toList
      }
      case None => {
        if (feat.mustHaveInputFile) {
          features(feat.name) = List()
        } else {
          var fArray: Array[FeatureI] = Array.fill(1)(feat.newInstanceCopy)
          fArray.foreach { _.reflectInstance }
          features(feat.name) = fArray.toList
          logger.info("initialised " + features(feat.name).length + " features of type " + feat.name)
        }
      }
    }
  }

  private def configureIgnoreOutputs() = featureClasses.foreach { (f: FeatureI) => configureIgnoreOutput(f) }
  
  private def configureIgnoreOutput(feat: FeatureI) {
     val featName: String = feat.name   
     feat.ignoreOutputs = c.featuresOutputIgnore(featName).toArray
     feat.constructIgnoreOutputArray
  }
  
  private def connectOutputs() = featureClasses.foreach { (f: FeatureI) => connectOutput(f) }

  private def connectOutput(feat: FeatureI) {
    val featName: String = feat.name
    val featOutputFilename: String = c.featuresOutputFilenames(featName)
    feat.outputFileName = Some(c.featuresOutputFilenames(featName))
    //println(featName + " --> " + featInputFilename)
    val ending = featOutputFilename.reverse.takeWhile(_ != '.').reverse

    var featureOutput: Option[divaOutput] = ending match {
      case "csv" =>
        try {
          Some(new divaCSVOutput(new FileOutputStream(featOutputFilename), featOutputFilename, logger))
        } catch {
          case e: Throwable =>
            logger.warn("feature output file " + featOutputFilename + " i/o problem(" + e.toString() + "). No " + featName + " features initialized."); None
        }

      case "psqlcode" =>
        try {
          Some(new divaPSQLOutput(new FileOutputStream(featOutputFilename), featOutputFilename, logger, c.tablePrefix + featName + "_output"))
        } catch {
          case e: Throwable =>
            logger.warn("feature output file " + featOutputFilename + " i/o problem(" + e.toString() + "). No " + featName + " features initialized."); None
        }

      case "psql" =>
        try {
          var tableName: String = c.tablePrefix + featName + "_output"
          var connection: Connection = null
          Class.forName("org.postgresql.Driver");
          connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/diva", "diva", "diva");
          logger.info("Opened diva database successfully");

          Some(new divaSQLOutput(connection, logger, c.tablePrefix + featName + "_output"))
        } catch {
          case e: Throwable =>
            logger.warn("feature output file " + featOutputFilename + " database problem(" + e.toString() + "). No " + featName + " features initialized."); None
        }
      //case (Some(fis), "his") => Some(new divaHISFeatureOutputStream(fis))
      case _ =>
        logger.warn("feature output file " + featOutputFilename + " has unknown type. No output for " + featName + "."); None
    }

    feat.outputStream = featureOutput
  }

  private def outputHeaders() = featureClasses.foreach { (f: FeatureI) => f.saveHeader() }

  private def outputFeatureData(f: FeatureI) {
    val fArray: Option[List[FeatureI]] = features.get(f.name)
    fArray match {
      case Some(l) => {
        f.saveInit
        l.foreach { (featInstance: FeatureI) => featInstance.saveDataEfficient(time, c.caseid, c.version) };
        f.saveFinish
      }
      case None => {}
    }
  }

  private def connectScenarios() = featureClasses.foreach { (f: FeatureI) => connectScenario(f) }

  private def connectScenario(feat: FeatureI) {
    val featName: String = feat.name
    var featScenarioFilenames: List[String] = c.featuresScenarioFilenames(featName)
    feat.scenarioFileName = Some(c.featuresScenarioFilenames(featName))
    val endings: List[String] = featScenarioFilenames.map(_.reverse.takeWhile(_ != '.').reverse)

    var scenarioFiles: List[Option[FileInputStream]] = featScenarioFilenames.map {
      x =>
        try { Some(new FileInputStream(x)) }
        catch {
          case fnfe: FileNotFoundException =>
            logger.warn("feature scenario file " + x + " not found. File ignored.")
            None
          case e: Throwable =>
            logger.warn("feature scenario file " + x + " i/o problem(" + e.toString + "). File ignored.")
            None
        }
    }
    
    featScenarioFilenames = featScenarioFilenames.zip(scenarioFiles).filter(x => (x._2 != None)).map(_._1)
    scenarioFiles = scenarioFiles.filter { x => (x != None) }
    val countExistingFiles: Int = scenarioFiles.length

    if ((countExistingFiles == 0) && feat.hasDrivers) {
      scenarioFiles = Nil
      logger.error("No feature scenario file found for " + featName + ", but " + featName + " has drivers (" + feat.drivers.toList + ").")
    }
    if ((countExistingFiles == 0) && !feat.hasDrivers) {
      scenarioFiles = Nil
      logger.warn("No feature scenario file found for " + featName + ", but this is ok as " + featName + " has no required drivers.")
    }
    
    var distinctEndings: List[String] = endings.distinct

    if (distinctEndings.length > 1) {
      logger.error("Found different scenario file types (extensions) for feature " + featName + ": " + featScenarioFilenames + " - this is not supported.")
    }

    var ending: String = if (distinctEndings.length == 0) "" else distinctEndings(0)
    
    var scenarioInput: Option[divaInput] =
      (scenarioFiles, ending) match {
      case ((fis :: x :: nil), "csv") =>
          //logger.warn("multiple feature input files (" + featInputFilenames + ") are not yet supported. No " + featName + " features initialized."); None
          Some(new divaMultiCSVInput(scenarioFiles.map(_.get), featScenarioFilenames, logger))
        case ((fis :: x :: nil), _) =>
          logger.warn("feature input files (" + featScenarioFilenames + ") have unknown type. No " + featName + " features initialized."); None
        case ((fis :: nil), "csv") => Some(new divaCSVInput(fis.get, featScenarioFilenames(0), logger))
        case ((fis :: nil), _) =>
          logger.warn("feature input file " + featScenarioFilenames(0) + " has unknown type. No " + featName + " features initialized."); None
        case (Nil, _) => None
      }

    // read the scenario completely, if there is one
    scenarioInput match {
      case (Some(inp)) => {
        inp.noValueString = c.noValue.toString
        inp.locationWildcard = c.scenarioWildcard
        inp.readMetainformation
        inp.readParameter
        feat.checkDrivers(inp.parameter ++ inp.metainformation)
        inp.readCompletely(c.extendedInputCheck)
        // TODO: check timesteps of scenario and of state for consistency
        checkTimesteps(inp, c.featuresScenarioFilenames(featName))
      }

      case None =>
    }

    feat.scenario = scenarioInput
  }

  private def readFeatureScenario(f: FeatureI) {
    val fArray: Option[List[FeatureI]] = features.get(f.name)
    fArray match {
      case Some(l) => l.foreach { (featInstance: FeatureI) => featInstance.readScenarioValues(featInstance.locationid, time) }
      case None    =>
    }
  }

  private def resetFeatureVariables(f: FeatureI) {
    val fArray: Option[List[FeatureI]] = features.get(f.name)
    fArray match {
      case Some(l) => l.foreach { (featInstance: FeatureI) => featInstance.resetVariables }
      case None    =>
    }
  }

  private def checkTimesteps(inp: divaInput, filenames: List[String]) {

    if (inp.timesteps.size == 0) {
      logger.error("No timesteps found in " + filenames)
    }

    // Not used yet?
    val startTimeOfScenario: collection.mutable.AnyRefMap[String,Int] = inp.timesteps.map { case (key, value) => (key, value.reduceLeft(_ min _)) }
    val endTimeOfScenario: collection.mutable.AnyRefMap[String,Int] = inp.timesteps.map { case (key, value) => (key, value.reduceLeft(_ max _)) }

    // get all timesteps requested by the state configuration which are NOT in the scenario
    // val requestedNonAvailableTimesteps: Array[Int] = timesteps.filterNot(inp.timesteps.contains(_))
    val requestedNonAvailableTimesteps: collection.mutable.AnyRefMap[String,List[Int]] = inp.timesteps.map { case (key, value) => (key,timesteps.filterNot(value.contains(_)).toList) }

    val requestedNonAvailableTimestepsBeforeScenarioStart: collection.mutable.AnyRefMap[String,List[Int]] = requestedNonAvailableTimesteps.map { case (key, value) => (key, requestedNonAvailableTimesteps(key).filter { x =>  x < startTimeOfScenario(key)}) }
    val requestedNonAvailableTimestepsAfterScenarioEnd: collection.mutable.AnyRefMap[String,List[Int]] = requestedNonAvailableTimesteps.map { case (key, value) => (key, requestedNonAvailableTimesteps(key).filter { x =>  x > endTimeOfScenario(key)}) }
    val requestedNonAvailableTimestepsInBetween: collection.mutable.AnyRefMap[String,List[Int]] = requestedNonAvailableTimesteps.map { case (key, value) => (key, requestedNonAvailableTimesteps(key).filter { x =>  ((x <= endTimeOfScenario(key)) && (x >= startTimeOfScenario(key)) )}) }

    if (requestedNonAvailableTimestepsBeforeScenarioStart.values.flatten.size>0) {
      logger.error("model configuration requested timesteps before the first timestep of the scenario " + filenames + " (" + startTimeOfScenario + "): " + requestedNonAvailableTimestepsBeforeScenarioStart)
    }

    if (requestedNonAvailableTimestepsAfterScenarioEnd.values.flatten.size>0) {
      logger.error("model configuration requested timesteps after the last timestep of the scenario " + filenames + " (" + endTimeOfScenario + "): " + requestedNonAvailableTimestepsAfterScenarioEnd)
    }

    if (requestedNonAvailableTimestepsInBetween.values.flatten.size>0) {
      logger.warn("model configuration requested timesteps which are not in the scenario " + filenames + " and will thus be interpolated: " + requestedNonAvailableTimestepsInBetween)
    }

  }

  def accumulateForNormalise[F1 <: FeatureI, F2 <: FeatureI, T: Fractional](fromGetter: F1 => T, toGetter: F2 => T, toSetter: (F2, T) => Unit, fromNormalise: F1 => T)(implicit ev1: Manifest[F1], ev2: Manifest[F2], ev3: Fractional[T], idendify: (F1, F2) => Boolean) {
    import ev3._
    val fromFeatures: List[F1] = getFeatures[F1]
    fromFeatures.foreach {
      fromFeature: F1 =>
        {
          val toFeatures = getFeaturesWith[F2](idendify(fromFeature, _))

          // we should not warn or abort here: there might be no features belonging to fromFeature
          // for instance, not every cls has a delta attached
          if (toFeatures.length > 0) {
            toSetter(toFeatures.head, toGetter(toFeatures.head) + fromGetter(fromFeature) * fromNormalise(fromFeature))
          } else {
            //println("found for " + fromFeature.locationid)
            //println(toFeatures.map(_.locationid))
          }
        }
    }

  }

}