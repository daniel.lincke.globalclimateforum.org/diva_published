/* Automatically generated data features for Riverdistributary
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data

import diva.annotations._
import diva.types._
import diva.types.TypeComputations._
import scala.collection.mutable._


class RiverdistributaryData {

  @Input
  @Parameter
  @Description(value = "Coast line segment to which the riverdistributary belongs")
  var clsid :  String = default[String]

  @Output
  @Input
  @Parameter
  @Description(value = "Length of the riverdistributary.")
  var length :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Flag if a riverdistributary is the root riverdistributary (if not, it is supposed to end up in another riverdistributary)")
  var river_distributary_is_root :  Boolean = default[Boolean]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1")
  var river_impact_length_initial_s1 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s10")
  var river_impact_length_initial_s10 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s100")
  var river_impact_length_initial_s100 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1000")
  var river_impact_length_initial_s1000 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1")
  var river_impact_length_initial_tide :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1")
  var river_impact_length_s1 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s10")
  var river_impact_length_s10 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s100")
  var river_impact_length_s100 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1000")
  var river_impact_length_s1000 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1")
  var river_impact_length_tide :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "")
  var riverdike_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "River dike height")
  var riverdike_height :  Float = default[Float]

  @Variable
  @Description(value = "River dike height after initialisation")
  var riverdike_height_initial :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of incremental riverdike maIntenace")
  var riverdike_increase_maintenance_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Total river dike length")
  var riverdike_length :  Float = 0f

  @Variable
  @Description(value = "Total river dike length after initialisation")
  var riverdike_length_initial :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of riverdike maIntenace")
  var riverdike_maintenance_cost :  Float = 0f

  @Output
  @Input
  @Parameter
  @Description(value = "River feature identifier code, associating each riverdistributary with the corresponding river information.")
  var riverid :  String = default[String]

  @Input
  @Parameter
  @Description(value = "Angle needed for salinisation area calculation")
  var sal_angle :  Float = 40f

  @Output
  @Variable
  @Description(value = "Area influenced by salinisation due to sea level rise")
  var salinity_area :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Length of river influenced by salt water Intrusion.")
  var salinity_intrusion_length :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "cost of surge barrier construction/update")
  var surge_barrier_cost :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "cost of surge barrier maintenance")
  var surge_barrier_maintenance_cost :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "number of surge barriers")
  var surge_barriers_number :  Float = default[Float]

  @Output
  @Input
  @Parameter
  @Description(value = "Surgebarrier feature identifier code, associating each river distributary with the corresponding surgebarrier information.")
  var surgebarrierid :  String = default[String]

  override def clone : RiverdistributaryData = {
    var ret: RiverdistributaryData  = new RiverdistributaryData
    ret.clsid = clsid
    ret.length = length
    ret.river_distributary_is_root = river_distributary_is_root
    ret.river_impact_length_initial_s1 = river_impact_length_initial_s1
    ret.river_impact_length_initial_s10 = river_impact_length_initial_s10
    ret.river_impact_length_initial_s100 = river_impact_length_initial_s100
    ret.river_impact_length_initial_s1000 = river_impact_length_initial_s1000
    ret.river_impact_length_initial_tide = river_impact_length_initial_tide
    ret.river_impact_length_s1 = river_impact_length_s1
    ret.river_impact_length_s10 = river_impact_length_s10
    ret.river_impact_length_s100 = river_impact_length_s100
    ret.river_impact_length_s1000 = river_impact_length_s1000
    ret.river_impact_length_tide = river_impact_length_tide
    ret.riverdike_cost = riverdike_cost
    ret.riverdike_height = riverdike_height
    ret.riverdike_height_initial = riverdike_height_initial
    ret.riverdike_increase_maintenance_cost = riverdike_increase_maintenance_cost
    ret.riverdike_length = riverdike_length
    ret.riverdike_length_initial = riverdike_length_initial
    ret.riverdike_maintenance_cost = riverdike_maintenance_cost
    ret.riverid = riverid
    ret.sal_angle = sal_angle
    ret.salinity_area = salinity_area
    ret.salinity_intrusion_length = salinity_intrusion_length
    ret.surge_barrier_cost = surge_barrier_cost
    ret.surge_barrier_maintenance_cost = surge_barrier_maintenance_cost
    ret.surge_barriers_number = surge_barriers_number
    ret.surgebarrierid = surgebarrierid
    ret
  }

}
