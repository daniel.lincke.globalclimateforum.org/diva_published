package diva.coastalplanemodel

import math._
import diva.toolbox.PartialLinearInterpolator

class CoastalWidth(val pWidths: Array[Float], pHeights: Array[Float])
  extends CoastalExposureI {

  val lCumulativeWiths: Array[Float] = pWidths.scanLeft(0.0f)(_ + _).drop(1)
  lLevels = lCumulativeWiths

  // pExposure values are given in noncummulative manner,
  // so we first cummulate them
  lCumulativeExposure = pHeights.clone

  // pExposure values are given in noncumulative manner,
  // so we first cumulate them
  lPartialLinearInterpolator = new PartialLinearInterpolator[Double](lLevels.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })

  var lPartialLinearInterpolatorInverse: PartialLinearInterpolator[Double] = _

  // the interpolator runs into the not-a-function-problem (thus slopes can be NaN)
  var levelsInverse: Array[Float] = lLevels.clone
  var lCumulativeExposureInverse: Array[Float] = lCumulativeExposure.clone

  // so we manipulate critical values
  for (i <- 1 until lLevels.length) {
    if (lLevels(i) == lLevels(i - 1)) {
      lLevels(i) = lLevels(i) + 0.01f
    }
  }

  lPartialLinearInterpolator = new PartialLinearInterpolator(lLevels.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })
  lPartialLinearInterpolatorInverse = new PartialLinearInterpolator(lCumulativeExposureInverse.map { x => x.toDouble }, levelsInverse.map { x => x.toDouble })

  // cumulative exposure below x
  def cumulativeExposureInverse(x: Exposure): Double = {
    lPartialLinearInterpolatorInverse.value(x) * lExposureFactor
  }

  override def recompute {
    lCumulativeExposure = lExposure.scanLeft(0.0f)(_ + _).drop(1)
    lPartialLinearInterpolator = new PartialLinearInterpolator(lLevels.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })

    lPartialLinearInterpolatorAttenuation = new PartialLinearInterpolator(lLevelsAttenuation.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })

  }

  // we implement a more efficient version of cumulativeDamageWithDike for area
  // as vul=1 and vul'=0 the whole integral is zero, so we do not have to compute it every time
  override def cumulativeDamageWithDike(d: Level, pDikePosition: Level = 0)(x: Level): Float = {
    if (x <= d) return 0
    // For area we have vulnerability=1 and therefore damage=exposure (the integral is zero, see ExposureI)
    depthDamage(0) * cumulativeExposure(x)
  }

  override def migrateBelow(pBelowThisElevation: Float, pAwayRate: Float): Float = 0

  override def clone: CoastalWidth = {
    val ret = new CoastalWidth(pWidths, pHeights)
    ret.deepCopy(this)
    ret
  }

}
