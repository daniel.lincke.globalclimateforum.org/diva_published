package diva.types

import scala.language.implicitConversions

object RiverProtectionMode extends Enumeration {
  type RiverProtectionMode = Value

  // no surge barriers in river mouths, only river dikes
  val OPEN_PROTECTION = Value("OPEN_PROTECTION")
  // only surge barriers in river mouths, no river dikes
  val CLOSED_PROTECTION = Value("CLOSED_PROTECTION")
  // least cost decision surge barriers/river dikes in river mouths
  val CBA_PROTECTION = Value("CBA_PROTECTION")
  // stylized rule 
  val STYLIZED_PROTECTION = Value("STYLIZED_PROTECTION")

  implicit def valueToRiverProtectionMode(v: Value): RiverProtectionMode = v.asInstanceOf[RiverProtectionMode]
  implicit def stringToRiverProtectionMode(s: String): RiverProtectionMode = RiverProtectionMode.withName(s)

}

