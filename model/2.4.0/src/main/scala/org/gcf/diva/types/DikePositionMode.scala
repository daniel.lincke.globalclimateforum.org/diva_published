package diva.types

import scala.language.implicitConversions

object DikePositionMode extends Enumeration {
  type DikePositionMode = Value

  // Linear interpolation 
  val ONLY_COASTAL = Value("ONLY_COASTAL")
  val ALSO_INLAND = Value("ALSO_INLAND")
  
  implicit def valueToDikePositionMode(v: Value): DikePositionMode = v.asInstanceOf[DikePositionMode]
  implicit def stringToDikePositionMode(s: String): DikePositionMode = DikePositionMode.withName(s)

}

