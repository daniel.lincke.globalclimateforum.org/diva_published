package diva.types

object AssetPopulationCorrectionMode extends Enumeration {
  type AssetPopulationCorrectionMode = Value

  val ASSETS_TO_POPULATION = Value("ASSETS_TO_POPULATION")
  val POPULATION_TO_ASSETS = Value("POPULATION_TO_ASSETS")

  implicit def valueToAssetPopulationCorrectionModeMode(v: Value): AssetPopulationCorrectionMode = v.asInstanceOf[AssetPopulationCorrectionMode]
  implicit def stringToAssetPopulationCorrectionMode(s: String): AssetPopulationCorrectionMode = AssetPopulationCorrectionMode.withName(s)

}
