package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI
import scala.reflect.api.materializeTypeTag

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class Delta extends FeatureI {
  type Data = DeltaData
  type DataIO = DeltaDataIO

  val dataTypeTag: ru.TypeTag[DeltaData] = ru.typeTag[DeltaData]
  val dataClassTag: ClassTag[DeltaData] = classTag[DeltaData]
  val dataTypeSave: ru.Type = ru.typeOf[DeltaData]

  var mustHaveInputFile = false
  
  var data: DeltaData = null
  var dataIO: DeltaDataIO = null
  
  var clss: Array[Cls] = Array()
  
  def init() {
    data = new DeltaData
    dataIO = new DeltaDataIO    
  }

  def newInstanceCopy: Delta = {
    val ret = new Delta
    ret.data = new DeltaData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }
}