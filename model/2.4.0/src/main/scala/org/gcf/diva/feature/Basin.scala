package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI

import diva.module.erosion.AsmitaCoefficients

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class Basin extends FeatureI {
  type Data = BasinData
  type DataIO = BasinDataIO

  val dataTypeTag: ru.TypeTag[BasinData] = ru.typeTag[BasinData]
  val dataClassTag: ClassTag[BasinData] = classTag[BasinData]
  val dataTypeSave: ru.Type = ru.typeOf[BasinData]

  var mustHaveInputFile = false
  
  var data: BasinData = null
  var dataIO: BasinDataIO = null
  
  var asmitaCoefficients: AsmitaCoefficients = null
  var clss: List[Cls] = List()
  
  def init() {
    data = new BasinData
    dataIO = new BasinDataIO    
  }

  def newInstanceCopy: Basin = {
    val ret = new Basin
    ret.data = new BasinData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }
}