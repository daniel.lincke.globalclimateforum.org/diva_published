package diva.coastalplanemodel

import diva.model.hasLogger
import diva.toolbox.NumericalIntegration._
import diva.toolbox.PartialLinearInterpolator
import diva.toolbox.exceptions.arrayLengthsNotEqualException
import diva.types.TypeComputations._

import scala.collection.mutable.Buffer

trait CoastalExposureI extends hasLogger {

  type Level = Float
  type Exposure = Float

  var lLevels: Array[Level] = new Array[Level](0)
  var lLevelsAttenuation: Array[Level] = new Array[Level](0)
  var lExposure: Array[Exposure] = new Array[Exposure](0)
  var lCumulativeExposure: Array[Exposure] = new Array[Exposure](0)
  var lExposureFactor: Float = 1.0f
  var integrationSteps = 50
  var lPartialLinearInterpolator: PartialLinearInterpolator[Double] = _
  var lPartialLinearInterpolatorAttenuation: PartialLinearInterpolator[Double] = _
  var halfDamageDepth: Float = 1.0f
  var nondamageableFration: Float = 0.0f

  def depthDamage(x: Level): Float = 1.0f
  def depthDamageDerivation(x: Level): Float = 0.0f

  def recompute {
    lCumulativeExposure = lExposure.scanLeft(0.0f)(_ + _).drop(1)
    lPartialLinearInterpolator = new PartialLinearInterpolator(lLevels.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })
    lPartialLinearInterpolatorAttenuation = new PartialLinearInterpolator(lLevelsAttenuation.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })
  }

  def rescale(pFactor: Float) {
    lExposure = lExposure.map((x: Exposure) => x * pFactor)
    lCumulativeExposure = lExposure.scanLeft(0.0f)(_ + _).drop(1)
    lPartialLinearInterpolator.rescale(pFactor)
    lPartialLinearInterpolatorAttenuation.rescale(pFactor)
  }

  def rescaleAbove(pFactor: Float, pElevation: Float) {
    //lExposure = lLevels.zip(lExposure).map(x => if(x._1 > pElevation) (x._1, x._2 * pFactor) else x).unzip._2
    for (i <- 0 until lLevels.length - 1) {
      if (lLevels(i) > pElevation) {
        lExposure(i) = lExposure(i) * pFactor
      }
    }
    lCumulativeExposure = lExposure.scanLeft(0.0f)(_ + _).drop(1)
    lPartialLinearInterpolator.rescaleAbove(pFactor, pElevation)
    lPartialLinearInterpolatorAttenuation.rescaleAbove(pFactor, pElevation)
  }

  def rescaleBelow(pFactor: Float, pElevation: Float) {
    //lExposure = lLevels.zip(lExposure).map(x => if(x._1 > pElevation) (x._1, x._2 * pFactor) else x).unzip._2
    for (i <- 0 until lLevels.length - 1) {
      if (lLevels(i) <= pElevation) {
        lExposure(i) = lExposure(i) * pFactor
      }
    }
    recompute
  }

  def rescaleStepwiseAbove(pLevels: Array[(Float, Float)], growthFactor: Array[Float], pElevation: Float) {
    //lExposure = lLevels.zip(lExposure).map(x => if(x._1 > pElevation) (x._1, x._2 * pFactor) else x).unzip._2

    for (i <- 0 until lLevels.length - 1) {
      if (lLevels(i) > pElevation) {
        findFactor(lLevels(i), pLevels, growthFactor)
        lExposure(i) = lExposure(i) * findFactor(lLevels(i), pLevels, growthFactor)
      } /*
      for (j <- 0 until pLevels.length - 1) {
        if ((lLevels(i) > pUpperElevation) && (lLevels(i) <= pUpperElevation)) {
          lExposure(i) = lExposure(i) * pFactor
        }
      }*/
    }
    recompute
  }

  def addPoint(pLevel: Level, pExpo: Exposure) {
    val ind_ : Int = lLevels.indexWhere((x: Level) => x > pLevel) //- 1
    val ind = if (ind_ < 0) 0 else ind_
    var lLevels_ = (lLevels.slice(0, ind) :+ pLevel) ++ lLevels.slice(ind, lLevels.length)
    var lLevelsAttenuation_ = (lLevelsAttenuation.slice(0, ind) :+ pLevel) ++ lLevelsAttenuation.slice(ind, lLevelsAttenuation.length)
    var lExposure_ = (lExposure.slice(0, ind) :+ pExpo) ++ lExposure.slice(ind, lExposure.length)
    lLevels = lLevels_
    lLevelsAttenuation = lLevelsAttenuation_
    lExposure = lExposure_

    lPartialLinearInterpolator.addPoint(pLevel, pExpo)
  }

  def resample(pInBetweenPoints: Int) {
    var lLevels_ : List[Level] = List[Level]()
    var lLevelsAttenuation_ : List[Level] = List[Level]()
    var lCumulativeExposure_ : List[Exposure] = List[Exposure]()
    for (i <- 0 until lLevels.length - 1) {
      val fromLevel = lLevels(i)
      val toLevel = lLevels(i + 1)
      val fromLevelAttenuation = lLevelsAttenuation(i)
      val toLevelAttenuation = lLevelsAttenuation(i + 1)
      val step: Float = (toLevel.asInstanceOf[Float] - fromLevel.asInstanceOf[Float]) / (pInBetweenPoints.asInstanceOf[Float])
      val stepAttenuation: Float = (toLevelAttenuation.asInstanceOf[Float] - fromLevelAttenuation.asInstanceOf[Float]) / (pInBetweenPoints.asInstanceOf[Float])
      for (j <- 0 until pInBetweenPoints) {
        lLevels_ = (fromLevel + (j * step)) :: lLevels_
        lLevelsAttenuation_ = (fromLevelAttenuation + (j * stepAttenuation)) :: lLevelsAttenuation_
        lCumulativeExposure_ = cumulativeExposure((fromLevel + (j * step))) :: lCumulativeExposure_
      }
    }
    if (lLevels.length > 0) {
      lLevels_ = lLevels(lLevels.length - 1) :: lLevels_
      lLevelsAttenuation_ = lLevelsAttenuation(lLevelsAttenuation.length - 1) :: lLevelsAttenuation_
      lCumulativeExposure_ = cumulativeExposure(lLevels(lLevels.length - 1)) :: lCumulativeExposure_
    }

    lLevels = lLevels_.reverse.toArray
    lLevelsAttenuation = lLevelsAttenuation_.reverse.toArray
    lCumulativeExposure = lCumulativeExposure_.reverse.toArray

    lExposure = lCumulativeExposure.clone
    for (i <- 0 until lExposure.length - 1) {
      lExposure(i + 1) = lCumulativeExposure(i + 1) - lCumulativeExposure(i)
    }
  }

  // cumulative exposure below x
  def cumulativeExposure(x: Level): Exposure = {
    lPartialLinearInterpolator.value(x).toFloat * lExposureFactor
  }

  def cumulativeExposureAttenuation(x: Level): Exposure = {
    lPartialLinearInterpolatorAttenuation.value(x).toFloat * lExposureFactor
  }

  // cumulative exposure below x
  /*
  def cumulativeExposureInverse(x: Exposure): Float = {
    lPartialLinearInterpolatorInverse.value(x).toFloat * lExposureFactor
  }*/

  // cumulative exposure only below a certain elevation limit (needed to compute damages in front of and behind dike)
  def cumulativeExposureOnlyBelow(limit: Level)(x: Level): Exposure = {
    val min: Level = scala.math.min(limit, x)
    lPartialLinearInterpolatorAttenuation.value(x).toFloat * lExposureFactor
  }

  // cumulative exposure only above a certain elevation limit (needed to compute damages in front of and behind dike)
  def cumulativeExposureOnlyAbove(limit: Level)(x: Level): Exposure = {
    if (x <= limit) 0 else cumulativeExposureAttenuation(x) - cumulativeExposureAttenuation(limit)
  }

  def cumulativeExposureRespectingVulnerability(pWaterLevel: Level)(x: Level): Exposure = {
    depthDamageDerivation(pWaterLevel - x) * cumulativeExposureAttenuation(x)
  }

  def cumulativeExposureRespectingVulnerabilityOnlyBelow(limit: Level)(pWaterLevel: Level)(x: Level): Exposure = {
    depthDamageDerivation(pWaterLevel - x) * cumulativeExposureOnlyBelow(limit)(x)
  }

  def cumulativeExposureRespectingVulnerabilityOnlyAbove(limit: Level)(pWaterLevel: Level)(x: Level): Exposure = {
    depthDamageDerivation(pWaterLevel - x) * cumulativeExposureOnlyAbove(limit)(x)
  }

  def cumulativeDamage(pWaterLevel: Level): Float = {
    cumulativeDamageWithDike(0, 0)(pWaterLevel)
  }

  def cumulativeDamageWithDike(pDikeHeight: Level, pDikePosition: Level)(pWaterLevel: Level): Float = {
    // catch the case of falling sea-level ... otherwise we might get negative damage.
    if (pWaterLevel < 0)
      0
    else {
      cumulativeDamageInFrontOfDike(pDikeHeight, pDikePosition)(pWaterLevel) +
        cumulativeDamageBehindDike(pDikeHeight, pDikePosition)(pWaterLevel)
    }
  }

  // These two are computationally expensive, as the expected damage is computed as an integral over the
  // cumulative damage. And as the cumulative is an integral itself, we compute an integral over an integral,
  // and that for every CLS. So if we have a more efficient computation for classes that implement this trait,
  // we will override this function.
  protected def cumulativeDamageInFrontOfDike(pDikeHeight: Level, pDikePosition: Level)(pWaterLevel: Level): Float = {
    if (pDikePosition == 0)
      0
    else
      depthDamage(0) * cumulativeExposureOnlyBelow(pDikePosition)(pWaterLevel) + integrate[Float](cumulativeExposureRespectingVulnerabilityOnlyBelow(pDikePosition)(pWaterLevel), 0, pWaterLevel, integrationSteps)
  }

  protected def cumulativeDamageBehindDike(pDikeHeight: Level, pDikePosition: Level)(pWaterLevel: Level): Float = {
    val realDikeHeight = pDikeHeight + pDikePosition
    if (pWaterLevel <= realDikeHeight)
      0
    else
      depthDamage(0) * cumulativeExposureOnlyAbove(pDikePosition)(pWaterLevel) + integrate[Float](cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel), 0, pWaterLevel, integrationSteps)
  }

  protected def cumulativeDamageBehindDike2(pDikeHeight: Level, pDikePosition: Level)(pWaterLevel: Level): Float = {
    val realDikeHeight = pDikeHeight + pDikePosition
    println("depthDamage(0) = " + depthDamage(0))
    println("cumulativeExposureOnlyAbove(pDikePosition)(pWaterLevel) = " + cumulativeExposureOnlyAbove(pDikePosition)(pWaterLevel))
    println("depthDamage(0) = " + depthDamage(0))
    println("pDikePosition = " + pDikePosition)
    println("pWaterLevel = " + pWaterLevel)
    println("cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel)(0) = " + cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel)(0))
    println("cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel)(1) = " + cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel)(1))
    println("cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel)(2) = " + cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel)(2))
    println("cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel)(3) = " + cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel)(3))
    println("integrate(cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel), 0, pWaterLevel, integrationSteps) = " + integrate[Float](cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel), 0, pWaterLevel, integrationSteps))

    if (pWaterLevel <= realDikeHeight)
      0
    else
      depthDamage(0) * cumulativeExposureOnlyAbove(pDikePosition)(pWaterLevel) + integrate[Float](cumulativeExposureRespectingVulnerabilityOnlyAbove(pDikePosition)(pWaterLevel), 0, pWaterLevel, integrationSteps)
  }

  def migrateBelow(pBelowThisElevation: Float, pAwayRate: Float): Float = {
    // how many people/assets have to be migrated?
    val numberBelow: Float = cumulativeExposure(pBelowThisElevation)
    // how many people/assets migrate away from coast?
    val numberToMigrateAway: Double = numberBelow * pAwayRate
    // how many people/assets stay at coast but migrate to higher elevation levels?
    val numberToRedisrtibute: Double = numberBelow - numberToMigrateAway
    // and how many are redistributed on the remaining elevation levels?
    val numberOfRemainingElevationLevels: Double = lLevels.length - (lPartialLinearInterpolator.index(pBelowThisElevation) + 1)
    val index: Int = lPartialLinearInterpolator.index(pBelowThisElevation)

    for (i <- 0 to index) {
      lExposure(i) = 0.0f
    }

    val peopleOnNextLowerLevel = lCumulativeExposure(index)
    if (index >= (lLevels.length - 1)) return numberBelow
    lExposure(index + 1) = lExposure(index + 1) - (numberBelow - peopleOnNextLowerLevel)

    var remaining: Double = lExposure.drop(index).foldLeft(0.0d)(_ + _)

    for (i <- (index + 1) until (lLevels.length)) {
      lExposure(i) = lExposure(i) +
        { if (remaining == 0) 0 else ((lExposure(i) / remaining) * numberToRedisrtibute) }
    }
    lCumulativeExposure = lExposure.scanLeft(0.0f)(_ + _).drop(1)
    if (!lLevels.contains(pBelowThisElevation)) {
      addPoint(pBelowThisElevation, 0)
    } else {
      lExposure(index) = 0
      recompute
    }
    lPartialLinearInterpolator = new PartialLinearInterpolator(lLevels.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })
    //println("sum = " + lCumulativeExposure(lCumulativeExposure.length - 1))
    //println("total sum = " + (lCumulativeExposure(lCumulativeExposure.length - 1) + numberOfPeopleToMigrateAway))

    //addPoint(pBelowThisElevation,0)

    numberBelow.asInstanceOf[Double]
  }

  // This function just migrates people back to the coastal zone
  // Attention: It does not check anything, it asumes the user who calls it knows what he does
  def migrateBackBelow(pBelowThisElevation: Double, number: Double) {
    val index: Int = lPartialLinearInterpolator.index(pBelowThisElevation)
    lExposure(index) = number
    lCumulativeExposure = lExposure.scanLeft(0.0f)(_ + _).drop(1)
    recompute
  }

  // ToDo: Finish
  // now it is a dirty hack, might fail in strange cases?
  // in addition, pAwayRate has no effect by now.
  def migrateBelowBetween(pMigrateBelowThisElevation: Float, pDistributeBelowThisElevation: Float, pAwayRate: Float): Float = {
    // how many people/assets have to be migrated?
    val numberBelow: Float = cumulativeExposure(pMigrateBelowThisElevation)
    // how many people/assets migrate away from coast?
    val numberToMigrateAway: Float = numberBelow * pAwayRate
    // how many people/assets stay at coast but migrate to higher elevation levels?
    val numberToRedisrtibute: Float = numberBelow - numberToMigrateAway
    // and how many are redistributed on the remaining elevation levels?
    val index1: Int = lPartialLinearInterpolator.index(pMigrateBelowThisElevation)
    val index2: Int = lPartialLinearInterpolator.index(pDistributeBelowThisElevation)
    val numberBelowDistributionLimit: Float = cumulativeExposure(pDistributeBelowThisElevation)

    // delete all removed people/assets
    for (i <- 0 to index1) {
      lExposure(i) = 0.0d
    }

    if (index2 < lLevels.length - 1) {
      val levelFraction: Float = (pDistributeBelowThisElevation - lLevels(index2)) / (lLevels(index2 + 1) - lLevels(index2))
      lExposure(index2 + 1) = lExposure(index2 + 1) * (1 - levelFraction)
    }

    var lExposureTemp = lExposure.toBuffer
    var lLevelsTemp = lLevels.toBuffer
    var lLevelsAttenuationTemp = lLevelsAttenuation.toBuffer

    for (i <- (index1 + 1) to (index2)) {
      lExposureTemp.remove(index1 + 1)
      lLevelsTemp.remove(index1 + 1)
      lLevelsAttenuationTemp.remove(index1 + 1)
    }

    lExposure = lExposureTemp.toArray
    lLevels = lLevelsTemp.toArray
    lLevelsAttenuation = lLevelsAttenuationTemp.toArray
    
    var rc: Boolean = false

    if (!lLevels.contains(pMigrateBelowThisElevation)) {
      addPoint(pMigrateBelowThisElevation, 0)
    } else {
      lExposure(index1) = 0
      rc = true
    }

    if (!lLevels.contains(pDistributeBelowThisElevation)) {
      addPoint(pDistributeBelowThisElevation, numberBelowDistributionLimit)
    } else {
      lExposure(index2) = numberBelowDistributionLimit
      rc = true
    }
    if (rc) recompute
    numberBelow
  }

  //override def clone: CoastalExposureI

  def deepCopy(ce: CoastalExposureI) {
    this.lLevels = ce.lLevels.clone
    this.lExposure = ce.lExposure.clone
    this.lCumulativeExposure = ce.lCumulativeExposure.clone
    this.lExposureFactor = ce.lExposureFactor
    this.integrationSteps = ce.integrationSteps
    this.lPartialLinearInterpolator = ce.lPartialLinearInterpolator
    //this.lPartialLinearInterpolatorInverse = ce.lPartialLinearInterpolatorInverse
    this.halfDamageDepth = ce.halfDamageDepth
    this.nondamageableFration = ce.nondamageableFration
  }

  // for debugging:
  def printForDebug() {
    print("Levels: \t")
    lLevels.foreach { x => print(x + ", ") }
    println()
    print("LevelsAttenuation: \t")
    lLevelsAttenuation.foreach { x => print(x + ", ") }
    println()
    print("Exposure: \t")
    lExposure.foreach { x => print(x + ", ") }
    println()
    print("Cummulative exposure: \t")
    lCumulativeExposure.foreach { x => print(x + ", ") }
    println()
  }

  def add(cei: CoastalExposureI) {

    // ???
    var lLevelsExposureZipped1: Array[(Float, Float)] = lLevels zip lExposure
    var lLevelsExposureZipped2: Array[(Float, Float)] = cei.lLevels zip cei.lExposure
    var lLevelsExposureZipped3: Array[(Float, Float)] = (lLevelsExposureZipped1 ++ lLevelsExposureZipped2).sortBy(_._1)
    var lLevelsExposureZipped4: Buffer[(Float, Float)] = lLevelsExposureZipped3.toBuffer

    var lLevelsAttenuationAll: Buffer[Float] = (lLevelsAttenuation ++ cei.lLevelsAttenuation).sortWith(_ < _).toBuffer
    //var lLevelsExposureZipped4: Buffer[(Float, Float)] = lLevelsExposureZipped3.toBuffer

    var i: Int = 0
    while (i < (lLevelsExposureZipped4.length - 1)) {
      if (lLevelsExposureZipped4(i)._1 == lLevelsExposureZipped4(i + 1)._1) {
        lLevelsExposureZipped4(i) = (lLevelsExposureZipped4(i)._1, (lLevelsExposureZipped4(i)._2 + lLevelsExposureZipped4(i + 1)._2))
        lLevelsExposureZipped4.remove(i + 1)
        lLevelsAttenuationAll.remove(i + 1)
        //i = i - 1
      }
      i = i + 1
    }

    lLevels = lLevelsExposureZipped4.unzip._1.toArray
    lLevelsAttenuation = lLevelsAttenuationAll.toArray
    lExposure = lLevelsExposureZipped4.unzip._2.toArray
    recompute
  }

  private def findFactor(lLevel: Level, pLevels: Array[(Level, Level)], growthFactor: Array[Float]): Float = {
    for (i <- 0 until pLevels.size) {
      if ((pLevels(i)._1 <= lLevel) && (lLevel < pLevels(i)._2)) {
        return growthFactor(i);
      }
    }
    1.0f
  }
}