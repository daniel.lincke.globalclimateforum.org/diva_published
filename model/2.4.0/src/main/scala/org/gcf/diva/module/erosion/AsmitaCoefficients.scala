package diva.module.erosion

class AsmitaCoefficients {
  
  var m1: Float = 0f
  var m2: Float = 0f
  var m3: Float = 0f
  var m4: Float = 0f
  var m5: Float = 0f
  var m6: Float = 0f
  var m7: Float = 0f
  var m8: Float = 0f
  var m9: Float = 0f
  var m10: Float = 0f
  var m11: Float = 0f
  var m12: Float = 0f
  
  var vf: Float = 0f
  var vfe: Float = 0f
  var vc: Float = 0f
  var vce: Float = 0f
  var nc: Float = 0f
  var nd: Float = 0f
  var vde: Float = 0f
  var nf: Float = 0f
  var vd: Float = 0f
  var af: Float = 0f
  var ac: Float = 0f
  var ad: Float = 0f
  var hf: Float = 0f
  
}