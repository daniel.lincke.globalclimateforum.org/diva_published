package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class City extends FeatureI {
  type Data = CityData
  type DataIO = CityDataIO

  val dataTypeTag: ru.TypeTag[CityData] = ru.typeTag[CityData]
  val dataClassTag: ClassTag[CityData] = classTag[CityData]
  val dataTypeSave: ru.Type = ru.typeOf[CityData]

  var mustHaveInputFile = false
  
  var data: CityData = null
  var dataIO: CityDataIO = null
  
  var clss: Array[Cls] = Array()
  var country: Country = _
  
  
  def init() {
    data = new CityData
    dataIO = new CityDataIO    
  }

  def newInstanceCopy: City = {
    val ret = new City
    ret.data = new CityData
    ret.dataIO = dataIO  
    ret.deepCopy(this)
    ret
  }
}