/* Automatically generated data IO for Riverdistributary
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data.io

import diva.io._
import diva.types.InterpolationMode.InterpolationMode
import diva.feature.data.RiverdistributaryData

class RiverdistributaryDataIO extends FeatureDataIO[RiverdistributaryData] {

  def writeData(out: divaOutput, d: RiverdistributaryData) {
    out.write(d.length)
    out.write(d.river_impact_length_initial_s1)
    out.write(d.river_impact_length_initial_s10)
    out.write(d.river_impact_length_initial_s100)
    out.write(d.river_impact_length_initial_s1000)
    out.write(d.river_impact_length_initial_tide)
    out.write(d.river_impact_length_s1)
    out.write(d.river_impact_length_s10)
    out.write(d.river_impact_length_s100)
    out.write(d.river_impact_length_s1000)
    out.write(d.river_impact_length_tide)
    out.write(d.riverdike_cost)
    out.write(d.riverdike_height)
    out.write(d.riverdike_increase_maintenance_cost)
    out.write(d.riverdike_length)
    out.write(d.riverdike_maintenance_cost)
    out.write(d.riverid)
    out.write(d.salinity_area)
    out.write(d.salinity_intrusion_length)
    out.write(d.surge_barrier_cost)
    out.write(d.surge_barrier_maintenance_cost)
    out.write(d.surge_barriers_number)
    out.write(d.surgebarrierid)
  }

  def writeSelectedData(out: divaOutput, d: RiverdistributaryData, ignoreThisOutput: Array[Boolean]) {
    var c: Int = 0
    if (!ignoreThisOutput(c)) {out.write(d.length)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_initial_s1)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_initial_s10)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_initial_s100)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_initial_s1000)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_initial_tide)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_s1)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_s10)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_s100)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_s1000)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_tide)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_height)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_increase_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_length)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverid)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.salinity_area)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.salinity_intrusion_length)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barriers_number)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surgebarrierid)}; c+=1
  }

  def readInputValue(in: divaInput, d:RiverdistributaryData, parName: String, locid: String, time: Int) {
    if (parName=="clsid") {d.clsid = in.getInputValue[String]("clsid", locid, time)}
    else if (parName=="length") {d.length = in.getInputValue[Float]("length", locid, time)}
    else if (parName=="river_distributary_is_root") {d.river_distributary_is_root = in.getInputValue[Boolean]("river_distributary_is_root", locid, time)}
    else if (parName=="riverid") {d.riverid = in.getInputValue[String]("riverid", locid, time)}
    else if (parName=="sal_angle") {d.sal_angle = in.getInputValue[Float]("sal_angle", locid, time)}
    else if (parName=="surgebarrierid") {d.surgebarrierid = in.getInputValue[String]("surgebarrierid", locid, time)}
    // else println("error:" + parName + " not found in Riverdistributary")
  }

  def readOptionInputValue(in: divaInput, d:RiverdistributaryData, parName: String, locid: String, time: Int) {
  }

  def readScenarioValue(in: divaInput, d:RiverdistributaryData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

  def readOptionScenarioValue(in: divaInput, d:RiverdistributaryData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

}
