package diva.coastalplanemodel

import scala.math.abs
import scala.math.log

import diva.toolbox.PartialLinearInterpolator

class CoastalAssets(val pHeights: Array[Float], val pHeightsAttenuation: Array[Float], pExposedAssets: Array[Float])
  extends CoastalExposureI {

  lLevels = pHeights.clone
  lLevelsAttenuation = pHeightsAttenuation.clone
  lExposure = pExposedAssets.clone

  // pExposure values are given in noncumulative manner,
  // so we first cumulate them

  lCumulativeExposure = pExposedAssets.scanLeft(0.0f)(_ + _).drop(1)
  lPartialLinearInterpolator = new PartialLinearInterpolator(lLevels.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })
  lPartialLinearInterpolatorAttenuation = new PartialLinearInterpolator(lLevelsAttenuation.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })

  // overwrite vulnerability function definition
  // halfDamageDepth is the parameter for the depth where half
  // of the exposed assets are destroyed
  override def depthDamage(depth: Level): Float = {
    ((depth / halfDamageDepth) / (1f + depth / halfDamageDepth)) * (1 - nondamageableFration)
  }

  override def depthDamageDerivation(depth: Level): Float = {
    //    (1 / halfDamageDepth) / ((1f + depth / halfDamageDepth) * (1f + depth / halfDamageDepth))
    ((-nondamageableFration + 1) * halfDamageDepth) / ((depth + halfDamageDepth) * (depth + halfDamageDepth))
  }

  // overwrite cumulative damage with a more efficient version
  // to switch to the old/unefficient version just comment/rename this function
  // this version is much simpler than the one below ...
  // as cudam(l) = integral(0 to l)[(sens(l − e) ∗ cuexpo' e)] de
  //             = sens(0) ∗ cuexpo(l) + integral(0 to l) [(sens'(l − e) ∗ cuexpo e)] de
  // Here we implement the first version, below the second one. And as we interpolate cuexpo linear between eleveation levels,
  // cuexpo' is just a constant value
  /*
  override def cumulativeDamageWithDikeOld(pDikeHeight: Level, pDikePosition: Level = 0)(pWaterLevel: Level): Float = {
    if (pWaterLevel <= pDikeHeight) return 0
        // x: elevation
    // Stammfunktion F(x) of  f(x) = vul(l-x) * cumulativeExposure'(x)
    //                             = vul(l-x) * (a*x + b)'
    //                 = a * vul(l-x)
    //                 = a * ((l-x)/h) / (1 +((l-x)/h))
    // F(x) = a * (h * ln(∣x−l−h∣) + x)
    // where a,b = slope, interception of the linear exposure
    // and h = halfDamageDepth
    def integralFunction(slope: Float)(x: Float): Float = slope * (halfDamageDepth * log(abs(x - pWaterLevel - halfDamageDepth)) + x)

    var integral: Float = 0.0
    val index: Int = lPartialLinearInterpolator.index(pWaterLevel)

    // We do piecewise: integral = integral + (F(b) - F(a))
    // where b = right bound and a = left bound
    // Note that these bounds are from the array of levels, so from 1.5,2.5,3.5, ...
    for (i <- 0 to (index - 1)) {
      integral = integral + (integralFunction(lPartialLinearInterpolator.lSlopes(i))(lLevels(i + 1)))
      integral = integral - (integralFunction(lPartialLinearInterpolator.lSlopes(i))(lLevels(i)))
    }

    // For the Last piece, we do not have to compute the whole area but only up to pWaterLevel
    integral = integral + (integralFunction(lPartialLinearInterpolator.lSlopes(index))(pWaterLevel))
    integral = integral - (integralFunction(lPartialLinearInterpolator.lSlopes(index))(lLevels(index)))

    integral
  }
  */

  override def cumulativeDamageInFrontOfDike(pDikeHeight: Level, pDikePosition: Level)(pWaterLevel: Level): Float = {
    if (pDikePosition == 0)
      0
    else
      integralComputation(0, pDikePosition)(pWaterLevel).toFloat
  }

  override def cumulativeDamageBehindDike(pDikeHeight: Level, pDikePosition: Level)(pWaterLevel: Level): Float = {
    val realDikeHeight = pDikeHeight + pDikePosition
    if (pWaterLevel <= realDikeHeight)
      0
    else {
      integralComputation(pDikePosition, pWaterLevel)(pWaterLevel).toFloat
    }
  }

  private def integralComputation(from: Level, to: Level)(pWaterLevel: Level): Double = {
    // x: elevation
    // Stammfunktion F(x) of  f(x) = vul(l-x) * cumulativeExposure'(x)
    //                             = vul(l-x) * (a*x + b)'
    //                 = a * vul(l-x)
    //                 = a * ((l-x)/h) / (1 +((l-x)/h))
    // F(x) = a * (h * ln(∣x−l−h∣) + x)
    // where a,b = slope, interception of the linear exposure
    // and h = halfDamageDepth
    def integralFunction(slope: Double)(x: Double): Double = slope * (halfDamageDepth * log(abs(x - pWaterLevel - halfDamageDepth)) + x)

    var integral: Double = 0.0d
    val indexStart: Int = lPartialLinearInterpolatorAttenuation.index(from)
    val indexEnd: Int = lPartialLinearInterpolatorAttenuation.index(scala.math.max(pWaterLevel, to))

    // We do piecewise: integral = integral + (F(b) - F(a))
    // where b = right bound and a = left bound
    // Note that these bounds are from the array of levels, so from 1.5,2.5,3.5, ...
    for (i <- indexStart to (indexEnd - 1)) {
      integral = integral + (integralFunction(lPartialLinearInterpolatorAttenuation.lSlopes(i))(lLevels(i + 1)))
      integral = integral - (integralFunction(lPartialLinearInterpolatorAttenuation.lSlopes(i))(lLevels(i)))
    }

    // For the Last piece, we do not have to compute the whole area but only up to pWaterLevel
    integral = integral + (integralFunction(lPartialLinearInterpolatorAttenuation.lSlopes(indexEnd))(pWaterLevel))
    integral = integral - (integralFunction(lPartialLinearInterpolatorAttenuation.lSlopes(indexEnd))(lLevels(indexEnd)))

    integral
  }

  override def clone: CoastalAssets = {
    val ret = new CoastalAssets(lLevels.map { x => x.toFloat }, lLevelsAttenuation.map { x => x.toFloat }, lExposure.map { x => x.toFloat })
    ret.deepCopy(this)
    ret
  }

}