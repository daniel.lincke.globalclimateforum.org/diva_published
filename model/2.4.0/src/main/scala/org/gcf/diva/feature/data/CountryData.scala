/* Automatically generated data features for Country
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data

import diva.annotations._
import diva.types._
import diva.types.TypeComputations._
import scala.collection.mutable._


class CountryData {

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Land area below ? m (ignoring see dikes)")
  var area_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Land area below the one in ? flood level (ignoring see dikes)")
  var area_below_h :  Map[Int ,Float] = default[Map[Int ,Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets below ? m (ignoring see dikes)")
  var assets_below_ :  Map[Float,Float] = default[Map[Float,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Assets below the one in ? flood level (ignoring see dikes)")
  var assets_below_h :  Map[Int ,Float] = default[Map[Int ,Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets in developable areas below ? m (ignoring see dikes)")
  var assets_developable_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets in developed areas below ? m (ignoring see dikes)")
  var assets_developed_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Variable
  @Description(value = "Market share of International tourist arrivals")
  var attract :  Float = default[Float]

  @Parameter
  @Description(value = "Initial market share of International tourist arrivals")
  var attract0 :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Costs of beach nourishment")
  var beachnourishment_cost :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "")
  var beachnourishment_length_beach :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "")
  var beachnourishment_length_shore :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Recommended nourishment")
  var beachnourishment_volume :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Buddhism dummy")
  var buddism :  Int = default[Int]

  @Input
  @Parameter
  @Description(value = "Christianity dummy")
  var christianity :  Int = default[Int]

  @Input
  @Variable
  @Description(value = "Index of civil liberty")
  var civlib :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "")
  var coastlength :  Float = 0f

  @Input
  @Variable
  @Description(value = "Index of corruption")
  var corrupt :  Float = default[Float]

  @Input
  @Variable
  @Description(value = "Index of democracy")
  var democracy :  Float = default[Float]

  @Variable
  @Description(value = "Number of International tourist departures")
  var depart :  Float = default[Float]

  @Parameter
  @Description(value = "Initial number of International tourist departures")
  var depart0 :  Int = default[Int]

  @Input
  @Variable
  @Description(value = "Index of economic freedom")
  var ecfreedom :  Float = default[Float]

  @Input
  @OptionalParameter
  @Description(value = "Fraction of undeveloped but buildable area in the coastal zone")
  var fraction_developable :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Fraction of developed area in the coastal zone")
  var fraction_developed :  Option[Float] = default[Option[Float]]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "GDP")
  var gdp :  Float = default[Float]

  @Output
  @Input
  @Variable
  @Description(value = "Average annual income")
  var gdpc :  Float = default[Float]

  @Driver
  @Interpolate(mode = "RIGHT")
  @Description(value = "Annual growth rate of GDP per capita")
  var gdpcgrowth :  Float = 0f
  var gdpcgrowth_memory: Map[Int ,Float] = Map()

  @Input
  @Variable
  @Description(value = "Gini index of income distribution")
  var gini :  Float = default[Float]

  @Input
  @Variable
  @Description(value = "Index of government quality")
  var govqual :  Float = default[Float]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Storm surge height for a ? in 1 year surge, allowing for relative sea-level rise.")
  var h :  Map[Int ,Float] = default[Map[Int ,Float]]

  @Variable
  @Description(value = "Storm surge height for a 1 in 1 year surge, allowing for relative sea-level rise.")
  var h1 :  Float = default[Float]

  @Variable
  @Description(value = "Storm surge height for a 1 in 10 year surge, allowing for relative sea-level rise.")
  var h10 :  Float = default[Float]

  @Variable
  @Description(value = "Storm surge height for a 1 in 100 year surge, allowing for relative sea-level rise.")
  var h100 :  Float = default[Float]

  @Variable
  @Description(value = "Storm surge height for a 1 in 1000 year surge, allowing for relative sea-level rise.")
  var h1000 :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Hinduism dummy")
  var hinduism :  Int = default[Int]

  @Input
  @Variable
  @Description(value = "Index of individualism")
  var individ :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Islam dummy")
  var islam :  Int = default[Int]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total land loss due to direct and indirect erosion, including the benefits of beach nourishment")
  var landloss_erosion_total :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Land area that is not protected but below the one in one flood level at initialisation")
  var landloss_initial_land_below_S1 :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Change in land area below the one in one flood level, taking Into account see dikes")
  var landloss_submergence :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total costs of land loss due to erosion")
  var landlosscost_erosion_total :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Change in land area below the one in one flood level, taking Into account see dikes")
  var landlosscost_submergence :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "latitude of the capital")
  var lati :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Length of the proctected coastline.")
  var length_protected :  Float = default[Float]

  @Input
  @Variable
  @Description(value = "Expected life time at birth")
  var lifeexp :  Float = default[Float]

  @Input
  @Variable
  @Description(value = "Percentage of adult population able to read and write")
  var literacy :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "longitude of the capital")
  var longi :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Hofstede (https://geerthofstede.com/culture-geert-hofstede-gert-jan-hofstede/6d-model-of-national-culture/)")
  var longterm :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Index of masculinity")
  var mascul :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to land loss (erosion) (per year)")
  var migration_erosion :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to retreat (per year)")
  var migration_retreat :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to land loss (flooding/submergence) (per year)")
  var migration_submergence :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration costs")
  var migrationcost_erosion :  Float = 0f

  @Output
  @Variable
  @Description(value = "Forced migration costs")
  var migrationcost_retreat :  Float = 0f

  @Output
  @Variable
  @Description(value = "Forced migration costs")
  var migrationcost_submergence :  Float = 0f

  @Output
  @Variable
  @Description(value = "Total area of open water created by wetland loss (segment level).")
  var openwater :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "People at risk of flooding: average number of people flooded per year by storm surge allowing for the effect of flood defences (segment level).")
  var par :  Float = default[Float]

  @Variable
  @Description(value = "Total population situated below or at 1m elevation from the overlay of population and elevation dataset")
  var pop01_0 :  Float = default[Float]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Exposure (people) that are located below ? m")
  var pop_below_ :  Map[Float,Float] = default[Map[Float,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Exposure (people) that are located below the h? flood")
  var pop_below_h :  Map[Int,Float] = default[Map[Int,Float]]

  @Driver
  @Interpolate(mode = "RIGHT")
  @Description(value = "")
  var popgrowth :  Float = default[Float]
  var popgrowth_memory: Map[Int ,Float] = Map()

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Forced migration due to the initial population correction")
  var population_correction :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Hofstede (https://geerthofstede.com/culture-geert-hofstede-gert-jan-hofstede/6d-model-of-national-culture/)")
  var power :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Implemented protection level/ return period")
  var protlevel_implemented :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Implemented protection level/ return period (before dike is raised)")
  var protlevel_implemented_before_dikeraise :  Float = default[Float]

  @Output
  @Input
  @Parameter
  @Description(value = "")
  var protlevel_ud :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "")
  var riverdike_cost :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of incremental riverdike maIntenace")
  var riverdike_increase_maintenance_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Total river dike length")
  var riverdike_length :  Float = 0f

  @Variable
  @Description(value = "Total river dike length after initialisation")
  var riverdike_length_initial :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of riverdike maIntenace")
  var riverdike_maintenance_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "total expected costs of river floods")
  var riverfloodcost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Change in relative sea level compared to the reference period, taking account of (1) human-induced climate change under the selected scenario, (2) uplift/subsidence due to glacial-isostatic adjustment and (3) natural subsidence of deltaic areas (segment level)")
  var rslr :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total costs of salinity Intrusion")
  var salinity_cost :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Sand loss direct on the open coast ")
  var sandloss_erosion_direct :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total sand loss due to erosion, allowing for direct and indirect erosion, and beach nourishment as appropriate")
  var sandloss_erosion_including_nourishment :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Sand loss indirect on the open coast ")
  var sandloss_erosion_indirect :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total sand loss due to erosion, allowing for direct and indirect erosion, ignoring beach nourishment")
  var sandloss_erosion_total :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total cost of seadike built")
  var seadike_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Average dike height")
  var seadike_height :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Average initial sea dike height")
  var seadike_height_initial :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of incremental seadike maIntenace")
  var seadike_increase_maintenance_cost :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of seadike maIntenace")
  var seadike_maintenance_cost :  Float = 0f

  @Input
  @Parameter
  @Description(value = "Costs of sea dike building in rural areas (in 2014 US$, see WB report 2017)")
  var seadike_unit_cost_rural :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Costs of sea dike building in urban areas (in 2014 US$, see WB report 2017)")
  var seadike_unit_cost_urban :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "total expected costs of sea floods")
  var seafloodcost :  Float = 0f

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Damage (assets) caused by the h? flood")
  var seafloodcost_h :  Map[Int,Float] = default[Map[Int,Float]]

  @Output
  @Variable
  @Description(value = "total expected costs of sea floods as fractionof local gdp")
  var seafloodcost_relative :  Float = 0f

  @Variable
  @Description(value = "Target sea flood costs (memory to keep seafloodcost constant over time)")
  var seafloodcost_target :  Float = 0f

  @Output
  @Variable
  @Description(value = "Length of coast with setback zone")
  var setback_length :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Administrative cost to maintain setback zones (after Snows1978 – see Croatia paper)")
  var setbackzone_administrative_cost :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Current setback zone area.")
  var setbackzone_area :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Accumulated capital deprecation in the setback zone.")
  var setbackzone_assets :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Accumulated capital deprecation in the setback zone.")
  var setbackzone_deprecation :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Population living currently in the setback zone.")
  var setbackzone_population :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Change in relative sea level compared to the reference period, only  human-induced climate change under the selected scenario")
  var slr_climate_induced :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Change in relative sea level compared to the referenceperiod, taking account only subsidence of deltaic and city areas (segment level) and isostatic adjustment")
  var slr_nonclimate_induced :  Float = default[Float]

  @Variable
  @Description(value = "stores the normalisation value for slr accumulation")
  var slr_normalizer :  Float = default[Float]

  @Input
  @Variable
  @Description(value = "Index of political stability")
  var stability :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "cost of surge barrier construction/update")
  var surge_barrier_cost :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "cost of surge barrier maintenance")
  var surge_barrier_maintenance_cost :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "number of surge barriers")
  var surge_barriers_number :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Annual mean temperature")
  var temp :  Float = default[Float]

  @Variable
  @Description(value = "Annual mean temperature/ previous period")
  var temp_last :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Difference between temperature in hottest month and average in year")
  var tempmult :  Float = default[Float]

  @Output
  @Input
  @Variable
  @Description(value = "Total population")
  var totalpop :  Float = default[Float]

  @Output
  @Input
  @Variable
  @Description(value = "Number of International tourist arrivals")
  var tourarr :  Float = default[Float]

  @Variable
  @Description(value = "Touristic area of the country, consisting of all beach areas below 1.5m")
  var touristic_area :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total costs of tourism")
  var tourlosscost :  Float = default[Float]

  @Output
  @Input
  @Variable
  @Description(value = "Added value per tourist")
  var tourtav :  Float = default[Float]

  @Output
  @Input
  @Variable
  @Description(value = "Share of population of a country living in urban areas")
  var urbanshare :  Float = 50f

  @Driver
  @Interpolate(mode = "RIGHT")
  @Description(value = "")
  var urbansharegrowth :  Float = default[Float]
  var urbansharegrowth_memory: Map[Int ,Float] = Map()

  @Output
  @Variable
  @Description(value = "Value of freshwater marsh in US$ 1995 (segment level).")
  var wetfresh_value :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total area of freshwater marsh (administrative level).  Aggregation rule, sum of coastline segments within administrative unit.")
  var wetland_area_freshmarshes :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total area of mangrove (segment level).")
  var wetland_area_mangroves :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total area of saltmarsh (segment level).")
  var wetland_area_saltmarshes :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total sum of all wetland areas")
  var wetland_area_total :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "total costs of wetland nourishment")
  var wetland_nourishment_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Total value of all wetland types in US$ (1995) (segment level). ")
  var wetland_value :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total area of low unvegetated wetland (country level).  Aggregate rule, sum of coastline segments within country unit.")
  var wetlow :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Value of low unvegetated wetland in US$ (1995) (country level).  Aggregation rule, sum of coastline segments within country feature.")
  var wetlow_value :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Value of mangrove in US$ 1995 (segment level).")
  var wetmang_value :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Value of saltmarsh in US$ 1995 (segment level).")
  var wetsalt_value :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total value of all wetland types in US$ (1995) (segment level). ")
  var wetvalueloss :  Float = default[Float]

  override def clone : CountryData = {
    var ret: CountryData  = new CountryData
    ret.area_below_ = area_below_
    ret.area_below_h = area_below_h
    ret.assets_below_ = assets_below_
    ret.assets_below_h = assets_below_h
    ret.assets_developable_below_ = assets_developable_below_
    ret.assets_developed_below_ = assets_developed_below_
    ret.attract = attract
    ret.attract0 = attract0
    ret.beachnourishment_cost = beachnourishment_cost
    ret.beachnourishment_length_beach = beachnourishment_length_beach
    ret.beachnourishment_length_shore = beachnourishment_length_shore
    ret.beachnourishment_volume = beachnourishment_volume
    ret.buddism = buddism
    ret.christianity = christianity
    ret.civlib = civlib
    ret.coastlength = coastlength
    ret.corrupt = corrupt
    ret.democracy = democracy
    ret.depart = depart
    ret.depart0 = depart0
    ret.ecfreedom = ecfreedom
    ret.fraction_developable = fraction_developable
    ret.fraction_developed = fraction_developed
    ret.gdp = gdp
    ret.gdpc = gdpc
    ret.gdpcgrowth = gdpcgrowth
    ret.gdpcgrowth_memory = gdpcgrowth_memory
    ret.gini = gini
    ret.govqual = govqual
    ret.h = h
    ret.h1 = h1
    ret.h10 = h10
    ret.h100 = h100
    ret.h1000 = h1000
    ret.hinduism = hinduism
    ret.individ = individ
    ret.islam = islam
    ret.landloss_erosion_total = landloss_erosion_total
    ret.landloss_initial_land_below_S1 = landloss_initial_land_below_S1
    ret.landloss_submergence = landloss_submergence
    ret.landlosscost_erosion_total = landlosscost_erosion_total
    ret.landlosscost_submergence = landlosscost_submergence
    ret.lati = lati
    ret.length_protected = length_protected
    ret.lifeexp = lifeexp
    ret.literacy = literacy
    ret.longi = longi
    ret.longterm = longterm
    ret.mascul = mascul
    ret.migration_erosion = migration_erosion
    ret.migration_retreat = migration_retreat
    ret.migration_submergence = migration_submergence
    ret.migrationcost_erosion = migrationcost_erosion
    ret.migrationcost_retreat = migrationcost_retreat
    ret.migrationcost_submergence = migrationcost_submergence
    ret.openwater = openwater
    ret.par = par
    ret.pop01_0 = pop01_0
    ret.pop_below_ = pop_below_
    ret.pop_below_h = pop_below_h
    ret.popgrowth = popgrowth
    ret.popgrowth_memory = popgrowth_memory
    ret.population_correction = population_correction
    ret.power = power
    ret.protlevel_implemented = protlevel_implemented
    ret.protlevel_implemented_before_dikeraise = protlevel_implemented_before_dikeraise
    ret.protlevel_ud = protlevel_ud
    ret.riverdike_cost = riverdike_cost
    ret.riverdike_increase_maintenance_cost = riverdike_increase_maintenance_cost
    ret.riverdike_length = riverdike_length
    ret.riverdike_length_initial = riverdike_length_initial
    ret.riverdike_maintenance_cost = riverdike_maintenance_cost
    ret.riverfloodcost = riverfloodcost
    ret.rslr = rslr
    ret.salinity_cost = salinity_cost
    ret.sandloss_erosion_direct = sandloss_erosion_direct
    ret.sandloss_erosion_including_nourishment = sandloss_erosion_including_nourishment
    ret.sandloss_erosion_indirect = sandloss_erosion_indirect
    ret.sandloss_erosion_total = sandloss_erosion_total
    ret.seadike_cost = seadike_cost
    ret.seadike_height = seadike_height
    ret.seadike_height_initial = seadike_height_initial
    ret.seadike_increase_maintenance_cost = seadike_increase_maintenance_cost
    ret.seadike_maintenance_cost = seadike_maintenance_cost
    ret.seadike_unit_cost_rural = seadike_unit_cost_rural
    ret.seadike_unit_cost_urban = seadike_unit_cost_urban
    ret.seafloodcost = seafloodcost
    ret.seafloodcost_h = seafloodcost_h
    ret.seafloodcost_relative = seafloodcost_relative
    ret.seafloodcost_target = seafloodcost_target
    ret.setback_length = setback_length
    ret.setbackzone_administrative_cost = setbackzone_administrative_cost
    ret.setbackzone_area = setbackzone_area
    ret.setbackzone_assets = setbackzone_assets
    ret.setbackzone_deprecation = setbackzone_deprecation
    ret.setbackzone_population = setbackzone_population
    ret.slr_climate_induced = slr_climate_induced
    ret.slr_nonclimate_induced = slr_nonclimate_induced
    ret.slr_normalizer = slr_normalizer
    ret.stability = stability
    ret.surge_barrier_cost = surge_barrier_cost
    ret.surge_barrier_maintenance_cost = surge_barrier_maintenance_cost
    ret.surge_barriers_number = surge_barriers_number
    ret.temp = temp
    ret.temp_last = temp_last
    ret.tempmult = tempmult
    ret.totalpop = totalpop
    ret.tourarr = tourarr
    ret.touristic_area = touristic_area
    ret.tourlosscost = tourlosscost
    ret.tourtav = tourtav
    ret.urbanshare = urbanshare
    ret.urbansharegrowth = urbansharegrowth
    ret.urbansharegrowth_memory = urbansharegrowth_memory
    ret.wetfresh_value = wetfresh_value
    ret.wetland_area_freshmarshes = wetland_area_freshmarshes
    ret.wetland_area_mangroves = wetland_area_mangroves
    ret.wetland_area_saltmarshes = wetland_area_saltmarshes
    ret.wetland_area_total = wetland_area_total
    ret.wetland_nourishment_cost = wetland_nourishment_cost
    ret.wetland_value = wetland_value
    ret.wetlow = wetlow
    ret.wetlow_value = wetlow_value
    ret.wetmang_value = wetmang_value
    ret.wetsalt_value = wetsalt_value
    ret.wetvalueloss = wetvalueloss
    ret
  }

}
