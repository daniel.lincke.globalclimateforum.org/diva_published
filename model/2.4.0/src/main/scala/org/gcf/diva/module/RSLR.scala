/*---

$Header: /home/lincke/Software/projects/gcf/diva/CSV/cvsroot/genial/src/diva/module/RSLR.scala,v 1.23 2011-09-01 12:37:57 hinkel Exp $

DIVA model
----------

Copyright (C) 2003 Jochen Hinkel and The Dinas Coast Consortium
http://pik-potsdam.de/dinas-coast

This is a pre-release of the DIVA model for circulation within the 
DINAS Coast Consortium. The algorithms are not finalized 
nor have they been validated. Please do not cite.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See COPYING.TXT for details.


---*/
package diva.module

import scala.collection.parallel.immutable.ParSeq

import diva.model.ModuleI
import diva.model.divaState
import diva.model.divaFeatures._
import diva.model.Config
import diva.feature._
import diva.toolbox.Toolbox._
import diva.types.TypeComputations._

import diva.types.SlrNormalisationMode._

class RSLR extends ModuleI {

  // configurable parameters -- additional SLR rates
  var gslrAdditionalRateInMMperYear: Float = 0
  var gslrAdditionalAccelerationInMMperSquareYear: Float = 0
  var citySubsidenceAdditionalRateInMMperYear: Float = 0
  var deltaSubsidenceAdditionalRateInMMperYear: Float = 0;

  // configurable parameters -- switches for different parts
  var includeIsostaticGlacialAdjustment: Boolean = true
  var includeDeltaSubsidence: Boolean = false
  var includeCitySubsidence: Boolean = false

  // normalize SLR when accumulate
  var normalizeBy: SlrNormalisationMode = CLS_LENGTH
  var normalizeDynamic: Boolean = true
  var normalizeByYear: Int = 1995

  // default values if subsidence is not known (in mm/yr)
  //var defaultNoDataDeltaSubsidence: Float = 0.0f
  var defaultNoDataCitySubsidence: Float = 0f

  override val scenarioModule: Boolean = true

  // stop geologics for control runs
  var geologicsStopYear: Int = 100000

  def moduleName: String = "Relative Sea Level Rise Module"
  def moduleVersion: String = "1.0"
  def moduleDescription: String = "Calculates relative sea level rise scenario for each segment including isostatic adjustment and subsidence in delta/city."
  def moduleAuthor: String = "Daniel Lincke"

  def initModule(initialState: divaState) {
    setConfigurableVariables(initialState)
  }

  def computeScenario(state: divaState) {
    var clss: ParSeq[Cls] = state.getFeatures[Cls].par
    clss.foreach { cls => computeRSLR(state, cls, state.elapsedTime, state.time, state.timeStep) }
  }

  def init(state: divaState) {
    var clss: ParSeq[Cls] = state.getFeatures[Cls].par
    clss.foreach { cls => getCurrentSLR(cls, state) }
    accumulate(state)
  }

  def invoke(state: divaState) {
    var clss: ParSeq[Cls] = state.getFeatures[Cls].par
    clss.foreach { cls => getCurrentSLR(cls, state) }
    accumulate(state)
  }

  private def setConfigurableVariables(initialState: divaState) {
    var rpBasedMap: scala.collection.mutable.Map[Int, Float] = scala.collection.mutable.Map();
    initialState.rpValuesForExposureOutput.foreach { rp => rpBasedMap += (rp -> 0f) }

    var clss = initialState.getFeatures[Cls]
    clss.par.foreach { cls => cls.data.h = rpBasedMap.clone }

    var admins = initialState.getFeatures[Admin]
    admins.par.foreach { admin => admin.data.h = rpBasedMap.clone }

    var deltas = initialState.getFeatures[Delta]
    deltas.par.foreach { delta => delta.data.h = rpBasedMap.clone }

    var cities = initialState.getFeatures[City]
    cities.par.foreach { city => city.data.h = rpBasedMap.clone }

    var countries = initialState.getFeatures[Country]
    countries.par.foreach { country => country.data.h = rpBasedMap.clone }

    var regions = initialState.getFeatures[Region]
    regions.par.foreach { region => region.data.h = rpBasedMap.clone }

    var globals = initialState.getFeatures[Global]
    globals.foreach { global => global.data.h = rpBasedMap.clone }
  }

  private def computeRSLR(state: divaState, cls: Cls, elapsedTime: Int, time: Int, timeStep: Int) {
    // climate induced slr comes directly from the climate scenario, but we add the additional rates (zero by default)

    cls.data.slr_climate_induced = 
      cls.data.sealevel_anomalie +
        (gslrAdditionalRateInMMperYear * elapsedTime) / 1000 +
        (elapsedTime * elapsedTime * gslrAdditionalAccelerationInMMperSquareYear) / 1000

    // cls.data.uplift in mm/yr --> (cls.data.uplift * timeStep / 1000) = the actual uplift contribution in this period (in m)
    // IMPORTANT: the sign here is confusing
    val slr_peltier_this_period =
      if ((includeIsostaticGlacialAdjustment) && (time<=geologicsStopYear)) {
        ((cls.data.uplift * timeStep.asInstanceOf[Float]) / 1000.0f)
      } else 0f

    // as for uplift: subsidence is given in mm/yr
    var slr_subsidence_this_period: Float = 0
    if ((includeCitySubsidence) && (cls.data.cityid != None)) {

      val city: City = state.getFeaturesWith[City](_.locationid == cls.data.cityid.get).head
      if ((city.data.subsidence > -999) && (time<=geologicsStopYear)) {
        slr_subsidence_this_period += (city.data.subsidence * timeStep.asInstanceOf[Float]) / 1000.0f
      } else {
        slr_subsidence_this_period += (defaultNoDataCitySubsidence * timeStep.asInstanceOf[Float]) / 1000.0f
      }
      slr_subsidence_this_period += (citySubsidenceAdditionalRateInMMperYear * timeStep.asInstanceOf[Float]) / 1000.0f
    }

    // as for uplift: subsidence is given in mm/yr
    if ((includeDeltaSubsidence) && (cls.data.deltaid != None) && (time<=geologicsStopYear)) {
      val delta: Delta = state.getFeaturesWith[Delta](_.locationid == cls.data.deltaid.get).head

      // just for segments that are no city-segemnts (if city dubsidence is on)
      // we assume city subsidence already includes delta subsidence
      if ((!includeCitySubsidence) || (cls.data.cityid == None)) {
        if (delta.data.is_ericson) {
          slr_subsidence_this_period += ((delta.data.subsidence_ericson_total) * timeStep.asInstanceOf[Float]) / 1000.0f
        } else {
          slr_subsidence_this_period += (delta.data.subsidence_non_ericson * timeStep.asInstanceOf[Float]) / 1000.0f
        }
        slr_subsidence_this_period += (deltaSubsidenceAdditionalRateInMMperYear * timeStep.asInstanceOf[Float]) / 1000.0f
      }
    }

    // adding all together
    cls.data.slr_nonclimate_induced += slr_peltier_this_period + slr_subsidence_this_period
    cls.data.rslr = cls.data.slr_climate_induced + cls.data.slr_nonclimate_induced

    // recording the scenarios
    cls.data.slr_climate_induced_memory += (time -> cls.data.slr_climate_induced)
    cls.data.slr_nonclimate_induced_memory += (time -> cls.data.slr_nonclimate_induced)
    cls.data.rslr_memory += (time -> cls.data.rslr)
  }

  private def getCurrentSLR(cls: Cls, state: divaState) {
    // we have precomputed the scenarios ... which we copy in the recent values
    cls.data.slr_climate_induced = cls.data.slr_climate_induced_memory(state.time)
    cls.data.slr_nonclimate_induced = cls.data.slr_nonclimate_induced_memory(state.time)
    cls.data.rslr = cls.data.rslr_memory(state.time)
    cls.floodHazard.shift = cls.data.rslr
    cls.data.h1 = cls.floodHazard.returnPeriodHeight(1.0d).asInstanceOf[Float]
    cls.data.h10 = cls.floodHazard.returnPeriodHeight(10.0d).asInstanceOf[Float]
    cls.data.h100 = cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float]
    cls.data.h1000 = cls.floodHazard.returnPeriodHeight(1000.0d).asInstanceOf[Float]
    cls.data.maxhwater_width = (cls.coastalPlane.computeHighWaterWidth(cls.data.maxhwater + cls.data.rslr))

    cls.data.slr_current_annual_rate =
      if (state.time == state.startTime)
        0
      else
        (cls.data.rslr_memory(state.time) - cls.data.rslr_memory(state.time - state.timeStep)) / state.timeStep

    cls.data.h.keys.foreach { rp: Int => cls.data.h(rp) = cls.floodHazard.returnPeriodHeight(rp).asInstanceOf[Float] }

    if ((normalizeDynamic) || (state.time <= normalizeByYear)) {
      cls.data.slr_normalizer = normalizeBy match {
        case CLS_LENGTH   => cls.data.length
        case CLS_POP_H100 => cls.coastalPlane.populationExposure.cumulativeExposure(cls.floodHazard.returnPeriodHeight(100))
        case CLS_POP_10M  => cls.coastalPlane.populationExposure.cumulativeExposure(10)
      }
    }
  }

  private def accumulate(state: divaState) {
    var cities: List[City] = state.getFeatures[City]
    cities.par.foreach {
      (city: City) =>
        city.data.slr_normalizer = city.clss.foldLeft(0f)((sum, cls) => sum + cls.data.slr_normalizer)
        city.data.slr_climate_induced = divideControlled(city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.slr_climate_induced * cls.data.slr_normalizer)), city.data.slr_normalizer)
        city.data.slr_nonclimate_induced = divideControlled(city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.slr_nonclimate_induced * cls.data.slr_normalizer)), city.data.slr_normalizer)
        city.data.h1 = divideControlled(city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h1 * cls.data.slr_normalizer)), city.data.slr_normalizer)
        city.data.h10 = divideControlled(city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h10 * cls.data.slr_normalizer)), city.data.slr_normalizer)
        city.data.h100 = divideControlled(city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h100 * cls.data.slr_normalizer)), city.data.slr_normalizer)
        city.data.h1000 = divideControlled(city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h1000 * cls.data.slr_normalizer)), city.data.slr_normalizer)
        city.data.rslr = city.data.slr_climate_induced + city.data.slr_nonclimate_induced
        city.data.h.keys.foreach { rp: Int => city.data.h(rp) = divideControlled(city.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h(rp) * cls.data.slr_normalizer)), city.data.slr_normalizer) }
    }

    var deltas: List[Delta] = state.getFeatures[Delta]
    deltas.par.foreach {
      (delta: Delta) =>
        delta.data.slr_normalizer = delta.clss.foldLeft(0f)((sum, cls) => sum + cls.data.slr_normalizer)
        delta.data.slr_climate_induced = divideControlled(delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.slr_climate_induced * cls.data.slr_normalizer)), delta.data.slr_normalizer)
        delta.data.slr_nonclimate_induced = divideControlled(delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.slr_nonclimate_induced * cls.data.slr_normalizer)), delta.data.slr_normalizer)
        delta.data.h1 = divideControlled(delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h1 * cls.data.slr_normalizer)), delta.data.slr_normalizer)
        delta.data.h10 = divideControlled(delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h10 * cls.data.slr_normalizer)), delta.data.slr_normalizer)
        delta.data.h100 = divideControlled(delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h100 * cls.data.slr_normalizer)), delta.data.slr_normalizer)
        delta.data.h1000 = divideControlled(delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h1000 * cls.data.slr_normalizer)), delta.data.slr_normalizer)
        delta.data.rslr = delta.data.slr_climate_induced + delta.data.slr_nonclimate_induced
        delta.data.h.keys.foreach { rp: Int => delta.data.h(rp) = divideControlled(delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h(rp) * cls.data.slr_normalizer)), delta.data.slr_normalizer) }
    }

    var admins: List[Admin] = state.getFeatures[Admin]
    admins.par.foreach {
      (admin: Admin) =>
        admin.data.slr_normalizer = admin.clss.foldLeft(0f)((sum, cls) => sum + cls.data.slr_normalizer)
        admin.data.slr_climate_induced = divideControlled(admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.slr_climate_induced * cls.data.slr_normalizer)), admin.data.slr_normalizer)
        admin.data.slr_nonclimate_induced = divideControlled(admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.slr_nonclimate_induced * cls.data.slr_normalizer)), admin.data.slr_normalizer)
        admin.data.h1 = divideControlled(admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h1 * cls.data.slr_normalizer)), admin.data.slr_normalizer)
        admin.data.h10 = divideControlled(admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h10 * cls.data.slr_normalizer)), admin.data.slr_normalizer)
        admin.data.h100 = divideControlled(admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h100 * cls.data.slr_normalizer)), admin.data.slr_normalizer)
        admin.data.h1000 = divideControlled(admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h1000 * cls.data.slr_normalizer)), admin.data.slr_normalizer)
        admin.data.rslr = admin.data.slr_climate_induced + admin.data.slr_nonclimate_induced
        admin.data.h.keys.foreach { rp: Int => admin.data.h(rp) = divideControlled(admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h(rp) * cls.data.slr_normalizer)), admin.data.slr_normalizer) }
    }

    var countries: List[Country] = state.getFeatures[Country]
    countries.par.foreach {
      (country: Country) =>
        country.data.slr_normalizer = country.clss.foldLeft(0f)((sum, cls) => sum + cls.data.slr_normalizer)
        country.data.slr_climate_induced = divideControlled(country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.slr_climate_induced * cls.data.slr_normalizer)), country.data.slr_normalizer)
        country.data.slr_nonclimate_induced = divideControlled(country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.slr_nonclimate_induced * cls.data.slr_normalizer)), country.data.slr_normalizer)
        country.data.h1 = divideControlled(country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h1 * cls.data.slr_normalizer)), country.data.slr_normalizer)
        country.data.h10 = divideControlled(country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h10 * cls.data.slr_normalizer)), country.data.slr_normalizer)
        country.data.h100 = divideControlled(country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h100 * cls.data.slr_normalizer)), country.data.slr_normalizer)
        country.data.h1000 = divideControlled(country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h1000 * cls.data.slr_normalizer)), country.data.slr_normalizer)
        country.data.rslr = country.data.slr_climate_induced + country.data.slr_nonclimate_induced
        country.data.h.keys.foreach { rp: Int => country.data.h(rp) = divideControlled(country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.h(rp) * cls.data.slr_normalizer)), country.data.slr_normalizer) }
    }

    var regions: List[Region] = state.getFeatures[Region]
    regions.par.foreach {
      (region: Region) =>
        region.data.slr_normalizer = region.admins.foldLeft(0f)((sum, admin) => sum + admin.data.slr_normalizer)
        region.data.slr_climate_induced = divideControlled(region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.slr_climate_induced * admin.data.slr_normalizer)), region.data.slr_normalizer)
        region.data.slr_nonclimate_induced = divideControlled(region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.slr_nonclimate_induced * admin.data.slr_normalizer)), region.data.slr_normalizer)
        region.data.h1 = divideControlled(region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.h1 * admin.data.slr_normalizer)), region.data.slr_normalizer)
        region.data.h10 = divideControlled(region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.h10 * admin.data.slr_normalizer)), region.data.slr_normalizer)
        region.data.h100 = divideControlled(region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.h100 * admin.data.slr_normalizer)), region.data.slr_normalizer)
        region.data.h1000 = divideControlled(region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.h1000 * admin.data.slr_normalizer)), region.data.slr_normalizer)
        region.data.rslr = region.data.slr_climate_induced + region.data.slr_nonclimate_induced
        region.data.h.keys.foreach { rp: Int => region.data.h(rp) = divideControlled(region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.h(rp) * admin.data.slr_normalizer)), region.data.slr_normalizer) }
    }
    var globals: List[Global] = state.getFeatures[Global]
    globals.foreach {
      (global: Global) =>
        global.data.slr_normalizer = global.countries.foldLeft(0f)((sum, country) => sum + country.data.slr_normalizer)
        global.data.slr_climate_induced = divideControlled(global.countries.foldLeft(0f)((sum, country) => sum + (country.data.slr_climate_induced * country.data.slr_normalizer)), global.data.slr_normalizer)
        global.data.slr_nonclimate_induced = divideControlled(global.countries.foldLeft(0f)((sum, country) => sum + (country.data.slr_nonclimate_induced * country.data.slr_normalizer)), global.data.slr_normalizer)
        global.data.h1 = divideControlled(global.countries.foldLeft(0f)((sum, country) => sum + (country.data.h1 * country.data.slr_normalizer)), global.data.slr_normalizer)
        global.data.h10 = divideControlled(global.countries.foldLeft(0f)((sum, country) => sum + (country.data.h10 * country.data.slr_normalizer)), global.data.slr_normalizer)
        global.data.h100 = divideControlled(global.countries.foldLeft(0f)((sum, country) => sum + (country.data.h100 * country.data.slr_normalizer)), global.data.slr_normalizer)
        global.data.h1000 = divideControlled(global.countries.foldLeft(0f)((sum, country) => sum + (country.data.h1000 * country.data.slr_normalizer)), global.data.slr_normalizer)
        global.data.rslr = global.data.slr_climate_induced + global.data.slr_nonclimate_induced
        global.data.h.keys.foreach { rp: Int => global.data.h(rp) = divideControlled(global.countries.foldLeft(0f)((sum, country) => sum + (country.data.h(rp) * country.data.slr_normalizer)), global.data.slr_normalizer) }
    }

  }
}
