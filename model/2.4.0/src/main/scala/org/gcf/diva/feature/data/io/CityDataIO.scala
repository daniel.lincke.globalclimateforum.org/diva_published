/* Automatically generated data IO for City
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data.io

import diva.io._
import diva.types.InterpolationMode.InterpolationMode
import diva.feature.data.CityData

class CityDataIO extends FeatureDataIO[CityData] {

  def writeData(out: divaOutput, d: CityData) {
    d.area_below_.keys.toList.sorted.foreach { x => out.write(d.area_below_(x)) }
    d.area_below_h.keys.toList.sorted.foreach { x => out.write(d.area_below_h(x)) }
    d.assets_below_.keys.toList.sorted.foreach { x => out.write(d.assets_below_(x)) }
    d.assets_below_h.keys.toList.sorted.foreach { x => out.write(d.assets_below_h(x)) }
    d.assets_developable_below_.keys.toList.sorted.foreach { x => out.write(d.assets_developable_below_(x)) }
    d.assets_developed_below_.keys.toList.sorted.foreach { x => out.write(d.assets_developed_below_(x)) }
    out.write(d.coastlength)
    out.write(d.countryid)
    out.write(d.gdpc)
    d.h.keys.toList.sorted.foreach { x => out.write(d.h(x)) }
    out.write(d.length_protected)
    out.write(d.localgdp)
    out.write(d.migration_erosion)
    out.write(d.migration_retreat)
    out.write(d.migration_submergence)
    out.write(d.par)
    d.pop_below_.keys.toList.sorted.foreach { x => out.write(d.pop_below_(x)) }
    d.pop_below_h.keys.toList.sorted.foreach { x => out.write(d.pop_below_h(x)) }
    out.write(d.protlevel_implemented)
    out.write(d.protlevel_implemented_before_dikeraise)
    out.write(d.protlevel_ud)
    out.write(d.rslr)
    out.write(d.seadike_cost)
    out.write(d.seadike_height)
    out.write(d.seadike_height_initial)
    out.write(d.seadike_increase_maintenance_cost)
    out.write(d.seadike_maintenance_cost)
    out.write(d.seafloodcost)
    d.seafloodcost_h.keys.toList.sorted.foreach { x => out.write(d.seafloodcost_h(x)) }
    out.write(d.seafloodcost_relative)
    out.write(d.slr_climate_induced)
    out.write(d.slr_nonclimate_induced)
    out.write(d.totalpop)
  }

  def writeSelectedData(out: divaOutput, d: CityData, ignoreThisOutput: Array[Boolean]) {
    var c: Int = 0
    d.area_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.area_below_(x))}; c+=1 } } 
    d.area_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.area_below_h(x))}; c+=1 } } 
    d.assets_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_below_(x))}; c+=1 } } 
    d.assets_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_below_h(x))}; c+=1 } } 
    d.assets_developable_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_developable_below_(x))}; c+=1 } } 
    d.assets_developed_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_developed_below_(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.coastlength)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.countryid)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.gdpc)}; c+=1
    d.h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.length_protected)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.localgdp)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_erosion)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_retreat)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.par)}; c+=1
    d.pop_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.pop_below_(x))}; c+=1 } } 
    d.pop_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.pop_below_h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.protlevel_implemented)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.protlevel_implemented_before_dikeraise)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.protlevel_ud)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.rslr)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_height)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_height_initial)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_increase_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost)}; c+=1
    d.seafloodcost_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_relative)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.slr_climate_induced)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.slr_nonclimate_induced)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.totalpop)}; c+=1
  }

  def readInputValue(in: divaInput, d:CityData, parName: String, locid: String, time: Int) {
    if (parName=="countryid") {d.countryid = in.getInputValue[String]("countryid", locid, time)}
    else if (parName=="lati") {d.lati = in.getInputValue[Float]("lati", locid, time)}
    else if (parName=="longi") {d.longi = in.getInputValue[Float]("longi", locid, time)}
    else if (parName=="protlevel_ud") {d.protlevel_ud = in.getInputValue[Float]("protlevel_ud", locid, time)}
    else if (parName=="totalpop") {d.totalpop = in.getInputValue[Float]("totalpop", locid, time)}
    // else println("error:" + parName + " not found in City")
  }

  def readOptionInputValue(in: divaInput, d:CityData, parName: String, locid: String, time: Int) {
  }

  def readScenarioValue(in: divaInput, d:CityData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
    if (parName=="subsidence") {d.subsidence = in.getScenarioValue[Float]("subsidence", locid, time, pInterpolationMode)}
    // else println("error:" + parName + " not found in City")
  }

  def readOptionScenarioValue(in: divaInput, d:CityData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

}
