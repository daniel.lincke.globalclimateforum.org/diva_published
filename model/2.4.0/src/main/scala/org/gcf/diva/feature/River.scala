package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class River extends FeatureI {
  type Data = RiverData
  type DataIO = RiverDataIO

  val dataTypeTag: ru.TypeTag[RiverData] = ru.typeTag[RiverData]
  val dataClassTag: ClassTag[RiverData] = classTag[RiverData]
  val dataTypeSave: ru.Type = ru.typeOf[RiverData]

  var data: RiverData = null
  var dataIO: RiverDataIO = null
  
  var riverdistributarys: Array[Riverdistributary] = Array()
  var delta: Option[Delta] = _

  def init() {
    data = new RiverData
    dataIO = new RiverDataIO
    delta = None
  }

  var mustHaveInputFile = false

  def newInstanceCopy: River = {
    val ret = new River
    ret.data = new RiverData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }
}