package diva.module

import scala.collection.parallel.immutable.ParSeq

import diva.model.ModuleI
import diva.model.divaState
import diva.model.divaFeatures._
import diva.types.BeachNourishmentMode._

import diva.feature._

class Erosion extends ModuleI {

  var inverseProfileSlope: Float = 100f
  var brfAddend: Float = 0f

  def moduleName: String = "Total Erosion"
  def moduleVersion: String = "2.0 (migrated to scala from old Version 1.2.6)"
  def moduleDescription: String = "Computes total erosion of the sandy coast on all coastline-segments. This is based on the sum of direct erosion (the Bruun Rule), indirect erosion of any tidal basins (from the Indirect Erosion algorithm) and any nourishment (from the Costing and Adaptation algorithm)."
  def moduleAuthor: String = "Robert Nicholls, Jochen Hinkel, Daniel Lincke"

  /*
  def getInput: Array[java.lang.String] = Array("cls.brf", "cls.rslr", "cls.length", "cls.bnour", "basin.sandlind", "cls.tidalrng", "cls.waveclim", "cls.basinid")
  def getOutput: Array[java.lang.String] = Array("cls.landlerotot", "cls.sandltot", "cls.nbbenefit", "cls.sandltot_potential", "admin.landlerotot", "admin.sandltot", "country.landlerotot", "country.sandltot", "sres.landlerotot", "sres.sandltot", "gva.landlerotot", "gva.sandltot", "global.landlerotot", "global.sandltot")
  */

  var erosionAdaptation: diva.module.erosion.ErosionAdaptation = _

  var allowShoreNourishmentInFetchLimitedSeas: Boolean = true

  // Welfare cost of migrating one person relative to GDP per capita
  var migrcostToGdpcRatio = 3f
  var popdensUninhabitedThreshold: Float = 30f

  // safety margin nourishment
  var bnourishmentSafetyMargin: Float = 1.0f

  var beachNourishmentMode: BeachNourishmentMode = BEACH_NOURISHMENT_CBA

  def initModule(initialState: divaState) = doNothing
  def computeScenario(currentState: diva.model.divaState) = shouldNotBeInvoked

  def init(state: divaState) = {
    erosionAdaptation = new diva.module.erosion.ErosionAdaptation(beachNourishmentMode)
    erosionAdaptation.allowShoreNourishmentInFetchLimitedSeas = allowShoreNourishmentInFetchLimitedSeas
    erosionAdaptation.migrcostToGdpcRatio = migrcostToGdpcRatio
    erosionAdaptation.popdensUninhabitedThreshold = popdensUninhabitedThreshold
  }

  def invoke(state: divaState) {

    var clss: ParSeq[Cls] = state.getFeatures[Cls].par
    clss.foreach { cls => computePotentialErosion(cls, state) }
    clss.foreach { cls => computeErosionAdaptation(cls) }
    clss.foreach { cls => computeActualErosion(cls, state) }
    accumulate(state)

  }

  private def computePotentialErosion(cls: Cls, state: divaState) {

    var profileHeight: Float =
      if (cls.data.waveclim < 1) 0f
      else if (cls.data.waveclim == 1) 4.5f + cls.data.mtidalrng
      else if (cls.data.waveclim == 2) 8.0f + cls.data.mtidalrng
      else if (cls.data.waveclim == 3) 10.5f + cls.data.mtidalrng
      else if (cls.data.waveclim == 4) 13.5f + cls.data.mtidalrng
      else 16.0f + cls.data.mtidalrng

    // Bruun rule factor uncertainty
    var brf: Float = cls.data.brf + brfAddend
    if (brf > 1) brf = 1f
    if (brf < 0) brf = 0f

    // potential sand- and landloss (what would be lost without nourishment)
    // inverseProfileSlope = profileWidth/profileHeight. Default value is 100;
    cls.data.sandloss_erosion_direct = ((inverseProfileSlope * (cls.data.slr_current_annual_rate) * cls.data.length * 1000 * profileHeight) * brf)

    //  Sand Loss Indirect (from the Indirect Erosion module)
    cls.data.sandloss_erosion_indirect =
      cls.data.basinid match {
        case None     => 0
        case Some(id) => cls.basin.get.data.sandloss_erosion_indirect
      }

    // Total Sand Loss, including nourishment (units m^3/yr)
    // and Total Potential Sand Loss (cls.sandltot_potential), 
    // excluding the effects of nourishment from the previous time step (units m^3/yr)(losses are positive)
    cls.data.sandloss_erosion_total = cls.data.sandloss_erosion_direct + cls.data.sandloss_erosion_indirect
    
  }

  private def computeErosionAdaptation(cls: Cls) {
    erosionAdaptation.nourish(cls)
  }

  private def computeActualErosion(cls: Cls, state: divaState) {

    var profileHeight: Float =
      if (cls.data.waveclim < 1) 0f
      else if (cls.data.waveclim == 1) 4.5f + cls.data.mtidalrng
      else if (cls.data.waveclim == 2) 8.0f + cls.data.mtidalrng
      else if (cls.data.waveclim == 3) 10.5f + cls.data.mtidalrng
      else if (cls.data.waveclim == 4) 13.5f + cls.data.mtidalrng
      else 16.0f + cls.data.mtidalrng

    cls.data.sandloss_erosion_total = cls.data.sandloss_erosion_direct + cls.data.sandloss_erosion_indirect

    cls.data.sandloss_erosion_including_nourishment = cls.data.sandloss_erosion_total - cls.data.beachnourishment_volume
    if (cls.data.sandloss_erosion_including_nourishment < 0) {
      cls.data.sandloss_erosion_including_nourishment = 0
    }

    // Land gained due to 1m^3 of beach nourishment
    // In the case that profileHeight = 0, cls.nbbenefit = 0
    cls.data.nbbenefit =
      if (profileHeight > 0)
        1 / profileHeight
      else
        0

    // Total Land Loss (units km^2/yr)
    // In the case that profileHeight=0, no land loss should occur
    cls.data.landloss_erosion_total =
      if (profileHeight > 0)
        cls.data.sandloss_erosion_including_nourishment / (profileHeight * 1000000f)
      else {
        0
      }


    if (cls.data.landloss_erosion_total < 0.0001f) cls.data.migration_erosion = 0f;
    else { cls.data.migration_erosion = Math.round(cls.data.landloss_erosion_total * cls.czs.head.coastalPlane.populationDensityBelow(16.5f)); }

    cls.data.migrationcost_erosion = migrcostToGdpcRatio * cls.data.migration_erosion * cls.data.gdpc
    cls.data.landlosscost_erosion_total = cls.data.landloss_erosion_total * cls.data.landvalue_unit

  }

  private def accumulate(state: divaState) {
    var deltas: List[Delta] = state.getFeatures[Delta]
    deltas.foreach {
      (delta: Delta) =>
        {
          delta.data.sandloss_erosion_total = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_total))
          delta.data.sandloss_erosion_direct = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_direct))
          delta.data.sandloss_erosion_including_nourishment = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_including_nourishment))
          delta.data.landloss_erosion_total = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.landloss_erosion_total))
          delta.data.landlosscost_erosion_total = delta.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.landlosscost_erosion_total)) / 1000000
        }
    }

    var admins: List[Admin] = state.getFeatures[Admin]
    admins.foreach {
      (admin: Admin) =>
        {
          admin.data.sandloss_erosion_total = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_total))
          admin.data.sandloss_erosion_direct = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_direct))
          admin.data.sandloss_erosion_including_nourishment = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_including_nourishment))
          admin.data.landloss_erosion_total = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.landloss_erosion_total))
          admin.data.landlosscost_erosion_total = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.landlosscost_erosion_total)) / 1000000
          admin.data.migration_erosion = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.migration_erosion)) / 1000
          admin.data.migrationcost_erosion = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.migrationcost_erosion)) / 1000000
          admin.data.beachnourishment_length_beach = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.beachnourishment_length_beach))
          admin.data.beachnourishment_length_shore = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.beachnourishment_length_shore))
          admin.data.beachnourishment_volume = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.beachnourishment_volume))
          admin.data.beachnourishment_cost = admin.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.beachnourishment_cost)) / 1000000
        }
    }

    var countries: List[Country] = state.getFeatures[Country]
    countries.foreach {
      (country: Country) =>
        {
          country.data.sandloss_erosion_total = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_total))
          country.data.sandloss_erosion_direct = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_direct))
          country.data.sandloss_erosion_including_nourishment = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.sandloss_erosion_including_nourishment))
          country.data.landloss_erosion_total = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.landloss_erosion_total))
          country.data.landlosscost_erosion_total = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.landlosscost_erosion_total)) / 1000000
          country.data.migration_erosion = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.migration_erosion)) / 1000
          country.data.migrationcost_erosion = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.migrationcost_erosion)) / 1000000
          country.data.beachnourishment_length_beach = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.beachnourishment_length_beach))
          country.data.beachnourishment_length_shore = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.beachnourishment_length_shore))
          country.data.beachnourishment_volume = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.beachnourishment_volume))
          country.data.beachnourishment_cost = country.clss.foldLeft(0f)((sum, cls) => sum + (cls.data.beachnourishment_cost)) / 1000000
        }
    }

    var regions: List[Region] = state.getFeatures[Region]
    regions.foreach {
      (region: Region) =>
        {
          region.data.sandloss_erosion_total = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.sandloss_erosion_total))
          region.data.sandloss_erosion_direct = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.sandloss_erosion_direct))
          region.data.sandloss_erosion_including_nourishment = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.sandloss_erosion_including_nourishment))
          region.data.landloss_erosion_total = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.landloss_erosion_total))
          region.data.landlosscost_erosion_total = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.landlosscost_erosion_total))
          region.data.migration_erosion = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.migration_erosion))
          region.data.migrationcost_erosion = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.migrationcost_erosion))
          region.data.beachnourishment_length_beach = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.beachnourishment_length_beach))
          region.data.beachnourishment_length_shore = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.beachnourishment_length_shore))
          region.data.beachnourishment_volume = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.beachnourishment_volume))
          region.data.beachnourishment_cost = region.admins.foldLeft(0f)((sum, admin) => sum + (admin.data.beachnourishment_cost))
        }
    }

    var globals: List[Global] = state.getFeatures[Global]
    globals.foreach {
      (global: Global) =>
        {
          global.data.sandloss_erosion_total = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.sandloss_erosion_total))
          global.data.sandloss_erosion_direct = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.sandloss_erosion_direct))
          global.data.sandloss_erosion_including_nourishment = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.sandloss_erosion_including_nourishment))
          global.data.landloss_erosion_total = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.landloss_erosion_total))
          global.data.landlosscost_erosion_total = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.landlosscost_erosion_total))
          global.data.migration_erosion = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.migration_erosion))
          global.data.migrationcost_erosion = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.migrationcost_erosion))
          global.data.beachnourishment_length_beach = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.beachnourishment_length_beach))
          global.data.beachnourishment_length_shore = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.beachnourishment_length_shore))
          global.data.beachnourishment_volume = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.beachnourishment_volume))
          global.data.beachnourishment_cost = global.countries.foldLeft(0f)((sum, country) => sum + (country.data.beachnourishment_cost))
        }
    }
  }
}