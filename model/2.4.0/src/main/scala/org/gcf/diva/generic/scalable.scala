package diva.generic

trait Scalable[T] {
  def times(x: T, y: Double): T

  class Ops(lhs: T) {
    def *(rhs: Double): T = times(lhs, rhs)
    def *(rhs: Float): T = times(lhs, rhs.toDouble)
  }

  implicit def mkScalableOps(lhs: T): Ops = new Ops(lhs)

}