/* Automatically generated data IO for Whs
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data.io

import diva.io._
import diva.types.InterpolationMode.InterpolationMode
import diva.feature.data.WhsData

class WhsDataIO extends FeatureDataIO[WhsData] {

  def writeData(out: divaOutput, d: WhsData) {
    out.write(d.threatened)
  }

  def writeSelectedData(out: divaOutput, d: WhsData, ignoreThisOutput: Array[Boolean]) {
    var c: Int = 0
    if (!ignoreThisOutput(c)) {out.write(d.threatened)}; c+=1
  }

  def readInputValue(in: divaInput, d:WhsData, parName: String, locid: String, time: Int) {
    if (parName=="id") {d.id = in.getInputValue[Int]("id", locid, time)}
    else if (parName=="lati") {d.lati = in.getInputValue[Float]("lati", locid, time)}
    else if (parName=="longi") {d.longi = in.getInputValue[Float]("longi", locid, time)}
    else if (parName=="mask") {d.mask = in.getInputValue[Int]("mask", locid, time)}
    else if (parName=="unescu") {d.unescu = in.getInputValue[Int]("unescu", locid, time)}
    // else println("error:" + parName + " not found in Whs")
  }

  def readOptionInputValue(in: divaInput, d:WhsData, parName: String, locid: String, time: Int) {
  }

  def readScenarioValue(in: divaInput, d:WhsData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

  def readOptionScenarioValue(in: divaInput, d:WhsData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

}
