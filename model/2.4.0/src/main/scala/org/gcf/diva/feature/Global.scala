package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class Global extends FeatureI {
  type Data = GlobalData
  type DataIO = GlobalDataIO

  val dataTypeTag: ru.TypeTag[GlobalData] = ru.typeTag[GlobalData]
  val dataClassTag: ClassTag[GlobalData] = classTag[GlobalData]
  val dataTypeSave: ru.Type = ru.typeOf[GlobalData]

  var mustHaveInputFile = false

  var data: GlobalData = null
  var dataIO: GlobalDataIO = null
  
  var countries: List[Country] = List()
  var basins: List[Basin] = List()

  def init() {
    data = new GlobalData
    dataIO = new GlobalDataIO
  }

  def newInstanceCopy: Global = {
    val ret = new Global
    ret.data = new GlobalData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }
}