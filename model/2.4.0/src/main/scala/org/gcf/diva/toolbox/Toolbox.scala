package diva.toolbox

import scala.language.implicitConversions

object Toolbox {

  case class Bool(b: Boolean) {
    def ?[X](t: => X) = new {
      def |(f: => X) = if (b) t else f
    }
  }

  object Bool {
    implicit def BooleanBool(b: Boolean) = Bool(b)
  }

  def limited(x: Float, lower: Float, upper: Float) = scala.math.min(scala.math.max(lower, x), upper)
  //def bounded_low(lower: Float, x: Float) = scala.math.max(lower, x)
  def bounded_low[A <: Ordered[A]](lower: A, x: A) = if (x < lower) lower else x
  def bounded_low[A](lower: A, x: A)(implicit ordering: Ordering[A]) = if (ordering.lt(x, lower)) lower else x

  //def bounded_high(x: Float, upper: Float) = scala.math.min(upper, x)
  def bounded_high[A <: Ordered[A]](x: A, upper: A) = if (x > upper) upper else x

  def shiftLeft[A](s: Seq[A]): Seq[A] = {
    try {
      s.tail :+ s.head
    } catch {
      case e: Throwable => throw e
    }
  }

  def shiftRight[A](s: Seq[A]): Seq[A] = {
    try {
      shiftLeft(s.reverse).reverse
    } catch {
      case e: Throwable => throw e
    }
  }

  //def divideControlled(t1: Float, t2: Float): Float = if (t2 == 0) 0 else t1 / t2
  //def divideControlled[T <: Fractional[T]](t1: T, t2: Float): Float = if (t2 == 0) 0 else Fractional[T].div(t1, t2)

  def divideControlled[T](t1: T, t2: T)(implicit frac: Fractional[T]): T = if (t2 == frac.zero) frac.zero else frac.div(t1, t2)

  /*
  def functionProduct[T](f: T => T, g: T => T)(x: T)(implicit frac: Fractional[T]): T = {
    import frac.mkNumericOps
    f(x) * g(x)
  }
  * 
  */

  def functionProduct(f: Float => Float, g: Float => Double)(x: Float): Float = {
    f(x) * g(x).toFloat
  }
}