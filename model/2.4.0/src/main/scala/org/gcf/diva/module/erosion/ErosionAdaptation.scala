package diva.module.erosion

import diva.types.BeachNourishmentMode._
import diva.feature.Cls
import diva.feature.Country
import diva.feature.City
import diva.model.divaState
import diva.model.hasLogger


// TODO: Introduce type for beach nourishment reason.

class ErosionAdaptation(beachNourishmentMode: BeachNourishmentMode) extends hasLogger {

  var allowShoreNourishmentInFetchLimitedSeas: Boolean = true

  // Welfare cost of migrating one person relative to GDP per capita
  var migrcostToGdpcRatio = 3f
  var popdensUninhabitedThreshold: Float = 30f

  // safety margin nourishment
  var bnourishmentSafetyMargin: Float = 1.0f

  def nourish(cls: Cls) {

    // Calculate nourishment unit costs (US$/m^2)
    // The further away the seand is the more expensive it gets.
    // If sand is near (brf > 0.5) shore nourishment = 3 US$/m ...
    var bnourUnitCost: Float =
      if (cls.data.brf > 0.5) 3f
      else if (cls.data.brf > 0.2) 6f
      else 9f

    var fetchLimited: Boolean =
      if (allowShoreNourishmentInFetchLimitedSeas)
        false
      else
        cls.data.fetchlimited

    // costs and benefits of nourishment
    var perArea_benefit_tour: Float = 0f;
    if (cls.country.data.touristic_area > 0) perArea_benefit_tour = (0.65f * cls.country.data.tourarr * 0.25f * cls.country.data.tourtav / cls.country.data.touristic_area / 1000000f);
    var perArea_benefit_agri = cls.data.landvalue_unit / 1000000;
    var perArea_benefit_migr = migrcostToGdpcRatio * cls.coastalPlane.populationDensityBelow(16.5f) * cls.data.gdpc / 1000000;

    beachNourishmentMode match {
      case BEACH_NOURISHMENT_CBA          => nourishCBA(cls, fetchLimited, bnourUnitCost, perArea_benefit_agri, perArea_benefit_migr, perArea_benefit_tour)
      case BEACH_NOURISHMENT_FULL         => nourishFull(cls, bnourUnitCost)
      case BEACH_NOURISHMENT_POPDENS_RULE => nourishPopdens(cls, bnourUnitCost)
      case BEACH_NOURISHMENT_NONE         => nourishNot(cls, perArea_benefit_tour)
      case BEACH_NOURISHMENT_UD           => nourishUD(cls, bnourUnitCost)
      case _                              => logger.error("unknown beachNourishmentMode " + beachNourishmentMode)
    }

    // LENGHT OF NOURISHED COASTLINE
    cls.data.beachnourishment_length_beach =
      if ((cls.data.beachnourishment_reason == 12) || (cls.data.beachnourishment_reason == 22) || (cls.data.beachnourishment_reason == 23)) {
        cls.data.length * cls.data.brf
      } else {
        0f
      }

    cls.data.beachnourishment_length_shore =
      if ((cls.data.beachnourishment_reason == 21) || (cls.data.beachnourishment_reason == 11)) {
        cls.data.length * cls.data.brf
      } else {
        0f
      }

  }

  private def nourishCBA(cls: Cls, fetchLimited: Boolean,
                         bnourUnitCost: Float, perArea_benefit_agri: Float, perArea_benefit_migr: Float, perArea_benefit_tour: Float) {
    // tourists present ?
    /*
    println(cls.locationid + ": nourishCBA")
    println("perArea_benefit_tour = " + perArea_benefit_tour)
    println("perArea_benefit_agri = " + perArea_benefit_agri)
    println("perArea_benefit_migr = " + perArea_benefit_migr)
    
    println("bnourUnitCost = " + bnourUnitCost)
    println("cls.data.nbbenefit = " + cls.data.nbbenefit)
    
    println(perArea_benefit_tour + " > " + ((bnourUnitCost + 3) / cls.data.nbbenefit))
    println((perArea_benefit_tour + perArea_benefit_agri + perArea_benefit_migr) + " > " + ((bnourUnitCost + 3) / cls.data.nbbenefit))
    println((perArea_benefit_agri + perArea_benefit_migr) + " > " + (bnourUnitCost / cls.data.nbbenefit))
    */
    if (cls.country.data.temp + cls.country.data.tempmult > 15) {
      // beach nourishment, tourists only
      if (perArea_benefit_tour > (bnourUnitCost + 3) / cls.data.nbbenefit) {
        cls.data.beachnourishment_reason = 23;
        cls.data.beachnourishment_volume = bnourishmentSafetyMargin * cls.data.sandloss_erosion_total
        if (cls.data.beachnourishment_volume < 0) cls.data.beachnourishment_volume = 0;
        cls.data.beachnourishment_cost = (bnourUnitCost + 3) * cls.data.beachnourishment_volume;
        cls.data.tourlosscost = 0f;
      } // beach nourishment, multiple benefits
      else if ((perArea_benefit_tour + perArea_benefit_agri + perArea_benefit_migr) > (bnourUnitCost + 3) / cls.data.nbbenefit) {
        cls.data.beachnourishment_reason = 22;
        cls.data.beachnourishment_volume = bnourishmentSafetyMargin * cls.data.sandloss_erosion_total
        if (cls.data.beachnourishment_volume < 0) cls.data.beachnourishment_volume = 0;
        cls.data.beachnourishment_cost = (bnourUnitCost + 3) * cls.data.beachnourishment_volume;
        cls.data.tourlosscost = 0f;
      } // else if apply cheaper shore nourishment when cost effective and not within a fecthlimite sea
      else if (((perArea_benefit_agri + perArea_benefit_migr) > bnourUnitCost / cls.data.nbbenefit) && !fetchLimited) {
        cls.data.beachnourishment_reason = 21;
        cls.data.beachnourishment_volume = bnourishmentSafetyMargin * cls.data.sandloss_erosion_total
        if (cls.data.beachnourishment_volume < 0) cls.data.beachnourishment_volume = 0;
        cls.data.beachnourishment_cost = bnourUnitCost * cls.data.beachnourishment_volume;
        cls.data.tourlosscost = perArea_benefit_tour * cls.data.length * cls.data.brf;
      } // do nothing
      else {
        cls.data.beachnourishment_reason = 20;
        cls.data.beachnourishment_volume = 0;
        cls.data.beachnourishment_cost = 0;
        cls.data.tourlosscost = perArea_benefit_tour * cls.data.length * cls.data.brf;
      }
    } // tourists not present since average annual temp < 15
    else {
      // if inside of fetch limited sea, test if beach nourishment is effective
      if (fetchLimited && ((perArea_benefit_agri + perArea_benefit_migr) > (bnourUnitCost + 3) / cls.data.nbbenefit)) {
        cls.data.beachnourishment_reason = 12
        cls.data.beachnourishment_volume = bnourishmentSafetyMargin * cls.data.sandloss_erosion_total
        if (cls.data.beachnourishment_volume < 0) cls.data.beachnourishment_volume = 0
        cls.data.beachnourishment_cost = (bnourUnitCost + 3) * cls.data.beachnourishment_volume
        cls.data.tourlosscost = 0f;
      } // if NOT inside a fetchlimted sea, test if shore nourishment is effective
      else if (!fetchLimited && (perArea_benefit_agri + perArea_benefit_migr) > bnourUnitCost / cls.data.nbbenefit) {
        cls.data.beachnourishment_reason = 11;
        cls.data.beachnourishment_volume = bnourishmentSafetyMargin * cls.data.sandloss_erosion_total
        if (cls.data.beachnourishment_volume < 0) cls.data.beachnourishment_volume = 0
        cls.data.beachnourishment_cost = bnourUnitCost * cls.data.beachnourishment_volume
        cls.data.tourlosscost = 0f
      } else {
        cls.data.beachnourishment_reason = 10
        cls.data.beachnourishment_volume = 0
        cls.data.beachnourishment_cost = 0
        cls.data.tourlosscost = 0f
      }
    }
  }

  private def nourishFull(cls: Cls, bnourUnitCost: Float) {
    cls.data.tourlosscost = 0f
    cls.data.beachnourishment_volume = bnourishmentSafetyMargin * cls.data.sandloss_erosion_total
    if (cls.country.data.temp + cls.country.data.tempmult > 15) {
      cls.data.beachnourishment_reason = 22;
      cls.data.beachnourishment_cost = (bnourUnitCost + 3) * cls.data.beachnourishment_volume;
    } else {
      cls.data.beachnourishment_reason = 11;
      cls.data.beachnourishment_cost = bnourUnitCost * cls.data.beachnourishment_volume;
    }
  }

  private def nourishPopdens(cls: Cls, bnourUnitCost: Float) {
    cls.data.tourlosscost = 0f
    val popdensInFullFloodPlain: Float = cls.coastalPlane.populationDensityBelow(16.5f)

    if (popdensInFullFloodPlain >= popdensUninhabitedThreshold) {
      cls.data.beachnourishment_volume = bnourishmentSafetyMargin * cls.data.sandloss_erosion_total
      cls.data.beachnourishment_reason = 999;
      cls.data.beachnourishment_cost = (bnourUnitCost + 3) * cls.data.beachnourishment_volume;
    } else {
      cls.data.beachnourishment_reason = 0;
      cls.data.beachnourishment_volume = 0;
      cls.data.beachnourishment_cost = 0;
    }
  }

  private def nourishNot(cls: Cls, perArea_benefit_tour: Float) {
    if (cls.country.data.temp + cls.country.data.tempmult > 15)
      cls.data.tourlosscost = perArea_benefit_tour;
    else
      cls.data.tourlosscost = 0f;
    cls.data.beachnourishment_reason = 0;
    cls.data.beachnourishment_volume = 0;
    cls.data.beachnourishment_cost = 0;
  }

  // TODO: check if this is correct
  private def nourishUD(cls: Cls, bnourUnitCost: Float) {
    cls.data.tourlosscost = 0f;
    cls.data.beachnourishment_volume = bnourishmentSafetyMargin * cls.admin.data.beachnourishment_ud * cls.data.sandloss_erosion_total
    cls.data.landloss_erosion_total = cls.data.landloss_erosion_total * (1 - cls.admin.data.beachnourishment_ud)
    if (cls.country.data.temp + cls.country.data.tempmult > 15) {
      cls.data.beachnourishment_reason = 22;
      cls.data.beachnourishment_cost = (bnourUnitCost + 3) * cls.data.beachnourishment_volume
    } else {
      cls.data.beachnourishment_reason = 11;
      cls.data.beachnourishment_cost = bnourUnitCost * cls.data.beachnourishment_volume
    }
  }

}