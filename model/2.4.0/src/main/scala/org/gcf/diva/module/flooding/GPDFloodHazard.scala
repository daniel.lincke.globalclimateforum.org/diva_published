package diva.module.flooding

import diva.toolbox.exceptions.GPDWithShapeZeroException
import diva.toolbox.exceptions.GPDWithNonPositiveScaleException

class GPDFloodHazard(pMue: Float, pSigma: Float, pShape: Float, pNumberEvents: Float)
  extends FloodHazardI {

  //if (pShape == 0) throw new GPDWithShapeZeroException()
  if (pSigma <= 0) throw new GPDWithNonPositiveScaleException()

  // Mathematically, this is
  // F(x) = P(X<=level)
  def cdf(level: Level): Probability = {
    if (pShape == 0) {
      if (level>pMue) {
        1-scala.math.exp((-(level.toDouble - (pMue + shift)) / pSigma) * pNumberEvents)
      } else {
        0
      }
      
    } else {
      val treshold = pMue + ((scala.math.pow(pNumberEvents.toDouble, pShape.toDouble) - 1) / pShape) * pSigma
      if (level > treshold) {
        1 - (scala.math.pow((1 + ((pShape.toDouble * (level.toDouble - (pMue.toDouble + shift.toDouble))) / pSigma.toDouble)), (-1 / pShape)) * pNumberEvents)
      } else {
        0
      }
    }
  }

  def cdf_inverse(cuprob: Probability): Level = {
    if (pShape == 0) {
      -pSigma  * scala.math.log(1-cuprob).toFloat/pNumberEvents + (pMue + shift)
    } else {
      (pSigma / pShape) * (scala.math.pow(pNumberEvents / (1 - cuprob), pShape) - 1).toFloat + (pMue + shift)
    }
  }

  // Mathematically this is
  // f(x) = F'(x)
  def cdf_prime(level: Level): Probability = {
    (pNumberEvents / pSigma) * scala.math.pow((1 + pShape * (level - (pMue + shift)) / pSigma), ((-1 / pShape) - 1))
  }

  def returnPeriodHeight(returnPeriod: TimePeriod): Level = {
    if (pShape == 0) {
      val rp: Double = if (returnPeriod == 1) returnPeriod + 1e-10d else returnPeriod
      -(scala.math.log(1/rp.toDouble).toFloat * pSigma) / pNumberEvents + (pMue + shift)
    } else {    
      (pMue + shift) + ((scala.math.pow((returnPeriod * pNumberEvents), pShape) - 1) * pSigma).toFloat / pShape
    }
  }

  def cloneHazard: GPDFloodHazard = {
    var ret: GPDFloodHazard = new GPDFloodHazard(pMue, pSigma, pShape, pNumberEvents)
    ret.shift = shift
    return ret
  }

}