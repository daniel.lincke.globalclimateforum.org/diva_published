package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class Country extends FeatureI {
  type Data = CountryData
  type DataIO = CountryDataIO

  val dataTypeTag: ru.TypeTag[CountryData] = ru.typeTag[CountryData]
  val dataClassTag: ClassTag[CountryData] = classTag[CountryData]
  val dataTypeSave: ru.Type = ru.typeOf[CountryData]

  var mustHaveInputFile = true
  
  var data: CountryData = null
  var dataIO: CountryDataIO = null
  
  var clss: Array[Cls] = Array()
  var czs: Array[Cz] = Array()
  var admins: Array[Admin] = Array()
  
  def init() {
    data = new CountryData
    dataIO = new CountryDataIO    
  }

  def newInstanceCopy: Country = {
    val ret = new Country
    ret.data = new CountryData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }
}