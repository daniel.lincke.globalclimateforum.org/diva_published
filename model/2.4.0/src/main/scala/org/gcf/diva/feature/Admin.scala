package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class Admin extends FeatureI {
  type Data = AdminData
  type DataIO = AdminDataIO

  val dataTypeTag: ru.TypeTag[AdminData] = ru.typeTag[AdminData]
  val dataClassTag: ClassTag[AdminData] = classTag[AdminData]
  val dataTypeSave: ru.Type = ru.typeOf[AdminData]

  var mustHaveInputFile = false

  var data: AdminData = null
  var dataIO: AdminDataIO = null
  
  var clss: Array[Cls] = Array()
  var czs: Array[Cz] = Array()
  var country: Country = _
  var region: Option[Region] = _

  def init() {
    data = new AdminData
    dataIO = new AdminDataIO
  }

  def newInstanceCopy: Admin = {
    val ret = new Admin
    ret.data = new AdminData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }
}