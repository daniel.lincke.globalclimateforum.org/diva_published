package diva.model

object divaModules {

  //
  val modules: List[diva.model.ModuleI] =
    List(new diva.module.InitModule,
      new diva.module.RSLR,
      new diva.module.SocioEconomicDrivers,
      new diva.module.Flooding,
      new diva.module.IndirectErosion,
      new diva.module.Erosion,
      new diva.module.RiverModule,
      new diva.module.WetlandChange
     )

}