/* Automatically generated data IO for Country
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data.io

import diva.io._
import diva.types.InterpolationMode.InterpolationMode
import diva.feature.data.CountryData

class CountryDataIO extends FeatureDataIO[CountryData] {

  def writeData(out: divaOutput, d: CountryData) {
    d.area_below_.keys.toList.sorted.foreach { x => out.write(d.area_below_(x)) }
    d.area_below_h.keys.toList.sorted.foreach { x => out.write(d.area_below_h(x)) }
    d.assets_below_.keys.toList.sorted.foreach { x => out.write(d.assets_below_(x)) }
    d.assets_below_h.keys.toList.sorted.foreach { x => out.write(d.assets_below_h(x)) }
    d.assets_developable_below_.keys.toList.sorted.foreach { x => out.write(d.assets_developable_below_(x)) }
    d.assets_developed_below_.keys.toList.sorted.foreach { x => out.write(d.assets_developed_below_(x)) }
    out.write(d.beachnourishment_cost)
    out.write(d.beachnourishment_length_beach)
    out.write(d.beachnourishment_length_shore)
    out.write(d.beachnourishment_volume)
    out.write(d.coastlength)
    out.write(d.gdp)
    out.write(d.gdpc)
    d.h.keys.toList.sorted.foreach { x => out.write(d.h(x)) }
    out.write(d.landloss_erosion_total)
    out.write(d.landloss_initial_land_below_S1)
    out.write(d.landloss_submergence)
    out.write(d.landlosscost_erosion_total)
    out.write(d.landlosscost_submergence)
    out.write(d.length_protected)
    out.write(d.migration_erosion)
    out.write(d.migration_retreat)
    out.write(d.migration_submergence)
    out.write(d.migrationcost_erosion)
    out.write(d.migrationcost_retreat)
    out.write(d.migrationcost_submergence)
    out.write(d.openwater)
    out.write(d.par)
    d.pop_below_.keys.toList.sorted.foreach { x => out.write(d.pop_below_(x)) }
    d.pop_below_h.keys.toList.sorted.foreach { x => out.write(d.pop_below_h(x)) }
    out.write(d.population_correction)
    out.write(d.protlevel_implemented)
    out.write(d.protlevel_implemented_before_dikeraise)
    out.write(d.protlevel_ud)
    out.write(d.riverdike_cost)
    out.write(d.riverdike_increase_maintenance_cost)
    out.write(d.riverdike_length)
    out.write(d.riverdike_maintenance_cost)
    out.write(d.riverfloodcost)
    out.write(d.rslr)
    out.write(d.salinity_cost)
    out.write(d.sandloss_erosion_direct)
    out.write(d.sandloss_erosion_including_nourishment)
    out.write(d.sandloss_erosion_indirect)
    out.write(d.sandloss_erosion_total)
    out.write(d.seadike_cost)
    out.write(d.seadike_height)
    out.write(d.seadike_height_initial)
    out.write(d.seadike_increase_maintenance_cost)
    out.write(d.seadike_maintenance_cost)
    out.write(d.seafloodcost)
    d.seafloodcost_h.keys.toList.sorted.foreach { x => out.write(d.seafloodcost_h(x)) }
    out.write(d.seafloodcost_relative)
    out.write(d.setback_length)
    out.write(d.setbackzone_administrative_cost)
    out.write(d.setbackzone_area)
    out.write(d.setbackzone_assets)
    out.write(d.setbackzone_deprecation)
    out.write(d.setbackzone_population)
    out.write(d.slr_climate_induced)
    out.write(d.slr_nonclimate_induced)
    out.write(d.surge_barrier_cost)
    out.write(d.surge_barrier_maintenance_cost)
    out.write(d.surge_barriers_number)
    out.write(d.totalpop)
    out.write(d.tourarr)
    out.write(d.tourlosscost)
    out.write(d.tourtav)
    out.write(d.urbanshare)
    out.write(d.wetfresh_value)
    out.write(d.wetland_area_freshmarshes)
    out.write(d.wetland_area_mangroves)
    out.write(d.wetland_area_saltmarshes)
    out.write(d.wetland_area_total)
    out.write(d.wetland_nourishment_cost)
    out.write(d.wetland_value)
    out.write(d.wetlow)
    out.write(d.wetlow_value)
    out.write(d.wetmang_value)
    out.write(d.wetsalt_value)
    out.write(d.wetvalueloss)
  }

  def writeSelectedData(out: divaOutput, d: CountryData, ignoreThisOutput: Array[Boolean]) {
    var c: Int = 0
    d.area_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.area_below_(x))}; c+=1 } } 
    d.area_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.area_below_h(x))}; c+=1 } } 
    d.assets_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_below_(x))}; c+=1 } } 
    d.assets_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_below_h(x))}; c+=1 } } 
    d.assets_developable_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_developable_below_(x))}; c+=1 } } 
    d.assets_developed_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.assets_developed_below_(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_length_beach)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_length_shore)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.beachnourishment_volume)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.coastlength)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.gdp)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.gdpc)}; c+=1
    d.h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.landloss_erosion_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landloss_initial_land_below_S1)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landloss_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landlosscost_erosion_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.landlosscost_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.length_protected)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_erosion)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_retreat)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migration_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migrationcost_erosion)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migrationcost_retreat)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.migrationcost_submergence)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.openwater)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.par)}; c+=1
    d.pop_below_.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.pop_below_(x))}; c+=1 } } 
    d.pop_below_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.pop_below_h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.population_correction)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.protlevel_implemented)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.protlevel_implemented_before_dikeraise)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.protlevel_ud)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_increase_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_length)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverfloodcost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.rslr)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.salinity_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_direct)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_including_nourishment)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_indirect)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.sandloss_erosion_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_height)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_height_initial)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_increase_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seadike_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost)}; c+=1
    d.seafloodcost_h.keys.toList.sorted.foreach { x => {if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_h(x))}; c+=1 } } 
    if (!ignoreThisOutput(c)) {out.write(d.seafloodcost_relative)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setback_length)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_administrative_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_area)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_assets)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_deprecation)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.setbackzone_population)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.slr_climate_induced)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.slr_nonclimate_induced)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barriers_number)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.totalpop)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.tourarr)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.tourlosscost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.tourtav)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.urbanshare)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetfresh_value)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_area_freshmarshes)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_area_mangroves)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_area_saltmarshes)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_area_total)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_nourishment_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetland_value)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetlow)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetlow_value)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetmang_value)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetsalt_value)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.wetvalueloss)}; c+=1
  }

  def readInputValue(in: divaInput, d:CountryData, parName: String, locid: String, time: Int) {
    if (parName=="buddism") {d.buddism = in.getInputValue[Int]("buddism", locid, time)}
    else if (parName=="christianity") {d.christianity = in.getInputValue[Int]("christianity", locid, time)}
    else if (parName=="civlib") {d.civlib = in.getInputValue[Float]("civlib", locid, time)}
    else if (parName=="corrupt") {d.corrupt = in.getInputValue[Float]("corrupt", locid, time)}
    else if (parName=="democracy") {d.democracy = in.getInputValue[Float]("democracy", locid, time)}
    else if (parName=="ecfreedom") {d.ecfreedom = in.getInputValue[Float]("ecfreedom", locid, time)}
    else if (parName=="gdpc") {d.gdpc = in.getInputValue[Float]("gdpc", locid, time)}
    else if (parName=="gini") {d.gini = in.getInputValue[Float]("gini", locid, time)}
    else if (parName=="govqual") {d.govqual = in.getInputValue[Float]("govqual", locid, time)}
    else if (parName=="hinduism") {d.hinduism = in.getInputValue[Int]("hinduism", locid, time)}
    else if (parName=="individ") {d.individ = in.getInputValue[Float]("individ", locid, time)}
    else if (parName=="islam") {d.islam = in.getInputValue[Int]("islam", locid, time)}
    else if (parName=="lati") {d.lati = in.getInputValue[Float]("lati", locid, time)}
    else if (parName=="lifeexp") {d.lifeexp = in.getInputValue[Float]("lifeexp", locid, time)}
    else if (parName=="literacy") {d.literacy = in.getInputValue[Float]("literacy", locid, time)}
    else if (parName=="longi") {d.longi = in.getInputValue[Float]("longi", locid, time)}
    else if (parName=="longterm") {d.longterm = in.getInputValue[Float]("longterm", locid, time)}
    else if (parName=="mascul") {d.mascul = in.getInputValue[Float]("mascul", locid, time)}
    else if (parName=="power") {d.power = in.getInputValue[Float]("power", locid, time)}
    else if (parName=="protlevel_ud") {d.protlevel_ud = in.getInputValue[Float]("protlevel_ud", locid, time)}
    else if (parName=="seadike_unit_cost_rural") {d.seadike_unit_cost_rural = in.getInputValue[Float]("seadike_unit_cost_rural", locid, time)}
    else if (parName=="seadike_unit_cost_urban") {d.seadike_unit_cost_urban = in.getInputValue[Float]("seadike_unit_cost_urban", locid, time)}
    else if (parName=="stability") {d.stability = in.getInputValue[Float]("stability", locid, time)}
    else if (parName=="temp") {d.temp = in.getInputValue[Float]("temp", locid, time)}
    else if (parName=="tempmult") {d.tempmult = in.getInputValue[Float]("tempmult", locid, time)}
    else if (parName=="totalpop") {d.totalpop = in.getInputValue[Float]("totalpop", locid, time)}
    else if (parName=="tourarr") {d.tourarr = in.getInputValue[Float]("tourarr", locid, time)}
    else if (parName=="tourtav") {d.tourtav = in.getInputValue[Float]("tourtav", locid, time)}
    else if (parName=="urbanshare") {d.urbanshare = in.getInputValue[Float]("urbanshare", locid, time)}
    // else println("error:" + parName + " not found in Country")
  }

  def readOptionInputValue(in: divaInput, d:CountryData, parName: String, locid: String, time: Int) {
    if (parName=="fraction_developable") {d.fraction_developable = in.getOptionInputValue[Float]("fraction_developable", locid, time)}
    else if (parName=="fraction_developed") {d.fraction_developed = in.getOptionInputValue[Float]("fraction_developed", locid, time)}
    // else println("error:" + parName + " not found in Country")
  }

  def readScenarioValue(in: divaInput, d:CountryData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
    if (parName=="gdpcgrowth") {d.gdpcgrowth = in.getScenarioValue[Float]("gdpcgrowth", locid, time, pInterpolationMode)}
    else if (parName=="popgrowth") {d.popgrowth = in.getScenarioValue[Float]("popgrowth", locid, time, pInterpolationMode)}
    else if (parName=="urbansharegrowth") {d.urbansharegrowth = in.getScenarioValue[Float]("urbansharegrowth", locid, time, pInterpolationMode)}
    // else println("error:" + parName + " not found in Country")
  }

  def readOptionScenarioValue(in: divaInput, d:CountryData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

}
