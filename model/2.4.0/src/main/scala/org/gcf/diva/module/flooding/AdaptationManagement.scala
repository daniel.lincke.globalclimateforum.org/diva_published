package diva.module.flooding

import scala.language.implicitConversions
import scala.language.reflectiveCalls
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap
import scala.util.control.Breaks._

import diva.types.DikePositionMode._
import diva.types.DikeMode._
import diva.toolbox.Toolbox._
import diva.feature.Cls
import diva.feature.Country
import diva.feature.City
import diva.model.divaState
import diva.toolbox.NumericalIntegration._
import diva.model.hasLogger
import diva.types.TypeComputations._

import org.apache.commons.math3.optim.univariate.UnivariateOptimizer
import org.apache.commons.math3.optim.univariate.BrentOptimizer
import org.apache.commons.math3.optim.OptimizationData
import org.apache.commons.math3.optim.MaxIter
import org.apache.commons.math3.optim.MaxEval
import org.apache.commons.math3.optim.InitialGuess
import org.apache.commons.math3.optim.univariate.SearchInterval
import org.apache.commons.math3.optim.univariate.UnivariateObjectiveFunction
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType
import org.apache.commons.math3.optim.nonlinear.scalar.MultiStartMultivariateOptimizer
import org.apache.commons.math3.optim.univariate.MultiStartUnivariateOptimizer
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.NelderMeadSimplex
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction
import org.apache.commons.math3.analysis.UnivariateFunction
import org.apache.commons.math3.analysis.MultivariateFunction
import org.apache.commons.math3.random.JDKRandomGenerator
import org.apache.commons.math3.random.UncorrelatedRandomVectorGenerator
import org.apache.commons.math3.random.UniformRandomGenerator

class AdaptationManagement(impactComputation: ImpactComputation, dikePositionMode: DikePositionMode, dikeInitilisationMode: DikeMode, dikeRaisingMode: DikeMode, dikeOptimizationRPStart: Int, dikeOptimizationRPEnd: Int, dikeOptimizationRPStep: Int, extendedDFS: Boolean = false) extends hasLogger {

  // parameters for calculating protlevel
  var popdensHalf: Float = 50f
  var popdensUninhabitedThreshold: Float = 30f
  var popdensRuralThreshold: Float = 300f
  var popdensUrbanThreshold: Float = 1000f
  var discountRate: Float = 0.0f
  var dikeRaisingStartYear: Int = 1995
  var submergenceMigration: Boolean = false
  var submergenceMigrationLevel: Float = 1.0f
  var protectionLevelFactor: Float = 1.0f
  var migrcostToGdpcRatio: Float = 3.0f
  var seadikeConstructionCostFactor: Float = 1.0f
  var protlevelInhabitedThreshold: Float = 30f
  var targetRelativeFloodRisk: Float = 0.0001f

  var bcrToAllowNewDike: Float = 1.0f

  // One dollar back in 1995 is equivalent to ... dollar in current dollar base year
  // Introduced to switch to another dollar base year.
  private val moneyForwardsFrom1995ConverionFactor: Float = 1.56f
  private val moneyBackwardsTo1995ConverionFactor: Float = 1 / moneyForwardsFrom1995ConverionFactor
  // scaling factor flood protection min 0.013 mean 0.013 max 0.013
  // scaling factor flood protection min 0.1165 mean 0.1165 max 0.1165
  private val cstfldprt: Float = if (extendedDFS) 0.1165f else 0.013f

  // income elasticity min 0.68 mean 1.02 max 1.36
  // income elasticity min 1.15 mean 1.59 max 1.93
  private val inc_elas: Float = if (extendedDFS) 1.59f else 1.02f

  // population density elasticity min 0.04 mean 0.24 max 0.44
  // population density elasticity min 1.00 mean 1.00 max 1.00
  private val pd_elas: Float = if (extendedDFS) 1.00f else 0.24f

  // income distribution elasticity
  // income distribution elasticity min 0.21 mean 1.89 max 3.57
  private val gini_elas = if (extendedDFS) -1.89f else 0f

  // democracy parameter
  // democracy parameter min -0.48 mean -0.30 max -0.12
  private val democ_par = if (extendedDFS) -0.30f else 0f

  // corruption parameter
  // corruption parameter min -1.39 mean -0.75 max -0.1
  private val corrupt_par = if (extendedDFS) -0.75f else 0f

  // economic freedom parameter
  // economic freedom parameter min 0.03 mean 0.17 max 0.31
  private val econfree_par = if (extendedDFS) 0.17f else 0f

  // literacy parameters
  // literacy parameters min -0.05 mean -0.03 max -0.01
  private val lit_par = if (extendedDFS) -0.03f else 0f

  // new DFS coefficients
  private val dfs_new_const: Float = -5.919699f
  private val dfs_new_coeff_urbanshare: Float = 0.953546f
  private val dfs_new_coeff_income: Float = 0.521271f
  private val dfs_new_coeff_assets: Float = 0.196141f
  private val dfs_new_coeff_stability: Float = 0.470919f
  private val dfs_new_coeff_masculinity: Float = -0.024471f
  private val dfs_new_coeff_longtermorientation: Float = 0.014788f

  val randomG: JDKRandomGenerator = new JDKRandomGenerator
  randomG.setSeed(44428400075l)

  def getDikeConstructionCost(cls: Cls, additionalDikeHeigth: Float): Float = {
    if (additionalDikeHeigth <= 0)
      getDikeDeconstructionCost(cls, additionalDikeHeigth)
    else
      getSeaDikeUnitCost(cls) * seadikeConstructionCostFactor * cls.data.length * additionalDikeHeigth
  }

  private def getDikeDeconstructionCost(cls: Cls, additionalDikeHeigth: Float): Float = {
    -0.2f * getSeaDikeUnitCost(cls) * seadikeConstructionCostFactor * cls.data.length * additionalDikeHeigth
  }

  def getDikeMaintenanceCost(cls: Cls, actualDikeHeigth: Float): Float = {
    0.01f * getSeaDikeUnitCost(cls) * seadikeConstructionCostFactor * cls.data.length * actualDikeHeigth
  }

  def getSeaDikeUnitCost(cls: Cls): Float = {
    cls.country.data.seadike_unit_cost_rural
    /*val fullFloodPlainLevel: Float = cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float]
    val popdensInFullFloodPlain: Float = cls.coastalPlane.populationDensityBelow(fullFloodPlainLevel)
    if (popdensInFullFloodPlain <= popdensRuralThreshold) {
      cls.country.data.seadike_unit_cost_rural
    } else {
      if (popdensInFullFloodPlain > popdensUrbanThreshold) {
        cls.country.data.seadike_unit_cost_urban
      } else {
        (((popdensInFullFloodPlain - popdensRuralThreshold) / (popdensUrbanThreshold - popdensRuralThreshold)) *
          (cls.country.data.seadike_unit_cost_urban - cls.country.data.seadike_unit_cost_rural)) + cls.country.data.seadike_unit_cost_rural
      }
    }
    */
  }

  def computeTargetProtection(cls: Cls, state: divaState, initialisation: Boolean): Float = initialisation match {
    case true => computeTargetProtection(cls, state, dikeInitilisationMode)
    case false => computeTargetProtection(cls, state, dikeRaisingMode)
  }

  def computeTargetProtectionPerCountry(clss: Array[Cls], country: Country, state: divaState, initialisation: Boolean) = initialisation match {
    case true => logger.error("computeTargetProtectionPerCountry in initialisation should never be invoked for dikeMode " + dikeInitilisationMode)
    case false => computeTargetProtection(clss, country, state, dikeRaisingMode)
  }

  def computeDikePosition(cls: Cls, dikePositionMode: DikePositionMode): Float = dikePositionMode match {
    case ONLY_COASTAL => 0
    case ALSO_INLAND => computeDikePositionSimple(cls)
  }

  private def computeTargetProtection(cls: Cls, state: divaState, dikeMode: DikeMode): Float = dikeMode match {
    case DEMAND_FOR_SAFETY_ORIGINAL => protectionLevelFactor * demandForSafetyProtectionLevel(cls, cls.country)
    case DEMAND_FOR_SAFETY_ORIGINAL_ONLY_SED => protectionLevelFactor * demandForSafetyProtectionLevel(cls, cls.country, true)
    case DEMAND_FOR_SAFETY_NEW => protectionLevelFactor * demandForSafetyNewProtectionLevel(cls, cls.country, true)
    case GLOBAL_EXPERT_TABLE => protectionLevelFactor * tableProtectionLevel(cls,state.time)

    case CONSTANT_PROTECTION_LEVELS => cls.data.protlevel_target
    case NO_DIKE_RAISE => cls.data.protlevel_implemented_before_dikeraise
    case CONSTANT_FLOOD_RISK => keepFloodRisk(cls, state, _.data.seafloodcost, currentFloodRisk)
    case CONSTANT_RELATIVE_FLOOD_RISK => keepFloodRisk(cls, state, _.data.seafloodcost_relative, currentRelativeFloodRisk)
    case TARGET_RELATIVE_FLOOD_RISK => keepFloodRisk(cls, state, _.data.localgdp * targetRelativeFloodRisk, currentProtectionCostWithLimitedFloodcost)

    case CONSTANT_FLOOD_RISK_INHABITED_AREAS => keepFloodRiskInhabitedAreas(cls, state, _.data.seafloodcost, currentFloodRisk)
    case CONSTANT_RELATIVE_FLOOD_RISK_INHABITED_AREAS => keepFloodRiskInhabitedAreas(cls, state, _.data.seafloodcost_relative, currentRelativeFloodRisk)
    case TARGET_RELATIVE_FLOOD_RISK_INHABITED_AREAS =>
      if (currentFloodRisk(cls)(cls.data.protlevel_implemented_before_dikeraise) < (cls.data.localgdp * targetRelativeFloodRisk)) {
        cls.data.protlevel_implemented_before_dikeraise
      } else {
        keepFloodRiskInhabitedAreas(cls, state, _.data.localgdp * targetRelativeFloodRisk, currentProtectionCostWithLimitedFloodcost)
      }
    case NO_DIKES => 0
    case TOTAL_PROTECTION_LEVEL_100 => 100
    case INHABITED_PROTECTION_LEVEL_100 => {
      if (cls.coastalPlane.populationDensityBelow(cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float]) >= protlevelInhabitedThreshold) protectionLevelFactor * 100 else 0
    }
    case PROTECTION_LEVEL_UD => protectionLevelFactor * udProtection(cls)
    case OPTIMAL_DISCRETE_DIKES => optimalDiscreteProtectionLevel(cls, state)
    case OPTIMAL_DIKES => optimalProtectionLevel(cls, state)
    //case DIAZ_DIKES              => diazProtectionTrajectory(cls, state)
    case OPTIMAL_DIKE_TRAJECTORY => optimalProtectionTrajectory(cls, state)

  }

  private def computeTargetProtection(clss: Array[Cls], country: Country, state: divaState, dikeMode: DikeMode) = dikeMode match {
    case COUNTRY_TARGET_RELATIVE_FLOOD_RISK => logger.error("dikeMode COUNTRY_TARGET_RELATIVE_FLOOD_RISK is not implemented");
    case COUNTRY_CONSTANT_RELATIVE_FLOOD_RISK => keepFloodRisk(clss.filter(cls => cls.data.protlevel_target > 0), clss.filter(cls => cls.data.protlevel_target == 0), country, state, _.data.seafloodcost_relative, currentRelativeFloodRiskCountry)
  }

  private def demandForSafetyProtectionLevel(cls: Cls, country: Country, ignoreSLR: Boolean = false): Float = {

    val fullFloodPlainLevel: Float = if (ignoreSLR) (cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float] - cls.data.rslr) else cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float]
    //val fullFloodPlainLevel: Float = 16.5f
    val h100ForCalculation: Float = if (ignoreSLR) (cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float] - cls.data.rslr) else cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float]
    val popdensInFullFloodPlain: Float = cls.coastalPlane.populationDensityBelow(fullFloodPlainLevel)
    val popPerKilometer: Float = cls.coastalPlane.populationExposure.cumulativeExposure(fullFloodPlainLevel).asInstanceOf[Float] / cls.data.length
    var fullDemandForSafetyProtlevel: Float = (cstfldprt * Math.pow((cls.data.gdpc * moneyBackwardsTo1995ConverionFactor), inc_elas).asInstanceOf[Float] * Math.pow(popdensInFullFloodPlain, pd_elas).asInstanceOf[Float]
      * 0.97 * Math.pow((h100ForCalculation / 2.14), -0.6667).asInstanceOf[Float]
      * Math.pow(country.data.gini, gini_elas).asInstanceOf[Float]
      * Math.exp((democ_par * country.data.democracy + corrupt_par * country.data.corrupt + econfree_par * country.data.ecfreedom + lit_par * country.data.literacy).asInstanceOf[Double])).asInstanceOf[Float]

    if (fullDemandForSafetyProtlevel.isNaN())
      fullDemandForSafetyProtlevel = 0
    val protlevel =
      //if ((popdensInFullFloodPlain < popdensUninhabitedThreshold) || (popPerKilometer < 100)) 0
      if (popdensInFullFloodPlain < popdensUninhabitedThreshold) 0
      else ((popdensInFullFloodPlain - popdensUninhabitedThreshold) / (popdensHalf - popdensUninhabitedThreshold)) / (1 + ((popdensInFullFloodPlain - popdensUninhabitedThreshold) / (popdensHalf - popdensUninhabitedThreshold))) * fullDemandForSafetyProtlevel

    limited(protlevel, 0f, 10000f)
  }

  private def demandForSafetyNewProtectionLevel(cls: Cls, country: Country, ignoreSLR: Boolean = false): Float = {

    val fullFloodPlainLevel: Float = if (ignoreSLR) (cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float] - cls.data.rslr) else cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float]
    //val fullFloodPlainLevel: Float = 16.5f
    val h100ForCalculation: Float = if (ignoreSLR) (cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float] - cls.data.rslr) else cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float]
    val popdensInFullFloodPlain: Float = cls.coastalPlane.populationDensityBelow(fullFloodPlainLevel)
    val popPerKilometer: Float = cls.coastalPlane.populationExposure.cumulativeExposure(fullFloodPlainLevel).asInstanceOf[Float] / cls.data.length
    var fullDemandForSafetyProtlevel: Float = (cstfldprt * Math.pow((cls.data.gdpc * moneyBackwardsTo1995ConverionFactor), inc_elas).asInstanceOf[Float] * Math.pow(popdensInFullFloodPlain, pd_elas).asInstanceOf[Float]
      * 0.97 * Math.pow((h100ForCalculation / 2.14), -0.6667).asInstanceOf[Float]
      * Math.pow(country.data.gini, gini_elas).asInstanceOf[Float]
      * Math.exp((democ_par * country.data.democracy + corrupt_par * country.data.corrupt + econfree_par * country.data.ecfreedom + lit_par * country.data.literacy).asInstanceOf[Double])).asInstanceOf[Float]

    if (fullDemandForSafetyProtlevel.isNaN())
      fullDemandForSafetyProtlevel = 0
    val protlevel =
      //if ((popdensInFullFloodPlain < popdensUninhabitedThreshold) || (popPerKilometer < 100)) 0
      if (popdensInFullFloodPlain < popdensUninhabitedThreshold) 0
      else ((popdensInFullFloodPlain - popdensUninhabitedThreshold) / (popdensHalf - popdensUninhabitedThreshold)) / (1 + ((popdensInFullFloodPlain - popdensUninhabitedThreshold) / (popdensHalf - popdensUninhabitedThreshold))) * fullDemandForSafetyProtlevel

    limited(protlevel, 0f, 10000f)
  }

  private def computeDikePositionSimple(cls: Cls): Float = {
    val densitiesAbove = shiftLeft(cls.coastalPlane.pHeights.map { x: Float => cls.coastalPlane.populationDensityBelow(x) }).toArray
    val elevationLevelsAndPopulationDensities: Array[(Float, Float)] = cls.coastalPlane.pHeights zip densitiesAbove
    val targetLevel: Option[(Float, Float)] = elevationLevelsAndPopulationDensities find { (x: (Float, Float)) => (x._2 >= popdensUninhabitedThreshold) }
    targetLevel match {
      case None => 0
      case Some(pair) => pair._1.asInstanceOf[Float]
    }
  }

  private def tableProtectionLevel(cls: Cls, time: Int, ignoreSLR: Boolean = false): Float = {
    if ((cls.country.locationid == "NLD") && (time>1950))
      5000.0f
    else {
      if ((cls.city != None) && (cls.city.get.data.totalpop>=1000)) cls.city.get.data.protlevel_ud
      else getTableProtectionLevel(cls, ignoreSLR)
    }
  }

  private def udProtection(cls: Cls): Float = {
    val fullFloodPlainLevel: Float = cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float]
    val popdensInFullFloodPlain: Float = cls.coastalPlane.populationDensityBelow(fullFloodPlainLevel)

    val uninhabited: Boolean = (popdensInFullFloodPlain < popdensUninhabitedThreshold)
    val urban = (popdensInFullFloodPlain >= popdensUrbanThreshold)

    val protlevel: Float =
      if (popdensInFullFloodPlain < popdensUninhabitedThreshold) 0
      else {
        if (cls.city != None) cls.city.get.data.protlevel_ud
        else cls.country.data.protlevel_ud
      }

    protlevel
  }

  private def getTableProtectionLevel(cls: Cls, ignoreSLR: Boolean = false): Float = {
    val fullFloodPlainLevel: Float = cls.floodHazard.returnPeriodHeight(100.0d).asInstanceOf[Float]
    val popdensInFullFloodPlain: Float = cls.coastalPlane.populationDensityBelow(fullFloodPlainLevel)

    val uninhabited: Boolean = (popdensInFullFloodPlain < popdensUninhabitedThreshold)
    val urban = (popdensInFullFloodPlain >= popdensUrbanThreshold)

    val protlevel: Float =
      if (uninhabited) {
        0
      } else if (urban) {
        // urban
        if (cls.data.gdpc < 1035) 10f
        else if (cls.data.gdpc < 4085) 25f
        else if (cls.data.gdpc < 12615) 100f
        else 200f
      } else {
        // rural
        if (cls.data.gdpc < 1035) 0f
        else if (cls.data.gdpc < 4085) 0f
        else if (cls.data.gdpc < 12615) 20f
        else 50f
      }

    protlevel
  }

  private def computeProtectionLevelCost(cls: Cls, state: divaState)(pl: Double): (Float, Float) = {

    var cls_new = cls.clone

    var lInvestmentCost: Float = 0
    var lDamageCost: Float = 0
    var lDikeHeigthLastPeriod: Float = cls.data.seadike_height
    var landLossBefore: Float = cls.coastalPlane.areaExposure.cumulativeExposure(cls.floodHazard.returnPeriodHeight(1.0f)).asInstanceOf[Float]
    for (t <- state.time to state.endTime by state.timeStep) {
      val lYearsInFuture: Int = t - state.time;
      val discountFactor: Float = scala.math.pow(1 - discountRate, lYearsInFuture).asInstanceOf[Float]
      cls_new.floodHazard.shift = cls_new.data.rslr_memory(t)
      cls_new.socioEconomicDevelopment(t, state.timeStep)

      var resultsIntermediatedStep: (Float, Float, Float, Float) = computeProtectionLevelCostAtTime(cls_new, state)(pl)(discountFactor)(lDikeHeigthLastPeriod, landLossBefore)

      lInvestmentCost += resultsIntermediatedStep._1
      lDamageCost += resultsIntermediatedStep._2
      landLossBefore = resultsIntermediatedStep._3
      lDikeHeigthLastPeriod = resultsIntermediatedStep._4
    }
    (lInvestmentCost, lDamageCost)
  }

  private def computeProtectionLevelCostAtTime(cls: Cls, state: divaState)(pl: Double)(discountFactor: Float)(lDikeHeigthLastPeriod: Float, landLossBefore: Float): (Float, Float, Float, Float) = {
    var landLossBeforeNew: Float = landLossBefore
    var dikeHeigthLastPeriodNew: Float = lDikeHeigthLastPeriod

    val lIntendedDikeHeight: Float = if (pl > 0) cls.floodHazard.returnPeriodHeight(pl).asInstanceOf[Float] else 0
    val seadikeConstrutionCost: Float = getDikeConstructionCost(cls, lIntendedDikeHeight - lDikeHeigthLastPeriod) * 1000000
    val seadikeMaintenanceCost: Float = getDikeMaintenanceCost(cls, lIntendedDikeHeight) * 1000000 * state.timeStep

    cls.data.seadike_height = lIntendedDikeHeight
    val discountedTotalcost: (Float, Float) =
      if (submergenceMigration && (pl == 0)) {
        
        val migrationHeigth: Float = cls.floodHazard.returnPeriodHeight(submergenceMigrationLevel).asInstanceOf[Float]
        var coastalCz = cls.czs.filter(cz => cz.data.coastalzone).head
        val migrationEffect: (Double, Double) = cls.czs.foldLeft((0.0, 0.0))((sums, cz) => {
          val sums_temp: (Double, Double) = cz.coastalPlane.migrateBelow(migrationHeigth, 1.0f)
          (sums._1 + sums_temp._1, sums._2 + sums_temp._2)
        })

        val migration_cost: Float = migrationEffect._1.asInstanceOf[Float] * migrcostToGdpcRatio * cls.data.gdpc
        cls.recomputeCoastalPlane

        val landLoss: Double = cls.coastalPlane.areaExposure.cumulativeExposure(cls.floodHazard.returnPeriodHeight(1.0f))
        val landLossCost: Float = ((landLoss - landLossBefore) * cls.data.landvalue_unit).asInstanceOf[Float]
        landLossBeforeNew = landLoss.asInstanceOf[Float]

        val impacts: (Float, Float) = impactComputation.getCurrentImpacts(cls)

        val totalInvesnmentCost: Float = seadikeConstrutionCost + seadikeMaintenanceCost
        val totalDamageCost: Float = impacts._2 * state.timeStep + migration_cost + landLossCost

        (totalInvesnmentCost * discountFactor, totalDamageCost * discountFactor)
      } else {
        val impacts: (Float, Float) = impactComputation.getCurrentImpacts(cls)

        val totalInvesnmentCost: Float = seadikeConstrutionCost + seadikeMaintenanceCost
        val totalDamageCost: Float = impacts._2 * state.timeStep

        (totalInvesnmentCost * discountFactor, totalDamageCost * discountFactor)
      }

    dikeHeigthLastPeriodNew = lIntendedDikeHeight.asInstanceOf[Float]
    // break the loop if cost are already too high
    //if (lCost>minimalCost) break;
    (discountedTotalcost._1, discountedTotalcost._2, landLossBeforeNew, dikeHeigthLastPeriodNew)
  }

  private def currentFloodRisk(cls: Cls)(pl: Double): Float = {
    val lIntendedDikeHeight: Float = if (pl > 0) cls.floodHazard.returnPeriodHeight(pl).asInstanceOf[Float] else 0
    var cls_new: Cls = cls.clone
    cls_new.data.seadike_height = lIntendedDikeHeight
    impactComputation.getCurrentImpacts(cls_new)._2
  }

  private def currentRelativeFloodRisk(cls: Cls)(pl: Double): Float = {
    val lIntendedDikeHeight: Float = if (pl > 0) cls.floodHazard.returnPeriodHeight(pl).asInstanceOf[Float] else 0
    var cls_new: Cls = cls.clone
    cls_new.data.seadike_height = lIntendedDikeHeight
    (impactComputation.getCurrentImpacts(cls_new)._2 / (cls_new.data.localgdp))
  }

  private def computeCurrentFloodRisk(cls: Array[Cls])(pl: Array[Double]): Float = {
    val damages: Array[Float] = (cls zip pl).map {
      clspl_ =>
        val lIntendedDikeHeight: Float = if (clspl_._2 > 0) clspl_._1.floodHazard.returnPeriodHeight(clspl_._2).asInstanceOf[Float] else 0
        var cls_new: Cls = clspl_._1.clone
        cls_new.data.seadike_height = lIntendedDikeHeight
        impactComputation.getCurrentImpacts(cls_new)._2
    }
    damages.fold(0f)(_ + _)
  }

  private def currentRelativeFloodRiskCountry(cls: Array[Cls])(country: Country)(pl: Array[Double]): Float = {
    val damages: Array[Float] = (cls zip pl).map {
      clspl_ =>
        val lIntendedDikeHeight: Float = if (clspl_._2 > 0) clspl_._1.floodHazard.returnPeriodHeight(clspl_._2).asInstanceOf[Float] else 0
        var cls_new: Cls = clspl_._1.clone
        cls_new.data.seadike_height = lIntendedDikeHeight
        impactComputation.getCurrentImpacts(cls_new)._2
    }
    (damages.fold(0f)(_ + _) / 1000000) / country.data.gdp
  }

  private def computeCurrentDikeCost(cls: Array[Cls])(pl: Array[Double]): Float = {
    val lDikeConstructionCost: Array[Float] = (cls zip pl).map {
      clspl_ =>
        if (clspl_._2 > 1f) {
          getDikeConstructionCost(clspl_._1, (clspl_._1.floodHazard.returnPeriodHeight(clspl_._2) - clspl_._1.data.seadike_height).asInstanceOf[Float])
        } else {
          0f
        }
    }

    val lDikeMaintenanceCost: Array[Float] = (cls zip pl).map {
      clspl_ =>
        if (clspl_._2 > 1f) {
          getDikeMaintenanceCost(clspl_._1, (clspl_._1.floodHazard.returnPeriodHeight(clspl_._2)).asInstanceOf[Float])
        } else {
          getDikeMaintenanceCost(clspl_._1, clspl_._1.data.seadike_height)
        }
    }
    /*
    println(pl.toList)
    println(((cls zip pl).map { clspl_ => clspl_._1.floodHazard.returnPeriodHeight(clspl_._2) - clspl_._1.data.seadike_height }).toList)
    println(lDikeConstructionCost.toList)
    println(lDikeMaintenanceCost.toList)
		*/

    lDikeConstructionCost.fold(0f)(_ + _) + lDikeMaintenanceCost.fold(0f)(_ + _)
  }

  private def currentProtectionCostWithLimitedFloodcost(cls: Cls)(pl: Double): Float = {
    val lDikeConstructionCost: Float = if (pl > 1) {
      getDikeConstructionCost(cls, (cls.floodHazard.returnPeriodHeight(pl) - cls.data.seadike_height).asInstanceOf[Float])
    } else {
      0f
    }

    val lDikeMaintenanceCost: Float = if (pl > 1f) {
      getDikeMaintenanceCost(cls, (cls.floodHazard.returnPeriodHeight(pl)).asInstanceOf[Float])
    } else {
      getDikeMaintenanceCost(cls, cls.data.seadike_height)
    }

    val lProtectionCost: Float = lDikeConstructionCost + lDikeMaintenanceCost
    val floodRisk: Float = currentFloodRisk(cls)(pl)
    val ret: Float =
      if (floodRisk > (cls.data.localgdp * targetRelativeFloodRisk))
        Float.MaxValue
      else
        lProtectionCost
    ret
  }

  private def keepFloodRiskInhabitedAreas(cls: Cls, state: divaState, targetRiskFunction: Cls => Float, riskFunction: Cls => Double => Float): Float = {

    if (cls.data.seadike_height == 0) {
      protectionLevelFactor * tableProtectionLevel(cls, state.time)
    } else {

      var target_protection_level: Float = keepFloodRisk(cls, state, targetRiskFunction, riskFunction)
      if (target_protection_level > 0) {
        target_protection_level
      } else {
        // This has to be done to avoid oscillation in cities and the NLD:
        // in one period we have target_protection_level = 0, in the next period we use the table and have target_protection_level >0
        if (tableProtectionLevel(cls, state.time) > 0) {
          tableProtectionLevel(cls, state.time)
        } else {
          0
        }
      }
    }
  }

  private def keepFloodRisk(cls: Cls, state: divaState, targetRiskFunction: Cls => Float, riskFunction: Cls => Double => Float): Float = {

    val targetFloodRisk: Double =
      if (cls.data.seafloodcost_target > 0) {
        cls.data.seafloodcost_target
      } else {
        cls.data.seafloodcost_target = targetRiskFunction(cls)
        cls.data.seafloodcost_target
      }

    // check: where does 1.0 come from? Change back to 0.0!
    if ((targetFloodRisk > 0) && (cls.data.protlevel_implemented > 0.0)) {

      val abs_stop = 0.0001 * targetFloodRisk

      val underlying: UnivariateOptimizer = new BrentOptimizer(1e-3, abs_stop)
      val optimizer: MultiStartUnivariateOptimizer = new MultiStartUnivariateOptimizer(underlying, 1, randomG)

      val protectionLevelCost: UnivariateFunction = new UnivariateFunction() {
        def value(x: Double): Double = {
          val temp: Float = riskFunction(cls)(x)
          scala.math.sqrt((temp - targetFloodRisk) * (temp - targetFloodRisk))
        }
      }

      var initialGuess: Array[Double] = new Array[Double](1)
      initialGuess(0) = cls.data.protlevel_target.toDouble
      val r = optimizer.optimize(
        new UnivariateObjectiveFunction(protectionLevelCost),
        new MaxEval(200), GoalType.MINIMIZE, new SearchInterval(10, 10e09), new InitialGuess(initialGuess))
      r.getPoint().toFloat
    } else {

      0
    }
  }

  private def keepFloodRisk(clssWithDikes: Array[Cls], clssWithoutDikes: Array[Cls], country: Country, state: divaState, targetRiskFunction: Country => Float, riskFunction: Array[Cls] => Country => Array[Double] => Float) = {
    if (country.locationid == "GBR") {
      println("ich optimiere jetzt für: " + country.locationid)
      val targetFloodRisk: Double =
        if (country.data.seafloodcost_target > 0) {
          country.data.seafloodcost_target
        } else {
          country.data.seafloodcost_target = targetRiskFunction(country)
          country.data.seafloodcost_target
        }
      println("targetFloodRisk: " + targetFloodRisk)

      //val randomVG: UnitSphereRandomVectorGenerator = new UnitSphereRandomVectorGenerator(clss.length, randomG)
      val randomVG: UncorrelatedRandomVectorGenerator = new UncorrelatedRandomVectorGenerator(clssWithDikes.map(_ => 5.0), clssWithDikes.map(_ => 2.0), new UniformRandomGenerator(randomG))

      val abs_stop: Double = 0.0001 * targetFloodRisk
      val underlying: SimplexOptimizer = new SimplexOptimizer(1e-01, abs_stop)
      //underlying.setSimplex(new org.apache.commons.math3.optim.direct.NelderMeadSimplex(clssWithDikes.map(_ => 5.0)))
      val optimizer: MultiStartMultivariateOptimizer = new MultiStartMultivariateOptimizer(underlying, 1, randomVG)

      val protectionLevelCostCountry: MultivariateFunction = new MultivariateFunction() {
        def value(xs: Array[Double]): Double = {
          if (xs.map(_ < 0).contains(true)) {
            // this is a penalty for negative protection levels. as the next point is chosen by a random generator,
            // negative values for the protection levels might be possible. we penaltize these solutions so that they have no chance of winning.
            Double.MaxValue
          } else {
            val floodRisk: Float = riskFunction(clssWithDikes ++ clssWithoutDikes)(country)(xs ++ clssWithoutDikes.map { _ => 0.0 })
            val dikeCostRisk: Float = computeCurrentDikeCost(clssWithDikes)(xs)

            val ret =
              if (floodRisk > targetFloodRisk)
                Double.MaxValue
              else
                dikeCostRisk
            //println("nextVec: " + randomVG.nextVector().toList)
            println("xs: " + xs.toList)
            println("\tfloodRisk = " + floodRisk + "\ttargetFloodRisk = " + targetFloodRisk + "\tdikeCostRisk = " + dikeCostRisk + "\t ret = " + ret)
            // println("\tcountry.seafloodcost = " + country.data.seafloodcost + "\tfloodRisk = " + computeCurrentFloodRisk(clssWithDikes)(xs) / 1000000)
            ret
          }
        }
      }

      //println("startpoint: " + clssWithDikes.map(cls => cls.data.protlevel_implemented.asInstanceOf[Double]).toList)

      //protectionLevelCostCountry.value(Array[Double](10.0))
      //System.exit(0)

      var initialGuess: Array[Double] = clssWithDikes.map(cls => cls.data.protlevel_implemented.asInstanceOf[Double])
      //println("startpoint: " + startpoint.toList)

      var resultValue: Double = Double.MaxValue
      var resultPoint: Array[Double] = Array[Double](clssWithDikes.length)
      try {
        var r =
          optimizer.optimize(
            GoalType.MINIMIZE,
            new MaxIter(20000),
            new MaxEval(20000),
            new InitialGuess(initialGuess),
            new ObjectiveFunction(protectionLevelCostCountry),
            new NelderMeadSimplex(initialGuess.length));
        resultValue = r.getValue
        resultPoint = r.getPoint
      } catch {
        case tmee: org.apache.commons.math3.exception.TooManyEvaluationsException =>
        //print("Optima: ")
        //optimizer.getOptima.foreach(x => print(x.getValue + ",  "))
        //println
      }

      while (resultValue == Double.MaxValue) {
        initialGuess = initialGuess.map(_ * 1.1)
        //println("startpoint: " + startpoint.toList)
        try {
          var r =
            optimizer.optimize(
              GoalType.MINIMIZE,
              new MaxIter(20000),
              new MaxEval(20000),
              new InitialGuess(initialGuess),
              new ObjectiveFunction(protectionLevelCostCountry),
              new NelderMeadSimplex(initialGuess.length));
          resultValue = r.getValue
          resultPoint = r.getPoint
        } catch {
          case tmee: org.apache.commons.math3.exception.TooManyEvaluationsException =>
          //print("Optima: " + optimizer.getOptima.toList)
          //optimizer.
          //optimizer.getOptima.foreach(x => print(x.getValue + ",  "))
          //println
        }
      }

      (clssWithDikes zip resultPoint).foreach(clspl => clspl._1.data.protlevel_target = clspl._2.asInstanceOf[Float])

      var endpoint: Array[Double] = clssWithDikes.map(cls => cls.data.protlevel_target.asInstanceOf[Double])

      println("Fertig!")
      println("gefunden: " + resultPoint.toList)
      println("gefunden: " + endpoint.toList)
      println("dike cost: " + resultValue)
    }
  }

  private def optimalProtectionLevel(cls: Cls, state: divaState): Float = {

    if (dikeRaisingStartYear != state.time) {
      cls.data.protlevel_target
    } else {
      val result: (Double, Double) =
        if (cls.coastalPlane.areaExposure.cumulativeExposure(cls.floodHazard.returnPeriodHeight(1000000f)) > 0f) {

          val underlying: UnivariateOptimizer = new BrentOptimizer(1e-6, 1e-8)
          val optimizer: MultiStartUnivariateOptimizer = new MultiStartUnivariateOptimizer(underlying, 1, randomG)

          val protectionLevelCost: UnivariateFunction = new UnivariateFunction() {
            def value(x: Double): Double = {
              val temp = computeProtectionLevelCost(cls, state)(x)
              temp._1 + temp._2
            }
          }

          val r = optimizer.optimize(
            new UnivariateObjectiveFunction(protectionLevelCost),
            new MaxEval(200), GoalType.MINIMIZE, new SearchInterval(10, 10e09), new InitialGuess(Array[Double](cls.data.protlevel_target.toDouble)))
          (r.getPoint(), r.getValue())
        } else {
          // if the cls is practically uninhabited, the lowest PL has the lowest cost (as there are no benefits)
          val r = computeProtectionLevelCost(cls, state)(dikeOptimizationRPStart)
          (dikeOptimizationRPStart.asInstanceOf[Double], (r._1 + r._2))
        }

      val optimalDikeLevel: Double = result._1
      val optimalDikeCost: Double = result._2
      val optimalDikeIDCost: (Float, Float) = computeProtectionLevelCost(cls, state)(optimalDikeLevel)
      val optimalDamageCost: Float = optimalDikeIDCost._2
      val optimalInvestmentCost: Float = optimalDikeIDCost._1

      val noDikeCost: Float = computeProtectionLevelCost(cls, state)(0)._2
      val dikeBCR: Double = if (cls.coastalPlane.dikeHeigth > 0f) 1.0 else bcrToAllowNewDike

      if ((optimalDikeCost < noDikeCost) && ((noDikeCost - optimalDamageCost) / optimalInvestmentCost > dikeBCR)) {
        cls.data.seadike_bcr = (noDikeCost - optimalDamageCost) / optimalInvestmentCost
        optimalDikeLevel.asInstanceOf[Float]
      } else {
        cls.data.seadike_bcr =
          if ((noDikeCost - optimalDamageCost) <= 0) {
            1.0f / optimalInvestmentCost
          } else {
            (noDikeCost - optimalDamageCost) / optimalInvestmentCost
          }
        0f
      }
    }
  }

  private def print_trajectory_cost(cls: Cls, state: divaState, protection_level: Float) {
    for (t <- state.time to state.endTime by state.timeStep) {
      /*
      val lYearsInFuture: Int = t - state.time;
      val discountFactor: Float = scala.math.pow(1 - discountRate, lYearsInFuture).asInstanceOf[Float]
      cls.floodHazard.shift = cls.data.rslr_memory(t)
      cls.socioEconomicDevelopment(t, state.timeStep)

      var resultsIntermediatedStep: (Float, Float, Float, Float) = computeProtectionLevelCostAtTime(cls, state)(optimalDikeLevel)(discountFactor)(lDikeHeigthLastPeriod, landLossBefore)
			*/
    }
  }

  private def optimalDiscreteProtectionLevel(cls: Cls, state: divaState): Float = {
    if (cls.locationid == "CLS_CHN_00164") {

      if ((dikeRaisingStartYear != state.time)) {
        cls.data.protlevel_target
      } else {
        val dikeOptimizationRPValues = (dikeOptimizationRPStart to dikeOptimizationRPEnd by dikeOptimizationRPStep).toList
        val dikeOptimizationRPValuesWithoutZero = dikeOptimizationRPValues.filter(_ > 0)
        var costsForProtectionLevelRPValuesTable: scala.collection.mutable.Map[Int, Float] = scala.collection.mutable.Map()
        var investmentCostsForProtectionLevelRPValuesTable: scala.collection.mutable.Map[Int, Float] = scala.collection.mutable.Map()
        var damageCostsForProtectionLevelRPValuesTable: scala.collection.mutable.Map[Int, Float] = scala.collection.mutable.Map()
        dikeOptimizationRPValuesWithoutZero.foreach {
          rpdh =>
            costsForProtectionLevelRPValuesTable += (rpdh -> scala.Float.MaxValue)
            investmentCostsForProtectionLevelRPValuesTable += (rpdh -> scala.Float.MaxValue)
            damageCostsForProtectionLevelRPValuesTable += (rpdh -> scala.Float.MaxValue)
        }

        for (pl_temp <- dikeOptimizationRPValuesWithoutZero) {
          val cost_temp: (Float, Float) = computeProtectionLevelCost(cls, state)(pl_temp)
          costsForProtectionLevelRPValuesTable(pl_temp) = cost_temp._1 + cost_temp._2
          investmentCostsForProtectionLevelRPValuesTable(pl_temp) = cost_temp._1
          damageCostsForProtectionLevelRPValuesTable(pl_temp) = cost_temp._2
        }

        val optimalDikeLevel: Int = costsForProtectionLevelRPValuesTable.minBy(_._2)._1
        val optimalDikeCost: Float = costsForProtectionLevelRPValuesTable(optimalDikeLevel)
        val noDikeCost: Float = computeProtectionLevelCost(cls, state)(0)._2

        val dikeBCR: Double = if (cls.coastalPlane.dikeHeigth > 0f) 1.0 else bcrToAllowNewDike

        if ((optimalDikeCost < noDikeCost) && ((noDikeCost - damageCostsForProtectionLevelRPValuesTable(optimalDikeLevel)) / investmentCostsForProtectionLevelRPValuesTable(optimalDikeLevel) > dikeBCR)) {
          cls.data.seadike_bcr = (noDikeCost - damageCostsForProtectionLevelRPValuesTable(optimalDikeLevel)) / investmentCostsForProtectionLevelRPValuesTable(optimalDikeLevel)
          println("optimalDikeLevel = " + optimalDikeLevel)

          optimalDikeLevel
        } else {
          cls.data.seadike_bcr =
            if ((noDikeCost - damageCostsForProtectionLevelRPValuesTable(optimalDikeLevel)) <= 0) {
              1.0f / investmentCostsForProtectionLevelRPValuesTable(optimalDikeLevel)
            } else {
              (noDikeCost - damageCostsForProtectionLevelRPValuesTable(optimalDikeLevel)) / investmentCostsForProtectionLevelRPValuesTable(optimalDikeLevel)
            }
          0
        }
      }
    } else { 0 }

  }

  private def optimalProtectionTrajectory(cls: Cls, state: divaState): Float = {
    if (cls.optimalProtectionTrajectory.size == 0) {
      computeOptimalProtectionTrajectory(cls, state)
      logger.info("Found optimal protection trajectory for cls " + cls.locationid)
    }
    if (cls.locationid == "CLS_CHN_00164") {
      cls.optimalProtectionTrajectory(state.time)
    } else { 0 }
  }

  private def computeOptimalProtectionTrajectory(cls: Cls, state: divaState) {

    if (cls.locationid == "CLS_CHN_00164") {
      // we build a copy forward ...
      var clsCopy = cls.clone
      for (t <- state.time to state.endTime by state.timeStep) {
        //val lYearsInFuture: Int = t - state.time;
        //val discountFactor: Float = scala.math.pow(1 - discountRate, lYearsInFuture).asInstanceOf[Float]
        clsCopy.floodHazard.shift = clsCopy.data.rslr_memory(t)
        clsCopy.socioEconomicDevelopment(t, state.timeStep)
      }
      cls.optimalProtectionTrajectory =
        computeOptimalProtectionTrajectoryBackwards(cls, clsCopy, state)
    }

  }

  private def computeOptimalProtectionTrajectoryBackwards(cls_init: Cls, cls: Cls, state: divaState): HashMap[Int, Float] = {
    val dikeOptimizationRPValues = (dikeOptimizationRPStart to dikeOptimizationRPEnd by dikeOptimizationRPStep).toArray

    var costsForProtectionLevel: scala.collection.mutable.HashMap[Int, Float] = scala.collection.mutable.HashMap()
    var trajectroyForProtectionLevel: scala.collection.mutable.HashMap[Int, ArrayBuffer[Int]] = scala.collection.mutable.HashMap()
    var trajectroyForProtectionLevel2: scala.collection.mutable.HashMap[Int, ArrayBuffer[Float]] = scala.collection.mutable.HashMap()

    // initial Step ... cost for PL pl_temp at the end of the time loop
    val lYearsInFuture: Int = state.endTime - state.time;
    val discountFactor: Float = scala.math.pow(1 - discountRate, lYearsInFuture).asInstanceOf[Float]

    println("lYearsInFuture = " + lYearsInFuture + "   discountFactor = " + discountFactor)

    for (pl_temp <- dikeOptimizationRPValues) {
      val lIntendedDikeHeight: Float = if (pl_temp > 0) cls.floodHazard.returnPeriodHeight(pl_temp).asInstanceOf[Float] else 0
      val discountedSeadikeMaintenanceCost: Float = getDikeMaintenanceCost(cls, lIntendedDikeHeight) * 1000000 * state.timeStep * discountFactor
      cls.data.seadike_height = lIntendedDikeHeight
      val impacts: (Float, Float) = impactComputation.getCurrentImpacts(cls)
      val discountedSeafloodCost: Float = impacts._2 * discountFactor * state.timeStep
      costsForProtectionLevel += (pl_temp -> (discountedSeafloodCost + discountedSeadikeMaintenanceCost))
      trajectroyForProtectionLevel += (pl_temp -> ArrayBuffer(pl_temp))
      trajectroyForProtectionLevel2 += (pl_temp -> ArrayBuffer(lIntendedDikeHeight))
    }

    // inductive step (backwards)
    //for (t <- (state.endTime - state.timeStep) to (state.endTime - state.timeStep) by -state.timeStep) {
    for (t <- (state.endTime - state.timeStep) to state.time by -state.timeStep) {
      // negative timestep = compute socio-economic development backwards
      cls.socioEconomicDevelopment(t, -state.timeStep)

      // we need a copy of the up-to-now-cost of the protection levels, otherwise we change the table we are working on
      var costsForProtectionLevelCopy: scala.collection.mutable.HashMap[Int, Float] = costsForProtectionLevel.clone()
      var trajectroyForProtectionLevelCopy: scala.collection.mutable.HashMap[Int, ArrayBuffer[Int]] = trajectroyForProtectionLevel.clone()
      var trajectroyForProtectionLevel2Copy: scala.collection.mutable.HashMap[Int, ArrayBuffer[Float]] = trajectroyForProtectionLevel2.clone()

      val lYearsInFuture: Int = t - state.time
      val discountFactor: Float = scala.math.pow(1 - discountRate, lYearsInFuture).asInstanceOf[Float]
      val lYearsInFutureNextStep: Int = lYearsInFuture + state.timeStep
      val discountFactorNextStep: Float = scala.math.pow(1 - discountRate, lYearsInFutureNextStep).asInstanceOf[Float]

      println("t: " + t + "  lYearsInFuture = " + lYearsInFuture + "   discountFactor = " + discountFactor)

      cls.floodHazard.shift = cls.data.rslr_memory(t)
      for (pl_temp <- dikeOptimizationRPValues) {
        val lIntendedDikeHeight: Float = if (pl_temp > 0) cls.floodHazard.returnPeriodHeight(pl_temp).asInstanceOf[Float] else 0
        val discountedSeadikeMaintenanceCost: Float = getDikeMaintenanceCost(cls, lIntendedDikeHeight) * 1000000 * state.timeStep * discountFactor
        cls.data.seadike_height = lIntendedDikeHeight
        val impacts: (Float, Float) = impactComputation.getCurrentImpacts(cls)
        val discountedSeafloodCost: Float = impacts._2 * discountFactor * state.timeStep
        val currentPLCost = discountedSeafloodCost + discountedSeadikeMaintenanceCost

        // println(pl_temp + ":  lIntendedDikeHeightNew = " + lIntendedDikeHeight + "  Cost = " + currentPLCost + "(" + discountedSeadikeMaintenanceCost + "," + discountedSeafloodCost + ")")

        val costsForNextStates: Array[Float] = dikeOptimizationRPValues.map {
          pl_temp_next =>
            {
              val lIntendedDikeHeightNew: Float = if (pl_temp_next > 0) cls.floodHazard.returnPeriodHeight(pl_temp_next).asInstanceOf[Float] + (cls.data.rslr_memory(t + state.timeStep) - cls.data.rslr_memory(t)) else 0
              // IMPORTANT: seadikes are technically build in the next step (although they mark the transition) -- therefore we need to take another discount factor here
              val discountedSeadikeConstrutionCost: Float = getDikeConstructionCost(cls, lIntendedDikeHeightNew - lIntendedDikeHeight) * 1000000 * discountFactorNextStep
              val remainingTrajectoryCost: Float = costsForProtectionLevelCopy(pl_temp_next) + currentPLCost + discountedSeadikeConstrutionCost

              remainingTrajectoryCost
            }
        }

        val minNextStepCost: Float = costsForNextStates.min
        val minNextStepCostPL: Int = dikeOptimizationRPValues(costsForNextStates.indexOf(minNextStepCost))

        // optimaler Übergang t -> t + 1:  pl_temp -> minNextStepCostPL (+ trajectory of minNextStepCostPL)

        costsForProtectionLevel(pl_temp) += minNextStepCost

        val optimalTrajectory: ArrayBuffer[Int] = trajectroyForProtectionLevel(minNextStepCostPL).clone()
        optimalTrajectory.prepend(pl_temp)
        trajectroyForProtectionLevelCopy(pl_temp) = optimalTrajectory

        val optimalTrajectory2: ArrayBuffer[Float] = trajectroyForProtectionLevel2(minNextStepCostPL).clone()
        optimalTrajectory2.prepend(cls.floodHazard.returnPeriodHeight(pl_temp).asInstanceOf[Float])
        trajectroyForProtectionLevel2Copy(pl_temp) = optimalTrajectory2
      }

      costsForProtectionLevel = costsForProtectionLevelCopy.clone()
      trajectroyForProtectionLevel = trajectroyForProtectionLevelCopy.clone()
      trajectroyForProtectionLevel2 = trajectroyForProtectionLevel2Copy.clone()
    }

    // final step: connect least cost trajectories with initial state (existing protection)
    for (pl_temp <- dikeOptimizationRPValues) {
      val lIntendedDikeHeight: Float = if (pl_temp > 0) cls.floodHazard.returnPeriodHeight(pl_temp).asInstanceOf[Float] else 0
      val discountedSeadikeConstrutionCost: Float = getDikeConstructionCost(cls, lIntendedDikeHeight - cls_init.data.seadike_height) * 1000000
      costsForProtectionLevel(pl_temp) += discountedSeadikeConstrutionCost
    }

    val optimalInitialProtectionLevel: Int = costsForProtectionLevel.minBy(_._2)._1
    val optimalTrajectroy: ArrayBuffer[Int] = trajectroyForProtectionLevel(optimalInitialProtectionLevel)

    val ret: Array[(Int, Float)] = (state.time to state.endTime by state.timeStep).toArray zip optimalTrajectroy.map(_.toFloat)
    var retMap: scala.collection.mutable.HashMap[Int, Float] = scala.collection.mutable.HashMap()

    ret.foreach { p: (Int, Float) => retMap.+=(p._1 -> p._2) }
    retMap

    //println("found: " + optimalTrajectroy.toString)
    //println("found: " + trajectroyForProtectionLevel2(optimalInitialProtectionLevel).toString)
    //optimalTrajectroy
  }

}