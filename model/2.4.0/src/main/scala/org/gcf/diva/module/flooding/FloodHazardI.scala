package diva.module.flooding

trait FloodHazardI {

  type Probability = Double
  type Level = Float
  type TimePeriod = Double

  var shift: Level = 0.0f

  // the exceedence probability
  // P(X>x)
  def exceedanceProb(x: Level): Probability = { 1 - cdf(x) }

  // the cumulative distribution function
  // F(x) = P(X<=x)
  // law: cdf(x) = 1 - ExceedenceProb(x)
  def cdf(x: Level): Probability

  // the inverse cdf
  def cdf_inverse(x: Probability): Level

  // the derivation of the cumulative distribution function
  def cdf_prime(x: Level): Probability

  // the return period of a given level
  def returnPeriod(x: Level): TimePeriod = { 1 / (1 - cdf(x)) }

  // the inverse of ReturnPeriod
  // computes the level for a given return period
  def returnPeriodHeight(returnPeriod: TimePeriod): Level

  // The cumulative probability of a given level-intervall
  def floodingProb(level_a: Level, level_b: Level): Probability = math.abs(cdf(level_a) - cdf(level_b))

  def cloneHazard: FloodHazardI

  // later ...
  // def Rotate(angle: Real) 

}