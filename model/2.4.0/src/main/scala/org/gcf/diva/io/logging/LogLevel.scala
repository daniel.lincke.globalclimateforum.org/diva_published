package diva.io.logging

import scala.language.implicitConversions

object LogLevel extends Enumeration {
  type LogLevel = Value

  val NONE = Value("NONE")
  val DEBUG = Value("DEBUG")
  val INFO = Value("INFO")
  val WARNING = Value("WARNING")
  val ERROR = Value("ERROR")
  val FATAL = Value("FATAL ERROR")
  
  implicit def valueToLogLevel(v: Value): LogLevel = v.asInstanceOf[LogLevel]
  implicit def stringToLogLevel(s: String): LogLevel = LogLevel.withName(s)
  
  // This one maybe?
  /*
  implicit def stringToLogLevel(s: String): LogLevel = 
    try {LogLevel.withName(s)}
    catch {
      case nsee: java.util.NoSuchElementException => NONE
    }
    */
}

