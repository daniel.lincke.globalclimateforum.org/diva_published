/* Automatically generated data features for Cls
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data

import diva.annotations._
import diva.types._
import diva.types.TypeComputations._
import scala.collection.mutable._


class ClsData {

  @Output
  @Input
  @Parameter
  @Description(value = "Administrative unit identifier code, associating each coastline segment with the corresponding administrative unit information.")
  var adminid :  String = default[String]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Land area below ? m (ignoring see dikes)")
  var area_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Land area below the one in ? flood level (ignoring see dikes)")
  var area_below_h :  Map[Int ,Float] = default[Map[Int ,Float]]

  @Variable
  @Description(value = "Accomodation space for wetlands. Distance end of wetland to barrier (natural – critical slope or antropogenic - dike)")
  var aspace :  Float = default[Float]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets below ? m (ignoring see dikes)")
  var assets_below_ :  Map[Float,Float] = default[Map[Float,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Assets below the one in ? flood level (ignoring see dikes)")
  var assets_below_h :  Map[Int ,Float] = default[Map[Int ,Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets in developable areas below ? m (ignoring see dikes)")
  var assets_developable_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Assets in developed areas below ? m (ignoring see dikes)")
  var assets_developed_below_ :  Map[Float ,Float] = default[Map[Float ,Float]]

  @Input
  @OptionalParameter
  @Description(value = "Water-level attenuation slope")
  var attenuation_slope :  Option[Float] = default[Option[Float]]

  @Output
  @Input
  @Parameter
  @Description(value = "Tidal basin feature identifier code, associating each coastline segment with the corresponding basin information.")
  var basinid :  Option[String] = default[Option[String]]

  @Output
  @Variable
  @Description(value = "Costs of beach nourishment")
  var beachnourishment_cost :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "")
  var beachnourishment_length_beach :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "")
  var beachnourishment_length_shore :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "The reason why nourishment is applied: 0=segment is not nourished")
  var beachnourishment_reason :  Int = default[Int]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Recommended nourishment")
  var beachnourishment_volume :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Factor which reflects an estimate of the proportion of sand within a coastline segment.   Used to determine the proportion of a segment to which the Bruun Rule can be applied.")
  var brf :  Float = default[Float]

  @Output
  @Input
  @Parameter
  @Description(value = "")
  var cityid :  Option[String] = default[Option[String]]

  @Output
  @Input
  @Parameter
  @Description(value = "Country identifier code, associating each coastline segment and admin unit with the corresponding country information.")
  var countryid :  String = default[String]

  @Input
  @Parameter
  @Description(value = "Differentiation of the coast Into a series of physical behavioural units, based on McGill's (1958) map of the geomorphic structure of the coastal environment.")
  var cpc :  Int = default[Int]

  @Variable
  @Description(value = "Current segment vulnerability score: the vulnerability value associated with the total wetland system within a coastline segment during the present model run.")
  var csvs :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Indicates weather cyclones are present")
  var cyclone :  Boolean = default[Boolean]

  @Output
  @Input
  @Parameter
  @Description(value = "")
  var deltaid :  Option[String] = default[Option[String]]

  @Variable
  @Description(value = "Coastal forest ecological sensitivity score: the total wetland vulnerability score weighted by the ecological response lag of coastal forest to sea-level forcing.")
  var essforst :  Float = default[Float]

  @Variable
  @Description(value = "Freshwater marsh ecological sensitivity score: the total wetland vulnerability score weighted by the ecological response lag of freshwater marsh to sea-level forcing.")
  var essfresh :  Float = default[Float]

  @Variable
  @Description(value = "High unvegetated wetland ecological sensitivity score: the total wetland vulnerability score weighted by the ecological response lag of high unvegetated wetlands to sea-level forcing.")
  var esshigh :  Float = default[Float]

  @Variable
  @Description(value = "Low unvegetated ecological sensitivity score: the total wetland vulnerability score weighted by the ecological response lag of low unvegetated wetlands to sea-level forcing.")
  var esslow :  Float = default[Float]

  @Variable
  @Description(value = "Mangrove ecological sensitivity score: the total wetland vulnerability score weighted by the ecological response lag of mangrove to sea-level forcing.")
  var essmang :  Float = default[Float]

  @Variable
  @Description(value = "Saltmarsh ecological sensitivity score: the total wetland vulnerability score weighted by the ecological response lag of saltmarsh to sea-level forcing.")
  var esssalt :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "")
  var fetchlimited :  Boolean = true

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Loss of tidal flats in basins (lagoons) due to indirect erosion")
  var flatloss_erosion_indirect :  Float = default[Float]

  @Variable
  @Description(value = "Area of coastal forest saved per unit nourishment.")
  var forst_saved :  Float = default[Float]

  @Variable
  @Description(value = "Area of freshmarsh saved per unit nourishment.")
  var fresh_saved :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Average annual income")
  var gdpc :  Float = default[Float]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Storm surge height for a ? in 1 year surge, allowing for relative sea-level rise.")
  var h :  Map[Int ,Float] = default[Map[Int ,Float]]

  @Variable
  @Description(value = "Storm surge height for a 1 in 1 year surge, allowing for relative sea-level rise.")
  var h1 :  Float = default[Float]

  @Variable
  @Description(value = "Storm surge height for a 1 in 10 year surge, allowing for relative sea-level rise.")
  var h10 :  Float = default[Float]

  @Variable
  @Description(value = "Storm surge height for a 1 in 100 year surge, allowing for relative sea-level rise.")
  var h100 :  Float = default[Float]

  @Variable
  @Description(value = "Storm surge height for a 1 in 1000 year surge, allowing for relative sea-level rise.")
  var h1000 :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total land loss due to direct and indirect erosion, including the benefits of beach nourishment")
  var landloss_erosion_total :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Land area that is not protected but below the one in one flood level at initialisation")
  var landloss_initial_land_below_S1 :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Change in land area below the one in one flood level, taking Into account see dikes")
  var landloss_submergence :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total costs of land loss due to erosion")
  var landlosscost_erosion_total :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Change in land area below the one in one flood level, taking Into account see dikes")
  var landlosscost_submergence :  Float = default[Float]

  @Output
  @Input
  @OptionalParameter
  @Description(value = "Land use from the Image data - 19 classes.")
  var landuse :  Option[Int] = default[Option[Int]]

  @Variable
  @Description(value = "")
  var landvalue_unit :  Float = default[Float]

  @Output
  @Input
  @Parameter
  @Description(value = "Latitude of the coastline segment.")
  var lati :  Float = default[Float]

  @Output
  @Input
  @Parameter
  @Description(value = "Length of the coastline segment.")
  var length :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Length of the proctected coastline.")
  var length_protected :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Local GDP below 20.5m")
  var localgdp :  Float = default[Float]

  @Output
  @Input
  @Parameter
  @Description(value = "Longitude of the coastline segment.")
  var longi :  Float = default[Float]

  @Variable
  @Description(value = "Maximum nourishment required to maIntain the current area of coastal forest.")
  var max_wnourforst  :  Float = default[Float]

  @Variable
  @Description(value = "Maximum nourishment required to maIntain the current area of freshmarsh.")
  var max_wnourfresh :  Float = default[Float]

  @Variable
  @Description(value = "Maximum nourishment required to maIntain the current area of high unvegetated wetland.")
  var max_wnourhigh :  Float = default[Float]

  @Variable
  @Description(value = "Maximum nourishment required to maIntain the current area of low unvegetated wetland. ")
  var max_wnourlow :  Float = default[Float]

  @Variable
  @Description(value = "Maximum nourishment required to maIntain the current area of mangrove.")
  var max_wnourmang :  Float = default[Float]

  @Variable
  @Description(value = "Maximum nourishment required to maIntain the current area of saltmarsh.")
  var max_wnoursalt :  Float = default[Float]

  @Output
  @Input
  @Parameter
  @Description(value = "Max waterlevel at high tide (usually at spring tides)")
  var maxhwater :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "With of the max high water compared with high tide")
  var maxhwater_width :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Mean waterlevel at high tide")
  var meanhwater :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Mean waterlevel at heap tide")
  var mhwn :  Float = default[Float]

  @Output
  @Input
  @Parameter
  @Description(value = "Mean waterlevel at spring tide")
  var mhws :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to land loss (erosion) (per year)")
  var migration_erosion :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to retreat (per year)")
  var migration_retreat :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration due to land loss (flooding/submergence) (per year)")
  var migration_submergence :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration costs")
  var migrationcost_erosion :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration costs")
  var migrationcost_retreat :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Forced migration costs")
  var migrationcost_submergence :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Mean tidal range at the location of the cls")
  var mtidalrng :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Land gained due to 1 m^3 of beach nourishment")
  var nbbenefit :  Float = default[Float]

  @Variable
  @Description(value = "Elevation below which nobody lives")
  var nopopbelow :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total area of open water created by wetland loss (segment level).")
  var openwater :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "People at risk of flooding: average number of people flooded per year by storm surge allowing for the effect of flood defences (segment level).")
  var par :  Float = default[Float]

  @Output
  @Variable
  @Configure(mode = "ELEVATION")
  @Description(value = "Exposure (people) that are located below ? m")
  var pop_below_ :  Map[Float,Float] = default[Map[Float,Float]]

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Exposure (people) that are located below the h? flood")
  var pop_below_h :  Map[Int,Float] = default[Map[Int,Float]]

  @Output
  @Variable
  @Description(value = "Average coastal population density (depends on elevation/population model).")
  var popdens :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Forced migration due to the initial population correction")
  var population_correction :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Implemented protection level/ return period")
  var protlevel_implemented :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Implemented protection level/ return period (before dike is raised)")
  var protlevel_implemented_before_dikeraise :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Recommended (by the demand-for-safety-function) protection level/ return period")
  var protlevel_target :  Float = default[Float]

  @Variable
  @Description(value = "")
  var retreat :  Boolean = false

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "")
  var riverdike_cost :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of incremental riverdike maIntenace")
  var riverdike_increase_maintenance_cost :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of riverdike maIntenace")
  var riverdike_maintenance_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Expected costs of river floods")
  var riverfloodcost :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Change in relative sea level compared to the reference period, taking account of (1) human-induced climate change under the selected scenario, (2) uplift/subsidence due to glacial-isostatic adjustment and (3) natural subsidence of deltaic areas (segment level)")
  var rslr :  Float = default[Float]
  var rslr_memory: Map[Int ,Float] = Map()

  @Input
  @Parameter
  @Description(value = "Height above mean sea level (includes high-water-level)")
  var s0001 :  Float = default[Float]

  @Input
  @OptionalParameter
  @Description(value = "Height above mean sea level (includes high-water-level)")
  var s0002 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Height above mean sea level (includes high-water-level)")
  var s0005 :  Option[Float] = default[Option[Float]]

  @Input
  @Parameter
  @Description(value = "Height above mean sea level (includes high-water-level)")
  var s0010 :  Float = default[Float]

  @Input
  @OptionalParameter
  @Description(value = "Height above mean sea level (includes high-water-level)")
  var s0025 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Height above mean sea level (includes high-water-level)")
  var s0050 :  Option[Float] = default[Option[Float]]

  @Input
  @Parameter
  @Description(value = "Height above mean sea level (includes high-water-level)")
  var s0100 :  Float = default[Float]

  @Input
  @OptionalParameter
  @Description(value = "Height above mean sea level (includes high-water-level)")
  var s0250 :  Option[Float] = default[Option[Float]]

  @Input
  @OptionalParameter
  @Description(value = "Height above mean sea level (includes high-water-level)")
  var s0500 :  Option[Float] = default[Option[Float]]

  @Input
  @Parameter
  @Description(value = "Height above mean sea level (includes high-water-level)")
  var s1000 :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total costs of salinity Intrusion")
  var salinity_cost :  Float = default[Float]

  @Variable
  @Description(value = "Area of saltmarsh saved per unit nourishment.")
  var salt_saved :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Sand loss direct on the open coast ")
  var sandloss_erosion_direct :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total sand loss due to erosion, allowing for direct and indirect erosion, and beach nourishment as appropriate")
  var sandloss_erosion_including_nourishment :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Sand loss indirect on the open coast ")
  var sandloss_erosion_indirect :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total sand loss due to erosion, allowing for direct and indirect erosion, ignoring beach nourishment")
  var sandloss_erosion_total :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "The year the seadike is abandoned")
  var seadike_abandon_year :  Int = default[Int]

  @Output
  @Variable
  @Description(value = "Ratio of benefits (=avoided cost) and cost of a seadike decision")
  var seadike_bcr :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "The year the seadike is initially constructed ")
  var seadike_construction_year :  Int = default[Int]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total cost of seadike built")
  var seadike_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Recommended dike height")
  var seadike_height :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Initial sea dike height")
  var seadike_height_initial :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of incremental seadike maIntenace")
  var seadike_increase_maintenance_cost :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of seadike maIntenace")
  var seadike_maintenance_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Recommended position of seadike (elevation level)")
  var seadikebase_elevation :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Recommended position of seadike (distance from original coastline)")
  var seadikepos_distance_orig_coastline :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Recommended position of seadike (distance from current coastline)")
  var seadikepos_distance_recent_coastline :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Expected costs of sea floods")
  var seafloodcost :  Float = 0f

  @Output
  @Variable
  @Configure(mode = "RETURNPERIOD")
  @Description(value = "Damage (assets) caused by the h? flood")
  var seafloodcost_h :  Map[Int,Float] = default[Map[Int,Float]]

  @Output
  @Variable
  @Description(value = "Sea flood cost by the minimal flood (the first one wich overtops the dike)")
  var seafloodcost_min :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "total expected costs of sea floods as fractionof local gdp")
  var seafloodcost_relative :  Float = 0f

  @Variable
  @Description(value = "Target sea flood costs (memory to keep seafloodcost constant over time)")
  var seafloodcost_target :  Float = 0f

  @Driver
  @Interpolate(mode = "LINEAR")
  @Description(value = "Sea-level anomalie against reference period")
  var sealevel_anomalie :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Setbackheight as computed by setback width")
  var setback_height :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Length of coast with setback zone")
  var setback_length :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Setbackheight as computed by setback width translated into a return period")
  var setback_returnperiod :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Setbackwidth as declared by policy in m against current shoreline")
  var setback_width :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Administrative cost to maintain setback zones (after Snows1978 – see Croatia paper)")
  var setbackzone_administrative_cost :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Current setback zone area.")
  var setbackzone_area :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Capital deprecation in the setback zone.")
  var setbackzone_assets :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Capital deprecation in the setback zone.")
  var setbackzone_deprecation :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Population living currently in the setback zone.")
  var setbackzone_population :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Land slope in segments to 3m above the maximum surge level.")
  var slopecst :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Change in relative sea level compared to the reference period, only  human-induced climate change under the selected scenario")
  var slr_climate_induced :  Float = default[Float]
  var slr_climate_induced_memory: Map[Int ,Float] = Map()

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual change in relative sea level under the selected scenario")
  var slr_current_annual_rate :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Change in relative sea level compared to the referenceperiod, taking account only subsidence of deltaic and city areas (segment level) and isostatic adjustment")
  var slr_nonclimate_induced :  Float = default[Float]
  var slr_nonclimate_induced_memory: Map[Int ,Float] = Map()

  @Variable
  @Description(value = "stores the normalisation value for slr accumulation")
  var slr_normalizer :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "cost of surge barrier construction/update")
  var surge_barrier_cost :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "cost of surge barrier maintenance")
  var surge_barrier_maintenance_cost :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "number of events per year paramenter of the gpd of the surges")
  var surgedist_location :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "location of the gpd of the surges")
  var surgedist_number_events :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "scale parameter of the gpd of the surges")
  var surgedist_scale :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "shape parameter of the gpd of the surges. If zero, an ordinary exponential distribution is assumed")
  var surgedist_shape :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "type of the gpd of the surges")
  var surgedist_type :  String = default[String]

  @Output
  @Variable
  @Description(value = "")
  var target_floodloss_risk :  Float = default[Float]

  @Driver
  @Interpolate(mode = "LINEAR")
  @Description(value = "temperature anomalie against reference period")
  var temperature_anomalie :  Float = 0f

  @Output
  @Variable
  @Description(value = "Total costs of tourism")
  var tourlosscost :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Total suspended matter (comes from GLOBColor)")
  var tsm :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Uplift/subsidence from estimates of glacio-isostatic adjustment from Peltier (2000) plus assuming subsidence of deltaic areas at a rate of 2mm/yr.")
  var uplift :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Wave climate at the location of the cls")
  var waveclim :  Int = 0

  @Output
  @Input
  @Variable
  @Description(value = "Total area of freshwater marsh (segment level).")
  var wetfresh :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Value of freshwater marsh in US$ 1995 (segment level).")
  var wetfresh_value :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Total sum of all wetland areas")
  var wetland_area_total :  Float = default[Float]

  @Variable
  @Description(value = "wetland fraction relative to toal area below mhws")
  var wetland_fraction :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "total length of the wetland strip")
  var wetland_length :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "total costs of wetland nourishment")
  var wetland_nourishment_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Total value of all wetland types in US$ (1995) (segment level). ")
  var wetland_value :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "total width of the wetland strip")
  var wetland_width :  Float = default[Float]

  @Output
  @Input
  @Variable
  @Description(value = "Total area of low unvegetated wetland (segment level).")
  var wetlow :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Value of low unvegetated wetland in US$ (1995) (country level).  Aggregation rule, sum of coastline segments within country feature.")
  var wetlow_value :  Float = default[Float]

  @Output
  @Input
  @Variable
  @Description(value = "Total area of mangrove (segment level).")
  var wetmang :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Value of mangrove in US$ 1995 (segment level).")
  var wetmang_value :  Float = default[Float]

  @Output
  @Input
  @Variable
  @Description(value = "Total area of saltmarsh (segment level).")
  var wetsalt :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Value of saltmarsh in US$ 1995 (segment level).")
  var wetsalt_value :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Costs of beach nourishment")
  var wnourcost :  Float = default[Float]

  @Variable
  @Description(value = "Actual amount of nourishment applied to freshwater marsh areas.")
  var wnourfresh :  Float = default[Float]

  @Variable
  @Description(value = "Actual amount of nourishment applied to low unvegetated wetland areas.")
  var wnourlow :  Float = default[Float]

  @Variable
  @Description(value = "Actual amount of nourishment applied to mangrove areas.")
  var wnourmang :  Float = default[Float]

  @Variable
  @Description(value = "Actual amount of nourishment applied to saltmarsh areas.")
  var wnoursalt :  Float = default[Float]

  override def clone : ClsData = {
    var ret: ClsData  = new ClsData
    ret.adminid = adminid
    ret.area_below_ = area_below_
    ret.area_below_h = area_below_h
    ret.aspace = aspace
    ret.assets_below_ = assets_below_
    ret.assets_below_h = assets_below_h
    ret.assets_developable_below_ = assets_developable_below_
    ret.assets_developed_below_ = assets_developed_below_
    ret.attenuation_slope = attenuation_slope
    ret.basinid = basinid
    ret.beachnourishment_cost = beachnourishment_cost
    ret.beachnourishment_length_beach = beachnourishment_length_beach
    ret.beachnourishment_length_shore = beachnourishment_length_shore
    ret.beachnourishment_reason = beachnourishment_reason
    ret.beachnourishment_volume = beachnourishment_volume
    ret.brf = brf
    ret.cityid = cityid
    ret.countryid = countryid
    ret.cpc = cpc
    ret.csvs = csvs
    ret.cyclone = cyclone
    ret.deltaid = deltaid
    ret.essforst = essforst
    ret.essfresh = essfresh
    ret.esshigh = esshigh
    ret.esslow = esslow
    ret.essmang = essmang
    ret.esssalt = esssalt
    ret.fetchlimited = fetchlimited
    ret.flatloss_erosion_indirect = flatloss_erosion_indirect
    ret.forst_saved = forst_saved
    ret.fresh_saved = fresh_saved
    ret.gdpc = gdpc
    ret.h = h
    ret.h1 = h1
    ret.h10 = h10
    ret.h100 = h100
    ret.h1000 = h1000
    ret.landloss_erosion_total = landloss_erosion_total
    ret.landloss_initial_land_below_S1 = landloss_initial_land_below_S1
    ret.landloss_submergence = landloss_submergence
    ret.landlosscost_erosion_total = landlosscost_erosion_total
    ret.landlosscost_submergence = landlosscost_submergence
    ret.landuse = landuse
    ret.landvalue_unit = landvalue_unit
    ret.lati = lati
    ret.length = length
    ret.length_protected = length_protected
    ret.localgdp = localgdp
    ret.longi = longi
    ret.max_wnourforst  = max_wnourforst 
    ret.max_wnourfresh = max_wnourfresh
    ret.max_wnourhigh = max_wnourhigh
    ret.max_wnourlow = max_wnourlow
    ret.max_wnourmang = max_wnourmang
    ret.max_wnoursalt = max_wnoursalt
    ret.maxhwater = maxhwater
    ret.maxhwater_width = maxhwater_width
    ret.meanhwater = meanhwater
    ret.mhwn = mhwn
    ret.mhws = mhws
    ret.migration_erosion = migration_erosion
    ret.migration_retreat = migration_retreat
    ret.migration_submergence = migration_submergence
    ret.migrationcost_erosion = migrationcost_erosion
    ret.migrationcost_retreat = migrationcost_retreat
    ret.migrationcost_submergence = migrationcost_submergence
    ret.mtidalrng = mtidalrng
    ret.nbbenefit = nbbenefit
    ret.nopopbelow = nopopbelow
    ret.openwater = openwater
    ret.par = par
    ret.pop_below_ = pop_below_
    ret.pop_below_h = pop_below_h
    ret.popdens = popdens
    ret.population_correction = population_correction
    ret.protlevel_implemented = protlevel_implemented
    ret.protlevel_implemented_before_dikeraise = protlevel_implemented_before_dikeraise
    ret.protlevel_target = protlevel_target
    ret.retreat = retreat
    ret.riverdike_cost = riverdike_cost
    ret.riverdike_increase_maintenance_cost = riverdike_increase_maintenance_cost
    ret.riverdike_maintenance_cost = riverdike_maintenance_cost
    ret.riverfloodcost = riverfloodcost
    ret.rslr = rslr
    ret.rslr_memory = rslr_memory
    ret.s0001 = s0001
    ret.s0002 = s0002
    ret.s0005 = s0005
    ret.s0010 = s0010
    ret.s0025 = s0025
    ret.s0050 = s0050
    ret.s0100 = s0100
    ret.s0250 = s0250
    ret.s0500 = s0500
    ret.s1000 = s1000
    ret.salinity_cost = salinity_cost
    ret.salt_saved = salt_saved
    ret.sandloss_erosion_direct = sandloss_erosion_direct
    ret.sandloss_erosion_including_nourishment = sandloss_erosion_including_nourishment
    ret.sandloss_erosion_indirect = sandloss_erosion_indirect
    ret.sandloss_erosion_total = sandloss_erosion_total
    ret.seadike_abandon_year = seadike_abandon_year
    ret.seadike_bcr = seadike_bcr
    ret.seadike_construction_year = seadike_construction_year
    ret.seadike_cost = seadike_cost
    ret.seadike_height = seadike_height
    ret.seadike_height_initial = seadike_height_initial
    ret.seadike_increase_maintenance_cost = seadike_increase_maintenance_cost
    ret.seadike_maintenance_cost = seadike_maintenance_cost
    ret.seadikebase_elevation = seadikebase_elevation
    ret.seadikepos_distance_orig_coastline = seadikepos_distance_orig_coastline
    ret.seadikepos_distance_recent_coastline = seadikepos_distance_recent_coastline
    ret.seafloodcost = seafloodcost
    ret.seafloodcost_h = seafloodcost_h
    ret.seafloodcost_min = seafloodcost_min
    ret.seafloodcost_relative = seafloodcost_relative
    ret.seafloodcost_target = seafloodcost_target
    ret.sealevel_anomalie = sealevel_anomalie
    ret.setback_height = setback_height
    ret.setback_length = setback_length
    ret.setback_returnperiod = setback_returnperiod
    ret.setback_width = setback_width
    ret.setbackzone_administrative_cost = setbackzone_administrative_cost
    ret.setbackzone_area = setbackzone_area
    ret.setbackzone_assets = setbackzone_assets
    ret.setbackzone_deprecation = setbackzone_deprecation
    ret.setbackzone_population = setbackzone_population
    ret.slopecst = slopecst
    ret.slr_climate_induced = slr_climate_induced
    ret.slr_climate_induced_memory = slr_climate_induced_memory
    ret.slr_current_annual_rate = slr_current_annual_rate
    ret.slr_nonclimate_induced = slr_nonclimate_induced
    ret.slr_nonclimate_induced_memory = slr_nonclimate_induced_memory
    ret.slr_normalizer = slr_normalizer
    ret.surge_barrier_cost = surge_barrier_cost
    ret.surge_barrier_maintenance_cost = surge_barrier_maintenance_cost
    ret.surgedist_location = surgedist_location
    ret.surgedist_number_events = surgedist_number_events
    ret.surgedist_scale = surgedist_scale
    ret.surgedist_shape = surgedist_shape
    ret.surgedist_type = surgedist_type
    ret.target_floodloss_risk = target_floodloss_risk
    ret.temperature_anomalie = temperature_anomalie
    ret.tourlosscost = tourlosscost
    ret.tsm = tsm
    ret.uplift = uplift
    ret.waveclim = waveclim
    ret.wetfresh = wetfresh
    ret.wetfresh_value = wetfresh_value
    ret.wetland_area_total = wetland_area_total
    ret.wetland_fraction = wetland_fraction
    ret.wetland_length = wetland_length
    ret.wetland_nourishment_cost = wetland_nourishment_cost
    ret.wetland_value = wetland_value
    ret.wetland_width = wetland_width
    ret.wetlow = wetlow
    ret.wetlow_value = wetlow_value
    ret.wetmang = wetmang
    ret.wetmang_value = wetmang_value
    ret.wetsalt = wetsalt
    ret.wetsalt_value = wetsalt_value
    ret.wnourcost = wnourcost
    ret.wnourfresh = wnourfresh
    ret.wnourlow = wnourlow
    ret.wnourmang = wnourmang
    ret.wnoursalt = wnoursalt
    ret
  }

}
