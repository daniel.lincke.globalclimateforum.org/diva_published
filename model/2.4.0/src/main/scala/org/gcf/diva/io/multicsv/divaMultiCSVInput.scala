package diva.io.multicsv

import diva.io.divaInput
import diva.io.logging.LoggerI
import diva.types.TypeComputations._
import diva.types._
import scala.collection.mutable.HashMap
import scala.collection.JavaConversions.mapAsScalaMap
import scala.collection.mutable.ListBuffer
import scala.collection.immutable.SortedMap
import scala.collection.mutable.AnyRefMap
import java.io.FileInputStream
import java.io.FileReader
import com.opencsv.CSVReader

class divaMultiCSVInput(fil: List[FileInputStream], val fnames: List[String], val pLogger: LoggerI) extends divaInput {
  logger = pLogger

  //var readers: List[CSVReader] = fil.map { fi => new CSVReader(new FileReader(fi.getFD)) }
  var namedReaders: List[(CSVReader, String)] = fil.map { fi => new CSVReader(new FileReader(fi.getFD)) }.zip(fnames)
  var parameter: Array[String] = _
  var metainformation: List[String] = _
  var locations: Array[String] = _
  var indexMap: AnyRefMap[String, (Int, Int)] = new AnyRefMap[String, (Int, Int)]

  private var firstLines: List[Array[String]] = _
  private var nextLines: ListBuffer[Array[String]] = _
  private var currentNextLine: Array[String] = _

  private var dataBuffer: ListBuffer[SortedMap[Int, Array[Array[String]]]] = ListBuffer()
  private var locationIndices: ListBuffer[AnyRefMap[String, Int]] = ListBuffer()

  var numberLocations: Int = 0

  var timesteps: AnyRefMap[String, List[Int]] = new AnyRefMap[String, List[Int]]

  var noValueString: String = ""

  var locationWildcard: String = ""

  def readMetainformation = {
    firstLines = namedReaders.map(namedReader => namedReader._1.readNext)
    firstLines = firstLines.map(fl => if (fl == null) { new Array[String](0) } else fl)

    for (i <- 0 until namedReaders.size) {
      val fname: String = namedReaders(i)._2
      val reader: CSVReader = namedReaders(i)._1
      val firstLine: Array[String] = firstLines(i)

      val metaInformationisComplete: Boolean = (firstLine.length >= 3)
      val correctMetainformation: Boolean =
        if (metaInformationisComplete) (firstLine(0) == "locationid") && (firstLine(1) == "locationname") && (firstLine(2) == "time") else false
      if (correctMetainformation) {
        metainformation = List(firstLine(0), firstLine(1), firstLine(2))
        logger.info("Read metainformation from " + fname + ": Found " + metainformation.toList.toString())
      } else {
        logger.error("Wrong metainformation format in " + fname + ": Found " + firstLine.toList.toString() + "; expected something starting with locationid, locationname, time.")
      }
      firstLine.foreach { x => indexMap(x) = (i, firstLine.indexOf(x)) }
      //firstLine.foreach { x => indexMap.put(x,firstLine.indexOf(x)) }
    }
  }

  def readParameter {
    var lParameterLists: List[List[String]] = List()
    for (i <- 0 until firstLines.size) {
      var lParameterList: List[String] = List()
      for (j <- 0 until lParameterLists.size) {
        val common: Array[String] = firstLines(i).drop(3).intersect(lParameterLists(j))
        if (common.length > 0) {
          logger.error("Found duplicate parameters in input files: " + common.toList.toString() + " has been found in " + fnames(i) + " and in " + fnames(j))
        }
      }
      lParameterLists = lParameterLists :+ (firstLines(i).drop(3).toList)
    }
    parameter = lParameterLists.flatten.toArray
    //println("parameter: " + parameter.toList.toString())
  }

  def readCompletely(check: Boolean) {
    nextLines = namedReaders.map(namedReader => namedReader._1.readNext).to[ListBuffer]

    for (i <- 0 until nextLines.size) {
      dataBuffer += SortedMap()
      if ((nextLines(i) == null) || (nextLines(i).size == 0)) {
        nextLines(i) = namedReaders(i)._1.readNext
      }

      if (checkLine(nextLines(i))) {
        while (nextLines(i) != null) {
          var dataBufferTemp: ListBuffer[Array[String]] = ListBuffer()
          val cTime: String = nextLines(i)(2)
          var time: String = cTime
          while ((time == cTime) && (nextLines(i) != null) && (nextLines(i).size > 0)) {
            dataBufferTemp += nextLines(i)
            nextLines(i) = namedReaders(i)._1.readNext
            time = if (checkLine(nextLines(i))) nextLines(i)(2) else time + 1
          }
          dataBuffer(i) += (cTime.toInt -> dataBufferTemp.toArray[Array[String]])
        }
      }

      // TODO: check if keySet is sorted  
      parameter.foreach(p => timesteps.put(p, dataBuffer(i).keySet.toList))
      metainformation.foreach(p => timesteps.put(p, dataBuffer(i).keySet.toList))
      if (check) {
        checkDatabufferConsistency(dataBuffer(i),fnames(i))
      } else {
        // TODO: Check if locations are equal 
        val allLocations = dataBuffer(i).map { case (k, v) => (k, v.map(x => x(0)).toList) }
        locations = allLocations.valuesIterator.toArray.distinct(0).toArray[String]
        var locationIndicesTemp = new AnyRefMap[String, Int]
        locations.foreach { x => locationIndicesTemp += (x -> locations.indexWhere(_ == x)) }
        locationIndices += locationIndicesTemp
        numberLocations = locations.length
      }
    }
  }

  def getInputValue[T](parName: String, locid: String, time: Int)(implicit ev1: diva.types.TypeComputations.StringParseOp[T], ev2: diva.types.TypeComputations.Floatable[T]): T = {
    try {
      val locationIndex = getLocationIndex(locid, time, (indexMap(parName)._1))
      if (timesteps.apply(parName).contains(time)) {
        parseString[T](dataBuffer(indexMap(parName)._1)(time)(locationIndex)(indexMap(parName)._2))(ev1)
      } else {
        // fake to avoid error
        parseString[T](dataBuffer(indexMap(parName)._1)(time)(locationIndex)(indexMap(parName)._2))(ev1)
      }
    } catch {
      case e: Throwable => throw e
    }
  }

  def getOptionInputValue[T](parName: String, locid: String, time: Int)(implicit ev1: diva.types.TypeComputations.StringParseOp[T], ev2: diva.types.TypeComputations.Floatable[T]): Option[T] = {
    try {
      val locationIndex = getLocationIndex(locid, time, (indexMap(parName)._1))
      val v: String = dataBuffer(indexMap(parName)._1)(time)(locationIndex)(indexMap(parName)._2)
      if (v == noValueString)
        None
      else
        Some(parseString[T](v)(ev1))
    } catch {
      case e: Throwable => throw e
    }
  }

  def readCurrentMetainformation[T](parName: String, locid: String, time: Int)(implicit ev1: diva.types.TypeComputations.StringParseOp[T], ev2: diva.types.TypeComputations.Floatable[T]): T = {
    val locationIndex = getLocationIndex(locid, time, (indexMap(parName)._1))
    if (parName == "locationname") {
      parseString[T](dataBuffer(0)(time)(locationIndex)(0))(ev1)
    } else if (parName == "locationid") {
      parseString[T](dataBuffer(0)(time)(locationIndex)(1))(ev1)
    } else if (parName == "time") {
      parseString[T](dataBuffer(0)(time)(locationIndex)(2))(ev1)
    } else {
      null.asInstanceOf[T]
    }
  }

  private def checkLine(line: Array[String]): Boolean = {
    if (line != null) {
      if (line.length != (parameter.length + metainformation.length)) {
        val lineId =
          if (line.length >= 3)
            line(0) + "(" + line(1) + "," + line(2) + ")"
          else
            "non-idendifiable line"
        //logger.error("line " + lineId + " in " + fname + " has wrong number of elements: " + line.length + " instead of " + (parameter.length + metainformation.length) + " (metainformation + parameter)")
      }
      true
    } else {
      false
    }
  }

  //def hasNextTime: Boolean = fi.getFD.valid()
  def hasNextTime: Boolean = true

  private def checkDatabufferConsistency(dataBuffer: SortedMap[Int, Array[Array[String]]], fname: String) = {
    //logger.info("found following timesteps in " + fname + ": " + timesteps)

    // check if all timesteps have the same number of locations
    // todo: better error message?
    if (dataBuffer.valuesIterator.toArray.map(_.length).distinct.size > 1) {
      logger.error("found different number of locations for different timesteps in " + fname + ": " + (SortedMap[Int, Any]() ++ (dataBuffer.map { case (k, v) => (k -> v.length) })).toString())
    }

    dataBuffer.foreach { case (k, v) => (k, v.sortWith((x1: Array[String], x2: Array[String]) => (x1(0) < x2(0)))) }
    val allLocations = dataBuffer.map { case (k, v) => (k, v.map(x => x(0)).toList) }

    if (allLocations.valuesIterator.toArray.distinct.size > 1) {
      val allLocationsList = allLocations.toSeq.toList
      val allLocationsListSlide = allLocationsList.iterator.sliding(2).toList
      for (i <- 0 to allLocationsListSlide.length) {
        val diff1 = allLocationsListSlide(i)(0)._2.filterNot(allLocationsListSlide(i)(1)._2.contains(_))
        val diff2 = allLocationsListSlide(i)(1)._2.filterNot(allLocationsListSlide(i)(0)._2.contains(_))
        if (diff1.size > 0) logger.error("found different locations for different timesteps in " + fnames(i) + ": the locations " + diff1 + " where available in " + allLocationsListSlide(i)(0)._1 + " but not in " + allLocationsListSlide(i)(1)._1)
        if (diff2.size > 0) logger.error("found different locations for different timesteps in " + fnames(i) + ": the locations " + diff2 + " where available in " + allLocationsListSlide(i)(1)._1 + " but not in " + allLocationsListSlide(i)(2)._1)
      }
      //logger.error("found different locations for different timesteps in " + fname)
    }

    locations = allLocations.valuesIterator.toArray.distinct(0).toArray[String]
    var locationIndicesTemp: AnyRefMap[String, Int] = new AnyRefMap[String, Int]()
    locations.foreach { x => locationIndicesTemp += (x -> locations.indexWhere(_ == x)) }
    locationIndices += locationIndicesTemp
    //println(i + ": " + locationIndices)
    numberLocations = locations.length

    val duplicateLocations = locations.diff(locations.distinct).distinct
    if (duplicateLocations.size > 0) {
      // To do: specify the differences?
      logger.error("found duplicate locationids in " + fname + ": " + duplicateLocations.toList + ". Locationids should be unique!")
    }
    if (allLocations.valuesIterator.toList.distinct(0).size != allLocations.valuesIterator.toList.distinct(0).distinct.size) {
      // To do: specify the differences?
      logger.error("found different locations for different timesteps in " + fname)
    }
  }

  private def getLocationIndex(locid: String, time: Int, index: Int): Int = {
    if (locationIndices(index).contains(locid)) locationIndices(index)(locid)
    else if (locationIndices(index).contains(locationWildcard)) locationIndices(index)(locationWildcard)
    else { 
      logger.error("found neither location with id " + locid + " nor the wildcard location " + locationWildcard + " for timestep " + time + " in " + fnames.toString()); 0 }
  }
}