package diva.module.flooding

import diva.feature._

class ImpactComputation(upperLimitReturnPeriod: Int, integrationMaxSteps: Int) {

  def getCurrentImpacts(cls: Cls): (Float, Float) = {
    // this computes impacts in ONE year in the given setup.
    // if used in accumlated numbers, needs to be multiplied by state.timestep

    val integrationLowerBound = cls.floodHazard.returnPeriodHeight(1.0f)
    val integrationUpperBound = cls.floodHazard.returnPeriodHeight(upperLimitReturnPeriod)

    var par: Float = 0f
    var seafloodcost: Float = 0f
    cls.czs.foreach {
      cz =>
        {
          cz.data.par = diva.toolbox.NumericalIntegration.integrate(diva.toolbox.Toolbox.functionProduct(cz.coastalPlane.populationExposure.cumulativeDamageWithDike(cls.data.seadike_height, cls.data.seadikebase_elevation), cls.floodHazard.cdf_prime), integrationLowerBound, integrationUpperBound, integrationMaxSteps).asInstanceOf[Float]
          cz.data.seafloodcost = diva.toolbox.NumericalIntegration.integrate(diva.toolbox.Toolbox.functionProduct(cz.coastalPlane.assetsExposure.cumulativeDamageWithDike(cls.data.seadike_height, cls.data.seadikebase_elevation), cls.floodHazard.cdf_prime), integrationLowerBound, integrationUpperBound, integrationMaxSteps).asInstanceOf[Float]

          par = par + cz.data.par
          seafloodcost = seafloodcost + cz.data.seafloodcost
        }
    }

    val result: (Float, Float) = (par, seafloodcost)
    result
  }

}
