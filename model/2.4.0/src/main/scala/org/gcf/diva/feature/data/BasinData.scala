/* Automatically generated data features for Basin
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data

import diva.annotations._
import diva.types._
import diva.types.TypeComputations._
import scala.collection.mutable._


class BasinData {

  @Input
  @Parameter
  @Description(value = "ASMITA Coefficient (Parameter of the Wadden Sea Model)")
  var ce :  Float = 1f

  @Input
  @Parameter
  @Description(value = "Coast line segment to which the basin belongs")
  var clsid :  String = default[String]

  @Parameter
  @Description(value = "ASMITA Coefficient (Parameter of the Wadden Sea Model)")
  var dcd :  Float = 6365f

  @Parameter
  @Description(value = "ASMITA Coefficient (Parameter of the Wadden Sea Model)")
  var dcf :  Float = 8000f

  @Parameter
  @Description(value = "ASMITA Coefficient (Parameter of the Wadden Sea Model)")
  var ddo :  Float = 4225f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Loss of tidal flats in basins (lagoons) due to indirect erosion")
  var flatloss_erosion_indirect :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "Wet area of the lagoon")
  var lagoona :  Float = 10000000f

  @Output
  @Variable
  @Description(value = "Land gained due to 1 m^3 of beach nourishment")
  var nbbenefit :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "ASMITA Coefficient (Parameter of the Wadden Sea Model)")
  var nc :  Float = -2f

  @Input
  @Parameter
  @Description(value = "ASMITA Coefficient (Parameter of the Wadden Sea Model)")
  var nd :  Float = 2f

  @Input
  @Parameter
  @Description(value = "ASMITA Coefficient (Parameter of the Wadden Sea Model)")
  var nf :  Float = 2f

  @Input
  @Parameter
  @Description(value = "Number of inlets per basin")
  var numinlet :  Int = 1

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Sand loss direct on the open coast ")
  var sandloss_erosion_direct :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total sand loss due to erosion, allowing for direct and indirect erosion, and beach nourishment as appropriate")
  var sandloss_erosion_including_nourishment :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Sand loss indirect on the open coast ")
  var sandloss_erosion_indirect :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Total sand loss due to erosion, allowing for direct and indirect erosion, ignoring beach nourishment")
  var sandloss_erosion_total :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "ASMITA Coefficient (Parameter of the Wadden Sea Model)")
  var wsc :  Float = 4.32f

  @Input
  @Parameter
  @Description(value = "ASMITA Coefficient (Parameter of the Wadden Sea Model)")
  var wsd :  Float = 0.86f

  @Input
  @Parameter
  @Description(value = "ASMITA Coefficient (Parameter of the Wadden Sea Model)")
  var wsf :  Float = 8.64f

  override def clone : BasinData = {
    var ret: BasinData  = new BasinData
    ret.ce = ce
    ret.clsid = clsid
    ret.dcd = dcd
    ret.dcf = dcf
    ret.ddo = ddo
    ret.flatloss_erosion_indirect = flatloss_erosion_indirect
    ret.lagoona = lagoona
    ret.nbbenefit = nbbenefit
    ret.nc = nc
    ret.nd = nd
    ret.nf = nf
    ret.numinlet = numinlet
    ret.sandloss_erosion_direct = sandloss_erosion_direct
    ret.sandloss_erosion_including_nourishment = sandloss_erosion_including_nourishment
    ret.sandloss_erosion_indirect = sandloss_erosion_indirect
    ret.sandloss_erosion_total = sandloss_erosion_total
    ret.wsc = wsc
    ret.wsd = wsd
    ret.wsf = wsf
    ret
  }

}
