package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class Surgebarrier extends FeatureI {
  type Data = SurgebarrierData
  type DataIO = SurgebarrierDataIO

  val dataTypeTag: ru.TypeTag[SurgebarrierData] = ru.typeTag[SurgebarrierData]
  val dataClassTag: ClassTag[SurgebarrierData] = classTag[SurgebarrierData]
  val dataTypeSave: ru.Type = ru.typeOf[SurgebarrierData]

  var data: SurgebarrierData = null
  var dataIO: SurgebarrierDataIO = null

  def init() {
    data = new SurgebarrierData
    dataIO = new SurgebarrierDataIO
  }

  var mustHaveInputFile = false

  def newInstanceCopy: Surgebarrier = {
    val ret = new Surgebarrier
    ret.data = new SurgebarrierData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }
}