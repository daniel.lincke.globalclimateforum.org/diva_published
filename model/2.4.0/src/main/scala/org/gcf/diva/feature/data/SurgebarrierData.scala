/* Automatically generated data features for Surgebarrier
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data

import diva.annotations._
import diva.types._
import diva.types.TypeComputations._
import scala.collection.mutable._


class SurgebarrierData {

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "cost of surge barrier construction/update")
  var surge_barrier_cost :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "height of the surge barrier")
  var surge_barrier_height :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "cost of surge barrier maintenance")
  var surge_barrier_maintenance_cost :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "protection level of the surge barrier")
  var surge_barrier_protection_level :  Float = default[Float]

  @Output
  @Input
  @Parameter
  @Description(value = "width of the surge barrier")
  var surge_barrier_width :  Float = default[Float]

  override def clone : SurgebarrierData = {
    var ret: SurgebarrierData  = new SurgebarrierData
    ret.surge_barrier_cost = surge_barrier_cost
    ret.surge_barrier_height = surge_barrier_height
    ret.surge_barrier_maintenance_cost = surge_barrier_maintenance_cost
    ret.surge_barrier_protection_level = surge_barrier_protection_level
    ret.surge_barrier_width = surge_barrier_width
    ret
  }

}
