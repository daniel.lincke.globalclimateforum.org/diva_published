package diva.coastalplanemodel

import math._
import diva.toolbox.PartialLinearInterpolator
import diva.types.TypeComputations._

class CoastalArea(val pHeights: Array[Float], val pHeightsAttenuation: Array[Float], pExposedArea: Array[Float])
  extends CoastalExposureI {

  lLevels = pHeights.clone
  lLevelsAttenuation = pHeightsAttenuation.clone
  lExposure = pExposedArea.clone

  // pExposure values are given in noncumulative manner,
  // so we first cumulate them
  lCumulativeExposure = pExposedArea.scanLeft(0.0f)(_ + _).drop(1)
  lPartialLinearInterpolator = new PartialLinearInterpolator[Double](lLevels.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })
  lPartialLinearInterpolatorAttenuation = new PartialLinearInterpolator(lLevelsAttenuation.map { x => x.toDouble }, lCumulativeExposure.map { x => x.toDouble })

  // we implement a more efficient version of cumulativeDamageWithDike for area
  // as vul=1 and vul'=0 the whole integral is zero, so we do not have to compute it every time
  override def cumulativeDamageWithDike(d: Level, pDikePosition: Level = 0)(x: Level): Float = {
    if (x <= d) return 0
    // For area we have vulnerability=1 and therefore damage=exposure (the integral is zero, see ExposureI)
    depthDamage(0) * cumulativeExposure(x)
  }

  override def migrateBelow(pBelowThisElevation: Float, pAwayRate: Float): Float = 0

  override def migrateBelowBetween(pMigrateBelowThisElevation: Float, pDistributeBelowThisElevation: Float, pAwayRate: Float): Float = {

    val index1: Int = lPartialLinearInterpolator.index(pMigrateBelowThisElevation)
    val index2: Int = lPartialLinearInterpolator.index(pDistributeBelowThisElevation)

    var lExposureTemp = lExposure.toBuffer
    var lLevelsTemp = lLevels.toBuffer
    var lLevelsAttenuationTemp = lLevelsAttenuation.clone.toBuffer

    for (i <- (index1 + 1) to (index2)) {
      lExposureTemp.remove(index1 + 1)
      lLevelsTemp.remove(index1 + 1)
      lLevelsAttenuationTemp.remove(index1 + 1)
    }

    lExposure = lExposureTemp.toArray
    lLevels = lLevelsTemp.toArray
    lLevelsAttenuation = lLevelsAttenuationTemp.toArray

    if (!lLevels.contains(pMigrateBelowThisElevation)) {
      addPoint(pMigrateBelowThisElevation, lPartialLinearInterpolator.value(pMigrateBelowThisElevation))
    }

    if (!lLevels.contains(pDistributeBelowThisElevation)) {
      addPoint(pDistributeBelowThisElevation, lPartialLinearInterpolator.value(pDistributeBelowThisElevation))
    }

    0.0d
  }

  override def clone: CoastalArea = {
    val ret = new CoastalArea(lLevels.map { x => x.toFloat }, lLevelsAttenuation.map { x => x.toFloat }, lExposure.map { x => x.toFloat })
    ret.deepCopy(this)
    ret
  }
}
