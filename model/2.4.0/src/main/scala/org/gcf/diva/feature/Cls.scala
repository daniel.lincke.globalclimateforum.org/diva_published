package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI
import diva.module.flooding._

import diva.coastalplanemodel.CoastalPlaneModel

import scala.collection.mutable.HashMap
import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag


class Cls extends FeatureI {
  type Data = ClsData
  type DataIO = ClsDataIO

  val dataTypeTag: ru.TypeTag[ClsData] = ru.typeTag[ClsData]
  val dataClassTag: ClassTag[ClsData] = classTag[ClsData]
  val dataTypeSave: ru.Type = ru.typeOf[ClsData]

  var data: ClsData = null
  var dataIO: ClsDataIO = null

  var mustHaveInputFile = true

  var floodHazard: FloodHazardI = null
  var coastalPlane: CoastalPlaneModel = null

  var city: Option[City] = _
  var delta: Option[Delta] = _
  //var river: Option[Riverdistributary] = _
  var basin: Option[Basin] = _
  var admin: Admin = _
  var czs: Array[Cz] = Array()
  var country: Country = _
  var region: Option[Region] = _

  var optimalProtectionTrajectory: HashMap[Int, Float] = new HashMap[Int, Float]()
  var optimalRetreatTrajectory: HashMap[Int, Float] = new HashMap[Int, Float]()
  var optimalActionTrajectory: HashMap[Int, String] = new HashMap[Int, String]()

  def socioEconomicDevelopment(time: Int, timeStep: Int) {
    czs.foreach {
      cz =>
        {
          cz.coastalPlane.socioEconomicDevelopment((1.0f + cz.getAnnualPopGrowthAtTime(time)), (1.0f + cz.data.gdpcgrowth_memory(time)), timeStep)
        }
    }
    recomputeCoastalPlane
  }

  def recomputeCoastalPlane {
    coastalPlane.reset
    czs.foreach { cz => coastalPlane.add(cz.coastalPlane) }
  }

  def init() {
    data = new ClsData
    dataIO = new ClsDataIO
    city = None
    region = None
    //river = None
    delta = None
  }

  def newInstanceCopy: Cls = {
    val ret = new Cls
    ret.data = new ClsData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }

  override def clone: Cls = {
    val ret = new Cls
    //ret.data = new ClsData
    ret.dataIO = dataIO
    ret.coastalPlane = coastalPlane.clone
    ret.floodHazard = floodHazard.cloneHazard
    ret.city = city
    ret.delta = delta
    //ret.river = river
    ret.basin = basin
    ret.admin = admin
    ret.country = country
    ret.region = region
    ret.czs = czs.map { cz => cz.clone }

    ret.deepCopy(this)
    ret.data = data.clone
    ret.locationid = locationid
    ret.locationname = locationname
    ret.optimalProtectionTrajectory = optimalProtectionTrajectory.clone
    ret
  }

}