/* Automatically generated data IO for River
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data.io

import diva.io._
import diva.types.InterpolationMode.InterpolationMode
import diva.feature.data.RiverData

class RiverDataIO extends FeatureDataIO[RiverData] {

  def writeData(out: divaOutput, d: RiverData) {
    out.write(d.deltaid)
    out.write(d.river_impact_length_initial_s1)
    out.write(d.river_impact_length_initial_s10)
    out.write(d.river_impact_length_initial_s100)
    out.write(d.river_impact_length_initial_s1000)
    out.write(d.river_impact_length_initial_tide)
    out.write(d.river_impact_length_s1)
    out.write(d.river_impact_length_s10)
    out.write(d.river_impact_length_s100)
    out.write(d.river_impact_length_s1000)
    out.write(d.river_impact_length_tide)
    out.write(d.riverdike_cost)
    out.write(d.riverdike_height)
    out.write(d.riverdike_increase_maintenance_cost)
    out.write(d.riverdike_length)
    out.write(d.riverdike_maintenance_cost)
    out.write(d.salinity_area)
    out.write(d.surge_barrier_cost)
    out.write(d.surge_barrier_maintenance_cost)
    out.write(d.surge_barriers_number)
  }

  def writeSelectedData(out: divaOutput, d: RiverData, ignoreThisOutput: Array[Boolean]) {
    var c: Int = 0
    if (!ignoreThisOutput(c)) {out.write(d.deltaid)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_initial_s1)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_initial_s10)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_initial_s100)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_initial_s1000)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_initial_tide)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_s1)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_s10)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_s100)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_s1000)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.river_impact_length_tide)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_height)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_increase_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_length)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.riverdike_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.salinity_area)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barrier_maintenance_cost)}; c+=1
    if (!ignoreThisOutput(c)) {out.write(d.surge_barriers_number)}; c+=1
  }

  def readInputValue(in: divaInput, d:RiverData, parName: String, locid: String, time: Int) {
    if (parName=="number_distributive_channels") {d.number_distributive_channels = in.getInputValue[Float]("number_distributive_channels", locid, time)}
    else if (parName=="river_depth") {d.river_depth = in.getInputValue[Float]("river_depth", locid, time)}
    else if (parName=="river_discharge") {d.river_discharge = in.getInputValue[Float]("river_discharge", locid, time)}
    else if (parName=="river_slope") {d.river_slope = in.getInputValue[Float]("river_slope", locid, time)}
    // else println("error:" + parName + " not found in River")
  }

  def readOptionInputValue(in: divaInput, d:RiverData, parName: String, locid: String, time: Int) {
    if (parName=="deltaid") {d.deltaid = in.getOptionInputValue[String]("deltaid", locid, time)}
    // else println("error:" + parName + " not found in River")
  }

  def readScenarioValue(in: divaInput, d:RiverData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

  def readOptionScenarioValue(in: divaInput, d:RiverData, parName: String, locid: String, time: Int, pInterpolationMode: InterpolationMode) {
  }

}
