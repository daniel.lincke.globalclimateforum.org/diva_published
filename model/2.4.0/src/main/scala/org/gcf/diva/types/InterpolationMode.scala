package diva.types

import scala.language.implicitConversions

object InterpolationMode extends Enumeration {
  type InterpolationMode = Value

  // Linear interpolation 
  val LINEAR = Value("LINEAR")
  val LEFT_INTERPOLATION = Value("LEFT")
  val RIGHT_INTERPOLATION = Value("RIGHT")
  
  implicit def valueToInterpolationMode(v: Value): InterpolationMode = v.asInstanceOf[InterpolationMode]
  implicit def stringToInterpolationMode(s: String): InterpolationMode = InterpolationMode.withName(s)

}

