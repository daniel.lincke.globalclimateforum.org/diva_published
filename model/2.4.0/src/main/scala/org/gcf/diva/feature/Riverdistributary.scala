package diva.feature

import diva.feature.data._
import diva.feature.data.io._
import diva.model.FeatureI

import scala.reflect.runtime.{ universe => ru }
import scala.reflect.ClassTag
import scala.reflect.classTag

class Riverdistributary extends FeatureI {
  type Data = RiverdistributaryData
  type DataIO = RiverdistributaryDataIO

  val dataTypeTag: ru.TypeTag[RiverdistributaryData] = ru.typeTag[RiverdistributaryData]
  val dataClassTag: ClassTag[RiverdistributaryData] = classTag[RiverdistributaryData]
  val dataTypeSave: ru.Type = ru.typeOf[RiverdistributaryData]

  var data: RiverdistributaryData = null
  var dataIO: RiverdistributaryDataIO = null
  
  var cls: Cls = _
  var surgeBarrier: Surgebarrier = _
  var river: River = _
  //var delta: Option[Delta] = _

  def init() {
    data = new RiverdistributaryData
    dataIO = new RiverdistributaryDataIO
  }

  var mustHaveInputFile = false

  def newInstanceCopy: Riverdistributary = {
    val ret = new Riverdistributary
    ret.data = new RiverdistributaryData
    ret.dataIO = dataIO
    ret.deepCopy(this)
    ret
  }
}