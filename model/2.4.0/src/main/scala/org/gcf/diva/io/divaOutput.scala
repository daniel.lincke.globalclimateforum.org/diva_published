package diva.io

import diva.model.hasLogger

trait divaOutput  extends hasLogger {
  // having to put the implicit requirement diva.types.TypeComputations.toPSQLString here sucks actually. 
  def write[T: diva.types.TypeComputations.toPSQLString](value: T)
  def writeHeaderLine(l: Array[String], types: scala.collection.mutable.ArraySeq[scala.reflect.runtime.universe.Type])
  def writeHeaderLine(l: Array[String], types: List[scala.reflect.runtime.universe.Type]) {
    var arr: scala.collection.mutable.ArraySeq[scala.reflect.runtime.universe.Type] = scala.collection.mutable.ArraySeq()
    types.foreach { x => arr = arr.:+(x) }
    writeHeaderLine(l, arr)
  }
    
  def writeStringLine(l: Array[String], types: scala.collection.mutable.ArraySeq[scala.reflect.runtime.universe.Type])
  def writeNewLine
  def writeInit
  def writeFinish
}