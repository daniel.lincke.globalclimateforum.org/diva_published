package diva.types

import scala.language.implicitConversions

object BeachNourishmentMode extends Enumeration {
  type BeachNourishmentMode = Value

  val BEACH_NOURISHMENT_CBA = Value("BEACH_NOURISHMENT_CBA")
  val BEACH_NOURISHMENT_POPDENS_RULE = Value("BEACH_NOURISHMENT_POPDENS_RULE")
  val BEACH_NOURISHMENT_FULL = Value("BEACH_NOURISHMENT_FULL")
  val BEACH_NOURISHMENT_NONE = Value("BEACH_NOURISHMENT_NONE")
  val BEACH_NOURISHMENT_UD = Value("BEACH_NOURISHMENT_UD")

  implicit def valueToBeachNourishmentMode(v: Value): BeachNourishmentMode = v.asInstanceOf[BeachNourishmentMode]
  implicit def stringToBeachNourishmentMode(s: String): BeachNourishmentMode = BeachNourishmentMode.withName(s)

}