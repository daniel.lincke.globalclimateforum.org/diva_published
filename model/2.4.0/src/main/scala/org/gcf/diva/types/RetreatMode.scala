package diva.types

import scala.language.implicitConversions

object RetreatMode extends Enumeration {
  type RetreatMode = Value

  val SETBACK_ZONE_ALSO_DECONSTRUCTION = Value("SETBACK_ALSO_DECONSTRUCTION")
  val SETBACK_ZONE_CONSTRUCTION_RESTRICTION = Value("SETBACK_ZONE_CONSTRUCTION_RESTRICTION")
  val RETREAT = Value("RETREAT")
  val DIAZ_RETREAT = Value("DIAZ_RETREAT")
  val NONE = Value("NONE")
  
  implicit def valueToDikePositionMode(v: Value): RetreatMode = v.asInstanceOf[RetreatMode]
  implicit def stringToDikePositionMode(s: String): RetreatMode = RetreatMode.withName(s)

}