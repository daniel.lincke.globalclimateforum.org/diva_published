
package diva.toolbox

import java.math.{ MathContext }

object SimpleNumericalIntegration {

  def leftRect[T](f: T => T, a: T, b: T): T = f(a)
  def midRect[T](f: T => T, a: T, b: T)(implicit n: Fractional[T]): T = f(n.div(n.plus(a, b), n.fromInt(2)))
  def rightRect[T](f: T => T, a: T, b: T): T = f(b)
  def trapezoid[T](f: T => T, a: T, b: T)(implicit n: Fractional[T]): T = n.div(n.plus(f(a), f(b)), n.fromInt(2))
  def simpson[T](f: T => T, a: T, b: T)(implicit n: Fractional[T]): T = n.div(n.plus(f(a), n.plus(n.times(n.fromInt(4), f(n.div(n.plus(a, b), n.fromInt(2)))), f(b))), n.fromInt(6))

  //type Method = (Double => Double, Double, Double) => Double

  def integrateSimple[T](f: T => T, a: T, b: T, steps: Int, m: (T => T, T, T) => T)(implicit frac: Fractional[T], ord: Ordering[T]): T = {
    import ord.mkOrderingOps
    import frac.mkNumericOps

    if (b < a) return frac.zero
    if (b == a) return f(a)
    val steps_inv: T = frac.one / frac.fromInt(steps)

    val delta: T = (b - a) * steps_inv
    //delta*(a until b by delta).foldLeft(0.0)((s,x) => s+m(f, x, x+delta))
    var ret: T = frac.zero
    var a_new: T = a

    for (i <- 1 to steps) {
      ret = ret + m(f, a_new, (a_new + delta))
      a_new = a_new + delta
    }

    // Final return value
    delta * ret
  }

}
