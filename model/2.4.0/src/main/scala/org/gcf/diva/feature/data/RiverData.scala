/* Automatically generated data features for River
*  generated on Sun Jun 14 13:12:26 CEST 2020
*/

package diva.feature.data

import diva.annotations._
import diva.types._
import diva.types.TypeComputations._
import scala.collection.mutable._


class RiverData {

  @Output
  @Input
  @Parameter
  @Description(value = "")
  var deltaid :  Option[String] = default[Option[String]]

  @Input
  @Parameter
  @Description(value = "")
  var number_distributive_channels :  Float = 0f

  @Input
  @Parameter
  @Description(value = "Bottom depth at the river mouth")
  var river_depth :  Float = 1f

  @Input
  @Parameter
  @Description(value = "Average river discharge")
  var river_discharge :  Float = 0f

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1")
  var river_impact_length_initial_s1 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s10")
  var river_impact_length_initial_s10 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s100")
  var river_impact_length_initial_s100 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1000")
  var river_impact_length_initial_s1000 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1")
  var river_impact_length_initial_tide :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1")
  var river_impact_length_s1 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s10")
  var river_impact_length_s10 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s100")
  var river_impact_length_s100 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1000")
  var river_impact_length_s1000 :  Float = default[Float]

  @Output
  @Variable
  @Description(value = "Increase of river impact length at storm surge level s1")
  var river_impact_length_tide :  Float = default[Float]

  @Input
  @Parameter
  @Description(value = "River bottom slope")
  var river_slope :  Float = 1f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "")
  var riverdike_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "River dike height")
  var riverdike_height :  Float = default[Float]

  @Variable
  @Description(value = "River dike height after initialisation")
  var riverdike_height_initial :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of incremental riverdike maIntenace")
  var riverdike_increase_maintenance_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Total river dike length")
  var riverdike_length :  Float = 0f

  @Variable
  @Description(value = "Total river dike length after initialisation")
  var riverdike_length_initial :  Float = 0f

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "Annual cost of riverdike maIntenace")
  var riverdike_maintenance_cost :  Float = 0f

  @Output
  @Variable
  @Description(value = "Area influenced by salinisation due to sea level rise")
  var salinity_area :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "cost of surge barrier construction/update")
  var surge_barrier_cost :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "cost of surge barrier maintenance")
  var surge_barrier_maintenance_cost :  Float = default[Float]

  @Output
  @ResetEveryPeriod
  @Variable
  @Description(value = "number of surge barriers")
  var surge_barriers_number :  Float = default[Float]

  override def clone : RiverData = {
    var ret: RiverData  = new RiverData
    ret.deltaid = deltaid
    ret.number_distributive_channels = number_distributive_channels
    ret.river_depth = river_depth
    ret.river_discharge = river_discharge
    ret.river_impact_length_initial_s1 = river_impact_length_initial_s1
    ret.river_impact_length_initial_s10 = river_impact_length_initial_s10
    ret.river_impact_length_initial_s100 = river_impact_length_initial_s100
    ret.river_impact_length_initial_s1000 = river_impact_length_initial_s1000
    ret.river_impact_length_initial_tide = river_impact_length_initial_tide
    ret.river_impact_length_s1 = river_impact_length_s1
    ret.river_impact_length_s10 = river_impact_length_s10
    ret.river_impact_length_s100 = river_impact_length_s100
    ret.river_impact_length_s1000 = river_impact_length_s1000
    ret.river_impact_length_tide = river_impact_length_tide
    ret.river_slope = river_slope
    ret.riverdike_cost = riverdike_cost
    ret.riverdike_height = riverdike_height
    ret.riverdike_height_initial = riverdike_height_initial
    ret.riverdike_increase_maintenance_cost = riverdike_increase_maintenance_cost
    ret.riverdike_length = riverdike_length
    ret.riverdike_length_initial = riverdike_length_initial
    ret.riverdike_maintenance_cost = riverdike_maintenance_cost
    ret.salinity_area = salinity_area
    ret.surge_barrier_cost = surge_barrier_cost
    ret.surge_barrier_maintenance_cost = surge_barrier_maintenance_cost
    ret.surge_barriers_number = surge_barriers_number
    ret
  }

}
