package diva.io.csv

import diva.io.divaOutput
import diva.io.logging.LoggerI
import scala.collection.mutable.ArrayBuffer
import java.io.FileOutputStream
import java.io.FileWriter
import com.opencsv.CSVWriter

class divaCSVOutput(fi: FileOutputStream, val fname: String, val pLogger: LoggerI) extends divaOutput {

  logger = pLogger
  var writer: CSVWriter = new CSVWriter(new FileWriter(fi.getFD), ',');
  var header: ArrayBuffer[String] = new ArrayBuffer[String]()
  var currentLine: ArrayBuffer[String] = new ArrayBuffer[String]()

  def writeInit = {}

  def write[T: diva.types.TypeComputations.toPSQLString](value: T) = {
    val tString = value.toString()
    currentLine += tString
  }

  def writeNewLine() {
    writer.writeNext(currentLine.toArray)
    writer.flush()
    currentLine.clear()
  }

  def writeFinish = {}

  def writeStringLine(l: Array[String], types: scala.collection.mutable.ArraySeq[scala.reflect.runtime.universe.Type]) {
    writer.writeNext(l)
    writer.flush()
  }

  def writeHeaderLine(l: Array[String], types: scala.collection.mutable.ArraySeq[scala.reflect.runtime.universe.Type]) {
    writer.writeNext(l)
    writer.flush()
  }

}
