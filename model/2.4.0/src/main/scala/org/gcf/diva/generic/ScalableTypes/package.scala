package diva.generic

package object ScalableTypes {
  abstract class DoubleScalable extends Scalable[Double] {
    def times(x: Double, y: Double): Double = x * y
  }

  abstract class FloatScalable extends Scalable[Float] {
    def times(x: Float, y: Double): Float = x * y
  }

  implicit object DoubleScalableObject extends DoubleScalable
  implicit object FloatScalableObject extends FloatScalable

  implicit class ScalableDouble(d: Double) {
    def *[A](that: A)(implicit ev_Scalable: Scalable[A]): A = ev_Scalable.times(that, d)
  }

  implicit class ScalableFloat(f: Float) {
    def *[A](that: A)(implicit ev_Scalable: Scalable[A]): A = ev_Scalable.times(that, f)
  }
}