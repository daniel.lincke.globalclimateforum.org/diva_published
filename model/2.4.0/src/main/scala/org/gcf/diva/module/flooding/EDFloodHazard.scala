package diva.module.flooding

import math._

class EDFloodHazard(pMue: Float, pSigma: Float)
    extends FloodHazardI {

  //private 
  var slope = pSigma
  //private 
  var interception: Float = pMue

  // Mathematically, this is
  // F(x) = P(X<=x) = 1 - (1/( 10^((x-(interception + shift))/slope)))
  def cdf(level: Level): Probability = if (level >= (interception + shift)) {
    1 - (1 / pow(10, (level - (interception + shift)) / slope))
  } else {
    0
  }

  // F⁻¹ (p) = x
  def cdf_inverse(cuprob: Probability): Level = {
    slope * log10(1 / (1 - cuprob)).toFloat + (interception + shift)
  }

  // Mathematically this is 
  // f(x) = F'(x) = log(10) / (slope * 10^((x - (interception + shift)) / slope))
  // 
  def cdf_prime(level: Level): Probability = {
    if (level >= interception) {
      return (log(10) / (slope * pow(10, (level.toDouble - (interception + shift)) / slope)))
    } else {
      return 0
    }
  }

  //def returnPeriod(level: Level): TimePeriod = {
    //pow(10, (level - (interception + shift)) / slope).asInstanceOf[Float]
  //}

  def returnPeriodHeight(returnPeriod: TimePeriod): Level = {
    (slope * log10(returnPeriod).toFloat + (interception + shift))
  }

  def cloneHazard: EDFloodHazard = {
    var ret: EDFloodHazard = new EDFloodHazard(interception, slope)
    ret.shift = shift
    return ret
  }

}