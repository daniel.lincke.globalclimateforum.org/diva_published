package tools.CSVappend

import scala.io.Source._

import com.opencsv.CSVReader
import com.opencsv.CSVWriter

import java.io.FileReader
import java.io.FileWriter
import java.io.FileInputStream
import java.io.FileOutputStream

object CSVappend {

  private var gDebug: Boolean = false;
  private var gFileList: Boolean = false;
  private var currentData: scala.collection.mutable.MutableList[Array[String]] = scala.collection.mutable.MutableList()

  def run(args: Array[String]) {

    val usage: String = """
    Usage: CSVappend [options] outputfile inputfile1 inputfile2 inputfile3 ... 

      --verbose
        be verbose
      --help
        prints this usage text
      --filelist
        use inputfile1 as a filelist

  """

    if ((args.contains("--h")) || (args.contains("--help"))) {
      println(usage)
      System.exit(0)
    }

    if (args.contains("--v") || args.contains("--verbose")) {
      println("verbose mode");
      gDebug = true;
    }

    if (args.contains("--f") || args.contains("--filelist")) {
      gFileList = true;
    }

    val filteredArgs: List[String] = args.toList diff List("--h", "--help", "--v", "--verbose", "--f", "--filelist")

    if (filteredArgs.length < 2) {
      println("not enough arguments");
      println(usage)
      System.exit(0)
    }

    val outputFileName: String = filteredArgs(0)
    var inputFileNames: List[String] = filteredArgs drop 1

    if (gDebug) {
      println("input files: " + inputFileNames)
      println("output file: " + outputFileName)
    }

    if (gFileList) {
      var filelist: String = inputFileNames(0)
      inputFileNames = List()
      for (line <- fromFile(filelist).getLines) {
        if (line != "") {
          inputFileNames = inputFileNames :+ line
        }
      }
    }

    var writer: CSVWriter = new CSVWriter(new FileWriter(new FileOutputStream(outputFileName).getFD), ',');
    var fc: Int = 0
    
    for (inpFile <- inputFileNames) {
      //val ifs = new FileInputStream(inpFile)
      currentData.clear
      try {
        var reader: CSVReader = new CSVReader(new FileReader(new FileInputStream(inpFile).getFD))
        val firstLine: Array[String] = reader.readNext
        if ((firstLine != null) && (fc == 0)) {
          currentData += firstLine
        }

        var nextLine: Array[String] = reader.readNext
        if (nextLine.size == 0) {
          nextLine = reader.readNext
        }

        while (nextLine != null) {
          if (nextLine.size != 0) {
            currentData += nextLine
          }
          nextLine = reader.readNext
        }

        for (d <- currentData) {
          writer.writeNext(d)
        }
        
        fc = fc + currentData.length
        if (gDebug) {
          println("read input file " + inpFile + ". Total Lines read by now: " + fc)
        }
      } catch {
        case fnfe: java.io.FileNotFoundException => println("input file " + inpFile + " not found, file not used.")
        case e: Throwable                        => println("input file " + inpFile + " exception(" + e.toString + "), file not used.")
      }
    }

    writer.flush
    writer.close
  }
}