package tools.Grid2Shape

import com.vividsolutions.jts.geom.Coordinate
import scala.collection.mutable.ListBuffer

import IntersectionMode._

class GridcellFinder(pDebug: Boolean, pIntersectionMode: String) {

  // upperLeft, upperRight, lowerLeft, lowerRight
  type BoundingBox = (Coordinate, Coordinate, Coordinate, Coordinate)
  def createBoundingBox: BoundingBox = (new Coordinate, new Coordinate, new Coordinate, new Coordinate)
  def createBoundingBox(c1: Coordinate, c2: Coordinate, c3: Coordinate, c4: Coordinate) = (c1, c2, c3, c4)

  var gIntersectionMode: IntersectionMode = pIntersectionMode match {
    case "LinewiseBox" => LINE_BOX
    case "Linewise"    => LINE
    case _             => SEGMENT
  }

  if (pDebug) {
    println("intersection mode set to: " + gIntersectionMode)
  }

  def computeBoundingBox(pPoints: Array[Coordinate]): BoundingBox = {
    var ret: BoundingBox = createBoundingBox
    val xValues: Array[Double] = pPoints.map { c => c.x }
    val yValues: Array[Double] = pPoints.map { c => c.y }
    createBoundingBox(
      new Coordinate(xValues.min, yValues.max),
      new Coordinate(xValues.max, yValues.max),
      new Coordinate(xValues.min, yValues.min),
      new Coordinate(xValues.max, yValues.min))
  }

  def computeBoundingBox(pP1: Coordinate, pP2: Coordinate): BoundingBox = {
    createBoundingBox(
      new Coordinate(scala.math.min(pP1.x, pP2.x), scala.math.max(pP1.y, pP2.y)),
      new Coordinate(scala.math.max(pP1.x, pP2.x), scala.math.max(pP1.y, pP2.y)),
      new Coordinate(scala.math.min(pP1.x, pP2.x), scala.math.min(pP1.y, pP2.y)),
      new Coordinate(scala.math.max(pP1.x, pP2.x), scala.math.min(pP1.y, pP2.y)))
  }

  def computeBoundingBox(pXmin: Double, pXmax: Double, pYmin: Double, pYmax: Double): BoundingBox = {
    createBoundingBox(
      new Coordinate(pXmin, pYmax),
      new Coordinate(pXmax, pYmax),
      new Coordinate(pXmin, pYmin),
      new Coordinate(pXmax, pYmin))
  }

  def findCells(pPoints: Array[Coordinate], pLon: Array[Double], pLat: Array[Double]): ListBuffer[(Int, Int)] = {
    // List of consecutive points
    val lPointpairs = pPoints.toList.sliding(2).toList.to[ListBuffer]

    var lRet: ListBuffer[(Int, Int)] = gIntersectionMode match {
      case LINE_BOX => lPointpairs.flatMap { x => findCellsIntersectedByBox(computeBoundingBox(x(0), x(1)), pLon, pLat) }
      case LINE     => lPointpairs.flatMap { x => findCellsIntersectedByLine(x(0), x(1), pLon, pLat) }
      case SEGMENT  => findCellsIntersectedByBox(computeBoundingBox(pPoints), pLon, pLat)
    }

    lRet.distinct
  }

  def findCellsInNeibourghhood(pCells: ListBuffer[(Int, Int)], pLon: Array[Double], pLat: Array[Double], pRadius: Int): ListBuffer[(Int, Int)] = {
    var ret: ListBuffer[(Int, Int)] = ListBuffer()

    pCells.foreach((x: (Int, Int)) => {
      //print("Cell: " + x + "  --  ")
      val minLon = scala.math.max(0, x._2 - pRadius)
      val maxLon = scala.math.min(pLon.length - 1, x._2 + pRadius)
      val minLat = scala.math.max(0, x._1 - pRadius)
      val maxLat = scala.math.min(pLat.length - 1, x._1 + pRadius)
      //println(minLon + " - " + maxLon + "  --  " + minLat + " - " + maxLat)
      for (lon <- minLon to maxLon) {
        ret += ((minLat, lon))
        ret += ((maxLat, lon))
      }
      for (lat <- minLat to maxLat) {
        ret += ((lat, minLon))
        ret += ((lat, maxLon))
      }
    })
    ret.distinct.filterNot(pCells.contains(_))
  }

  private def findCellsIntersectedByLine(pP1: Coordinate, pP2: Coordinate, pLon: Array[Double], pLat: Array[Double]): ListBuffer[(Int, Int)] = {

    var ret: ListBuffer[(Int, Int)] = ListBuffer()
    //println(pP1 + " to " + pP2)
    var minLon = binaryFind(pLon, (x: Double) => (x <= scala.math.min(pP1.x, pP2.x)))
    if (minLon > 0) minLon = minLon - 1
    var maxLon = binaryFind(pLon, (x: Double) => (!(scala.math.max(pP1.x, pP2.x) < x)))
    if (maxLon < pLon.length - 1) maxLon = maxLon + 1;
    var maxLat = binaryFind(pLat, (x: Double) => (!(scala.math.max(pP1.y, pP2.y) <= x)))
    if (maxLat < pLat.length - 1) maxLat = maxLat + 1;
    var minLat = binaryFind(pLat, (x: Double) => (x <= scala.math.min(pP1.y, pP2.y)))
    if (minLat > 0) minLat = minLat - 1
    //println ("lon: " + minLon + " - " + maxLon + " ... (" + pLon(minLon) + "," + pLon(maxLon) + ")")
    //println ("lat: " + minLat + " - " + maxLat + " ... (" + pLat(minLat) + "," + pLat(maxLat) + ")")
    for (lon <- minLon to maxLon) {
      val lCurrentLon = pLon(lon)
      var lLonBox = box(pLon, lon)
      var lLonBox2 = if (lon == 0) (-180.0d, lLonBox._2) else lLonBox
      lLonBox = lLonBox2
      for (lat <- minLat to maxLat) {
        val lCurrentLat = pLat(lat)
        val lLatBox = box(pLat, lat)

        //println(lCurrentLon + " -- " + lCurrentLat)
        //println(lLonBox + " ---- " + lLatBox)
        //println(pointInBox(pP1, lLonBox, lLatBox) + "  " + pointInBox(pP2, lLonBox, lLatBox) + "  " + intersectsBoxLines(pP1, pP2, lLonBox, lLatBox))

        val cellIntersected: Boolean =
          pointInBox(pP1, lLonBox, lLatBox) ||
            pointInBox(pP2, lLonBox, lLatBox) ||
            intersectsBoxLines(pP1, pP2, lLonBox, lLatBox)

        if (cellIntersected) ret += ((lat, lon))
      }
    }

    ret
  }

  private def findCellsIntersectedByBox(pBox: BoundingBox, pLon: Array[Double], pLat: Array[Double]): ListBuffer[(Int, Int)] = {
    var ret: ListBuffer[(Int, Int)] = ListBuffer()
    val minLon = binaryFind(pLon, (x: Double) => (x <= pBox._1.x))
    val maxLon = binaryFind(pLon, (x: Double) => (!(pBox._4.x <= x)))
    val maxLat = binaryFind(pLat, (x: Double) => (x <= pBox._1.y))
    val minLat = binaryFind(pLat, (x: Double) => (!(pBox._4.y <= x)))
    for (lon <- minLon to maxLon) {
      val lCurrentLon = pLon(lon)
      val lLonBox = box(pLon, lon)
      for (lat <- minLat to maxLat) {
        val lCurrentLat = pLat(lat)
        val lLatBox = box(pLat, lat)

        /*println(lCurrentLon + " -- " + lCurrentLat)
        println(lLonBox + " ---- " + lLatBox)
        println("pBox: " + pBox)
        println (pointInBox(pBox._1, lLonBox, lLatBox) + "  " + pointInBox(pBox._2, lLonBox, lLatBox) + "  " + pointInBox(pBox._3, lLonBox, lLatBox) + "  " + pointInBox(pBox._4, lLonBox, lLatBox))
*/
        val cellIntersected: Boolean =
          pointInBox(pBox._1, lLonBox, lLatBox) ||
            pointInBox(pBox._2, lLonBox, lLatBox) ||
            pointInBox(pBox._3, lLonBox, lLatBox) ||
            pointInBox(pBox._4, lLonBox, lLatBox) ||
            intersectsBoxLines(pBox._1, pBox._2, lLonBox, lLatBox) ||
            intersectsBoxLines(pBox._1, pBox._3, lLonBox, lLatBox) ||
            intersectsBoxLines(pBox._2, pBox._4, lLonBox, lLatBox) ||
            intersectsBoxLines(pBox._3, pBox._4, lLonBox, lLatBox)

        if (cellIntersected) ret = ret :+ (lat, lon)
        //println(ret)
      }
    }

    //System.exit(0)
    ret
  }

  private def box(pAxis: Array[Double], pInd: Int): (Double, Double) = {
    if (pAxis.length <= 1) {
      (pAxis(pInd), pAxis(pInd))
    } else {
      var lMin =
        if (pInd > 0) pAxis(pInd) - ((pAxis(pInd) - pAxis(pInd - 1)) / 2)
        else pAxis(pInd) - ((pAxis(pInd + 1) - pAxis(pInd)) / 2)
      var lMax =
        if (pInd < pAxis.length - 1) pAxis(pInd) + ((pAxis(pInd + 1) - pAxis(pInd)) / 2)
        else pAxis(pInd) + ((pAxis(pInd) - pAxis(pInd - 1)) / 2)
      (lMin, lMax)
    }
  }

  private def pointInBox(pP1: Coordinate, pLonInterval: (Double, Double), pLatInterval: (Double, Double)): Boolean = {
    ((pLatInterval._1 <= pP1.y) && (pP1.y <= pLatInterval._2) && (pLonInterval._1 <= pP1.x) && (pP1.x <= pLonInterval._2))
  }

  private def intersectLines(pP1: Coordinate, pP2: Coordinate, pP3: Coordinate, pP4: Coordinate): Boolean = {
    val lDiffX1 = pP1.x - pP2.x
    val lDiffY1 = pP1.y - pP2.y
    val lDiffX2 = pP3.x - pP4.x
    val lDiffY2 = pP3.y - pP4.y

    val lSlope1 = if (lDiffY1 != 0) (lDiffY1 / lDiffX1) else Double.NaN
    val lSlope2 = if (lDiffY2 != 0) (lDiffY2 / lDiffX2) else Double.NaN

    val lYInterception1 = pP1.y - lSlope1 * pP1.x;
    val lYInterception2 = pP2.y - lSlope2 * pP2.x;

    val lInterceptionX = (lYInterception2 - lYInterception1) / (lSlope1 - lSlope2);
    val lInterceptionY = (lSlope1 * lInterceptionX) + lYInterception1;

    if (lInterceptionX == Double.NaN) return false;
    if (lInterceptionY == Double.NaN) return false;

    val lVec1Length = Math.sqrt((pP1.x - lInterceptionX) * (pP1.x - lInterceptionX) + (pP1.y - lInterceptionY) * (pP1.y - lInterceptionY));
    val lVec2Length = Math.sqrt((pP2.x - lInterceptionX) * (pP2.x - lInterceptionX) + (pP2.y - lInterceptionY) * (pP2.y - lInterceptionY));
    val lVec3Length = Math.sqrt((pP2.x - pP1.x) * (pP2.x - pP1.x) + (pP2.y - pP1.y) * (pP2.y - pP1.y));

    (lVec1Length <= lVec3Length) && (lVec2Length <= lVec3Length)
  }

  private def intersectsBoxLines(pP1: Coordinate, pP2: Coordinate, pLonInterval: (Double, Double), pLatInterval: (Double, Double)): Boolean = {
    val lUpperLeft = new Coordinate(pLonInterval._1, pLatInterval._2);
    val lUpperRight = new Coordinate(pLonInterval._2, pLatInterval._2);
    val lLowerLeft = new Coordinate(pLonInterval._1, pLatInterval._1);
    val lLowerRight = new Coordinate(pLonInterval._2, pLatInterval._1);

    intersectLines(pP1, pP2, lUpperLeft, lUpperRight) ||
      intersectLines(pP1, pP2, lUpperLeft, lLowerLeft) ||
      intersectLines(pP1, pP2, lLowerLeft, lLowerRight) ||
      intersectLines(pP1, pP2, lLowerRight, lUpperRight)
  }

  // find last element in sorted pSeq which satisfies p 
  private def binaryFind[A](pSeq: Array[A], p: (A) => Boolean): Int = {
    if (pSeq.length > 0 && !p(pSeq(0))) 0
    else if (pSeq.length > 0 && p(pSeq(pSeq.length - 1))) pSeq.length - 1
    else if (pSeq.length < 5) {
      var ret = 0
      for (i <- 1 until pSeq.length) {
        if (p(pSeq(i - 1)) && (!p(pSeq(i))))
          ret = i - 1
      }
      ret
    } else {
      if (p(pSeq(pSeq.length / 2)))
        (pSeq.length / 2) + binaryFind(pSeq.slice((pSeq.length / 2), pSeq.length), p)
      else
        binaryFind(pSeq.slice(0, (pSeq.length / 2)), p)
    }
  }
}