package tools.CSV2NC

import scala.io.Source._

import com.opencsv.CSVReader
import com.opencsv.CSVWriter

import java.io.FileReader
import java.io.FileWriter
import java.io.FileInputStream
import java.io.FileOutputStream

import scala.collection.mutable.AnyRefMap

object CSV2NC {

  type OptionMap = Map[String, String]

  private var gDebug: Boolean = false;
  private var gFileList: Boolean = false;

  var indexMap: AnyRefMap[String, Int] = new AnyRefMap[String, Int]

  private var firstLine: Array[String] = _
  private var nextLine: Array[String] = _
  private var currentNextLine: Array[String] = _

  def main(args: Array[String]) {

    val usage: String = """
    Usage: CSV2NC [options] outputfile inputfile1 inputfile2 inputfile3 ... 

      --verbose
        be verbose
      --help
        prints this usage text
      --lon

      --lat

      --time


  """

    var options = nextOption(Map(), args.toList)

    if (!options.contains("gridfile")) {
      println("no grid file given!")
      System.exit(0)
    }

    if (!options.contains("shapefile")) {
      println("no shape file given!")
      System.exit(0)
    }

    if (!options.contains("outfile")) {
      println("no ouput file given!")
      System.exit(0)
    }

    if (!options.contains("gridfile")) {
      println("no grid file given!")
      System.exit(0)
    }

    options = checkOption(options, "time", "time")
    options = checkOption(options, "lon", "lon")
    options = checkOption(options, "lat", "lat")
    options = checkOption(options, "novalue", "NaN")

    if ((args.contains("--h")) || (args.contains("--help"))) {
      println(usage)
      System.exit(0)
    }

    if (args.contains("--v") || args.contains("--verbose")) {
      println("verbose mode");
      gDebug = true;
    }

    val filteredArgs: List[String] = args.toList diff List("--h", "--help", "--v", "--verbose", "-lon", "-lat", "-time", "-novalue")

    if (filteredArgs.length < 2) {
      println("not enough arguments");
      println(usage)
      System.exit(0)
    }

    val outputFileName: String = filteredArgs(0)
    var inputFileName: String = filteredArgs(1)

    if (gDebug) {
      println("input file: " + inputFileName)
      println("output file: " + outputFileName)
    }

    var inputFile: FileInputStream =
      try { new FileInputStream(inputFileName) }
      catch {
        case e: Throwable => println("Alarm."); System.exit(0); null;
      }

    var reader: CSVReader = new CSVReader(new FileReader(inputFile.getFD))

    /*
    if (gFileList) {
      var filelist: String = inputFileNames(0)
      inputFileNames = List()
      for (line <- fromFile(filelist).getLines) {
        if (line != "") {
          inputFileNames = inputFileNames :+ line
        }
      }
    }

    var writer: CSVWriter = new CSVWriter(new FileWriter(new FileOutputStream(outputFileName).getFD), ',');
    var fc: Int = 0

    for (inpFile <- inputFileNames) {
      //val ifs = new FileInputStream(inpFile)
      currentData.clear
      try {
        var reader: CSVReader = new CSVReader(new FileReader(new FileInputStream(inpFile).getFD))
        val firstLine: Array[String] = reader.readNext
        if ((firstLine != null) && (fc == 0)) {
          currentData += firstLine
        }

        var nextLine: Array[String] = reader.readNext
        if (nextLine.size == 0) {
          nextLine = reader.readNext
        }

        while (nextLine != null) {
          if (nextLine.size != 0) {
            currentData += nextLine
          }
          nextLine = reader.readNext
        }

        for (d <- currentData) {
          writer.writeNext(d)
        }

        fc = fc + currentData.length
        if (gDebug) {
          println("read input file " + inpFile + ". Total Lines read by now: " + fc)
        }
      } catch {
        case fnfe: java.io.FileNotFoundException => println("input file " + inpFile + " not found, file not used.")
        case e: Throwable                        => println("input file " + inpFile + " exception(" + e.toString + "), file not used.")
      }
    }

    writer.flush
    writer.close
    * * 
  */
  }

  private def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    def isSwitch(s: String) = (s(0) == '-')
    list match {
      case Nil => map
      case "-v" :: varname :: tail =>
        nextOption(map ++ Map("varname" -> varname), tail)
      case "--v" :: tail =>
        nextOption(map, tail)
      case "--verbose" :: tail =>
        nextOption(map, tail)
      case "--h" :: tail =>
        nextOption(map, tail)
      case "--help" :: tail =>
        nextOption(map, tail)
      case "-lon" :: lon :: tail =>
        nextOption(map ++ Map("lon" -> lon), tail)
      case "-lat" :: lat :: tail =>
        nextOption(map ++ Map("lat" -> lat), tail)
      case "-novalue" :: noval :: tail =>
        nextOption(map ++ Map("novalue" -> noval), tail)
      case "-time" :: time :: tail =>
        nextOption(map ++ Map("time" -> time), tail)
      case option :: tail =>
        println("Unknown option " + option)
        System.exit(0)
        map
    }
  }

  private def checkOption(map: OptionMap, option: String, default: String): OptionMap = {
    if (!map.contains(option)) {
      if (gDebug) {
        println("setting " + option + " to value " + default)
      }
      map ++ Map(option -> default)
    } else {
      map
    }
  }

}