package tools.Grid2Shape

import scala.collection.JavaConverters._
import collection.immutable.SortedMap
import scala.collection.mutable.ListBuffer

import java.io.File
import java.io.FileWriter
import java.io.BufferedWriter
import java.nio.charset.Charset;

import ucar.nc2._

import org.geotools.data.shapefile.shp._
import org.geotools.data.shapefile.files.ShpFiles
import org.geotools.data.shapefile.dbf._

import com.vividsolutions.jts.geom.GeometryFactory
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Coordinate;

object Grid2Shape {

  type OptionMap = Map[String, String]

  private var gDebug = false;
  private var gGridFileName: String = ""
  var ncFile: NetcdfFile = _
  var shapeFile: ShpFiles = _

  var gTimeArray: Array[Int] = _
  var gLonArray: Array[Double] = _
  var gLatArray: Array[Double] = _

  var g3DDataValues: Map[String, Array[Array[Array[Double]]]] = Map.empty
  var g2DDataValues: Map[String, Array[Array[Double]]] = Map.empty
  var segmentShapes: SortedMap[String, Array[Coordinate]] = SortedMap.empty[String, Array[Coordinate]]
  var variableGridcells: Map[String, Map[String, (Int, ListBuffer[(Int, Int)])]] = Map()

  var gShapefileCellIndices: Map[String, Map[Object, Array[(Int, Int)]]] = _

  var shapeReader: ShapefileReader = _
  var dbfReader: DbaseFileReader = _
  var lIDIndex: Int = -1

  def run(args: Array[String]) {

    val usage = """
    Usage: grid2shape [options] gridfile shapefile 

      -v <variablename>
        variable from the gridfile to project to shapefile
        if no variable is given, all variables found in the gridfile are processed
      -o <filename> 
        output to the given file.
      -lon <name>
        name of the variable (or dimension) to be used for longitude values in the gridfile
        default value: 'lon'
      -lat <name>
        name of the variable (or dimension) to be used for lattitude values in the gridfile
        default value: 'lat'
      -time <name>
        name of the variable (or dimension) to be used for time values in the girdfile
        default value: 'time'
      -idendifier <name>
        name of the idendifier field in the dbf of the shapefile
        default value: 'id'
      -novalue <value>
        the value used for novalues
        default: NaN

      --verbose
        be verbose
      --help
        prints this usage text

      lon and lat have to be sorted in increasing order

  """

    if ((args.contains("--h")) || (args.contains("--help"))) {
      println(usage)
      System.exit(0)
    }

    if (args.contains("--v") || args.contains("--verbose")) {
      println("verbose mode");
      gDebug = true;
    }

    if (args.length < 2) {
      println("not enough arguments");
      println(usage)
      System.exit(0)
    }

    var options = nextOption(Map(), args.toList)

    if (!options.contains("gridfile")) {
      println("no grid file given!")
      System.exit(0)
    }

    if (!options.contains("shapefile")) {
      println("no shape file given!")
      System.exit(0)
    }

    if (!options.contains("outfile")) {
      println("no ouput file given!")
      System.exit(0)
    }

    if (!options.contains("gridfile")) {
      println("no grid file given!")
      System.exit(0)
    }

    options = checkOption(options, "time", "time")
    options = checkOption(options, "lon", "lon")
    options = checkOption(options, "lat", "lat")
    options = checkOption(options, "idendifier", "id")
    options = checkOption(options, "novalue", "NaN")

    try {
      // open file
      ncFile = NetcdfFile.open(options("gridfile"));
    } catch {
      case e: Throwable => println("could not open grid file " + options("gridfile")); System.exit(0)
    }

    // read the time dimension
    try {
      var varTime: Variable = ncFile.findVariable(options("time"));
      var arrayTime: ucar.ma2.Array = null;
      arrayTime = varTime.read()
      if (gDebug) {
        dumpNCArray("time:", arrayTime);
      }
      gTimeArray = NCArrayToIntArray(arrayTime)
      if (gTimeArray.toList.sortWith(_ < _) != gTimeArray.toList) {
        println("Time dimension is not sorted."); System.exit(0)
      }
    } catch {
      case e: Throwable => println("Could not read time dimension - using only one time slice.")
    }

    try {
      var varLon: Variable = ncFile.findVariable(options("lon"));
      var arrayLon: ucar.ma2.Array = null;
      arrayLon = varLon.read();
      if (gDebug) {
        dumpNCArray("lon:", arrayLon);
      }
      gLonArray = NCArrayToFloatArray(arrayLon)
      if (gLonArray.toList.sortWith(_ < _) != gLonArray.toList) {
        println("Lon dimension is not sorted."); System.exit(0)
      }
    } catch {
      case e: Throwable => println("Could not read longitude dimension " + options("lon") + "."); System.exit(0)
    }

    try {
      var varLat: Variable = ncFile.findVariable(options("lat"));
      var arrayLat: ucar.ma2.Array = null;
      arrayLat = varLat.read();
      if (gDebug) {
        dumpNCArray("lat:", arrayLat);
      }
      gLatArray = NCArrayToFloatArray(arrayLat)
      if (gLatArray.toList.sortWith(_ < _) != gLatArray.toList) {
        println("Lat dimension is not sorted."); System.exit(0)
      }
    } catch {
      case e: Throwable => println("Could not read lattitude dimension " + options("lat") + "."); System.exit(0)
    }

    var variables: List[Variable] = ncFile.getVariables.asScala.toList
    if (options.contains("varname")) {
      var varTemp: List[Variable] = variables.toList.filter { x: Variable => x.getFullName == options("varname") }
      if (varTemp.length == 0) {
        println("Could not find variable " + options("varname") + "; using all variables")
      } else {
        variables = varTemp
      }
    }

    //variables.foreach { x: Variable => println(x.getFullName + " -- " + x.getDimensions.length) }
    var varUnused: List[Variable] = variables.filter { x: Variable => ((x.getDimensions.asScala.length != 2) && (x.getDimensions.asScala.length != 3)) }
    variables = variables.toList.filter { x: Variable => ((x.getDimensions.asScala.length == 2) || (x.getDimensions.asScala.length == 3)) }

    if (gDebug) {
      if (varUnused.length > 0) { println("The following gridfile variables do not have dimension two or three and are therefore ignored: " + varUnused.map(_.getFullName).toString()) }
      println("Reshaping the following gridfile variables: " + variables.map(_.getFullName).toList.toString)
    }

    variables.foreach { x => { if (x.getDimensions.asScala.length == 2) { g2DDataValues = g2DDataValues ++ Map(x.getFullName -> dataArray2D(x)) } } }
    variables.foreach { x => { if (x.getDimensions.asScala.length == 3) { g3DDataValues = g3DDataValues ++ Map(x.getFullName -> dataArray3D(x)) } } }

    // reorde longitde, if necessary (longitude should be -180 ... 180 instead of 0 ... 360
    val reordedLon = reorderLon(gLonArray)

    reordedLon match {
      case (Some(v), l) => {
        if (gDebug) {
          println("Bringing longitude dimension to normal form ... ")
          print("before: "); gLonArray.foreach(x => print(x + ", ")); println()
          print("normal form: "); l.foreach(x => print(x + ", ")); println()
        }
        gLonArray = l
        g2DDataValues = g2DDataValues.map({ case (x, d) => (x, reorder2DVariable(d, v)) })
        g3DDataValues = g3DDataValues.map({ case (x, d) => (x, reorder3DVariable(d, v)) })
      }
      case (None, l) =>
    }

    // read the shapefile
    try {
      shapeFile = new ShpFiles(new File(options("shapefile")))
      val gf = new GeometryFactory
      shapeReader = new ShapefileReader(shapeFile, false, true, gf)
      dbfReader = new DbaseFileReader(shapeFile, false, Charset.forName("UTF-8"))
      val dbfHeader = dbfReader.getHeader
      lIDIndex = -1
      for (l <- 0 until dbfHeader.getNumFields()) {
        if (dbfHeader.getFieldName(l).equals(options("idendifier"))) {
          lIDIndex = l;
        }
      }
      //println(dbfHeader)
      if (lIDIndex == -1) {
        println("Could find index(" + options("idendifier") + ") in shapefile dfb."); System.exit(0)
      }
    } catch {
      case e: Throwable => println("Could not open shapefile " + options("shapefile")); System.exit(0)
    }

    val lCellFinder = new GridcellFinder(gDebug, "Linewise");

    while (shapeReader.hasNext) {
      val dbfRow = dbfReader.readRow
      val lInd = dbfRow.read(lIDIndex)
      val rec = shapeReader.nextRecord
      val shape: Geometry = rec.shape.asInstanceOf[Geometry]
      val points: Array[Coordinate] = shape.getCoordinates
      if (gDebug) {
        println("found segment with id " + lInd.toString + " and " + points.length + " segment points.")
      }
      val segString = lInd.toString.reverse.padTo(5, "0").mkString.reverse
      segmentShapes = segmentShapes ++ Map(segString -> points)
    }

    //findCellsForSegment(g2DDataValues("MTR"), lCellFinder, gLonArray, gLatArray, segmentShapes("1449"), options("novalue"))

    g2DDataValues.foreach((x: (String, Array[Array[Double]])) => {
      variableGridcells = variableGridcells ++ Map(x._1 -> findCells(segmentShapes, x._2, lCellFinder, gLonArray, gLatArray, options("novalue")));
      println("gridcell matching complete for variable " + x._1)
    })

    g3DDataValues.foreach((x: (String, Array[Array[Array[Double]]])) => {
      variableGridcells = variableGridcells ++ Map(x._1 -> findCells(segmentShapes, x._2(0), lCellFinder, gLonArray, gLatArray, options("novalue")));
    })

    variableGridcells.foreach(x => { println(x._1); println((x._2)) })

    val file = new File(options("outfile"))
    val bw = new BufferedWriter(new FileWriter(file))

    bw.write("segmentID")
    if (gTimeArray != null) {
      bw.write(",time")
    }
    variableGridcells.foreach(x => {
      bw.write(",avg_" + x._1)
    })
    bw.write("\n")

    if (gTimeArray == null) {
      segmentShapes.foreach(x => {
        bw.write(x._1.replaceAll("^0*", ""))
        variableGridcells.foreach(y => {
          var dataArray: Array[Array[Double]] =
            g2DDataValues.get(y._1) match {
              case None       => g3DDataValues(y._1)(0)
              case Some(data) => data
            }

          val valueList: ListBuffer[Double] = y._2(x._1)._2.map(p => dataArray(p._1)(p._2))
          val coordinateList = y._2(x._1)._2.map(p => (gLonArray(p._2), gLatArray(p._1)))
          bw.write("," + (valueList.foldLeft(0d)(_ + _) / valueList.size))
        })
        bw.write("\n")
      })
    } else {
      for (t <- 0 until gTimeArray.size) {
        segmentShapes.foreach(x => {
          bw.write(x._1.replaceAll("^0*", ""))
          bw.write("," + gTimeArray(t))
          variableGridcells.foreach(y => {
            var dataArray: Array[Array[Double]] =
              g2DDataValues.get(y._1) match {
                case None       => g3DDataValues(y._1)(t)
                case Some(data) => data
              }

            val valueList: ListBuffer[Double] = y._2(x._1)._2.map(p => dataArray(p._1)(p._2))
            val coordinateList = y._2(x._1)._2.map(p => (gLonArray(p._2), gLatArray(p._1)))
            bw.write("," + (valueList.foldLeft(0d)(_ + _) / valueList.size))
          })
          bw.write("\n")
        })
      }
    }

    bw.close()

    if (gDebug) {
      val debugFile1 = new File("debug1.csv")
      val debugFile2 = new File("debug2.csv")

      val bwDebug1 = new BufferedWriter(new FileWriter(debugFile1))
      val bwDebug2 = new BufferedWriter(new FileWriter(debugFile2))

      bwDebug1.write("segmentID")
      variableGridcells.foreach(x => {
        bwDebug1.write(",number_of_gridcells_for_" + x._1 + ",radius_for_" + x._1 + ",gridcells_for_" + x._1)
      })
      bwDebug1.write("\n")

      segmentShapes.foreach(x => {
        bwDebug1.write(x._1)
        variableGridcells.foreach(y => {
          val coordinateList: ListBuffer[(Double, Double)] = y._2(x._1)._2.map(p => (gLonArray(p._2), gLatArray(p._1)))
          bwDebug1.write("," + y._2(x._1)._2.length + "," + y._2(x._1)._1 + ",\"" + coordinateList + "\"")
        })
        bwDebug1.write("\n")
      })

      bwDebug1.close()

      bwDebug2.write("segmentID")
      variableGridcells.foreach(x => {
        bwDebug2.write(",avg_" + x._1 + ",min_" + x._1 + ",max_" + x._1 + ",range_" + x._1)
      })
      bwDebug2.write("\n")

      segmentShapes.foreach(x => {
        bwDebug2.write(x._1)
        variableGridcells.foreach(y => {
          var dataArray: Array[Array[Double]] =
            g2DDataValues.get(y._1) match {
              case None       => g3DDataValues(y._1)(0)
              case Some(data) => data
            }

          val valueList: ListBuffer[Double] = y._2(x._1)._2.map(p => dataArray(p._1)(p._2))
          val coordinateList = y._2(x._1)._2.map(p => (gLonArray(p._2), gLatArray(p._1)))
          bwDebug2.write("," + (valueList.foldLeft(0d)(_ + _) / valueList.size) + "," + valueList.min + "," + valueList.max + "," + (valueList.max - valueList.min))
        })
        bwDebug2.write("\n")
      })
      bwDebug2.close()

      var lNCOutputFile: NetcdfFileWriter = null
      try {
        val lNCFileName: String = (options("gridfile") + ".debug").split("/").reverse(0)
        lNCOutputFile = NetcdfFileWriter.createNew(NetcdfFileWriter.Version.netcdf3, lNCFileName)
      } catch {
        case e: Throwable => println("could not create grid file " + options("gridfile")); System.exit(0)
      }

      var timeVar: Variable = null
      if (gTimeArray != null) {
        var timeDim: Dimension = lNCOutputFile.addDimension(null, options("time"), gTimeArray.length);
        timeVar = lNCOutputFile.addVariable(null, options("time"), ucar.ma2.DataType.INT, List(timeDim).asJava)
        timeVar.addAttribute(new Attribute("units", "year"))
        timeVar.addAttribute(new Attribute("description", "beginning year of the 5-year-period"))
        //lNCOutputFile.write(timeVar, ucar.ma2.Array.factory(ucar.ma2.DataType.INT, gTimeArray))
      }

      var lonDim: Dimension = lNCOutputFile.addDimension(null, options("lon"), gLonArray.length)
      var latDim: Dimension = lNCOutputFile.addDimension(null, options("lat"), gLatArray.length)
      val lonVar = lNCOutputFile.addVariable(null, options("lon"), ucar.ma2.DataType.FLOAT, List(lonDim).asJava)
      lonVar.addAttribute(new Attribute("units", "degrees_east"))
      val latVar = lNCOutputFile.addVariable(null, options("lat"), ucar.ma2.DataType.FLOAT, List(latDim).asJava)
      latVar.addAttribute(new Attribute("units", "degrees_north"))

      var g2DDataVariables: Map[String, Variable] = Map.empty
      g2DDataValues.foreach((x: (String, Array[Array[Double]])) => {
        val varTemp: Option[Variable] = variables.find { y: Variable => y.getFullName == x._1 }
        val attributes: List[Attribute] = varTemp.get.getAttributes.asScala.toList
        val lVar = lNCOutputFile.addVariable(null, x._1, ucar.ma2.DataType.DOUBLE, varTemp.get.getDimensions)
        attributes.foreach { attr => lVar.addAttribute(attr) }
        if ((attributes.find { attr => attr.getShortName == "missing_value" }) == None) {
          lVar.addAttribute(new Attribute("missing_value", options("novalue")))
        }
        if ((attributes.find { attr => attr.getShortName == "_FillValue" }) == None) {
          lVar.addAttribute(new Attribute("_FillValue", options("novalue")))
        }
        g2DDataVariables = g2DDataVariables ++ Map(x._1 -> lVar)
      })

      var g3DDataVariables: Map[String, Variable] = Map.empty
      g3DDataValues.foreach((x: (String, Array[Array[Array[Double]]])) => {
        val varTemp: Option[Variable] = variables.find { y: Variable => y.getFullName == x._1 }
        val attributes: List[Attribute] = varTemp.get.getAttributes.asScala.toList
        val lVar = lNCOutputFile.addVariable(null, x._1, ucar.ma2.DataType.DOUBLE, varTemp.get.getDimensions)
        attributes.foreach { attr => lVar.addAttribute(attr) }
        if ((attributes.find { attr => attr.getShortName == "missing_value" }) == None) {
          lVar.addAttribute(new Attribute("missing_value", options("novalue")))
        }
        if ((attributes.find { attr => attr.getShortName == "_FillValue" }) == None) {
          lVar.addAttribute(new Attribute("_FillValue", options("novalue")))
        }
        g3DDataVariables = g3DDataVariables ++ Map(x._1 -> lVar)
      })

      lNCOutputFile.create
      lNCOutputFile.write(lonVar, FloatArrayToNCArray(gLonArray))
      lNCOutputFile.write(latVar, ucar.ma2.Array.factory(gLatArray))
      if (gTimeArray != null) {
        println(gTimeArray.toList)
        lNCOutputFile.write(timeVar, ucar.ma2.Array.factory(gTimeArray))

        g3DDataValues.foreach((x: (String, Array[Array[Array[Double]]])) => {
          lNCOutputFile.write(g3DDataVariables.get(x._1).get, ucar.ma2.Array.factory(x._2))
        })
      }

      g2DDataValues.foreach((x: (String, Array[Array[Double]])) => {
        lNCOutputFile.write(g2DDataVariables.get(x._1).get, ucar.ma2.Array.factory(x._2))
      })

      lNCOutputFile.close
    }

  }

  private def nextOption(map: OptionMap, list: List[String]): OptionMap = {
    def isSwitch(s: String) = (s(0) == '-')
    list match {
      case Nil => map
      case "-v" :: varname :: tail =>
        nextOption(map ++ Map("varname" -> varname), tail)
      case "--v" :: tail =>
        nextOption(map, tail)
      case "--verbose" :: tail =>
        nextOption(map, tail)
      case "-lon" :: lon :: tail =>
        nextOption(map ++ Map("lon" -> lon), tail)
      case "-lat" :: lat :: tail =>
        nextOption(map ++ Map("lat" -> lat), tail)
      case "-idendifier" :: id :: tail =>
        nextOption(map ++ Map("idendifier" -> id), tail)
      case "-novalue" :: noval :: tail =>
        nextOption(map ++ Map("novalue" -> noval), tail)
      case "-time" :: time :: tail =>
        nextOption(map ++ Map("time" -> time), tail)
      case "-o" :: filename :: tail =>
        nextOption(map ++ Map("outfile" -> filename), tail)
      case string1 :: string2 :: opt2 :: tail if isSwitch(opt2) =>
        nextOption(map ++ Map("gridfile" -> string1, "shapefile" -> string2), list.tail.tail)
      case string1 :: string2 :: nil =>
        map ++ Map("gridfile" -> string1, "shapefile" -> string2)
      case option :: tail =>
        println("Unknown option " + option)
        System.exit(0)
        map
    }
  }

  private def checkOption(map: OptionMap, option: String, default: String): OptionMap = {
    if (!map.contains(option)) {
      if (gDebug) {
        println("setting " + option + " to value " + default)
      }
      map ++ Map(option -> default)
    } else {
      map
    }
  }

  private def findCells(pSegments: Map[String, Array[Coordinate]], pData: Array[Array[Double]], pCellFinder: GridcellFinder, pLon: Array[Double], pLat: Array[Double], pNoValue: String): Map[String, (Int, ListBuffer[(Int, Int)])] = {
    var ret: Map[String, (Int, ListBuffer[(Int, Int)])] = Map()
    pSegments.foreach((x: (String, Array[Coordinate])) => {
      if (gDebug) {
        print("segment " + x._1 + ": ")
      }
      ret = ret ++ Map(x._1 -> findCellsForSegment(pData, pCellFinder, pLon, pLat, x._2, pNoValue))
      if (gDebug) {
        println("found " + ret(x._1)._2.length + " cells (radius " + ret(x._1)._1 + ").")
      }
    })
    ret
  }

  private def findCellsForSegment(pData: Array[Array[Double]], pCellFinder: GridcellFinder, pLon: Array[Double], pLat: Array[Double], pPoints: Array[Coordinate], pNoValue: String): (Int, ListBuffer[(Int, Int)]) = {
    var lRadius: Int = 0
    var lOrigCells: ListBuffer[(Int, Int)] = pCellFinder.findCells(pPoints, pLon, pLat)
    var lRet: ListBuffer[(Int, Int)] = lOrigCells
    var lCurrentCells: ListBuffer[(Int, Int)] = lOrigCells

    var coordinateList: ListBuffer[(Double, Double)] = lRet.map(p => (gLonArray(p._2), gLatArray(p._1)))
    var valueList: ListBuffer[Double] = lRet.map(p => pData(p._1)(p._2))
    /* println("orig: " + lOrigCells)
    println("radius: " + lRadius)
    println("coordinateList: " + coordinateList)
    println("valueList: " + valueList)
    */
    lRet =
      if (pNoValue.toLowerCase == "nan")
        lRet.filterNot(x => pData(x._1)(x._2).isNaN)
      else
        lRet.filterNot(x => pData(x._1)(x._2) == pNoValue.toDouble)
    var dataCount: Int = lRet.length

    while (dataCount == 0) {
      print (lRadius + ", ")
      lRadius += 1
      lCurrentCells = pCellFinder.findCellsInNeibourghhood(lCurrentCells, pLon, pLat, 1)
      lCurrentCells 
      
      lRet =
        if (pNoValue.toLowerCase == "nan")
          lCurrentCells.filterNot(x => pData(x._1)(x._2).isNaN)
        else
          lCurrentCells.filterNot(x => pData(x._1)(x._2) == pNoValue.toDouble)
      dataCount = lRet.length
    }

    if (pNoValue.toLowerCase == "nan")
      (lRadius, lRet.filterNot(x => pData(x._1)(x._2).isNaN))
    else
      (lRadius, lRet.filterNot(x => pData(x._1)(x._2) == pNoValue.toDouble))
  }

  private def dumpNCArray(pName: String, pData: ucar.ma2.Array) {
    System.out.println(pName);
    NCdumpW.printArray(pData);
    System.out.println();
    System.out.println();
  }

  private def NCArrayToFloatArray(pNCArray: ucar.ma2.Array): Array[Double] = {
    val shape: Array[Int] = pNCArray.getShape
    val length: Int = shape(0)
    var ret: Array[Double] = new Array(length)
    val it = pNCArray.getIndexIterator()
    var index = 0
    while (it.hasNext()) {
      ret(index) = it.getFloatNext
      index += 1
    }
    ret
  }

  private def NCArrayToIntArray(pNCArray: ucar.ma2.Array): Array[Int] = {
    val shape: Array[Int] = pNCArray.getShape
    val length: Int = shape(0)
    var ret: Array[Int] = new Array(length)
    val it = pNCArray.getIndexIterator()
    var index = 0
    while (it.hasNext()) {
      ret(index) = it.getIntNext
      index += 1
    }
    ret
  }

  private def FloatArrayToNCArray(pArray: Array[Double]): ucar.ma2.Array = {
    var ret: ucar.ma2.Array = new ucar.ma2.ArrayDouble.D1(pArray.length)
    var index = 0

    var ima: ucar.ma2.Index = ret.getIndex
    for (i <- 0 until pArray.length) {
      ret.setDouble(ima.set(i), pArray(i))
    }
    ret
  }

  private def dataArray2D(v: Variable): Array[Array[Double]] = {
    val lDimensions = v.getDimensions
    if (lDimensions.size != 2) {
      println("parameter " + v.getFullName + " has " + lDimensions.size + " dimension instead of expected 2");
      null
    } else {
      val data = v.read
      convertNC2DArrayTo2DArray(data);
    }
  }

  private def dataArray3D(v: Variable): Array[Array[Array[Double]]] = {
    val lDimensions = v.getDimensions
    if (lDimensions.size != 3) {
      println("parameter " + v.getFullName + " has " + lDimensions.size + " dimension instead of expected 3");
      null
    } else {
      val data = v.read
      convertNC3DArrayTo3DArray(data);
    }
  }

  private def convertNC2DArrayTo2DArray(pNCArray: ucar.ma2.Array): Array[Array[Double]] = {
    var lShape = pNCArray.getShape
    var retArray = Array.ofDim[Double](lShape(0), lShape(1))
    var iter = pNCArray.getIndexIterator
    for (d1 <- 0 until lShape(0)) {
      for (d2 <- 0 until lShape(1)) {
        retArray(d1)(d2) = iter.getFloatNext
      }
    }
    return retArray
  }

  private def convertNC3DArrayTo3DArray(pNCArray: ucar.ma2.Array): Array[Array[Array[Double]]] = {
    var lShape = pNCArray.getShape
    var retArray = Array.ofDim[Double](lShape(0), lShape(1), lShape(2))
    var iter = pNCArray.getIndexIterator
    for (d1 <- 0 until lShape(0)) {
      for (d2 <- 0 until lShape(1)) {
        for (d3 <- 0 until lShape(2)) {
          retArray(d1)(d2)(d3) = iter.getFloatNext
        }
      }
    }
    return retArray
  }

  private def reorderLon(pLon: Array[Double]): (Option[Int], Array[Double]) = {
    val breakingIndex = pLon.toList.indexWhere(x => x > 180)
    if (breakingIndex >= 0)
      (Some(breakingIndex), reorderAndTransformArray(pLon, breakingIndex))
    else
      (None, pLon)
  }

  private def reorder3DVariable(pData: Array[Array[Array[Double]]], pIndex: Int): Array[Array[Array[Double]]] = {
    pData.map((x: Array[Array[Double]]) => reorder2DVariable(x, pIndex))
  }

  private def reorder2DVariable(pData: Array[Array[Double]], pIndex: Int): Array[Array[Double]] = {
    pData.map((x: Array[Double]) => reorderArray(x, pIndex))
  }

  private def reorderArray(pData: Array[Double], pIndex: Int): Array[Double] = {
    pData.slice(pIndex, pData.length).map(x => x) ++ pData.slice(0, pIndex)
  }

  private def reorderAndTransformArray(pData: Array[Double], pIndex: Int): Array[Double] = {
    pData.slice(pIndex, pData.length).map(x => x - 360) ++ pData.slice(0, pIndex)
  }
}

