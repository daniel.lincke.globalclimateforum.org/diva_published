package tools.Grid2Shape

object IntersectionMode extends Enumeration {
  type IntersectionMode = Value

  val SEGMENT = Value("SEGMENT")
  val LINE_BOX = Value("LINE_BOX")
  val LINE = Value("LINE")

}