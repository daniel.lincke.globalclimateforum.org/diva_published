@echo off
rem -------------------------------------------------------------------------
rem Environment Variable Prerequisites:
rem
rem   DIVA_HOME    (Optional) Should point to the directory containing
rem                  DIVA's "etc" and "lib" directory.
rem                  If not present, the current working directory is
rem                  assumed.
rem                  Note: This batch file does not function properly
rem                  if DIVA_HOME contains spaces.
rem
rem -------------------------------------------------------------------------
if not "%DIVA_HOME%" == "" goto gotTcHome
set DIVA_HOME=.
:gotTcHome
python "%DIVA_HOME%"\bin\diva-multi-run %1 %2 %3 %4 %5 %6 %7 %8 %9