@echo off
rem -------------------------------------------------------------------------
rem Environment Variable Prerequisites:
rem
rem   DIVA_HOME    (Optional) Should point to the directory containing
rem                  DIVA's "etc" and "lib" directory.
rem                  If not present, the current working directory is
rem                  assumed.
rem                  Note: This batch file does not function properly
rem                  if DIVA_HOME contains spaces.
rem
rem -------------------------------------------------------------------------
if not "%DIVA_HOME%" == "" goto gotTcHome
echo "System environment variable DIVA2_HOME must be defined and point to the diva/model/version directory."
GOTO :EOF
rem set DIVA_HOME=.
:gotTcHome
java -jar $DIVA_HOME/target/diva-jar-with-dependencies.jar csvappend %*
